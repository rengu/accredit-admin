 var path="http://localhost/khubaraa/";
 // close model dialog jquery
  $(document).ready(function(){
 	$(".cancelEdit").click(function(){
 		$('.close').click();
 	});

 });
// close bootstrap messages	
function closeMessage(){
 	$('.alert').fadeOut(500);
}

// change status
function changeStatus(status,id){
	$(".dropdown").addClass('open');

$(".drodown").attr('aria-expanded',true);	
	$('.loader-status').show();
	$.ajax({
		url: path+'change-online-status/'+status,
		type: 'GET',
		data: false,
		success: function(response) {
			$('.loader-status').hide();
			$('.online-status').removeClass('active');
			$('#'+id).addClass('active');
		}            
	});

}

// image validation
function in_array(needle, haystack) {
	for (var i = 0, j = haystack.length; i < j; i++) {
		if (needle == haystack[i])
			return true;
	}
	return false;
}

function getExt(filename) {
	var dot_pos = filename.lastIndexOf(".");
	if (dot_pos == -1)
		return "";
	return filename.substr(dot_pos + 1).toLowerCase();
}



function imageValidation() {
	var filename = document.getElementById("profile_image").value;
	var filetype = ['jpeg', 'png', 'jpg', 'gif'];
	if (filename != '') {
		var ext = getExt(filename);
		ext = ext.toLowerCase();
		var checktype = in_array(ext, filetype);
		if (!checktype) {
			alert(ext + " file not allowed for Image.");
			document.getElementById("profile_image").value = "";
			return false;
		} else {
			var fi = document.getElementById('profile_image');
			var filesize = fi.files[0].size;
			if (filesize > 2097152) {
				alert('Maximum 2MB file size allowed for Image.');
				document.getElementById("profile_image").value = "";
				return false;
			}
		}
	}
	return true;
}
function portfolioImageValidation() {
	var filename = document.getElementById("image").value;
	var filetype = ['jpeg', 'png', 'jpg', 'gif'];
	if (filename != '') {
		var ext = getExt(filename);
		ext = ext.toLowerCase();
		var checktype = in_array(ext, filetype);
		if (!checktype) {
			alert(ext + " file not allowed for Image.");
			document.getElementById("image").value = "";
			return false;
		} else {
			var fi = document.getElementById('image');
			var filesize = fi.files[0].size;
			if (filesize > 2097152) {
				alert('Maximum 2MB file size allowed for Image.');
				document.getElementById("image").value = "";
				return false;
			}
		}
	}
	return true;
}

function documentValidation() {
	var filename = document.getElementById("document").value;
	var filetype = ['jpeg', 'png', 'jpg', 'gif','doc','docx','pdf'];
	if (filename != '') {
		var ext = getExt(filename);
		ext = ext.toLowerCase();
		var checktype = in_array(ext, filetype);
		if (!checktype) {
			alert(ext + " file not allowed for document.");
			document.getElementById("document").value = "";
			return false;
		} else {
			var fi = document.getElementById('document');
			var filesize = fi.files[0].size;
			if (filesize > 10485760) {
				alert('Maximum 10MB file size allowed for Image.');
				document.getElementById("document").value = "";
				return false;
			}
		}
	}
	return true;
}

