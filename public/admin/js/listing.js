function setAction(val) {
    document.getElementById('action').value = val;
}

function checkAll(checkEm) {
    var cbs = document.getElementsByTagName('input');
    for (var i = 0; i < cbs.length; i++) {
        if (cbs[i].type == 'checkbox') {
            if (cbs[i].name == 'chkRecordId[]') {
                cbs[i].checked = checkEm;
            }
        }
    }
}

function ajaxFormPost(url, formId) {
    $.ajax({
        url: url,
        type: 'POST',
        data: $('#' + formId).serialize(),
        success: function(result) {

        }
    });
}


function isAllSelect(frmObject) {
    var flgChk = 0;
    for (i = 1; i < frmObject.chkRecordId.length; i++)
    {
        if (frmObject.chkRecordId[i].checked == false)
        {
            flgChk = 1;
            break;
        }
    }
    if (flgChk == 1) {
        frmObject.chkRecordId[0].checked = false;
    } else {
        frmObject.chkRecordId[0].checked = true;
    }
}

function confirmAction(action) {
    var con = confirm("Are you sure want to "+action+"?");
    if(con){
    return true;    
    } else {
    return false;
    }
}

function isAnySelect() {
    var checkaction = 1;
    var checkselect = 1;
    var action = document.getElementById('action').value;
    if (action == "") {
        checkaction = 0;
    }
    var cbs = document.getElementsByTagName('input');

    for (var i = 0; i < cbs.length; i++) {
        if (cbs[i].type == 'checkbox') {
            if (cbs[i].name == 'chkRecordId[]') {

                if (cbs[i].checked == true) {
                    checkselect = 1;
                    break;
                } else {
                    checkselect = 0;
                }
            }
        }
    }
    if (checkselect == 0) {
        alert("Please select atleast one record");
        return false;
    } else if (checkaction == 0) {
        alert("Please select atleast one action");
        return false;
    } else {
        return true;
    }
//        document.getElementById('idList').value = varAllId;
//        return true;

}

function isAnySelectAction() {
    var action = document.getElementById('action').value;

    if (action == "") {
        alert("Please select atleast one action");
        return false;
    } else {
        return true;
    }

}

function showMessage(msg) {
    msg = "Records are " + msg + " successfully.";
    document.getElementById('listingJS').style.display = '';
    document.getElementById('listingJS').innerHTML = msg;
}


function checkSelect(frmObject, id) {
    varAllId = "";
    for (i = 1; i < frmObject.chkRecordId.length; i++)
    {
        if (frmObject.chkRecordId[i].checked == true) {
            if (varAllId == "") {
                varAllId = frmObject.chkRecordId[i].value;
            }
            else {
                varAllId += "," + frmObject.chkRecordId[i].value;
            }
        }
    }
    //alert(varAllId+"xs");

    if (varAllId == "") {
        alert("Please select atleast one record");
        return false;
    } else {
        document.getElementById(id).value = varAllId;
        return true;
    }
}

function disable()
{
    var service;
    service = document.getElementById('service').value;

    if (service != '')
    {
        document.getElementById('add1').disabled = false;
        document.getElementById('add2').disabled = true;
        document.getElementById('add3').disabled = true;
        document.getElementById('add4').disabled = true;
    }

}
function disables()
{
    var procedure;

    procedure = document.getElementById('procedure').value;

    if (procedure != '')
    {
        document.getElementById('add1').disabled = true;
        document.getElementById('add2').disabled = false;
        document.getElementById('add3').disabled = true;
        document.getElementById('add4').disabled = true;
    }

}
function disabless()
{
    var condition;

    condition = document.getElementById('condition').value;

    if (condition != '')
    {
        document.getElementById('add1').disabled = true;
        document.getElementById('add3').disabled = false;
        document.getElementById('add2').disabled = true;
        document.getElementById('add4').disabled = true;
    }

}
function disablesss()
{
    var symptom;

    symptom = document.getElementById('symptom').value;

    if (symptom != '')
    {
        document.getElementById('add1').disabled = true;
        document.getElementById('add3').disabled = true;
        document.getElementById('add2').disabled = true;
        document.getElementById('add4').disabled = false;
    }
}
function active()
{
    document.getElementById('add5').disabled = false;
}

function ajaxForm(id){
   $('#'+id).submit(    function(e){     
    e.preventDefault();
    var $form = $(this);
    var submitBtnText = $('#'+id+' :input[type=submit]').attr('value');
            // check if the input is valid
            if(! $form.valid()) return false;
            $('#'+id+' :input[type=submit]').attr('disabled',true);
            $('#'+id+' :input[type=submit]').attr('value','Please wait...');
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function(response) {
                    if(response.errors==true){
                        var errors = response.message;
                        errorsHtml = '<div class="alert alert-danger"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><ul>';
                        $.each( errors, function( key, value ) {
                              errorsHtml += '<li>'+ value[0] + '</li>'; //showing only the first error.
                          });
                        errorsHtml += '</ul></div>';
                        $( '#form-errors' ).html( errorsHtml ); 
                        $('#'+id+' :input[type=submit]').removeAttr('disabled');
                        $('#'+id+' :input[type=submit]').attr('value',submitBtnText);
                    } else if(response.error==true){
                        errorsHtml = '<div class="alert alert-danger"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><ul>';
                        errorsHtml +=  response.message; 
                        errorsHtml += '</div>';
                        $( '#form-errors' ).html( errorsHtml );
                        $('#'+id+' :input[type=submit]').removeAttr('disabled');
                        $('#'+id+' :input[type=submit]').attr('value',submitBtnText);
                    } else if(response.errors==false){
                        successHtml = '<div class="alert alert-success"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button><ul>';
                        successHtml +=  response.message; 
                        successHtml += '</div>';
                        $( '#form-errors' ).html( successHtml ); 
                        $('#'+id+' :input[type=submit]').removeAttr('disabled');
                        $('#'+id+' :input[type=submit]').attr('value',submitBtnText);
                        setTimeout(function(){
                            parent.location.reload();
                        },2000);

                    } else {
                        alert('Something went wrong! Please try again later.');
                         $('#'+id+' :input[type=submit]').removeAttr('disabled');
                        $('#'+id+' :input[type=submit]').attr('value',submitBtnText);
                    }
                },
                error: function (error) {
                    alert('Something went wrong! Please try again later.');
                     $('#'+id+' :input[type=submit]').removeAttr('disabled');
                        $('#'+id+' :input[type=submit]').attr('value',submitBtnText);
                }
            });
        });
}