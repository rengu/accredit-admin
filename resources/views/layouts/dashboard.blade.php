
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title></title>

    
    <!-- CORE CSS -->
    <link href="{!! asset('/public/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('/public/css/font-awesome.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('/public/css/style.css') !!}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 
    <link rel="stylesheet" href="{!! asset('/public/css/animate.css') !!}">  
    <link rel="stylesheet" href="{!! asset('/public/css/sweetalert/sweetalert.css') !!}">     
    <!-- Javascript -->
    <script src="{!! asset('/public/js/jquery-3.1.0.min.js') !!}"></script>
    <script src="{!! asset('/public/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('/public/js/jquery.validate.js') !!}"></script>
    <script src="{!! asset('/public/js/sweetalert/sweetalert.js') !!}"></script>
    <script src="{!! asset('/public/js/common.js') !!}"></script>
</head>
<body>
    @include('elements.header_dashboard')
    @yield('content')
    @include('elements.footer')
    

</body> 
</html> 