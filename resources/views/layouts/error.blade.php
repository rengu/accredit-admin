<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center; flex-wrap:wrap;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 36px;
                padding: 20px;
            }
            .errorcom-page{ background:#f5f5f5;}
            .error-section img{ width:120px; height:120px; border-radius:50%; border:1px solid #fff;}
            .error-section h1{ font-size:120px; color:#e40001; margin:0; line-height:130px; margin-top:25px;}
            .error-section p{ font-size:22px; margin:0;}
            .error-section a{ text-transform:uppercase; text-decoration:none; background:#6b6b6b; color:#fff; font-size:18px; border-radius:50px; margin-top:40px; display:inline-block; padding:10px 35px; transition:all .5s ease;}
            .error-section a:hover{background:#4d4d4d;}
        </style>
    </head>
    <body class="errorcom-page">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title">
                    @yield('message')
                </div>
            </div>
        </div>
    </body>
</html>