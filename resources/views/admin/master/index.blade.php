@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Form Field Management')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php
if (isset($_REQUEST['form_type_id']) && !empty($_REQUEST['form_type_id'])) {
    $form_type_id = $_REQUEST['form_type_id'];
} else {
    $form_type_id = "";
}
if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
} else {
    $search = "";
}
if (isset($_REQUEST['from_date']) && !empty($_REQUEST['from_date'])) {
    $_REQUEST['from_date'];

    $from_date = date('d-m-y', strtotime($_REQUEST['from_date']));
} else {
    $from_date = "";
}
if (isset($_REQUEST['to_date']) && !empty($_REQUEST['to_date'])) {
    $to_date = date('d-m-y', strtotime($_REQUEST['to_date']));
} else {
    $to_date = "";
}
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Form Field Management</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                   <div class="block-title">
                    <h2><strong>Form Field Management</strong> 
                        <div class="addBtnSec">
                        <a href="{!! url('/admin/cms/add') !!}" class="fancyboxIframe">
                            <!-- <button type="button" class="btn btn-labeled btn-success">
                                <span class="btn-label"><i class="fa fa-plus"></i></span> Add
                            </button> -->
                        </a>    
                    </div>
                    </h2>
                </div>    

                <div class="row">
                    

                        <section class="panel comm-listing-list">

                            <div class="panel-body">
                                {!! View::make('elements.actionMessage')->render() !!}
                                {!! Form::open(array('url' => 'admin/master/index', 'method' => 'get', 'id' => 'adminAdd', 'files' => true,'class'=>'row')) !!}
                                 <div class="form-group col-md-4">
                                    {!! HTML::decode(Form::label('search', "Form Name <span class='require'></span>",array('class'=>"control-label"))) !!}
                                    <div>
                                       {!! Form::select('form_type_id', ['' => 'Please Select Form Name']+ $form_type, $form_type_id, array('class' => 'form-control'))!!}
                                       
                                    </div>
                                </div>

                                <div class="form-group col-md-4">
                                    {!! HTML::decode(Form::label('search', "Keyword <span class='require'></span>",array('class'=>"control-label"))) !!}
                                    <div>
                                        {!! Form::text('search', $search, array('class' => 'required form-control','placeholder'=>"Your Keyword")) !!}
                                      <!--   <span class="help-block">Search keyword by typing their Question OR Answer</span> -->
                                    </div>
                                </div>
                                
                                <div class="form-group form-actions col-md-4">
                                        {!! Form::submit('Search', array('class' => "btn btn-success")) !!}
                                        <a href="{!! url('admin/master/index') !!}" class="btn btn-default">Clear Filters</a>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </section>
                    
                   
                </div>
            </div>
            <!-- END Basic Form Elements Block -->
            <?php
            if (!$Field->isEmpty()) {
                ?>

                {!! Form::open(array('url' => 'admin/master/index', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Field List</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                 <th>{!! App\Commands\SortableTrait::link_to_sorting_action('name', 'name') !!} </th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('status', 'status') !!}	</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('is_required', 'is_required') !!}    </th>
                                                 <th>Form type   </th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('created', 'Created') !!}	</th>
                                                <th style="width:180px; min-width:180px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($Field as $data) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                                <tr>
                                                    <td data-title="Name">
                                                        {!! isset($data->name)?$data->name:"" !!}
                                                    </td>
													<td data-title="Name">
                                                        {!! isset($data->status) && $data->status == 1?"Enable":"Disable" !!}
                                                    </td>
                                                    <td data-title="Name">
                                                         {!! isset($data->is_required) && $data->is_required == 1?"Required":"Not Required" !!}
                                                       
                                                    </td>
                                                     <td data-title="Created">
                                                         {!! isset($data->FormType)?$data->FormType->name:"" !!}
                                                        </td>
                                                    
                                                    <td data-title="Created">
                                                        {!!  date("M d, Y", strtotime($data->created_at)) !!}</td>

                                                    <td data-title="Action" class="admusers-grobtn">
                                                        <?php
                                                       
                                                        if ($data->status == 1)
                                                            echo html_entity_decode(link_to('admin/master/index?deactivate=1&id=' . $data->id.'&form_type_id='.$form_type_id.'&search='.$search, '<i class="fa fa-check"></i>', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Enable", 'onclick' => "return confirmAction('deactivate');")));
                                                        else
                                                            echo html_entity_decode(link_to('admin/master/index?active=1&id=' . $data->id.'&form_type_id='.$form_type_id.'&search='.$search,'<i class="fa fa-check"></i>', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Disable", 'onclick' => "return confirmAction('activate');")));

                                                          if ($data->is_required == 1)
                                                            echo html_entity_decode(link_to('admin/master/index?optional=1&id=' . $data->id.'&form_type_id='.$form_type_id.'&search='.$search, '<i class="fa fa-check"></i>', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Required", 'onclick' => "return confirmAction('Not Required');")));
                                                        else
                                                            echo html_entity_decode(link_to('admin/master/index?required=1&id=' . $data->id.'&form_type_id='.$form_type_id.'&search='.$search, '<i class="fa fa-check"></i>', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Not Required", 'onclick' => "return confirmAction('required');")));
                                                      

                                                      //  echo html_entity_decode(link_to('admin/cms/view/' . $data->id, '<i class="fa fa-search"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'View')));
														
                                                        ?>
                                                    </td>	
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
               
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                loans List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
              <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {!! $Field->appends(request()->except('page'))->links() !!}
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection