@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Loan Type')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Loan Type</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
                <div class="row admedit-profile">
                    <div class="col-md-12">
                        <!-- Basic Form Elements Block -->
                        <div class="block">
                            <!-- Basic Form Elements Title -->
                            <div class="block-title">
                                <h2><strong>Loan Type </strong> </h2>
                            </div>
                            <!-- END Form Elements Title -->
                            {!! View::make('elements.actionMessage')->render() !!}
                            <!-- Basic Form Elements Content -->
                            <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
                            <?php
                            if(!empty($editdata))
                            {
                                  echo Form::model($editdata, ['url' => ['admin/master/loanType?edit=1&id='.$editdata->id], 'id' => 'myform', 'class' => 'form-horizontal form-bordered'], array('method' => 'post', 'id' => 'adminAdd')); 
                                
                            }
                            else{
                                  ?>
                                  {!! Form::open(array('url' => 'admin/master/loanType', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                                  <?php
                            }
                            ?>
                                       <input type="hidden" name="formsubmit" value="1">  
                                    <div class="form-group"> 
                                        <div class="col-md-3 col-lg-2">
                                        {!! HTML::decode(Form::label('name', "Loan Type<span class='require'>*</span>",array('class'=>"control-label"))) !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::text('name', Input::old('name'), array('class' => 'required form-control','autocomplete'=>'off')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group"> 
                                        <div class="col-md-3 col-lg-2">
                                        {!! HTML::decode(Form::label('interest_rate', "Interest Rate <br/>Per Month<span class='require'>*</span>",array('class'=>"control-label"))) !!}
                                        </div>
                                        <div class="col-md-6">
                                       
                                            {!! Form::text('interest_rate', Input::old('interest_rate'), array('class' => 'required number form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group form-actions">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            {!! Form::submit('Save', array('class' => "btn btn-danger")) !!}
                                            {!! Form::reset('Reset', array('class'=>"btn btn-default",'pattern'=>'[A-Za-z]')) !!}
                                        </div>
                                    </div>

                                    {!! Form::close() !!}
                            <!-- END Basic Form Elements Content -->
                        </div>
                        <!-- END Basic Form Elements Block -->
                    </div>
                </div>
            <!-- END Basic Form Elements Block -->
            <?php
            if (!$LoanTypeData->isEmpty()) {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Loan Type</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                <th>Id</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('name', 'Name') !!} </th>
                                                <th>Interest Rate</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('created', 'Created') !!}    </th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($LoanTypeData as $data) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                                <tr>
                                                    <td data-title="Name">
                                                        {!! $i !!}
                                                    </td>
                                                    
                                                    <td data-title="Name">
                                                        {!! $data->name !!}
                                                    </td>

                                                    <td data-title="Name">
                                                         {!! $data->interest_rate !!}

                                                      
                                                    </td>
                                                   

                                                   
                                                    <td data-title="Created">
                                                        {!!  date("M d, Y h:i A", strtotime($data->created)) !!}</td>

                                                    <td data-title="Action" class="admusers-grobtn">
                                                        <?php
                                                       
                                                        if ($data->status == 1)
                                                            echo html_entity_decode(link_to('admin/master/loanType?deactivate==1&id=' . $data->id, '<i class="fa fa-check"></i>', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Active", 'onclick' => "return confirmAction('deactivate');")));
                                                        else
                                                            echo html_entity_decode(link_to('admin/master/loanType?activate=1&id=' . $data->id, '<i class="fa fa-check"></i>', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Deactivate", 'onclick' => "return confirmAction('activate');")));

                                                        echo html_entity_decode(link_to('admin/master/loanType?edit=1&id=' . $data->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'Edit')));
                                                        echo html_entity_decode(link_to('admin/master/loanType?delete=1&id=' . $data->id, '<i class="fa fa-trash-o"></i>', array('title' => 'Delete', 'class' => 'btn btn-danger btn-xs action-list delete-list', 'escape' => false, 'onclick' => "return confirmAction('delete');")));

                                                      //  echo html_entity_decode(link_to('admin/cms/view/' . $data->id, '<i class="fa fa-search"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'View')));
                                                        
                                                        ?>
                                                    </td>   
                                                    
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                              Loan Type
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
              <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {!! $LoanTypeData->appends(request()->except('page'))->links() !!}
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
        </div>
    </div>
</div>
 <SCRIPT language=Javascript>
       <!--
       $('.number').keypress(function(event) {
            var $this = $(this);
            if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
               ((event.which < 48 || event.which > 57) &&
               (event.which != 0 && event.which != 8))) {
                   event.preventDefault();
            }

            var text = $(this).val();
            if ((event.which == 46) && (text.indexOf('.') == -1)) {
                setTimeout(function() {
                    if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                        $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                    }
                }, 1);
            }

            if ((text.indexOf('.') != -1) &&
                (text.substring(text.indexOf('.')).length > 2) &&
                (event.which != 0 && event.which != 8) &&
                ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
            }      
        });
        $('.number').bind("paste", function(e)
         {
        var text = e.originalEvent.clipboardData.getData('Text');
        if ($.isNumeric(text)) {
            if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
                e.preventDefault();
                $(this).val(text.substring(0, text.indexOf('.') + 3));
           }
        }
        else {
                e.preventDefault();
             }
        });
       //-->
    </SCRIPT>
@endsection