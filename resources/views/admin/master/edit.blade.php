@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Edit Profile')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{{ URL::asset('public/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $.validator.addMethod("contact", function (value, element) {
        return  this.optional(element) || (/^[0-9-]+$/.test(value));
    }, "Contact Number is not valid.");
    $("#myform").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Edit Profile</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Edit Profile</strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <?php echo Form::model($detail, ['url' => ['/admin/cms/edit/'.$detail->id], 'id' => 'myform', 'class' => 'form-horizontal form-bordered'], array('method' => 'post', 'id' => 'adminAdd')); ?>
                {!! View::make('elements.actionMessage')->render() !!}
                <div class="form-group">
                    <div class="col-md-3 col-lg-2">
                        <label class="control-label">Question <span class="require">*</span></label>
                    </div>
                    <div class="col-md-6">
                        <?php echo Form::text('question', Input::old('question'), array('id' => 'question', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'question')); ?>
                    </div>
                </div>
              
                <div class="form-group ">
                    <div class="col-md-3 col-lg-2">
                    <label for="answer" class="control-label">Answer<span class="require">*</span></label>
                </div>
                    <div class="col-md-6">
                        <?php
                        echo Form::textarea('answer', Input::old('answer'), array('id' => 'answer', 'autofocus' => true, 'class' => "form-control", 'placeholder' => 'answer'));
                        ?>
                        
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <?php echo Form::button('<i class="fa fa-angle-right"></i> Update', array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>
                        {!! html_entity_decode(link_to('/admin/dashboard', 'Cancel', array('escape' => false,'class'=>"btn btn-default"))) !!}
                    </div>
                </div>
                 <?php echo Form::close(); ?>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>

@endsection