@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Add User')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{!! URL::asset('public/js/jquery.validate.js') !!}"></script>
<link rel="stylesheet" href="{{ asset('public/css/jquery-ui.css') }}">
<script src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('public/css/magicsearch/tokenize2.css') }}">
<script src="{{ asset('public/js/magicsearch/tokenize2.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.tokenize-sample-demo1').tokenize2();
    $("#adminAdd").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/cms/index', 'CMS', array('escape' => false,'class'=>"","title"=>"CMS"))) !!}</li>
        <li>FAQ</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add FAQ</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
                        {!! Form::open(array('url' => 'admin/cms/add', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}

                        <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('faq_category_id', "Category <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                        </div>
                            <div class="col-md-6">
                                <select class="form-control" id="faq_category_id" name="faq_category_id" required="true">
                                    <option value="">Please Select Category</option>
                                    <?php 
                                        if(!$dataCategory->isEmpty())
                                        {
                                            foreach ($dataCategory as $key => $value)
                                            {
                                                ?>
                                                    <option value="<?php echo $value->id ?>"><?php echo $value->name; ?></option>
                                                <?php    
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('question', "Question <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('question', Input::old('question'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('answer', "Answer <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                        </div>
                            <div class="col-md-6">
                                {!! Form::textarea('answer', Input::old('answer'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                        {!! HTML::decode(Form::label('title', "Priority ",array('class'=>"control-label"))) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::number('priority', Input::old('priority'),array('class'=>'form-control priority')); !!}
                        </div>
                        <span id="msg"></span>
                    </div>

                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">             
                   
                    
                        <div class="form-group form-actions">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Save', array('class' => "btn btn-danger")) !!}
                                {!! Form::reset('Reset', array('class'=>"btn btn-default")) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.priority').keyup(function()
    {
        var value = $(this).val();
        var faq_category_id = $( "#faq_category_id" ).val();
        if(value != '' && faq_category_id > 0)
        {
        $.ajax({
                  type: 'POST',
                  url: "<?php echo SITE_URL.'/admin/cms/checkPriority' ?>",
                  data: {'value':value,'_token':$('#token').val(),'faq_category_id':faq_category_id},
                  dataType: "text",
                  success: function(resultData)
                  { 
                    console.log(resultData);
                    if(resultData == 0)
                    {
                        $('#msg').css('color','red');
                        $('#msg').text('Please change priority.This priority already exits for another document !');
                        $('#btnSubmit').attr('disable',true);
                    }
                    else
                    {
                        $('#msg').html('');
                    }
                  }
            });
     }
    });
</script>
@endsection