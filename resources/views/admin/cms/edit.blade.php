<?php
   
 ?>
@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Edit Profile')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{{ URL::asset('public/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $.validator.addMethod("contact", function (value, element) {
        return  this.optional(element) || (/^[0-9-]+$/.test(value));
    }, "Contact Number is not valid.");
    $("#myform").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Edit Profile</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Edit Profile</strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <?php echo Form::model($detail, ['url' => ['/admin/cms/edit/'.$detail->id], 'id' => 'myform', 'class' => 'form-horizontal form-bordered'], array('method' => 'post', 'id' => 'adminAdd')); ?>
                {!! View::make('elements.actionMessage')->render() !!}

                 <div class="form-group">
                    <div class="col-md-3 col-lg-2">
                        <label class="control-label">Category <span class="require">*</span></label>
                    </div>
                    <div class="col-md-6">
                        <select class="form-control" id="faq_category_id" name="faq_category_id" required="true">
                                    <option value="">Please Select Category</option>
                                    <?php 
                                        if(!$dataCategory->isEmpty())
                                        {
                                            foreach ($dataCategory as $key => $value)
                                            {
                                                ?>
                                                    <option <?php if($detail->faqcategory->id == $value->id){ echo "selected='selected'"; } ?> value="<?php echo $value->id ?>"><?php echo $value->name; ?></option>
                                                <?php    
                                            }
                                        }
                                    ?>
                                </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-3 col-lg-2">
                        <label class="control-label">Question <span class="require">*</span></label>
                    </div>
                    <div class="col-md-6">
                        <?php echo Form::text('question', Input::old('question'), array('id' => 'question', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'question')); ?>
                    </div>
                </div>
              
                <div class="form-group ">
                    <div class="col-md-3 col-lg-2">
                    <label for="answer" class="control-label">Answer<span class="require">*</span></label>
                </div>
                    <div class="col-md-6">
                        <?php
                        echo Form::textarea('answer', Input::old('answer'), array('id' => 'answer', 'autofocus' => true, 'class' => "form-control", 'placeholder' => 'answer'));
                        ?>
                        
                    </div>
                </div>

                <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                        {!! HTML::decode(Form::label('title', "Priority ",array('class'=>"control-label"))) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::number('priority', Input::old('priority'),array('class'=>'form-control priority')); !!}
                        </div>
                        <span id="msg"></span>
                    </div>

                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <?php echo Form::button('<i class="fa fa-angle-right"></i> Update', array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>
                        {!! html_entity_decode(link_to('/admin/dashboard', 'Cancel', array('escape' => false,'class'=>"btn btn-default"))) !!}
                    </div>
                </div>
                 <?php echo Form::close(); ?>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.priority').keyup(function()
    {
        var value = $(this).val();
        var faq_category_id = $( "#faq_category_id" ).val();
        if(value != '' && faq_category_id > 0)
        {
        $.ajax({
                  type: 'POST',
                  url: "<?php echo SITE_URL.'/admin/cms/checkPriority' ?>",
                  data: {'value':value,'_token':$('#token').val(),'faq_category_id':faq_category_id},
                  dataType: "text",
                  success: function(resultData)
                  { 
                    console.log(resultData);
                    if(resultData == 0)
                    {
                        $('#msg').css('color','red');
                        $('#msg').text('Please change priority.This priority already exits for another document !');
                        $('#btnSubmit').attr('disable',true);
                    }
                    else
                    {
                        $('#msg').html('');
                    }
                  }
            });
     }
    });
</script>
@endsection