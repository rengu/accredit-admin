@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'FAQ Listing')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php
if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
} else {
    $search = "";
}
if (isset($_REQUEST['from_date']) && !empty($_REQUEST['from_date'])) {
    $_REQUEST['from_date'];

    $from_date = date('d-m-y', strtotime($_REQUEST['from_date']));
} else {
    $from_date = "";
}
if (isset($_REQUEST['to_date']) && !empty($_REQUEST['to_date'])) {
    $to_date = date('d-m-y', strtotime($_REQUEST['to_date']));
} else {
    $to_date = "";
}
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>FAQ Listing</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                   <div class="block-title">
                    <h2><strong>FAQ Listing</strong> 
                        <div class="addBtnSec">
                        <a href="{!! url('/admin/cms/add') !!}" class="fancyboxIframe">
                            <button type="button" class="btn btn-labeled btn-success">
                                <span class="btn-label"><i class="fa fa-plus"></i></span> Add
                            </button>
                        </a>    
                    </div>
                    </h2>
                </div>    

                <div class="row">
                    

                        <section class="panel comm-listing-list">

                            <div class="panel-body">
                                {!! View::make('elements.actionMessage')->render() !!}
                                {!! Form::open(array('url' => 'admin/cms/index', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>'row')) !!}
                                <div class="form-group col-md-4">
                                    {!! HTML::decode(Form::label('search', "Keyword <span class='require'></span>",array('class'=>"control-label"))) !!}
                                    <div>
                                        {!! Form::text('search', $search, array('class' => 'required form-control','placeholder'=>"Your Keyword")) !!}
                                        <span class="help-block">Search Faq by typing their Question OR Answer</span>
                                    </div>
                                </div>
                                
                                <div class="form-group form-actions col-md-4">
                                        {!! Form::submit('Search', array('class' => "btn btn-success")) !!}
                                        <a href="{!! url('admin/cms/index') !!}" class="btn btn-default">Clear Filters</a>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </section>
                    
                   
                </div>
            </div>
            <!-- END Basic Form Elements Block -->
            <?php
            if (!$faq->isEmpty()) {
                ?>

                {!! Form::open(array('url' => 'admin/loans/index', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Loan Applications List</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                <th>Category</th>
                                                 <th>{!! App\Commands\SortableTrait::link_to_sorting_action('question', 'Question') !!} </th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('answer', 'Answer') !!}	</th>

                                                 <th>Priority</th>
                                               
                                                <th style="width:180px; min-width:180px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($faq as $data) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                                <tr>

                                                    <td data-title="Category">
                                                        {!! isset($data->faqcategory->name)?$data->faqcategory->name:"" !!}
                                                    </td>

                                                    <td data-title="Name">
                                                        {!! isset($data->question)?$data->question:"" !!}
                                                    </td>
													<td data-title="Name">
                                                        {!! isset($data->answer)?$data->answer:"" !!}
                                                    </td>
                                                    
                                                    
                                                    <td data-title="Created">
                                                        {!! isset($data->priority)?$data->priority:0 !!}</td>

                                                    <td data-title="Action" class="admusers-grobtn">
                                                        <?php
                                                       
                                                        if ($data->status == 1)
                                                            echo html_entity_decode(link_to('admin/cms/deactivate/' . $data->id, '<i class="fa fa-check"></i>', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Active", 'onclick' => "return confirmAction('deactivate');")));
                                                        else
                                                            echo html_entity_decode(link_to('admin/cms/activate/' . $data->id, '<i class="fa fa-check"></i>', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Deactivate", 'onclick' => "return confirmAction('activate');")));

                                                        echo html_entity_decode(link_to('admin/cms/edit/' . $data->id, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'Edit')));
                                                        echo html_entity_decode(link_to('admin/cms/delete/' . $data->id, '<i class="fa fa-trash-o"></i>', array('title' => 'Delete', 'class' => 'btn btn-danger btn-xs action-list delete-list', 'escape' => false, 'onclick' => "return confirmAction('delete');")));

                                                      //  echo html_entity_decode(link_to('admin/cms/view/' . $data->id, '<i class="fa fa-search"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'View')));
														
                                                        ?>
                                                    </td>	
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
               
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                loans List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
              <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {!! $faq->appends(request()->except('page'))->links() !!}
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection