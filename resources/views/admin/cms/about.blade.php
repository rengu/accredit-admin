@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'About Us')
@extends('admin.layouts.admin_dashboard')
@section('content')
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>About Us</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add About Us</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
               
                        {!! Form::model($about, array('url' => '/admin/cms/about/', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                        <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('content', " About Content <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::textarea('content', Input::old('content'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Save', array('class' => "btn btn-danger")) !!}
                                {!! Form::reset('Reset', array('class'=>"btn btn-default")) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>

        </div>
    </div>
</div>
@endsection