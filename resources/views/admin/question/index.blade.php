@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Questions Listing')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Questions Listing</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            {!! View::make('elements.actionMessage')->render() !!}
            <div class="block">
                <div class="block-title">
                    <h2><strong>Questions Listing</strong> 
                        <div class="addBtnSec">
                        <a href="{!! url('/admin/question/add') !!}" class="fancyboxIframe">
                            <button type="button" class="btn btn-labeled btn-success">
                                <span class="btn-label"><i class="fa fa-plus"></i></span> Add
                            </button>
                        </a>    
                    </div>
                    </h2>
                </div>          
            <?php
                if (!empty($questions)) {
            ?>

                {!! Form::open(array('url' => 'admin/question/index', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                           <!--  <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Package List</h3>
                            </header> -->
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                            <!--     <th></th> -->
                                                <th>Question Text</th>
                                              
                                                <th>Question Type</th>
                                                <th>Options</th>
                                                <th>Status</th>
                                                <th style="width:180px; min-width:180px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($questions as $value) {
                                              
                                                ?>
                                                <tr>
                                                   <!--  <td data-title="Select">
                                                        {!! Form::checkbox('id', $value->id,null, array("onclick"=>"javascript:isAllSelect(this.form);",'name'=>"chkRecordId[]")) !!}
                                                    </td> -->
                                                   <td> {{ $value->question_text }} </td>

                                                    <td> 
                                                        <?php
                                                        if(isset(questionType[$value->question_type]))
                                                        {
                                                           echo questionType[$value->question_type];
                                                        }
                                                       
                                                        ?>

                                                     </td>
                                                     <td> 
                                                        <?php
                                                        if(isset($value->option) && !empty($value->option))
                                                        {
                                                        $options= json_decode($value->option,true);
                                                         echo implode(",", $options);
                                                        }
                                                        ?> </td>
                                                         <td> 
                                                        <?php
                                                        if($value->status == 1)
                                                        {
                                                             echo 'Enable';
                                                        }
                                                        else{
                                                             echo 'Disable';
                                                        }
                                                        ?> </td>
                                                 
                                                    <td data-title="Action" class="admvalues-grobtn">
                                                        <?php
                                                        
                                                        echo html_entity_decode(link_to('admin/question/edit/' . $value->id, '<i class="fa fa-pencil"></i>', array('class' => ' btn-primary btn-xs', 'title' => 'Edit')));
                                                        echo html_entity_decode(link_to('admin/question/delete/' . $value->id, '<i class="fa fa-trash-o"></i>', array('title' => 'Delete', 'class' => ' btn-danger btn-xs action-list delete-list', 'escape' => false, 'onclick' => "return confirmAction('delete');")));
                                                        ?>
                                                    </td>	
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                     <!--    <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    
                                </div>
                            </div>
                            <div class="panel-body">
                                <button type="button" name="chkRecordId" onclick="checkAll(true);"  class="btn btn-success">Select All</button>
                                <button type="button" name="chkRecordId" onclick="checkAll(false);" class="btn btn-success">Unselect All</button>
                                <?php
                                $arr = array(
                                    "" => "Action for selected...",
                                    'Activate' => "Activate",
                                    'Deactivate' => "Deactivate",
                                    'Delete' => "Delete",
                                );
                                //  echo form_dropdown("action", $arr, '', "class='small form-control' id='table-action'");
                                ?>
                                {!! Form::select('action', $arr, null, array('class'=>"small form-control btninspace",'id'=>'action')) !!}

                                <button type="submit" class="small btn btn-success btn-cons" onclick=" return isAnySelect();" id="submit_action">Ok</button>
                            </div>
                        </section> -->
                    </div>
                </div>
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Package List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
        </div>
    </div>
</div>
@endsection