@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Edit Page')
@extends('admin.layouts.admin_dashboard')
@section('content')

<script type="text/javascript">
    $(document).ready(function () {
        $("#adminAdd").validate();
    });
</script>

<div id="page-content" class="admedit-profile">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/question/index', 'Question', array('escape' => false,'class'=>"","title"=>"Questions Listings"))) !!}</li>
        <li>Add Question</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add Question</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
               {!! Form::open(array('url' => 'admin/question/add', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                <div class="form-group">
                    <div class="col-md-3 col-lg-2">
                        {!! HTML::decode(Form::label('question_text', "Question<span class='require'>*</span>",array('class'=>"control-label"))) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::text('question_text', Input::old('question_text'), array('class' => 'required form-control','autocomplete'=>'off')); !!}
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-3 col-lg-2">
                    {!! HTML::decode(Form::label('question_type', "Question Type<span class='require'>*</span>",array('class'=>"control-label"))) !!}
                     </div>
                    <div class="col-md-4">
                       {!!Form::select('question_type', questionType,null, ['class' => 'form-control'])!!}
                    </div>        
                </div>
                 <div class="input_fields_wrap" >
                    
                    <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                             {!! HTML::decode(Form::label('option1', "Option 1 <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('option[0]', Input::old('option'), array('class' => 'required form-control textOption','placeholder'=>'Option')); !!}
                        </div>
                        <button class="add_field_button">+</button>
                    </div>
                </div>


          

                <div class="form-group">
                    <div class="col-md-3 col-lg-2">
                        {!! HTML::decode(Form::label('status', "Status <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                    </div>
                    <div class="col-md-6">
                        <div class="raio">
                        {!! Form::radio('status', '1', true, array('id'=>'Enable')); !!}
                            <label>Enable</label>
                        </div>
                        <div class="raio">
                            {!! Form::radio('status', '0', false,array('id'=>'Disable')); !!}
                            <label>Disable</label>
                        </div>
                    </div>
                </div>
                 
                <div class="form-group form-actions">
                    <div class="col-lg-offset-2 col-lg-10">
                        {!! Form::submit('Save', array('class' => "btn btn-primary")) !!}
                        {!! Form::reset('Reset', array('class'=>"btn btn-default")) !!}
                    </div>
                </div>
                {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
<script type="text/javascript">

$( "#question_type" ) .change(function () {
alert('sggg'); 
if($(this).val() != 3 )
{
    alert('sfsf');
    $('.input_fields_wrap').show();
     $(".textOption").attr("disabled", "false");
}
else{
    alert('else');
    $('.input_fields_wrap').hide();
    $(".textOption").attr("disabled", "disabled");
    
}
});  
</script>

<script>
    $(document).ready(function() {
    var max_fields      = 5; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
         //text box increment
        var y= x+1;
            $(wrapper).append('<div class="form-group"><div class="col-md-3 col-lg-2"><label for="operator" class="control-label">Option '+y+' <span class="require">*</span></label></div><div class="col-md-4"><input class="required form-control textOption" name="option['+x+']" type="text" aria-required="true" placeholder="Option"></div><a href="#" class="remove_field">X</a></div>'); //add input box
            x++;

        }
        else{
            alert('You can add only Five row');
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

@endsection
