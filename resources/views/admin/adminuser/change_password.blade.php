@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Edit User')
@extends('admin.layouts.admin_dashboard')
@section('content')

<script src="{!! URL::asset('public/js/jquery.validate.js') !!}"></script>
<link rel="stylesheet" href="{{ asset('public/css/jquery-ui.css') }}">
<script src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('public/css/magicsearch/tokenize2.css') }}">
<script src="{{ asset('public/js/magicsearch/tokenize2.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tokenize-sample-demo1').tokenize2();
        $("#adminAdd").validate();
    });
</script>

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/users/index', 'Users', array('escape' => false,'class'=>"","title"=>"User Listings"))) !!}</li>
        <li>User Password Change</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>User Password Change - <?php echo $detail->name; ?></strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
                        {!! Form::model($detail, array('url' => '/admin/adminuser/change_password/'.$detail->slug, 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                       <div class="form-group ">
                <div class="col-md-3 col-lg-2">
                    <label for="password" class="control-label">New Password <span class="require">*</span></label>
                </div>
                    <div class="col-md-6">
                        <?php
                        echo Form::password('password', array('id' => 'password', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'Password'));
                        ?>
                    </div>
                </div>
                <div class="form-group ">
                <div class="col-md-3 col-lg-2">
                    <label for="cpassword" class="control-label">Confirm Password <span class="require">*</span></label>
                </div>
                    <div class="col-md-6">
                        <?php
                        echo Form::password('cpassword', array('id' => 'cpassword', 'equalTo' => '#password', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'Confirm Password'));
                        ?>
                    </div>
                </div>

                        <div class="form-group form-actions">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Update', array('class' => "btn btn-danger")) !!}
                                {!! html_entity_decode(link_to("admin/adminuser/index","Cancel", array("class"=>"btn btn-default"))) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>

@endsection