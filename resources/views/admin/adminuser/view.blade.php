@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'User Details')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/users/index', 'User Listings', array('escape' => false,'class'=>"",'title'=>"Users Listings"))) !!}</li>
        <li>User Details</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Interactive Title -->
                <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>User Detail</strong> 
                    <div class="addBtnSec">
                    <a href="{!! url('/admin/users/index') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                </div></h2>
                </div>
                <!-- END Interactive Title -->
                <div class="row">
                        <div class="col-lg-6">
                            <h5 class="sub-header">Username: <b> {!! $userData->username !!}</b></h5>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="sub-header">Email: <b> {!! $userData->email !!}</b></h5>
                        </div>
                         <div class="col-lg-6">
                            <h5 class="sub-header">Name: <b> {!! $userData->name !!}</b></h5>
                        </div>
                         <div class="col-lg-6">
                            <h5 class="sub-header">Phone: <b> {!! $userData->phone !!}</b></h5>
                        </div>  
                        <div class="col-lg-6">
                            <h5 class="sub-header">User Type: <b>{!! !empty($userData->user_type->name)?$userData->user_type->name:'' !!} </b></h5>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="sub-header">Site ID: <b> {!! $siteNameValues !!}</b></h5>
                        </div>  
                        <div class="col-lg-6">
                            <h5 class="sub-header">Status: <b> {!! ($userData->status==1)?"Activated":"Deactivated" !!}</b></h5>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="sub-header">Created: <b> {!! $userData->created_at !!}</b></h5>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="sub-header">Updated: <b> {!! $userData->updated_at !!}</b></h5>
                        </div>
						 <div class="col-lg-6">
                            <h5 class="sub-header">Fake GPS apps: <b> {!! $userData->app !!} <?php $App = explode(",",$userData->app); ?></b></h5>
                        </div>
                </div>        
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
@endsection