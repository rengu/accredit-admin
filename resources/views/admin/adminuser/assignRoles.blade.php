
@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Assgin Roles')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php
use App\Http\Controllers\Controller;

  /*foreach ($dataModule as $key => $value)
  {
     
     foreach ($value as $key11 => $value11)
     {
          //print_r($subAuths);die;
          if(!empty($subAuths[$value11['controller']]))
          {
            if($subAuths[$value11['controller']]['controller'] = $value11['controller'] && in_array($value11['action'],$subAuths[$value11['controller']]['action']))
           {
              echo "b"; 
           }
           else
           {
             echo "a"; 
           }
          }
          else
         {
            echo "c"; 
        }

     }
       }die;*/
  
?>


<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Users Roles</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>User Rights</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                
                        {!! Form::model('frm_assignrole',array('url' => 'admin/adminuser/assignRoles/'.$id, 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                        
                        <div class="table-responsive">
          <table class="table table-bordered tableassign">
            <thead>
              <tr>
              </tr>
            </thead>
            <tbody>
                <?php 
              //  echo '<pre>';
              //  print_r($dataModule);
                    if(!empty($dataModule))
                    {
                         //print_r($dataModule);exit();
                        $j= 0;
                        foreach ($dataModule as $key => $value)
                        {
                           ?>  
                              <tr>
                                 
                                 <?php 
                                    if(!empty($value))
                                    {
                                        $i=0;
                                        foreach ($value as $key11 => $value11)
                                        {
                                            if($i==0)
                                            {
                                                ?>
                                                   <td><?php echo ucwords($value11['name']); ?></td>
                                                <?php
                                            }
                                            

                                            ?>
                                                 <td>
                                                  <div class="custom-checkbox">
                                                      <input <?php 
         if(!empty($subAuths[$value11['controller']]))
          {
            if($subAuths[$value11['controller']]['controller'] = $value11['controller'] && in_array($value11['action'],$subAuths[$value11['controller']]['action']))
           {
              echo "checked"; 
           }
        } 


                                                       ?> id="{!! $j; !!}{!! $i; !!}" class="view" name='data[{!! $value11["name"] !!}][{!!  $value11["controller"]!!}][{!! $value11["action"] !!}]'  type="checkbox">
                                                      <label for="{!! $j; !!}{!! $i; !!}">

                                                      <?php echo ucwords($value11['action_title']) ?></label>
                                                  </div>
                                                </td>
                                            <?php
                                            $i++;
                                        }
                                    }
                                 ?> 

                              </tr>
                           <?php
                           $j++;
                        }
                    }
                ?>

            </tbody>
          </table>
        </div>
                     {!! Form::submit('Update', array('class' => "btn btn-danger")) !!}
        

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
</div>
</div>
@endsection