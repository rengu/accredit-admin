@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Users Listings')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php

if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
} else {
    $search = "";
}
if (isset($_REQUEST['from_date']) && !empty($_REQUEST['from_date'])) {
    $_REQUEST['from_date'];

    $from_date = date('d-m-y', strtotime($_REQUEST['from_date']));
} else {
    $from_date = "";
}
if (isset($_REQUEST['to_date']) && !empty($_REQUEST['to_date'])) {
    $to_date = date('d-m-y', strtotime($_REQUEST['to_date']));
} else {
    $to_date = "";
}
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Users Roles</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add User</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
                        {!! Form::open(array('url' => 'admin/adminuser/role', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                        <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('name', " Role Name <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('name', Input::old('name'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Save', array('class' => "btn btn-danger")) !!}
                                {!! Form::reset('Reset', array('class'=>"btn btn-default")) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
            <!-- END Basic Form Elements Block -->
            <?php
            if (!$roles->isEmpty()) {
                ?>

                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Users Role</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($roles as $user) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                                <tr>
                                                    <td data-title="Name">
                                                        {!! $i !!}
                                                    </td>
                                                    
                                                    <td data-title="Name">
                                                        {!! $user->name !!}
                                                    </td>
                                                   

                                                   
                                                    <td data-title="Created">
                                                        {!!  date("M d, Y h:i A", strtotime($user->created)) !!}</td>

                                                    <td data-title="Action">
                                                        <?php echo html_entity_decode(link_to('admin/adminuser/assignRoles/' . base64_encode($user->id), 'Assign Modules', array('class' => 'btn btn-primary btn-xs', 'title' => 'Edit'))); 
                                                        ?>
                                                    </td>

                                                    
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
              
              
         

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Users List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
        </div>
    </div>
</div>
@endsection