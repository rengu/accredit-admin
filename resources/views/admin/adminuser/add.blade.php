<?php 
    $routeArray = app('request')->route()->getAction();
    $controllerAction = class_basename($routeArray['controller']);
    list($controller, $action) = explode('Controller@', $controllerAction);
    //print_r($controller); die;
?>
@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Add User')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{!! URL::asset('public/js/jquery.validate.js') !!}"></script>
<link rel="stylesheet" href="{{ asset('public/css/jquery-ui.css') }}">
<script src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('public/css/magicsearch/tokenize2.css') }}">
<script src="{{ asset('public/js/magicsearch/tokenize2.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.tokenize-sample-demo1').tokenize2();
    $("#adminAdd").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/adminuser/index', 'Admin Users', array('escape' => false,'class'=>"","title"=>"Admin User"))) !!}</li>
        <li>Add User</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add User</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
                        {!! Form::open(array('url' => 'admin/adminuser/add', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                        <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('name', "Name <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('name', Input::old('name'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('username', "Username <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                        </div>
                            <div class="col-md-6">
                                {!! Form::text('username', Input::old('username'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('email', "Email Address <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                        </div>                          
                            <div class="col-md-6">
                                {!! Form::text('email', Input::old('email'), array('class' => 'required email form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('contact', "Contact Number <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                        </div>
                            <div class="col-md-6">
                                {!! Form::text('phone',Input::old('phone'), array('class' => 'required number form-control','maxlength'=>'16'))!!}
                            </div>
                        </div>
                        <div class="form-group">
                        <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('admin_typ_id', "Role <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                        </div>
                            <div class="col-md-6">
                                 <!--  {!! Form::select('admin_type_id', ['' => 'Please Select']+ $roles,array('class' => 'form-control'))!!} -->
                                 {!! Form::select('admin_type_id', ['' => 'Please Select'] + $roles, null, array('class' => 'form-control'))!!}
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Save', array('class' => "btn btn-danger")) !!}
                                {!! Form::reset('Reset', array('class'=>"btn btn-default")) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
<script type="text/javascript">
      $(document).ready(function(){
        if($('#all').is(':checked')){
            $("#sites-show").hide();
        } else {
            $("#sites-show").show();
        }
    });
    function checkAssignType(val){
        if(val=='all'){
            $("#sites-show").hide();
        } else {
            $("#sites-show").show();
        }
    }
</script>
@endsection
