@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Payment Report')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php

if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
} else {
    $search = "";
}
if (isset($_REQUEST['from_date']) && !empty($_REQUEST['from_date'])) {
    $_REQUEST['from_date'];

    $from_date = date('d-m-y', strtotime($_REQUEST['from_date']));
} else {
    $from_date = "";
}
if (isset($_REQUEST['to_date']) && !empty($_REQUEST['to_date'])) {
    $to_date = date('d-m-y', strtotime($_REQUEST['to_date']));
} else {
    $to_date = "";
}
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Payment Report</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">                 
                  
                <div class="row">                    

                        <section class="panel comm-listing-list">

                            <div class="panel-body">
                                {!! View::make('elements.actionMessage')->render() !!}
                                {!! Form::open(array('url' => 'admin/Reports/paymentReport', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>'row')) !!}
                                <div class="form-group col-md-4">
                                    {!! HTML::decode(Form::label('search', "Keyword <span class='require'></span>",array('class'=>"control-label"))) !!}
                                    <div>
                                       <select class="form-control" name="search" id="search">
                                           <option <?php if($search_keyword == 1){ echo "selected='selected'"; } ?> value="1">Today</option>
                                           <option <?php if($search_keyword == 7){ echo "selected='selected'"; } ?> value="7">Weekly</option>
                                           <option <?php if($search_keyword == 30 || empty($search_keyword)){ echo "selected='selected'"; } ?> value="30">Monthly</option>
                                           <option <?php if($search_keyword == 90){ echo "selected='selected'"; } ?> value="90">Quaterly</option>
                                           <option <?php if($search_keyword == 180){ echo "selected='selected'"; } ?> value="180">Half Yearly</option>
                                           <option <?php if($search_keyword == 365){ echo "selected='selected'"; } ?> value="365">Yearly</option>
                                       </select>
                                        <span class="help-block">Generate Report by select time period</span>
                                    </div>
                                </div>
                                
                                <div class="form-group form-actions col-md-4">
                                        {!! Form::submit('Search', array('class' => "btn btn-success")) !!}
                                        <a href="{!! url('admin/Reports/paymentReport') !!}" class="btn btn-default">Clear Filters</a>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </section>
                    
                   
                </div>
            </div>
            <!-- END Basic Form Elements Block -->
            <?php
            if (!$data->isEmpty()) {
                ?>

                {!! Form::open(array('url' => 'admin/loans/index', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">

                    <div class="col-lg-12">
                        <?php if(empty($search_keyword)){ $search_keyword = 30; } ?>
                        <section class="panel">
                              <div class="addBtnSec">
                               <!--  <a href="{!! url('/admin/Reports/downloadReports/'.$search_keyword) !!}" class="fancyboxIframe"> -->
                                 <!-- <a href="#" class="fancyboxIframe">
                                    <button type="button" class="btn btn-labeled btn-success">
                                        Download Report
                                    </button>
                                </a>     -->
                            </div>
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Payment Report</h3>
                            </header>



                            <div class="panel-body">

                              
                      
                    

                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                 <th>User Name</th>
                                                <th>Loan Number</th>
                                                <th>Installment Name</th>
                                                <th>Payment Date</th>
                                                <th>Principal Amount</th>
                                                <th>Interest Amount</th>
                                                <th>Total Paid</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php 
                                            if(!empty($data))
                                            {
                                                foreach ($data as $key => $value)
                                                {
                                                   ?>
                                                   <tr>
                                                     <td data-title="Name">
                                                        {!! isset($value->userinfo->name)?$value->userinfo->name:"" !!}
                                                    </td>
                                                    <?php 
                                                    if($value->loan_type == 3)
                                                    {
                                                      ?>
                                                        <td>
                                                            {!! isset($value->loanenquiryindividual->loan_no)?$value->loanenquiryindividual->loan_no:"" !!}
                                                        </td>
                                                      <?php
                                                    }
                                                    else if($value->loan_type == 4)
                                                    {
                                                        ?>
                                                            {!! isset($value->loanenquirycompany->loan_no)?$value->loanenquirycompany->loan_no:"" !!}
                                                        <?php
                                                    }
                                                    ?>
                                                    <td> {!! isset($value->installment_name)?$value->installment_name:"" !!}</td>

                                                    <td> {!! isset($value->date_of_payment)?$value->date_of_payment:"" !!}</td>

                                                    <td> {!! isset($value->principal_amount)?$value->principal_amount:"" !!}</td>

                                                      <td> {!! isset($value->interest_amount)?$value->interest_amount:"" !!}</td>

                                                      <td> {!! isset($value->total_emi)?$value->total_emi:"" !!}</td>


                                                  </tr>
                                                   <?php
                                                }
                                            }
                                           ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
               
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                              Payment Report
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
              <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {!! $data->appends(request()->except('page'))->links() !!}
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection
