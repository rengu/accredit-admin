<?php 
    $routeArray = app('request')->route()->getAction();
    $controllerAction = class_basename($routeArray['controller']);
    list($controller, $action) = explode('Controller@', $controllerAction);
    //print_r($controller); die;
?>
@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Add Document')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{!! URL::asset('public/js/jquery.validate.js') !!}"></script>
<link rel="stylesheet" href="{{ asset('public/css/jquery-ui.css') }}">
<script src="{{ asset('public/js/jquery-ui.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('public/css/magicsearch/tokenize2.css') }}">
<script src="{{ asset('public/js/magicsearch/tokenize2.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.tokenize-sample-demo1').tokenize2();
    $("#adminAdd").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/document/index', 'Documents', array('escape' => false,'class'=>"","title"=>"Document Listings"))) !!}</li>
        <li>Add Document</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Add Document</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>

                        {!! Form::open(array('url' => 'admin/documents/add', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}

                        <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('title', "Title <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('title', '',array('class'=>'form-control required')); !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('title', "Priority ",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::number('priority', '',array('class'=>'form-control priority')); !!}
                            </div>
                            <span id="msg"></span>
                        </div>

                        <!--  <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('url', "URL <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('url', '',array('class'=>'form-control required')); !!}
                            </div>
                        </div> -->

                         <div class="form-group">
                            <div class="col-md-3 col-lg-2">
                            {!! HTML::decode(Form::label('Document', "Upload Document<span class='require'>*</span>",array('class'=>"control-label"))) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::file('document',array('class'=>'form-control required','id'=>'document','accept'=>'pdf')); !!}
                            </div>
                        </div>

                            <div class="form-group form-actions">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Save', array('class' => "btn btn-danger",'id'=>'btnSubmit')) !!}
                               
                            </div>
                        </div>
                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
<script type="text/javascript">
      $(document).ready(function(){
        if($('#all').is(':checked')){
            $("#sites-show").hide();
        } else {
            $("#sites-show").show();
        }
    });
    function checkAssignType(val){
        if(val=='all'){
            $("#sites-show").hide();
        } else {
            $("#sites-show").show();
        }
    }

    $('.priority').keyup(function()
    {
        var value = $(this).val();
        if(value != '')
        {
        $.ajax({
                  type: 'POST',
                  url: "<?php echo SITE_URL.'/admin/documents/checkPriority' ?>",
                  data: {'value':value,'_token':$('#token').val()},
                  dataType: "text",
                  success: function(resultData)
                  { 
                    if(resultData == 0)
                    {
                        $('#msg').css('color','red');
                        $('#msg').text('Please change priority.This priority already exits for another document !');
                        $('#btnSubmit').attr('disable',true);
                    }
                    else
                    {
                        $('#msg').html('');
                    }
                  }
            });
     }
    });

    $('#btnSubmit').click(function()
    {
        /*const file = $('#document')[0].files[0];
        const  fileType = file['type'];
        alert(fileType);
        if(fileType != 'application/pdf')
        {
            alert('Please upload an valid pdf file !');
            return false;
        }
        else
        {
            return true;
        }*/
        const file = $('#document')[0].files[0];
         const  fileType = file['type'];
         if(fileType != 'application/pdf')
        {
            alert('Please upload an valid pdf file !');
            return false;
        }
        else
        {
            return true;
        }
        return false;
    });

</script>
@endsection