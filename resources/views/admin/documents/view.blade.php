@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Document Details')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/documents', 'Document Details', array('escape' => false,'class'=>"",'title'=>"Documents Details"))) !!}</li>
        <li>Document Details</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Interactive Title -->
                <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>Document Details</strong> 
                    <div class="addBtnSec">
                    <a href="{!! url('/admin/documents') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                </div></h2>
                </div>
                <!-- END Interactive Title -->
                <div class="row">
                        <div class="col-lg-6">
                            <h5 class="sub-header">Title: <b> <?php echo $data->title ?></b></h5>
                        </div>
                        
                        
                         
                    <div class="col-lg-6">
                            <h5 class="sub-header">Priority: <b> <?php echo $data->priority ?></b></h5>
                        </div>

                        <div class="col-lg-6">
                            <h5 class="sub-header">Status: <b> <?php if($data->status == 1){ echo "Activate"; }else{ echo "Deactivate"; } ?></b></h5>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="sub-header">Created: <b> <?php echo $data->created_at ?></b></h5>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="sub-header">Updated: <b> <?php echo $data->updated_at ?></b></h5>
                        </div>

                        <?php 
                            if(!empty($data->document))
                            {
                                ?>
                                    <div class="col-lg-12">
                                      <iframe width="500" height="400" src="{{ URL::asset('/public/files/admindocuments/'.$data->document) }}"></iframe>
                                    </div>
                                <?php
                            }
                         ?>
						
                </div>        
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
@endsection