@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Documents Listing')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Documents Listings</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
             <div class="addBtnSec">
                        <a href="{!! url('/admin/documents/add') !!}" class="fancyboxIframe">
                            <button type="button" class="btn btn-labeled btn-success">
                                <span class="btn-label"><i class="fa fa-plus"></i></span> Add
                            </button>
                        </a>    
                    </div>
           
            <?php
            if (!$data->isEmpty()) {               
                ?>
                {!! View::make('elements.actionMessage')->render() !!}
                {!! Form::open(array('url' => 'admin/documents', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">
                    <div class="col-lg-12">

                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Documents List</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                <th></th>
												<th>Title</th>    
                                                <th>Priority</th>               
                                                <th>Created</th>        
                                                <th style="width:180px; min-width:180px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            
                                            foreach ($data as $key => $data11) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                                <tr>
                                                    <td data-title="Select">
                                                        {!! Form::checkbox('id', $data11->id,null, array("onclick"=>"javascript:isAllSelect(this.form);",'name'=>"chkRecordId[]")) !!}

                                                    </td>
                                                    <td data-title="Title">
                                                        {!! isset($data11->title)?$data11->title:"" !!}


                                                    </td>
                                                    <td data-title="Title">
                                                        {!! isset($data11->priority)?$data11->priority:0 !!}


                                                    </td>
													
                                                    <td data-title="Document">
                                                          {!! date('d-M-Y',strtotime($data11->created_at)) !!}
                                                    </td>
                                                    
                                            
                                                    <td data-title="Action" class="admusers-grobtn">
                                                        <?php
                                                        if ($data11->status == 1)
                                                            echo html_entity_decode(link_to('admin/documents/deactivate/' . $data11->id, '<i class="fa fa-check"></i>', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Active", 'onclick' => "return confirmAction('deactivate');")));
                                                        else
                                                            echo html_entity_decode(link_to('admin/documents/activate/' . $data11->id, '<i class="fa fa-check"></i>', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Deactivate", 'onclick' => "return confirmAction('activate');")));

                                                      //  echo html_entity_decode(link_to('admin/loans/edit/' . $data->slug, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'Edit')));
                                                        echo html_entity_decode(link_to('admin/documents/delete/' . $data11->id, '<i class="fa fa-trash-o"></i>', array('title' => 'Delete', 'class' => 'btn btn-danger btn-xs action-list delete-list', 'escape' => false, 'onclick' => "return confirmAction('delete');")));

                                                        echo html_entity_decode(link_to('admin/documents/view/' . $data11->id, '<i class="fa fa-search"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'View')));
														
                                                        ?>
                                                    </td>	
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {!! $data->appends(request()->except('page'))->links() !!}
                                </div>
                            </div>
                            <div class="panel-body">
                                <button type="button" name="chkRecordId" onclick="checkAll(true);"  class="btn btn-success">Select All</button>
                                <button type="button" name="chkRecordId" onclick="checkAll(false);" class="btn btn-success">Unselect All</button>
                                <?php
                                $arr = array(
                                    "" => "Action for selected...",
                                    'Activate' => "Activate",
                                    'Deactivate' => "Deactivate",
                                    'Delete' => "Delete",
                                );
                                //  echo form_dropdown("action", $arr, '', "class='small form-control' id='table-action'");
                                ?>
                                {!! Form::select('action', $arr, null, array('class'=>"small form-control btninspace",'id'=>'action')) !!}
                                {!! Form::hidden('search', '', array('id' => '')) !!}

                                <button type="submit" class="small btn btn-success btn-cons" onclick=" return isAnySelect();" id="submit_action">Ok</button>
                            </div>
                        </section>
                    </div>
                </div>
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Document List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
        </div>
    </div>
</div>
@endsection