@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Loan Details')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/loans/index', 'Loan Applications Listings', array('escape' => false,'class'=>"",'title'=>"Loan Applications Listings"))) !!}</li>
        <li>Individual Loan Enquiry Details</li>
    </ul>
    <!-- END Forms General Header -->
   <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Interactive Title -->
                <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>Individual Loan Enquiry Details</strong> 
                    <div class="addBtnSec">
                    <a href="{!! url('/admin/loans/companyLoanEnquiries') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                </div></h2>
                </div>
                <!-- END Interactive Title -->
                <div class="row">
                        <div class="col-lg-4">
                            <h5 class="sub-header">Name: <b> {!! isset($data->name)?$data->name:"" !!}</b></h5>
                        </div>
                        <div class="col-lg-4">
                            <h5 class="sub-header">Email: <b> {!! isset($data->email)?$data->email:"" !!}</b></h5>
                        </div>
                         
                         <div class="col-md-4 col-sm-4">
                         <h5 class="sub-header">Office Phone No.: <b> {!! isset($data->office_number)?$data->office_number:"" !!}</b></h5> 
                         </div> 

                        <div class="col-lg-4">
                        <h5 class="sub-header">Loan Type: <b> {!! isset($data->loantype->name)?$data->loantype->name:''; !!}</b></h5>
                        </div>
                   
                        <div class="col-lg-4">
                        <h5 class="sub-header">How did i get to know Accredit: <b> {!! isset($data->knowaccrdit->name)?$data->knowaccrdit->name:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                        <h5 class="sub-header">How did i get to know Accredit: <b> {!! isset($data->knowaccrdit->name)?$data->knowaccrdit->name:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                        <h5 class="sub-header">Loan Amount Requested: <b> {!! isset($data->loan_amount_requested)?$data->loan_amount_requested:''; !!}</b></h5>
                        </div>

                  
                   
                        <div class="col-lg-4">
                        <h5 class="sub-header">Loan Purpose: <b> {!! isset($data->loanpurpose->name)?$data->loanpurpose->name:''; !!}</b></h5>
                        </div>

                        
                        <div class="col-lg-4">
                        <h5 class="sub-header">Reason For Loan: <b> {!! isset($data->reasonforloan->name)?$data->reasonforloan->name:''; !!}</b></h5>
                        </div>
                           
                        
                         
                                <div class="col-lg-4">
                                <h5 class="sub-header">Finacial Year End As At: <b> {!! isset($data->financial_year_end_as_at)?$data->financial_year_end_as_at:''; !!}</b></h5>
                                </div>
                           
                                <div class="col-lg-4">
                                <h5 class="sub-header">Annual Gross Profit: <b> {!! isset($data->annual_gross_profit)?$data->annual_gross_profit:0.00; !!}</b></h5>
                                </div>
                           
                         
                                <div class="col-lg-4">
                                <h5 class="sub-header">Annual Profit <b> {!! isset($data->annual_net_profit->name)?$data->annual_net_profit->name:0.00; !!}</b></h5>
                                </div>
                          
                        
                                <div class="col-lg-4">
                                <h5 class="sub-header">Total Assets: <b> {!! isset($data->total_asset)?$data->total_asset:0.00; !!}</b></h5>
                                </div>
                           
                        
                                <div class="col-lg-4">
                                <h5 class="sub-header">Total Current Asset: <b> {!! isset($data->total_current_asset)?$data->total_current_asset:0.00; !!}</b></h5>
                                </div>
                            

                         
                                <div class="col-lg-4">
                                <h5 class="sub-header">Total Liabilities: <b> {!! isset($data->total_liabilities)?$data->total_liabilities:0.00; !!}</b></h5>
                                </div>
                           

                                <div class="col-lg-4">
                                <h5 class="sub-header">Total Current Liabilities: <b> {!! isset($data->total_current_liabilities)?$data->total_current_liabilities:0.00; !!}</b></h5>
                                </div>

                         
                                <div class="col-lg-4">
                                <h5 class="sub-header">Employment Status: <b> {!! isset($data->employmentstatus->name)?$data->employmentstatus->name:'' !!}</b></h5>
                                </div>
                           

                        
                                <div class="col-lg-4">
                                <h5 class="sub-header">Company Office Address One: <b> {!! isset($data->company_office_address_one)?$data->company_office_address_one:'' !!}</b></h5>
                                </div>
                      

                       
                                 <div class="col-lg-4">
                                <h5 class="sub-header">Company Office Address Two: <b> {!! isset($data->company_office_address_two)?$data->company_office_address_two:'' !!}</b></h5>
                                </div>
                        
                                <div class="col-lg-4">
                                <h5 class="sub-header">Specialization: <b> {!! isset($data->specialization->name)?$data->specialization->name:'' !!}</b></h5>
                                </div>
                        
                             <div class="col-lg-4">
                                <h5 class="sub-header">Residential Type: <b> {!! isset($data->residentialtype->name)?$data->residentialtype->name:'' !!}</b></h5>
                            </div>
                         
                        <div class="col-lg-4">
                        <h5 class="sub-header">Property Ownership: <b> {!! isset($data->propertyownership->name)?$data->propertyownership->name:''; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Annual Profit: <b> {!! isset($data->annual_profit)?$data->annual_profit:0.00; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                        <h5 class="sub-header">Outstanding Loan: <b> {!! isset($data->outstanding_loan)?$data->outstanding_loan:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                        <h5 class="sub-header">Name of License Moneylender: <b> {!! isset($data->name_of_license_moneylender)?$data->name_of_license_moneylender:''; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Loan Amount: <b> {!! isset($data->loan_amount)?$data->loan_amount:0.00; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Payment Frequency: <b> {!! isset($data->payment_frequency)?$data->payment_frequency:0; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Bank Name: <b> {!! isset($data->bank_name)?$data->bank_name:''; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Account Number: <b> {!! isset($data->account_number)?$data->account_number:''; !!}</b></h5>
                        </div>

                        <?php 
                            if($data->status == 1)
                            {
                                ?>
                                     <div class="col-lg-4">
                                    <h5 class="sub-header">Loan Status: <b>Pending</b></h5>
                                    </div>
                                <?php
                            }
                            else if($data->status == 2)
                            {
                                ?>
                                     <div class="col-lg-4">
                                    <h5 class="sub-header">Loan Status: <b>Completed</b></h5>
                                    </div>
                                <?php
                            }
                        ?>               
                        
                </div>  
                    
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
@endsection