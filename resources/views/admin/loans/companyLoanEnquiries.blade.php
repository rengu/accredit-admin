@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Company Loans Enquries Listing')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php
if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
} else {
    $search = "";
}
if (isset($_REQUEST['from_date']) && !empty($_REQUEST['from_date'])) {
    $_REQUEST['from_date'];

    $from_date = date('d-m-y', strtotime($_REQUEST['from_date']));
} else {
    $from_date = "";
}
if (isset($_REQUEST['to_date']) && !empty($_REQUEST['to_date'])) {
    $to_date = date('d-m-y', strtotime($_REQUEST['to_date']));
} else {
    $to_date = "";
}
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Company Loans Enquries Listing</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">


                <div class="row">
                    

                        <section class="panel comm-listing-list">

                            <div class="panel-body">
                                {!! View::make('elements.actionMessage')->render() !!}
                                {!! Form::open(array('url' => 'admin/loans/companyLoanEnquiries', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>'row')) !!}
                                <div class="form-group col-md-4">
                                    {!! HTML::decode(Form::label('search', "Keyword <span class='require'></span>",array('class'=>"control-label"))) !!}
                                    <div>
                                        {!! Form::text('search', $search, array('class' => 'required form-control','placeholder'=>"Your Keyword")) !!}
                                        <span class="help-block">Search loans by typing their company name</span>
                                    </div>
                                </div>
                                
                                <div class="form-group form-actions col-md-4">
                                        {!! Form::submit('Search', array('class' => "btn btn-success")) !!}
                                        <a href="{!! url('admin/loans/companyLoanEnquiries') !!}" class="btn btn-default">Clear Filters</a>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </section>
                    
                   
                </div>
            </div>
            <!-- END Basic Form Elements Block -->
            <?php
            if (!$data->isEmpty()) {
                ?>

                {!! Form::open(array('url' => 'admin/loans/companyLoanEnquiries', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Company Loan Enquiries List</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('name', 'Name') !!}</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('email', 'Email') !!}</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('phone', 'Phone') !!}</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('loan_amount_requested', 'Loan Amount') !!}</th>
                                                <!-- <th>Approved Status</th>      
                                                <th>Active Status</th>
                                                <th>Payments</th>
                                                <th>Completed</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($data as $key=> $value) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                               <tr>
                                                    <td data-title="Name">
                                                        {!! isset($value->name)?$value->name:"" !!}
                                                    </td>
                                                    <td data-title="Email">
                                                        {!! isset($value->email)?$value->email:"" !!}
                                                    </td>
                                                    <td data-title="Phone">
                                                        {!! isset($value->phone)?$value->phone:"" !!}
                                                    </td>
                                                    
                                                    <td data-title="Created">
                                                      {!! isset($value->loan_amount_requested)?$value->loan_amount_requested:"" !!}  </td>

                                                      
                                                <!-- <td> -->
                                                   <?php 
                                                    // if($value->approved_status == 1)
                                                    // {
                                                    //     echo html_entity_decode(link_to('admin/loans/approved/' . $value->id.'?type=4', 'Pending', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Pending", 'onclick' => "return confirmAction('Approved this loan');")));
                                                    // }
                                                    // else if($value->approved_status == 2)
                                                    // {
                                                    //     echo html_entity_decode(link_to('admin/loans/unapproved/' . $value->id.'?type=4', 'Approved', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Active", 'onclick' => "return confirmAction('Un-Approved this loan');")));
                                                    // }
                                                    ?>
                                                <!-- </td> -->
                                                <!-- <td> -->
                                                    <?php 
                                                    // if($value->active_status == 1)
                                                    // {
                                                    //     echo html_entity_decode(link_to('admin/loans/active/' . $value->id.'?type=4', 'In-Active', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "In-Active", 'onclick' => "return confirmAction('Active this loan');")));
                                                    // }
                                                    // else if($value->active_status == 2)
                                                    // {
                                                    //     echo html_entity_decode(link_to('admin/loans/inactive/' . $value->id.'?type=4', 'Active', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Active", 'onclick' => "return confirmAction('In-Active this loan');")));
                                                    // }

                                                    ?>
                                                <!-- </td> -->
                                                <!-- <td> -->
                                                <?php 
                                                 // if($value->active_status == 2)
                                                 // {
                                                 //    echo html_entity_decode(link_to('admin/loans/payments/' . base64_encode($value->id).'?type=4', 'Payments', array('class' => 'btn btn-default btn-xs action-list', 'title' => "Payment")));
                                                 
                                                 // }
                                                
                                                ?>
                                            <!-- </td> -->

                                            <!-- <td> -->
                                            <?php 
                                            // if($value->is_completed == 0 && $value->active_status == 2)
                                            // {
                                            //     echo html_entity_decode(link_to('admin/loans/loanCompleted/' . $value->id.'?type=4', 'Make Completed', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Make Completed", 'onclick' => "return confirmAction('Completed this loan');")));
                                            // }
                                            // else if($value->is_completed == 1 && $value->active_status == 2)
                                            // {
                                            //     echo html_entity_decode(link_to('admin/loans/loanInCompleted/' . $value->id.'?type=4', 'Completed', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Completed", 'onclick' => "return confirmAction('In-Completed this loan');")));
                                            // }

                                            ?>
                                            <!-- </td> -->
                                                          
                                                    <td data-title="Action" class="admusers-grobtn">
                                                        <?php
                                                        echo html_entity_decode(link_to('admin/loans/cdelete/' . $value->id, '<i class="fa fa-trash-o"></i>', array('title' => 'Delete', 'class' => 'btn btn-danger btn-xs action-list delete-list', 'escape' => false, 'onclick' => "return confirmAction('delete');")));

                                                        echo html_entity_decode(link_to('admin/loans/viewCompany/' . $value->id, '<i class="fa fa-search"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'View')));
                                                        
                                                        ?>
                                                    </td>   
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {!! $data->appends(request()->except('page'))->links() !!}
                                </div>
                            </div>
                           <!--  <div class="panel-body">
                                <button type="button" name="chkRecordId" onclick="checkAll(true);"  class="btn btn-success">Select All</button>
                                <button type="button" name="chkRecordId" onclick="checkAll(false);" class="btn btn-success">Unselect All</button>
                                <?php
                                $arr = array(
                                    "" => "Action for selected...",
                                    'Activate' => "Activate",
                                    'Deactivate' => "Deactivate",
                                    'Delete' => "Delete",
                                );
                                //  echo form_dropdown("action", $arr, '', "class='small form-control' id='table-action'");
                                ?>
                                {!! Form::select('action', $arr, null, array('class'=>"small form-control btninspace",'id'=>'action')) !!}
                                {!! Form::hidden('search', $search, array('id' => '')) !!}

                                <button type="submit" class="small btn btn-success btn-cons" onclick=" return isAnySelect();" id="submit_action">Ok</button>
                            </div> -->
                        </section>
                    </div>
                </div>
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Company Loan Enquiries List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
        </div>
    </div>
</div>
@endsection