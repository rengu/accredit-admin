@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Loan Details')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/loans/individualLoanEnquiries', 'Individual Loans Enquries Listing', array('escape' => false,'class'=>"",'title'=>"Loan Applications Listings"))) !!}</li>
        <li>Individual Loan Enquiry Details</li>
    </ul>
    <!-- END Forms General Header -->
   <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Interactive Title -->
                <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>Individual Loan Enquiry Details</strong> 
                    <div class="addBtnSec">
                    <a href="{!! url('/admin/loans/individualLoanEnquiries') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                </div></h2>
                </div>
                <!-- END Interactive Title -->
                <div class="row">
                        <div class="col-lg-4">
                            <h5 class="sub-header">Name: <b> {!! isset($data->name)?$data->name:"" !!}</b></h5>
                        </div>
                        <div class="col-lg-4">
                            <h5 class="sub-header">Email: <b> {!! isset($data->email)?$data->email:"" !!}</b></h5>
                        </div>
                         
		                 <div class="col-md-4 col-sm-4">
                         <h5 class="sub-header">Phone: <b> {!! isset($data->phone)?$data->phone:"" !!}</b></h5> 
		                 </div> 

                        <div class="col-lg-4">
                        <h5 class="sub-header">Loan Type: <b> {!! isset($data->loantype->name)?$data->loantype->name:''; !!}</b></h5>
                        </div>
                   
                        <div class="col-lg-4">
                        <h5 class="sub-header">How Did I Get to Know Accredit: <b> {!! isset($data->knowaccrdit->name)?$data->knowaccrdit->name:''; !!}</b></h5>
                        </div>

                        <!-- <div class="col-lg-4">
                        <h5 class="sub-header">How did i get to know Accredit: <b> {!! isset($data->knowaccrdit->name)?$data->knowaccrdit->name:''; !!}</b></h5>
                        </div> -->

                        <div class="col-lg-4">
                        <h5 class="sub-header">Loan Amount Requested: <b> {!! isset($data->loan_amount_requested)?$data->loan_amount_requested:''; !!}</b></h5>
                        </div>

                  
                   
                        <div class="col-lg-4">
                        <h5 class="sub-header">Loan Purpose: <b> {!! isset($data->loanpurpose->name)?$data->loanpurpose->name:''; !!}</b></h5>
                        </div>

                        
                        <div class="col-lg-4">
                        <h5 class="sub-header">Reason For Loan: <b> {!! isset($data->reasonforloan->name)?$data->reasonforloan->name:$data->reason_for_loan; !!}</b></h5>
                        </div>
                           
                        <?php $maritalStatus = "";
                            if ($data->marital_status == 1) {
                                $maritalStatus = "Single";
                            } else if ($data->marital_status == 2) {
                                $maritalStatus = "Married";
                            } else if ($data->marital_status == 3) {
                                $maritalStatus = "Divorced";
                            } else if ($data->marital_status == 4) {
                                $maritalStatus = "Widowed";
                            }
                        ?>
                            <div class="col-lg-4">
                                <h5 class="sub-header">Marital Status: <b><?= $maritalStatus; ?></b></h5>
                            </div>

                            <?php
                          /*}
                          else if($data->marital_status == 2)
                          {*/
                            ?>

                                <!-- <div class="col-lg-4">
                                <h5 class="sub-header">Marital Staus: <b>Married</b></h5>
                                </div> -->
                            <?php
                          /*}
                          else
                          {*/
                            ?>
                              <!-- <div class="col-lg-4">
                                <h5 class="sub-header">Marital Staus: <b></b></h5>
                                </div> -->
                            <?php
                          // }
			    ?>

                                <div class="col-lg-4">
                                <h5 class="sub-header">Residential Address One: <b> {!! isset($data->residential_address_one)?$data->residential_address_one:''; !!}</b></h5>
                                </div>
                           
                                <div class="col-lg-4">
                                <h5 class="sub-header">Residential Address Two: <b> {!! isset($data->residential_address_two)?$data->residential_address_two:''; !!}</b></h5>
                                </div>
                           
                         
                                <div class="col-lg-4">
                                <h5 class="sub-header">Residential Type: <b> {!! isset($data->residentialtype->name)?$data->residentialtype->name:''; !!}</b></h5>
                                </div>
                          
                        
                                <div class="col-lg-4">
                                <h5 class="sub-header">Property Ownership: <b> {!! isset($data->propertyownership->name)?$data->propertyownership->name:''; !!}</b></h5>
                                </div>
                           
                        
                                <!-- <div class="col-lg-4">
                                <h5 class="sub-header">Residential Address Two: <b> {!! isset($data->residential_address_two)?$data->residential_address_two:'' !!}</b></h5>
                                </div> -->
                            

                         
                                <div class="col-lg-4">
                                <h5 class="sub-header">Length of Stay: <b> {!! isset($data->length_of_stay)?$data->length_of_stay:'' !!}</b></h5>
                                </div>
                           

                         <?php 
                          if(!empty($data->educationlevel->name))
                          {
                            ?>
                                <div class="col-lg-4">
                                <h5 class="sub-header">Education Level: <b> {!! $data->educationlevel->name !!}</b></h5>
                                </div>
                            <?php
                          }
                         ?>

                         
                                <div class="col-lg-4">
                                <h5 class="sub-header">Employment Status: <b> {!! isset($data->employmentstatus->name)?$data->employmentstatus->name:'' !!}</b></h5>
                                </div>
                           

                        
                                <div class="col-lg-4">
                                <h5 class="sub-header">Company Name: <b> {!! isset($data->company_name)?$data->company_name:'' !!}</b></h5>
                                </div>
                      

                       
                                <div class="col-lg-4">
                                <h5 class="sub-header">Job Title: <b> {!! isset($data->jobtitle->name)?$data->jobtitle->name:'' !!}</b></h5>
                                </div>
                          
                        
                                <div class="col-lg-4">
                                <h5 class="sub-header">Specialization: <b> {!! isset($data->specialization->name)?$data->specialization->name:'' !!}</b></h5>
                                </div>
                        
                       <div class="col-lg-4">
                                <h5 class="sub-header">Start Date of Employment: <b> {!! isset($data->start_date_of_employment)?$data->start_date_of_employment:'' !!}</b></h5>
                        </div>
                         
                        <div class="col-lg-4">
                        <h5 class="sub-header">Length of Current Employment: <b> {!! isset($data->length_of_current_employment)?$data->length_of_current_employment:''; !!}</b></h5>
                        </div>
                        
                         
                        <div class="col-lg-4">
                        <h5 class="sub-header">Salary Paid: <b> {!! isset($data->salary_paid)?$data->salary_paid:0; !!}</b></h5>                          
                        </div>

                         <div class="col-lg-4">
                            <h5 class="sub-header">Annul Income: <b> {!! isset($data->annual_income)?$data->annual_income:0.00 !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                            <h5 class="sub-header">Monthly Income: <b> {!! isset($data->monthly_income)?$data->monthly_income:0.00 !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                            <h5 class="sub-header">Pay Date of Salary: <b> {!! isset($data->pay_date_of_salary)?$data->pay_date_of_salary:''; !!}</b></h5>
                        </div>

                          <div class="col-lg-4">
                            <h5 class="sub-header">Office Address One: <b> {!! isset($data->office_address_one)?$data->office_address_one:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                            <h5 class="sub-header">Office Address Two: <b> {!! isset($data->office_address_two)?$data->office_address_two:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                            <h5 class="sub-header">Office Number: <b> {!! isset($data->office_number)?$data->office_number:''; !!}</b></h5>
                        </div>
                        
                         <div class="col-lg-4">
                            <h5 class="sub-header">Outstanding Loan: <b> {!! isset($data->outstanding_loan)?$data->outstanding_loan:''; !!}</b></h5>
                        </div>
                        
                         <div class="col-lg-4">
                            <h5 class="sub-header">Name of License Money Lender: <b> {!! isset($data->name_of_license_moneylender)?$data->name_of_license_moneylender:''; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                            <h5 class="sub-header">Loan Amount: <b> {!! isset($data->loan_amount)?$data->loan_amount:0.00; !!}</b></h5>
                        </div>

                          <div class="col-lg-4">
                            <h5 class="sub-header">Payment Frequency: <b> {!! isset($data->payment_frequency)?$data->payment_frequency:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                        <h5 class="sub-header">Bank Name: <b> {!! isset($data->bank_name)?$data->bank_name:''; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Account Number: <b> {!! isset($data->account_number)?$data->account_number:''; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Number of Dependents: <b> {!! isset($data->number_of_dependents)?$data->number_of_dependents:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                        <h5 class="sub-header">Next of Kin Name: <b> {!! isset($data->next_of_kin_name)?$data->next_of_kin_name:''; !!}</b></h5>
                        </div>

                        <div class="col-lg-4">
                        <h5 class="sub-header">Next of Kin Contact Number: <b> {!! isset($data->next_of_kin_contact_number)?$data->next_of_kin_contact_number:''; !!}</b></h5>
                        </div>

                         <div class="col-lg-4">
                        <h5 class="sub-header">Next of Kin Relationship: <b> {!! isset($data->next_of_kin_relationship)?$data->next_of_kin_relationship:''; !!}</b></h5>
                        </div>
                        <div class="col-lg-4">
                        <h5 class="sub-header">Office Branch: <b> {!! isset($office_branch_Name->name)?$office_branch_Name->name:''; !!}</b></h5>
                        </div>

                        <?php 
                            if(!empty($data->proof_of_income))
                            {
                                ?>  
                                    <div class="col-lg-4">
                                    <h5 class="sub-header">Proof of Income: <b> <img width="250" height="250"  src="<?php  echo SITE_URL. '/public/files/proofofincome/'.$data->proof_of_income; ?>" /></b></h5>
                                    </div>
                                <?php
                            }
                        ?>

                         <?php 
                            if(!empty($data->other_document))
                            {
                                ?>  
                                    <div class="col-lg-8">
                                    <h5 class="sub-header">Other Document: <b> <img width="250" height="250" src="<?php  echo SITE_URL. '/public/files/otherdocument/'.$data->other_document; ?>" /></b></h5>
                                    </div>
                                <?php
                            }
                        ?>

                          <?php 
                            if($data->status == 1)
                            {
                                ?>
                                     <div class="col-lg-8">
                                    <h5 class="sub-header">Loan Status: <b>Pending</b></h5>
                                    </div>
                                <?php
                            }
                            else if($data->status == 2)
                            {
                                ?>
                                     <div class="col-lg-4">
                                    <h5 class="sub-header">Loan Status: <b>Completed</b></h5>
                                    </div>
                                <?php
                            }
                        ?>      

                         
                            
                            
                </div>  
                    
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
@endsection
