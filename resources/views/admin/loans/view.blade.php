@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Loan Application Details')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/loans/index', 'Loan Applications Listings', array('escape' => false,'class'=>"",'title'=>"Loan Applications Listings"))) !!}</li>
        <li>Loan Application Details</li>
    </ul>
    <!-- END Forms General Header -->
   <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Interactive Title -->
                <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>Loan Detail</strong> 
                    <div class="addBtnSec">
                    <a href="{!! url('/admin/loans/index') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                </div></h2>
                </div>
                <!-- END Interactive Title -->
                <div class="row">
                        <div class="col-lg-6">
                            <h5 class="sub-header">Username: <b> {!! isset($loanData->user)?$loanData->user->name:"" !!}</b></h5>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="sub-header">Email: <b> {!! isset($loanData->user)?$loanData->user->email:"" !!}</b></h5>
                        </div>
                         
		                 <div class="col-md-4 col-sm-4">
		                            <div class="order-summary-admin">
		                                <h5>Order Summary</h5>
		                                {!! View::make('elements.actionMessage')->render() !!}
		                                {!! Form::model($loanData, array('url' => '/admin/loans/changestatus/'.$loanData->id, 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"")) !!}
		                                    <div class="form-group">
		                                        <label>Status</label>
		                                        <?php $orderStatusArray = array(
		                                                                    '1'=>'Pending',
		                                                                    '2'=>'Approved',
		                                                                    '3'=>'Cancelled',
		                                                                    );  ?>
		                                        {!! Form::select('status', $orderStatusArray, null, ['class' => '']) !!}
		                                        <p class="help-text"></p>
		                                    </div>
		                                    <div class="form-group">
		                                        <label>Comments</label>
		                                        {!! Form::textarea('comment', null, ['class' => '']) !!}
		                                    </div>
		                                    <button>Submit</button>
		                                {!! Form::close() !!}
		                            </div>
		                        </div>       
                </div>  
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Loan Repayment Listings</h3>
                            </header>
                            <div class="panel-body">
                                <?php if(!empty($loanRepaymentData)) { ?>
                                    <section id="no-more-tables">
                                        <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-condensed table-hover cf">
                                            <thead class="cf">
                                                <tr>
                                                    <th>Installment No.</th>
                                                    <th>Amount</th>
                                                    <th>Repayment Date</th>
                                                    <th style="width:180px; min-width:180px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i = 1;
                                                foreach ($loanRepaymentData as $data) {
                                                    if ($i % 2 == 0) {
                                                        $class = 'colr1';
                                                    } else {
                                                        $class = '';
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td data-title="Select">
                                                            {!! $i !!}
                                                        </td>
                                                        <td data-title="Name">
                                                            {!! CURR.number_format($data->repayment_amount,2) !!}
                                                        </td>
                                                        <td data-title="Name">
                                                            {!! date('m/d/Y', strtotime($data->repayment_date)) !!}
                                                        </td>
                                                        
                                                        <td data-title="Action" class="admusers-grobtn">
                                                            <?php
                                                            if ($data->status==1)
                                                                echo html_entity_decode('<a href=""><i class="fa fa-check"></i></a>');
                                                            else
                                                                echo html_entity_decode(link_to('admin/loans/receive-payment/' . $data->id, '<i class="fa fa-check"></i>', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Deactivate", 'onclick' => "return confirmAction('Payment Received');")));
                                                            ?>
                                                        </td>   
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                            </table>
                                    </section>
                                <?php } ?>
                            </div>
                        </section>
                    </div>
                </div>      
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
@endsection