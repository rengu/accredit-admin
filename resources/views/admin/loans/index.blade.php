@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Loans Listings')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php
if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $search = $_REQUEST['search'];
} else {
    $search = "";
}
if (isset($_REQUEST['from_date']) && !empty($_REQUEST['from_date'])) {
    $_REQUEST['from_date'];

    $from_date = date('d-m-y', strtotime($_REQUEST['from_date']));
} else {
    $from_date = "";
}
if (isset($_REQUEST['to_date']) && !empty($_REQUEST['to_date'])) {
    $to_date = date('d-m-y', strtotime($_REQUEST['to_date']));
} else {
    $to_date = "";
}
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Loan Applications Listings</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">


                <div class="row">
                    

                        <section class="panel comm-listing-list">

                            <div class="panel-body">
                                {!! View::make('elements.actionMessage')->render() !!}
                                {!! Form::open(array('url' => 'admin/loans/index', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>'row')) !!}
                                <div class="form-group col-md-4">
                                    {!! HTML::decode(Form::label('search', "Keyword <span class='require'></span>",array('class'=>"control-label"))) !!}
                                    <div>
                                        {!! Form::text('search', $search, array('class' => 'required form-control','placeholder'=>"Your Keyword")) !!}
                                        <span class="help-block">Search loans by typing their company name</span>
                                    </div>
                                </div>
                                
                                <div class="form-group form-actions col-md-4">
                                        {!! Form::submit('Search', array('class' => "btn btn-success")) !!}
                                        <a href="{!! url('admin/loans/index') !!}" class="btn btn-default">Clear Filters</a>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </section>
                    
                   
                </div>
            </div>
            <!-- END Basic Form Elements Block -->
            <?php
            if (!$loans->isEmpty()) {
                ?>

                {!! Form::open(array('url' => 'admin/loans/index', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Loan Applications List</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                                <th></th>
												<th>Username</th>
                                                <th>User Email</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('company_name', 'Company Name') !!}	</th>
                                                <th>{!! App\Commands\SortableTrait::link_to_sorting_action('created_at', 'Created') !!}	</th>
                                                <th style="width:180px; min-width:180px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($loans as $data) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                                <tr>
                                                    <td data-title="Select">
                                                        {!! Form::checkbox('id', $data->id,null, array("onclick"=>"javascript:isAllSelect(this.form);",'name'=>"chkRecordId[]")) !!}
                                                    </td>
                                                    <td data-title="Name">
                                                        {!! isset($data->user)?$data->user->name:"" !!}
                                                    </td>
													<td data-title="Name">
                                                        {!! isset($data->user)?$data->user->email:"" !!}
                                                    </td>
                                                    <td data-title="Name">
                                                        {!! $data->company_name !!}
                                                    </td>
                                                    
                                                    <td data-title="Created">
                                                        {!!  date("M d, Y h:i A", strtotime($data->created_at)) !!}</td>

                                                    <td data-title="Action" class="admusers-grobtn">
                                                        <?php
                                                        if (!$data->status)
                                                            echo html_entity_decode(link_to('admin/loans/activate/' . $data->slug, '<i class="fa fa-check"></i>', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Active", 'onclick' => "return confirmAction('activate');")));
                                                        else
                                                            echo html_entity_decode(link_to('admin/loans/deactivate/' . $data->slug, '<i class="fa fa-check"></i>', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "Deactivate", 'onclick' => "return confirmAction('deactivate');")));

                                                      //  echo html_entity_decode(link_to('admin/loans/edit/' . $data->slug, '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'Edit')));
                                                        echo html_entity_decode(link_to('admin/loans/delete/' . $data->slug, '<i class="fa fa-trash-o"></i>', array('title' => 'Delete', 'class' => 'btn btn-danger btn-xs action-list delete-list', 'escape' => false, 'onclick' => "return confirmAction('delete');")));

                                                        echo html_entity_decode(link_to('admin/loans/view/' . $data->id, '<i class="fa fa-search"></i>', array('class' => 'btn btn-primary btn-xs', 'title' => 'View')));
														
                                                        ?>
                                                    </td>	
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                    {!! $loans->appends(request()->except('page'))->links() !!}
                                </div>
                            </div>
                            <div class="panel-body">
                                <button type="button" name="chkRecordId" onclick="checkAll(true);"  class="btn btn-success">Select All</button>
                                <button type="button" name="chkRecordId" onclick="checkAll(false);" class="btn btn-success">Unselect All</button>
                                <?php
                                $arr = array(
                                    "" => "Action for selected...",
                                    'Activate' => "Activate",
                                    'Deactivate' => "Deactivate",
                                    'Delete' => "Delete",
                                );
                                //  echo form_dropdown("action", $arr, '', "class='small form-control' id='table-action'");
                                ?>
                                {!! Form::select('action', $arr, null, array('class'=>"small form-control btninspace",'id'=>'action')) !!}
                                {!! Form::hidden('search', $search, array('id' => '')) !!}

                                <button type="submit" class="small btn btn-success btn-cons" onclick=" return isAnySelect();" id="submit_action">Ok</button>
                            </div>
                        </section>
                    </div>
                </div>
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                loans List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
        </div>
    </div>
</div>
@endsection