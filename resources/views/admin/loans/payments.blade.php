@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Loan Installments')
@extends('admin.layouts.admin_dashboard')
@section('content')
<?php 
    use Illuminate\Http\Request;
    {{ $type = app('request')->input('type'); }};
   
?>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Loan Installments</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">

        <div class="col-md-12">
           <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>Document Details</strong> 
                    <div class="addBtnSec">
                    <?php 
                     if($type == 3)
                     {
                        ?>  
                          <a href="{!! url('/admin/loans/individualLoanEnquiries') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                        <?php
                     }
                     else if($type == 4)
                     {
                        ?>
                             <a href="{!! url('/admin/loans/companyLoanEnquiries') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                        <?php
                     }
                    ?>
                    
                </div></h2>
                </div>
            <?php
            if (!$data->isEmpty()) {

                ?>

                {!! Form::open(array('url' => 'admin/loans/individualLoanEnquiries', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-inline form")) !!}
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading compage-heading">
                               <h3 class="mt-0">Loan Applications List</h3>
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed table-hover cf">
                                        <thead class="cf">
                                            <tr>
                                               
												<th>Installment Name</th>
                                                <th>Due Date</th>
                                                <th>Principal Amount</th>
                                                <th>Interest Amount</th>
                                                <th>EMI Amount</th>
                                                <th>Late Fee</th>
                                                <th>Status</th>      
                                                <th>Make Completed</th>                                 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($data as $key=> $value) {
                                                if ($i % 2 == 0) {
                                                    $class = 'colr1';
                                                } else {
                                                    $class = '';
                                                }
                                                ?>
                                                <tr>
                                                    
                                                    <td data-title="Name">
                                                        {!! isset($value->installment_name)?$value->installment_name:"" !!}
                                                    </td>
                                                     <td data-title="Due Date">
                                                        {!! isset($value->due_date)?$value->due_date:"" !!}
                                                    </td>
													<td data-title="Principal Amount">
                                                        {!! isset($value->principal_amount)?$value->principal_amount:"" !!}
                                                    </td>
                                                    <td data-title="Interest Amount">
                                                        {!! isset($value->interest_amount)?$value->interest_amount:"" !!}
                                                    </td>
                                                    <td data-title="EMI Amount">
                                                        {!! isset($value->total_emi)?$value->total_emi:"" !!}
                                                    </td>
                                                <td>
                                                    {!! $value->late_fee !!}
                                                </td>
                                                      
                                                <td>
                                                   <?php 
                                                    if($value->status == 1 && $value->is_completed == 0)
                                                    {                      
                                                      ?>
                                                        <span style="color:blue">Pending</span>
                                                      <?php  
                                                    }
                                                    else if($value->status == 2 && $value->is_completed == 0)
                                                    {
                                                        ?>
                                                        <span style="color:red">OverDue</span>
                                                      <?php 
                                                    }
                                                    else
                                                    {
                                                        ?>
                                                         <span style="color:green">Completed</span>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                
                                                <td>
                                                    <?php 
                                                    if($value->is_completed == 0)
                                                    {
                                                        echo html_entity_decode(link_to('admin/loans/completed/' . $value->id.'?type='.$value->loan_type, 'In-Completed', array('class' => 'btn btn-danger btn-xs action-list', 'title' => "In-Completed", 'onclick' => "return confirmAction('Completed this installment');")));
                                                    }
                                                    else if($value->is_completed == 1)
                                                    {
                                                        echo html_entity_decode(link_to('admin/loans/incompleted/' . $value->id.'?type='.$value->loan_type, 'Completed', array('class' => 'btn btn-success btn-xs action-list', 'title' => "Completed", 'onclick' => "return confirmAction('In-Completed this installment');")));
                                                    }

                                                    ?>
                                                </td>                    
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </table>
                                </section>
                            </div>
                        </section>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <div class="panel-body border-bottom">
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                   
                                </div>
                            </div>
                            <div class="panel-body">
                                <button type="button" name="chkRecordId" onclick="checkAll(true);"  class="btn btn-success">Select All</button>
                                <button type="button" name="chkRecordId" onclick="checkAll(false);" class="btn btn-success">Unselect All</button>
                                <?php
                                $arr = array(
                                    "" => "Action for selected...",
                                    'Activate' => "Activate",
                                    'Deactivate' => "Deactivate",
                                    'Delete' => "Delete",
                                );
                                //  echo form_dropdown("action", $arr, '', "class='small form-control' id='table-action'");
                                ?>
                                {!! Form::select('action', $arr, null, array('class'=>"small form-control btninspace",'id'=>'action')) !!}
                                {!! Form::hidden('search', '', array('id' => '')) !!}

                                <button type="submit" class="small btn btn-success btn-cons" onclick=" return isAnySelect();" id="submit_action">Ok</button>
                            </div>
                        </section>
                    </div>
                </div>
                {!! Form::close() !!} 

            <?php } else {
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Individual Loan Enquiries List
                            </header>
                            <div class="panel-body">
                                <section id="no-more-tables">No Record Found.</section>
                            </div>
                        </section>
                    </div>
                </div>  
            <?php }
            ?>
        </div>
    </div>
</div>
@endsection