<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>@yield('title')</title>

        <meta name="description" content="{{ SITE_TITLE }}">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{!! asset('public/admin/images/favicon.png') !!}">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon57.png') }}" sizes="57x57">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon72.png') }}" sizes="72x72">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon76.png') }}" sizes="76x76">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon114.png') }}" sizes="114x114">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon120.png') }}" sizes="120x120">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon144.png') }}" sizes="144x144">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon152.png') }}" sizes="152x152">
        <link rel="apple-touch-icon" href="{{ asset('public/admin/images/icon180.png') }}" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{ URL::asset('public/admin/css/bootstrap.min.css') }}">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{ URL::asset('public/admin/css/plugins.css') }}">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{ URL::asset('public/admin/css/main.css') }}">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{ URL::asset('public/admin/css/themes.css') }}">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="{{ URL::asset('public/admin/js/vendor/modernizr-respond.min.js') }}"></script>
        
        <script src="{{ URL::asset('public/admin/js/vendor/jquery-1.11.3.min.js') }}"></script>
    </head>
    <body>
        
        <img src="{{ asset('public/admin/images/placeholders/backgrounds/login_full_bg.jpg') }}" alt="Login Full Background" class="full-bg animation-pulseSlow">
        <!-- Set admin login view content -->
        @yield('content')
        <!-- End admin login view content -->

      
        <!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
        
        <script src="{{ URL::asset('public/admin/js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('public/admin/js/plugins.js') }}"></script>
        <script src="{{ URL::asset('public/admin/js/app.js') }}"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="{{ URL::asset('public/admin/js/pages/login.js') }}"></script>
        <script>$(function(){ Login.init(); });</script>
    </body>
</html>