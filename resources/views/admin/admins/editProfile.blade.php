@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Edit Profile')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{{ URL::asset('public/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $.validator.addMethod("contact", function (value, element) {
        return  this.optional(element) || (/^[0-9-]+$/.test(value));
    }, "Contact Number is not valid.");
    $("#myform").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Edit Profile</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Edit User</strong> </h2>
                </div>
                <!-- END Form Elements Title -->
                {!! View::make('elements.actionMessage')->render() !!}
                <!-- Basic Form Elements Content -->
                <span class="require_sign">Please note that all fields that have an asterisk (*) are required. </span>
                    <?php echo Form::model($detail, ['url' => ['/admin/editProfile'], 'id' => 'myform', 'class' => 'form-horizontal form-bordered'], array('method' => 'post', 'id' => 'adminAdd')); ?>
                             {!! View::make('elements.actionMessage')->render() !!}
                       <div class="form-group">
                            {!! HTML::decode(Form::label('name', "Name <span class='require'>*</span>",array('class'=>"control-label col-lg-2"))) !!}
                            <div class="col-lg-6">
                                {!! Form::text('name', Input::old('name'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            {!! HTML::decode(Form::label('username', "Username <span class='require'>*</span>",array('class'=>"control-label col-lg-2"))) !!}
                            <div class="col-lg-6">
                                {!! Form::text('username', Input::old('username'), array('class' => 'required form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! HTML::decode(Form::label('email', "Email Address <span class='require'>*</span>",array('class'=>"control-label col-lg-2"))) !!}
                            <div class="col-lg-6">
                                {!! Form::text('email', Input::old('email'), array('class' => 'required email form-control')) !!}
                            </div>
                        </div>


                       <div class="form-group">
                            {!! HTML::decode(Form::label('contact', "Contact Number <span class='require'>*</span>",array('class'=>"control-label col-lg-2"))) !!}
                            <div class="col-lg-6">
                                {!! Form::text('phone',Input::old('phone'), array('class' => 'required number form-control','maxlength'=>'16'))!!}
                            </div>
                        </div>
                        <div class="form-group">
                       
                            {!! HTML::decode(Form::label('admin_typ_id', "Role <span class='require'>*</span>",array('class'=>"control-label col-lg-2"))) !!}
                            <div class="col-md-6">
                                
                               {!! Form::select('admin_type_id', ['' => 'Please Select']+ $roles,$detail->admin_type_id, array('class' => 'form-control'))!!}
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-lg-offset-2 col-lg-10">
                                {!! Form::submit('Update', array('class' => "btn btn-danger")) !!}
                                {!! html_entity_decode(link_to("/admin/users/index","Cancel", array("class"=>"btn btn-default"))) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                <!-- END Basic Form Elements Content -->
            </div>

            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>

@endsection