@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Admin Dashboard')
@extends('admin.layouts.admin_dashboard')
@section('content')
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-sm">
                    <h1>Welcome <strong>Admin</strong><br></h1>
                </div>
                <!-- END Main Title -->

            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="{{ asset('public/admin/images/placeholders/headers/dashboard_header.jpg') }}" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row widget-dashboard">
        {!! View::make('elements.actionMessage')->render() !!}
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
	    <a href="<?php echo URL::to('admin/users/index'); ?>" class="widget widget-hover-effect1">
	        <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3 class="widget-content text-center animation-pullDown">
                        <strong><?php echo $totalUsers; ?></strong><br>
                        <small>Users</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>

         <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="<?php echo URL::to('admin/users/index/1'); // where 1 is role_id ?>" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3 class="widget-content text-center animation-pullDown">
                        <strong><?php echo $totalIndividualUsers; ?></strong><br>
                        <small>Total Individual Users</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>

         <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="<?php echo URL::to('admin/users/index/2'); // where 2 is role_id ?>" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3 class="widget-content text-center animation-pullDown">
                        <strong><?php echo $totalCompanyUsers; ?></strong><br>
                        <small>Total Company Users</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="#" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3 class="widget-content text-center animation-pullDown">
                        <strong><?php echo $totalActiveLoan; ?></strong><br>
                        <small>Total Approved Loan</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>

        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="#" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3 class="widget-content text-center animation-pullDown">
                        <strong><?php echo $totalApprovedLoan; ?></strong><br>
                        <small>Total Active Loan</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
       
       
        
        
    <!-- END Mini Top Stats Row -->

   
</div>

@stop
