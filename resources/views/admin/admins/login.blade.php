@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Admin Login')
@extends('admin.layouts.admin_login')
@section('content')

<!-- Login Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->

<!-- END Login Full Background -->

<script src="{{ URL::asset('public/admin/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#login-form").validate();
    $("#password-recovery").validate();
    $("#link-reminder-login-form").click(function () {
        $("#login-form").hide();
        $("#form-reminder").show();
    })
    $("#link-reminder").click(function () {
        $("#form-reminder").hide();
        $("#login-form").show();
    })
<?php if (Session::get('is_forgot') == 1) { ?>
        $("#login-form").hide();
        $("#form-reminder").show();
<?php } ?>
});
function refreshCaptcha()
{

    /*var img = document.images['captchaimg'];
    var img_reset = document.images['captchaimg_reset'];
    img_reset.src = img.src = img.src.substring(0, img.src.lastIndexOf("?")) + "?rand=" + Math.random() * 1000;*/

}
</script>
<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
    <!-- Login Title -->
    <div class="login-title text-center">
        <!-- <img src="<?php echo HTTP_PATH. "public/admin/images/icon32.png" ; ?>" alt="Airtel" > -->
       <h1 style="color:black">Accredit <br><small>Administrator Login</strong></h1>
    </div>
    <!-- END Login Title -->

    <!-- Login Block -->
    <div class="block push-bit">
        {!! view('elements.actionMessage')->render() !!}
        <!-- Login Form -->
        <div class="login-form">
            <?php echo Form::open(array('url' => 'admin/login', 'method' => 'post', 'id' => 'login-form', 'class' => 'form-horizontal form-bordered form-control-borderless')); ?>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        <?php echo Form::text('username', null, array('id' => 'login', 'autofocus' => true, 'class' => "required form-control input-lg", 'placeholder' => 'Username')); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                        <?php echo Form::password('password', array('id' => 'pass', 'class' => "required form-control", 'placeholder' => 'Password', 'type' => "password")); ?>
                    </div>
                </div>
            </div>
            <?php
            // check captcha code here
          /*  if (Session::has('captcha')) {
                $class = "";
            } else {
                $class = "captcha_show";
            }*/
            ?>
            <!-- <div class="<?php //echo $class; ?> captcha-section">
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"></span>
                            <img src="<?php //echo HTTP_PATH; ?>captcha?rand=<?php //echo rand(); ?>" id='captchaimg' >
                            <a href='javascript: refreshCaptcha();'>
                                <img src="{{ URL::asset('public/admin/images') }}/reload-arrow.png" width="35" height="35" alt="">
                            </a>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                            <?php
                            //echo Form::text('captcha', null, array('autocomplete'=>'off', 'class' => "required form-control", 'placeholder' => 'Type security code shown above'));
                            ?>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="form-group form-actions">

                <div class="col-xs-12 text-center">
                    <?php echo Form::button("<i class='fa fa-angle-right'></i> Login to Dashboard", array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <a href="javascript:void(0)" id="link-reminder-login-form"><small>Forgot password?</small></a>
                </div>
            </div>
            <?php echo Form::close(); ?>
        </div>
        <!-- END Login Form -->

        <!-- Reminder Form -->
        <div id="form-reminder" style="display:none;">
            <?php echo Form::open(array('url' => 'admin/forgotPassword', 'method' => 'post', 'id' => 'password-recovery', 'class' => 'form-horizontal form-bordered form-control-borderless')); ?>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        <?php echo Form::text('email', null, array('id' => 'emailid', 'autofocus' => true, 'class' => "required email form-control input-lg", 'placeholder' => 'Email Address')); ?>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-12 text-right">
                    <?php echo Form::button("<i class='fa fa-angle-right'></i> Reset Password", array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
                </div>
            </div>
            <?php echo Form::close(); ?>
        </div>
        <!-- END Reminder Form -->

    </div>
    <!-- END Login Block -->
</div>
<!-- END Login Container -->

@stop