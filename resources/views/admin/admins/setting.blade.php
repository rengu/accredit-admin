    @section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Setting')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{{ URL::asset('public/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $.validator.addMethod("contact", function (value, element) {
        return  this.optional(element) || (/^[0-9-]+$/.test(value));
    }, "Contact Number is not valid.");
    $("#myform").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Setting</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Setting</strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
          

                <?php echo Form::model($detail, ['url' => ['/admin/setting'], 'id' => 'myform', 'class' => 'form-horizontal form-bordered'], array('method' => 'post', 'id' => 'setting')); ?>
                {!! View::make('elements.actionMessage')->render() !!}
                <div class="form-group ">
                <div class="col-md-3 col-lg-2">
                    <label for="opassword" class="control-label">Config Email <span class="require">*</span></label>
                </div>
                    <div class="col-md-6">

                         <div class="col-md-6">
                                            {!! Form::text('send_from', Input::old('send_from'), array('id' => 'send_from', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'Configration Email','autocomplete'=>'off')) !!}
                                        </div>

                        <?php
                      //  echo Form::text('email_from', array('id' => 'email_from', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'Configration Email','autocomplete'=>'off'));
                        ?>
                    </div>
                </div>
                
             <div class="form-group ">
                 <div class="col-md-3 col-lg-2">
                        {!! HTML::decode(Form::label('Individual Loan', "Individual Loan <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                    </div>
                    <div class="col-md-6">
                        <div class="raio">
                        {!! Form::radio('Individual', '1', true, array('id'=>'Enable')); !!}
                            <label>Enable</label>
                        </div>
                        <div class="raio">
                            {!! Form::radio('Individual', '0', false,array('id'=>'Disable')); !!}
                            <label>Disable</label>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                 <div class="col-md-3 col-lg-2">
                        {!! HTML::decode(Form::label('Company Loan', "Company Loan <span class='require'>*</span>",array('class'=>"control-label"))) !!}
                    </div>
                    <div class="col-md-6">
                        <div class="raio">
                        {!! Form::radio('Company', '1', true, array('id'=>'Enable')); !!}
                            <label>Enable</label>
                        </div>
                        <div class="raio">
                            {!! Form::radio('Company', '0', false,array('id'=>'Disable')); !!}
                            <label>Disable</label>
                        </div>
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <?php echo Form::button('<i class="fa fa-angle-right"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>
                        {!! html_entity_decode(link_to('/admin/dashboard', 'Cancel', array('escape' => false,'class'=>"btn btn-default"))) !!}
                    </div>
                </div>
                 <?php echo Form::close(); ?>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>

    
</div>
@endsection