<?php 
    $routeArray = app('request')->route()->getAction();
    $controllerAction = class_basename($routeArray['controller']);
    list($controller, $action) = explode('Controller@', $controllerAction);
     
?>
@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Change Password')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{{ URL::asset('public/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $.validator.addMethod("contact", function (value, element) {
        return  this.optional(element) || (/^[0-9-]+$/.test(value));
    }, "Contact Number is not valid.");
    $("#myform").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>Change Password</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row admedit-profile">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Change Password</strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <?php echo Form::open(array('url' => 'admin/changePassword', 'method' => 'post', 'id' => 'myform', 'class' => 'form-horizontal form-bordered')); ?>
                {!! View::make('elements.actionMessage')->render() !!}
                <div class="form-group ">
                <div class="col-md-3 col-lg-2">
                    <label for="opassword" class="control-label">Old Password  <span class="require">*</span></label>
                </div>
                    <div class="col-md-6">
                        <?php
                        echo Form::password('opassword', array('id' => 'opassword', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'Old Password'));
                        ?>
                    </div>
                </div>
                <div class="form-group ">
                <div class="col-md-3 col-lg-2">
                    <label for="password" class="control-label">Password <span class="require">*</span></label>
                </div>
                    <div class="col-md-6">
                        <?php
                        echo Form::password('password', array('id' => 'password', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'Password'));
                        ?>
                    </div>
                </div>
                <div class="form-group ">
                <div class="col-md-3 col-lg-2">
                    <label for="cpassword" class="control-label">Confirm Password <span class="require">*</span></label>
                </div>
                    <div class="col-md-6">
                        <?php
                        echo Form::password('cpassword', array('id' => 'cpassword', 'equalTo' => '#password', 'autofocus' => true, 'class' => "required form-control", 'placeholder' => 'Confirm Password'));
                        ?>
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <?php echo Form::button('<i class="fa fa-angle-right"></i> Submit', array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>
                        {!! html_entity_decode(link_to('/admin/dashboard', 'Cancel', array('escape' => false,'class'=>"btn btn-default"))) !!}
                    </div>
                </div>
                 <?php echo Form::close(); ?>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>

    
</div>
@endsection