@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Admin Reset Password')
@extends('admin.layouts.admin_login')
@section('content')

<!-- Login Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->

<!-- END Login Full Background -->

<script src="{{ URL::asset('public/admin/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#login-form").validate();
    
});

</script>
<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
    <!-- Login Title -->
    <div class="login-title text-center">
        <h1><i class="gi gi-flash"></i> <strong>{{ SITE_TITLE }}</strong><br><small><strong>Administrator Reset Password</strong></small></h1>
    </div>
    <!-- END Login Title -->

    <!-- Login Block -->
    <div class="block push-bit">
        {!! view('elements.actionMessage')->render() !!}
        <!-- Login Form -->
        <div class="login-form">
            <?php echo Form::open(array('url' => 'admin/reset-password-create', 'method' => 'post', 'id' => 'login-form', 'class' => 'form-horizontal form-bordered form-control-borderless')); ?>
            {!! Form::hidden('id', $id, array('id' => 'hidden')) !!}
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                         {!! Form::password('password',  array('class'=>'required form-control input-lg','minLength'=>8,'placeholder'=>"New Password",'id'=>'password')); !!}
                    </div>
                   
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                     <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                          {!! Form::password('cpassword',  array('class'=>'required form-control input-lg','placeholder'=>"Confirm Password",'equalTo'=>'#password')); !!}
                    </div>
                </div>
            </div>
          
         
            <div class="form-group form-actions">

                <div class="col-xs-8 text-right">
                    <?php echo Form::button("<i class='fa fa-angle-right'></i> Submit", array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>

                    
                </div>

            </div>
            <div class="form-group form-actions">
                <div class="col-xs-8 text-right">
            <div class="help-tx">Remembered Password? click on <a href="{!! url('admin/login') !!}">login here</a></div>
        </div>
        </div>
           
            <?php echo Form::close(); ?>
        </div>
        <!-- END Login Form -->

        <!-- Reminder Form -->
        <div id="form-reminder" style="display:none;">
            <?php echo Form::open(array('url' => 'admin/forgotPassword', 'method' => 'post', 'id' => 'password-recovery', 'class' => 'form-horizontal form-bordered form-control-borderless')); ?>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                        <?php echo Form::text('email', null, array('id' => 'emailid', 'autofocus' => true, 'class' => "required email form-control input-lg", 'placeholder' => 'Email Address')); ?>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-12 text-right">
                    <?php echo Form::button("<i class='fa fa-angle-right'></i> Reset Password", array('type' => 'submit', 'class' => 'btn btn-sm btn-primary')); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
                </div>
            </div>
            <?php echo Form::close(); ?>
        </div>
        <!-- END Reminder Form -->

    </div>
    <!-- END Login Block -->
</div>
<!-- END Login Container -->

@stop