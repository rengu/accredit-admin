<!--BEGIN SIDEBAR MENU-->
<nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;"
        data-position="right" class="navbar-default navbar-static-side">
    <div class="sidebar-collapse menu-scroll">
        <ul id="side-menu" class="nav">
            
             <div class="clearfix"></div>
            <li class="active"><a href="<?php echo action('Admin\HomeController@index'); ?>"><i class="fa fa-tachometer fa-fw">
                <div class="icon-bg bg-orange"></div>
            </i><span class="menu-title">Dashboard</span></a></li>
            <li class="active"><a href="<?php echo action('Admin\ServiceController@index'); ?>"><i class="fa fa-cubes fa-fw">
                <div class="icon-bg bg-orange"></div>
            </i><span class="menu-title">Services</span></a></li>
        </ul>
    </div>
</nav>
    <!--END SIDEBAR MENU-->