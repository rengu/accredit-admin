@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'Upload Offer')
@extends('admin.layouts.admin_dashboard')
@section('content')
<script src="{{ URL::asset('public/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#adminAdd").validate();
});
</script>
<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/users/index', 'Users', array('escape' => false,'class'=>"","title"=>"User Listings"))) !!}</li>
        <li>Upload Users Details</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong>Upload Users Details</strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                
                {!! View::make('elements.actionMessage')->render() !!}
                <div class="form-group">
                    <div class="col-lg-12">
                        <span class="require_sign">Please note that read below points before upload xls. </span>
                        <span class="help-block">Only use download excel format.</span>
                        <span class="help-block">Do not change header row of Excel.</span>
                        <span class="help-block">For multiple site id, please use comma seperated site id like(site1,site2).</span>
                        <span class="help-block">Username and Email address will be unique in Excel.</span>
                        <span class="help-block">For fill user type, use numeric value  
                            <?php
                            if(!empty($userTypeArr)) {
                                $userTypeArrSet = array();
                                foreach($userTypeArr as $data)  {
                                    $userTypeArrSet[] = $data->id."=".$data->name;
                                }
                                echo $userTData = "(".implode(', ', $userTypeArrSet).")";

                            } else {
                                echo "()";
                            }

                             ?></span>
                            
                    </div>
                </div>
                
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                       <a class="btn btn-primary" href="{!! url('admin/users/download-sample-excel') !!}">Download Sample Excel</a>
                    </div>
                </div>
                 
                <!-- END Basic Form Elements Content -->

            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
                    <h2><strong></strong> </h2>
                </div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                {!! Form::open(array('url' => 'admin/users/upload-users', 'method' => 'post', 'id' => 'adminAdd', 'files' => true,'class'=>"form-horizontal form-bordered")) !!}
                {!! View::make('elements.actionMessage')->render() !!}
                <div class="form-group">
                    <label class="col-md-3 control-label col-lg-2">Upload xls or xlsx file <span class="require">*</span></label>
                    <div class="col-lg-6">
                        <?php echo Form::file('document', array('id' => 'document', 'autofocus' => true, 'class' => "required", 'placeholder' => '')); ?>
                        <span class="help-block">Supported File Types: xls,xlsx</span>
                    </div>
                </div>
                
                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        {!! Form::submit('Update', array('class' => "btn btn-danger",'onclick' => 'return documentValidation();')) !!}
                        {!! html_entity_decode(link_to('/admin/users/index', 'Cancel', array('escape' => false,'class'=>"btn btn-default"))) !!}
                    </div>
                </div>
                 <?php echo Form::close(); ?>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
<script>
    function in_array(needle, haystack) {
        for (var i = 0, j = haystack.length; i < j; i++) {
            if (needle == haystack[i])
                return true;
        }
        return false;
    }

    function getExt(filename) {
        var dot_pos = filename.lastIndexOf(".");
        if (dot_pos == -1)
            return "";
        return filename.substr(dot_pos + 1).toLowerCase();
    }



    function documentValidation() {
        var filename = document.getElementById("document").value;

        var filetype = ['xls','xlsx'];
        if (filename != '') {
            var ext = getExt(filename);
            ext = ext.toLowerCase();
            var checktype = in_array(ext, filetype);
            if (!checktype) {
                alert(ext + " file not allowed for document.");
                document.getElementById("document").value = "";
                return false;
            } else {
                var fi = document.getElementById('document');
                var filesize = fi.files[0].size;
                // if (filesize > 2097152) {
                //     alert('Maximum 2MB file size allowed for document.');
                //     document.getElementById("document").value = "";
                //     return false;
                // }
            }
        }
        return true;
    }

</script>
@endsection