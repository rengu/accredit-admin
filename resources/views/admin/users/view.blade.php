@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'User Details')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/users/index', 'Application User Listings', array('escape' => false,'class'=>"",'title'=>"Loan Applications Listings"))) !!}</li>
        <li>User Details</li>
    </ul>
    <!-- END Forms General Header -->
   <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Interactive Title -->
                <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>User Details</strong> 
                    <div class="addBtnSec">
                    <a href="{!! url('/admin/users/index') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Back
                        </button>
                    </a>    
                </div></h2>
                </div>
                <!-- END Interactive Title -->
                <div class="row">
                    <?php 
                        if($data->role_id == 1)
                        {
                            ?>
                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">User Type: <b> Individual User</b></h5> 
                             </div>

			    <?php $salution = "";
                                if ($data->salution == 1) {
                                    $salution = "Mr.";
                                } else if ($data->marital_status == 2) {
                                    $salution = "Mrs.";
                                } else if ($data->marital_status == 3) {
                                    $salution = "Ms.";
                                } else if ($data->marital_status == 4) {
                                    $salution = "Miss.";
                                }
                            ?>
                            <div class="col-lg-4">
                                <h5 class="sub-header">Salution: <b><?= $salution; ?></b></h5>
                            </div>
			    
			    <div class="col-lg-4">
                            <h5 class="sub-header">Name: <b> {!! isset($data->name)?$data->name:"" !!}</b></h5>
                            </div>

                            <div class="col-lg-4">
                                <h5 class="sub-header">Email: <b> {!! isset($data->email)?$data->email:"" !!}</b></h5>
                            </div>
                         
                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Phone No.: <b> {!! isset($data->phone)?$data->phone:"" !!}</b></h5> 
                             </div>

                            

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Id Type.: <b> {!! isset($data->idtype->name)?$data->idtype->name:"" !!}</b></h5> 
                             </div>

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Nationality: <b> {!! isset($data->nationality)?$data->nationality:"" !!}</b></h5> 
                             </div> 

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Id Number: <b> {!! isset($data->id_number)?$data->id_number:"" !!}</b></h5> 
                             </div> 

                             <?php 
                             if($data->gender == 1)
                             {
                                ?>
                                     <div class="col-md-4 col-sm-4">
                                 <h5 class="sub-header">Gender: <b> Male</b></h5> 
                                 </div> 
                                <?php
                             }
                             else if($data->gender == 2)
                             {
                                ?>
                                <div class="col-md-4 col-sm-4">
                                 <h5 class="sub-header">Gender: <b> Female</b></h5> 
                                 </div> 
                                <?php
                             }
                             else
                             {
                                ?>
                                <div class="col-md-4 col-sm-4">
                                 <h5 class="sub-header">Gender: <b> </b></h5> 
                                 </div> 
                                <?php
                             }
                             ?>

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Date Of Birth: <b> {!! isset($data->dob)?$data->dob:"" !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Date Of Birth: <b> {!! isset($data->dob)?$data->dob:"" !!}</b></h5> 
                             </div>

                        <?php 
                          if($data->marital_status == 1)
                          {
                            ?>
                                <div class="col-lg-4">
                                <h5 class="sub-header">Marital Staus: <b> Not Married</b></h5>
                                </div>
                            <?php
                          }
                          else if($data->marital_status == 2)
                          {
                            ?>

                                <div class="col-lg-4">
                                <h5 class="sub-header">Marital Staus: <b>Married</b></h5>
                                </div>
                            <?php
                          }
                          else
                          {
                            ?>
                                  <div class="col-lg-4">
                                <h5 class="sub-header">Marital Staus: <b></b></h5>
                                </div>
                            <?php
                          }
                         ?>

                           <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Residential Address One: <b> {!! isset($data->residential_address_one)?$data->residential_address_one:"" !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Residential Address Two: <b> {!! isset($data->residential_address_two)?$data->residential_address_two:"" !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Home Number: <b> {!! isset($data->home_number)?$data->home_number:"" !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Office Number: <b> {!! isset($data->office_number)?$data->office_number:"" !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Education Level: <b> {!! isset($data->educationlevel->name)?$data->educationlevel->name:"" !!}</b></h5> 
                             </div>

                            <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Monthly Income: <b> {!! isset($data->monthly_income)?$data->monthly_income:0.00 !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Loan Amount: <b> {!! isset($data->loan_amount)?$data->loan_amount:0.00 !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Years Of Stay: <b> {!! isset($data->years_of_stay)?$data->years_of_stay:0; !!}</b></h5> 
                             </div>
                           


                            <?php
                        }
                        else
                        {
                            ?>
                            <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">User Type: <b> Company User</b></h5> 
                             </div>

                            
                            <div class="col-lg-4">
                            <h5 class="sub-header">Name: <b> {!! isset($data->name)?$data->name:"" !!}</b></h5>
                            </div>

                            <div class="col-lg-4">
                                <h5 class="sub-header">Email: <b> {!! isset($data->email)?$data->email:"" !!}</b></h5>
                            </div>
                         
                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Phone No.: <b> {!! isset($data->phone)?$data->phone:"" !!}</b></h5> 
                             </div>

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">UEN Number: <b> {!! isset($data->uen_no)?$data->uen_no:"" !!}</b></h5> 
                             </div>

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Business Type: <b> {!! isset($data->businesstype->name)?$data->businesstype->name:"" !!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Paid Up Capital: <b> {!! isset($data->paid_up_capital)?$data->paid_up_capital:0.00 !!}</b></h5> 
                             </div>

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Date Of Registration: <b> {!! isset($data->date_of_registration)?$data->date_of_registration:'';!!}</b></h5> 
                             </div>

                               <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Registred Address One: <b> {!! isset($data->registered_office_one)?$data->registered_office_one:'';!!}</b></h5> 
                            </div>

                                <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Registred Address Two: <b> {!! isset($data->registered_office_two)?$data->registered_office_two:'';!!}</b></h5> 
                             </div>

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Business Address One: <b> {!! isset($data->business_address_one)?$data->business_address_one:'';!!}</b></h5> 
                             </div>

                             <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Business Address Two: <b> {!! isset($data->business_address_two)?$data->business_address_two:'';!!}</b></h5> 
                             </div>

                              <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Office Number: <b> {!! isset($data->office_number)?$data->office_number:'';!!}</b></h5> 
                             </div>

                               <div class="col-md-4 col-sm-4">
                             <h5 class="sub-header">Contact Person: <b> {!! isset($data->contact_person)?$data->contact_person:'';!!}</b></h5> 
                             </div>


                            <?php
                        }
                        ?> 
                        
                </div>  
                    
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
@endsection
