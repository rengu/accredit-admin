@section('title', 'Administrator :: '.TITLE_FOR_PAGES.'QR Code Details')
@extends('admin.layouts.admin_dashboard')
@section('content')

<div id="page-content">

    <ul class="breadcrumb breadcrumb-top">
        <li>{!! html_entity_decode(link_to('/admin/dashboard', 'Admin Dashboard', array('escape' => false,'class'=>""))) !!}</li>
        <li>{!! html_entity_decode(link_to('/admin/QrCodes', 'QR Code Details', array('escape' => false,'class'=>"",'title'=>"QR Code Details"))) !!}</li>
        <li>QR Code Details</li>
    </ul>
    <!-- END Forms General Header -->

    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Interactive Title -->
                <div class="block-title">
                    <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
                    <div class="block-options pull-right">
                        <a data-toggle="block-toggle-fullscreen" class="btn btn-alt btn-sm btn-primary" href="javascript:void(0)"><i class="fa fa-desktop"></i></a>
                    </div>
                    <h2><strong>QR Code Details</strong> 
                    <div class="addBtnSec">
                    <a href="{!! url('/admin/QrCodes/generateQrCode') !!}">
                        <button type="button" class="btn btn-labeled btn-success">
                            <span class="btn-label"></span> Generate QR Code
                        </button>
                    </a>    
                    <?php 
                    if(!empty($data->url))
                    {
                        ?>
                            <a target="blank" href="<?php echo $data->url; ?>">
                                <button type="button" class="btn btn-labeled btn-success">
                                    <span class="btn-label"></span>Download QR Code for Android
                                </button>
                            </a>   
                        <?php
                    }
                    ?>
                    
                    <?php 
                    if(!empty($data->url_ios))
                    {
                        ?>
                            <a target="blank" href="<?php echo $data->url_ios; ?>">
                                <button type="button" class="btn btn-labeled btn-success">
                                    <span class="btn-label"></span>Download QR Code for IOS
                                </button>
                            </a>   
                        <?php
                    }
                    ?>
                      
                </div></h2>
                </div>
                <!-- END Interactive Title -->
                <div class="row">
                     
                       
              <div class="col-lg-12">
                  <div class="qr">
                      <span>Android</span>
                      
                      <span><img src="<?php echo $data->url; ?>"></span>
                  </div>
                  <div class="qr">
                      <span>IOS</span>
                      
                      <span class="ios"><img src="<?php echo $data->url_ios; ?>"></span>
                  </div>
                      
                        
                    </div>
                    
                         
                        
                </div>        
            </div>
            <!-- END Basic Form Elements Block -->
        </div>
    </div>
</div>
@endsection