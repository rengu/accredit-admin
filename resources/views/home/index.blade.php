@extends('layouts.main')
@section('content')
<div class="banner">
    <div class="banner-slider">
        <div class="banner-m" style="background-image:url(public/images/banner1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner-content">
                            <h1>Dan’s thrilled
                                and so is his CEO</h1>
                                <p style="color:#fff;">Get more done with freelancers</p>
                                <a href="{!! url('user/signup'); !!}">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-m" style="background-image:url(public/images/banner2.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="banner-content">
                                <h1>Dan’s thrilled
                                    and so is his CEO</h1>
                                    <p style="color:#000;">Get more done with freelancers</p>
                                    <a href="{!! url('user/signup'); !!}">Get Started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-m" style="background-image:url(public/images/banner3.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="banner-content">
                                    <h1>Dan’s thrilled
                                        and so is his CEO</h1>
                                        <p style="color:#077fc8;">Get more done with freelancers</p>
                                        <a href="{!! url('user/signup'); !!}">Get Started</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section id="work-someone">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading text-color1">
                                <h2>Work with someone perfect for your team</h2>
                            </div>
                            <ul>
                                <li>
                                    <img src="public/images/work1.png" alt="">
                                    <p>Web developers</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                                <li>
                                    <img src="public/images/work2.png" alt="">
                                    <p>Mobile developers</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                                <li>
                                    <img src="public/images/work-3.png" alt="">
                                    <p>Designer & Creatives</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                                <li>
                                    <img src="public/images/work4.png" alt="">
                                    <p>Writers</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                                <li>
                                    <img src="public/images/work5.png" alt="">
                                    <p>Virtual Assistants</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                                <li>
                                    <img src="public/images/work6.png" alt="">
                                    <p>Customer Service Agents</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                                <li>
                                    <img src="public/images/work7.png" alt="">
                                    <p>Sales & marketing experts</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                                <li>
                                    <img src="public/images/work8.png" alt="">
                                    <p>Accountant & Consultants</p>
                                    <div class="work-with">
                                        <span>FRONT-END DEVELOPERS</span>
                                        <span>BACK-END DEVELOPERS</span>
                                        <span>SOFTWARE PROGRAMMERS</span>
                                        <a href="#">and more....</a>
                                    </div>
                                </li>
                            </ul>
                            <div class="categories-btn">
                                <a href="{!! url('categories'); !!}">See All Categories</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="how-work">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading text-color2">
                                <h2>How it works</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="work-detail" style="border-top:3px solid #aa98c8">
                                <div class="work-img">
                                    <img src="public/images/find1.png" alt="">
                                </div>
                                <div class="work-content">
                                    <h5 style="color:#aa98c8">Find</h5>
                                    <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="work-detail" style="border-top:3px solid #ff6a49">
                                <div class="work-img">
                                    <img src="public/images/find2.png" alt="">
                                </div>
                                <div class="work-content">
                                    <h5 style="color:#ff6a49">Hire</h5>
                                    <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="work-detail" style="border-top:3px solid #6ec9ff">
                                <div class="work-img">
                                    <img src="public/images/find3.png" alt="">
                                </div>
                                <div class="work-content">
                                    <h5 style="color:#6ec9ff">Work</h5>
                                    <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="work-detail" style="border-top:3px solid #efa949">
                                <div class="work-img">
                                    <img src="public/images/find4.png" alt="">
                                </div>
                                <div class="work-content">
                                    <h5 style="color:#efa949">Pay</h5>
                                    <p>Post a job to tell us about your project. We'll quickly match you with the right freelancers.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section id="browse-skill">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading text-color1">
                                <h2>Work with someone perfect for your team</h2>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="skill-m">
                                <ul>
                                    <li><a href="#">Android Developers</a></li>
                                    <li><a href="#">Content Writers</a></li>
                                    <li><a href="#">Email Marketing Consultants</a></li>
                                    <li><a href="#">iOS Developers</a></li>
                                    <li><a href="#">Objective-C Developers</a></li>
                                    <li><a href="#">SEO Experts</a></li>
                                    <li><a href="#">UI Designers</a></li>
                                    <li><a href="#">Wordpress Developers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="skill-m">
                                <ul>
                                    <li><a href="#">Android Developers</a></li>
                                    <li><a href="#">Content Writers</a></li>
                                    <li><a href="#">Email Marketing Consultants</a></li>
                                    <li><a href="#">iOS Developers</a></li>
                                    <li><a href="#">Objective-C Developers</a></li>
                                    <li><a href="#">SEO Experts</a></li>
                                    <li><a href="#">UI Designers</a></li>
                                    <li><a href="#">Wordpress Developers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="skill-m">
                                <ul>
                                    <li><a href="#">Android Developers</a></li>
                                    <li><a href="#">Content Writers</a></li>
                                    <li><a href="#">Email Marketing Consultants</a></li>
                                    <li><a href="#">iOS Developers</a></li>
                                    <li><a href="#">Objective-C Developers</a></li>
                                    <li><a href="#">SEO Experts</a></li>
                                    <li><a href="#">UI Designers</a></li>
                                    <li><a href="#">Wordpress Developers</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="skill-m">
                                <ul>
                                    <li><a href="#">Android Developers</a></li>
                                    <li><a href="#">Content Writers</a></li>
                                    <li><a href="#">Email Marketing Consultants</a></li>
                                    <li><a href="#">iOS Developers</a></li>
                                    <li><a href="#">Objective-C Developers</a></li>
                                    <li><a href="#">SEO Experts</a></li>
                                    <li><a href="#">UI Designers</a></li>
                                    <li><a href="#">Wordpress Developers</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="get-sttart-btn">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Build your online team</h2>
                            <a href="#">Get Started</a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="service-level">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading text-color2">
                                <h2>Find the level of service that works for you</h2>
                            </div>
                            <div class="service-level-m">
                               <table width="100%">
                                   <thead>
                                       <tr>
                                           <th>Each option includes access to khubaraa's large pool of top-quality freelancers. Choose the level of service you need.</th>
                                           <th>
                                            <h5>Basic</h5>
                                            <p>$20<span>Per Month</span></p>
                                            <a href="#">Sign Up</a>
                                        </th>
                                        <th>
                                            <h5>Pro</h5>
                                            <p>$30<span>Per Month</span></p>
                                            <a href="#">Sign Up</a>
                                        </th>
                                        <th>
                                            <h5>Premium</h5>
                                            <p>$50<span>Per Month</span></p>
                                            <a href="#">Sign Up</a>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <tr>
                                       <td><b>Bids Per Month</b> </td>
                                       <td>50 Bids Per Month</td>
                                       <td>150 Bids Per Month</td>
                                       <td>Unlimited Bids Per Month</td>
                                   </tr>
                                   <tr>
                                       <td><b>Skills </b> </td>
                                       <td>5 Skills</td>
                                       <td>100 Skills</td>
                                       <td>Unlimited Skills</td>
                                   </tr>
                                   <tr>
                                       <td><b>Project Bookmarks</b> </td>
                                       <td>15 Project Bookmarks</td>
                                       <td>50 Project Bookmarks</td>
                                       <td>Unlimited Project Bookmarks</td>
                                   </tr>
                                   <tr>
                                       <td><b>Custom Cover Photo</b> </td>
                                       <td>No Custom Cover Photo</td>
                                       <td>2 Cover Photo</td>
                                       <td>Unlimited Cover Photo</td>
                                   </tr>
                                   <tr>
                                       <td><b>Featured Projects</b> </td>
                                       <td>No Featured Projects</td>
                                       <td>10 Featured Projects</td>
                                       <td>Unlimited Featured Projects</td>
                                   </tr>
                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       </section>
       <script src="{!! asset('/public/js/slick.js') !!}" type="text/javascript" charset="utf-8"></script>
       <script type="text/javascript">

        $(document).ready(function() {
            $('.banner-slider').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1,
                autoplay: true,
                arrows: false,
                dots:true,
                responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            },
            {
              breakpoint: 800,
              settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1
            }
        }
        ]
    });
            $('.success-slider').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1,
                autoplay: true,
                arrows: false,
                dots:false,
                responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 2
                }
            },
            {
              breakpoint: 800,
              settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 1
            }
        }
        ]
    });    
        });
    </script> 
    @endsection
