@extends('layouts.main')
@section('content')
<div class="inner-banner categories-m" style="background-image:url(public/images/categories.png);">
</div>
<?php if(!empty($categories)) {
   foreach ($categories as $category) {
      $getSubCategory= DB::table('categories')->where('parent_id',$category->id)->where('status',1)->get();
 ?>
<section id="browse-skill">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="heading text-color1">
               <h2>{!! $category->name !!}</h2>
            </div>
            <?php if(!empty($getSubCategory)) { ?>
            <div class="skill-m categories">
               <ul>
                  <?php foreach($getSubCategory as $subcategory) { ?>
                  <li><a href="{!! url('jobs/search/subcategory='.$subcategory->id); !!}">{!! $subcategory->name !!}</a></li>
                  <?php } ?>
               </ul>
            </div>
            <?php } ?>
         </div>
      </div>
   </div>
</section>
<?php }
}  ?>

@endsection
