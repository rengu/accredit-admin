@extends('layouts.login-signup')
@section('content')
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-form">
                    <h3>Log in and get to work</h3>
                    @include('elements.success-error-message')
                    {!! Form::open(['url' => 'user/createLogin', 'method' => 'POST','id'=>'loginform']) !!}
                        <div class="form-group">
                            {!! Form::text('email', Cookie::get('email'), array('class'=>'required email','placeholder'=>"Email Address")); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::input('password','password', Cookie::get('password'), array('class'=>'required','placeholder'=>"Password",'id'=>"password")); !!}
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    @if(!empty(Cookie::get('remember'))) 
                                    {!! Form::checkbox('remember', '0',1) !!}
                                    @else
                                    {!! Form::checkbox('remember', '0',0) !!}    
                                    @endif
                                    <label>Remember me next time</label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="link-btn">
                                        <a href="{!! url('user/forgotpassword'); !!}">Forgot password?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button>Login</button>
                        </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $("#loginform").validate();
    });
</script>
@endsection
