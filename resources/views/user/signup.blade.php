@extends('layouts.login-signup')
@section('content')
    <section id="login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-form">
                    @include('elements.success-error-message')
                    
                <h3>Sign up for free today!</h3>
                {!! Form::open(['url' => 'user/create', 'method' => 'POST','id'=>'signup']) !!}
                <div class="form-group">
                    {!! Form::text('email', '', array('class'=>'required email','placeholder'=>"Email Address")); !!}
                </div>
                <div class="form-group">
                    {!! Form::text('username', '', array('class'=>'required','placeholder'=>"Username")); !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password',  array('class'=>'required','placeholder'=>"Password",'minLength'=>8,'id'=>"password")); !!}
                </div>
                <div class="form-group">
                    {!! Form::password('c_password',  array('class'=>'required','placeholder'=>"Confirm Password",'equalTo'=>"#password")); !!}
                </div>
                <div class="form-group">
                    <span>I want to</span>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                        <input type="radio" checked="checked" name="type" value="Hire" placeholder="" class="required">
                        <label>Hire</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                        <input type="radio" name="type" value="Work" placeholder="" class="required">
                        <label>Work</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                <p>By registering you confirm that you accept the <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
                </div>
                <div class="form-group">
                <button>Create Account</button>
                </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="{!! asset('/public/css/passwordmeter/password.css') !!}">
<script src="{!! asset('/public/js/passwordmeter/password.js') !!}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#password').password();
        $("#signup").validate();
    });
</script>
@endsection
