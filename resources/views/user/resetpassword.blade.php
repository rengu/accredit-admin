@extends('layouts.login-signup')
@section('content')
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-form">
                    <h3>Reset Password</h3>
                    @include('elements.success-error-message')
                    {!! Form::open(['url' => 'reset-password-create', 'method' => 'POST','id'=>'resetpassword']) !!}
                    {!! Form::hidden('user_id', $user_id, array('id' => 'hidden')) !!}
                        <div class="form-group">
                            {!! Form::password('password',  array('class'=>'required','minLength'=>8,'placeholder'=>"New Password",'id'=>'password')); !!}
                            
                        </div>
                        <div class="form-group">
                            {!! Form::password('cpassword',  array('class'=>'required','placeholder'=>"Confirm Password",'equalTo'=>'#password')); !!}
                        </div>
                       
                        <div class="form-group">
                            <button>Submit</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="{!! asset('/public/css/passwordmeter/password.css') !!}">
<script src="{!! asset('/public/js/passwordmeter/password.js') !!}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#password').password();
        $("#resetpassword").validate();
    });
</script>
@endsection
