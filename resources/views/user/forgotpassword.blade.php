@extends('layouts.login-signup')
@section('content')
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="login-form">
                    <h3>Forgot Password</h3>
                    @include('elements.success-error-message')
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="valid-error">{{ $error }}</div>
                        @endforeach
                    @endif
                    {!! Form::open(['url' => 'user/forgotpassword', 'method' => 'POST','id'=>'forgotpassword']) !!}
                        <div class="form-group">
                            {!! Form::text('email', '', array('class'=>'required email','placeholder'=>"Email Address")); !!}
                        </div>
                       
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="link-btn">
                                        Remembered Password? Please <a href="{!! url('user/login'); !!}"> login here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button>Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $("#forgotpassword").validate();
    });
</script>
@endsection
