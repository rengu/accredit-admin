@extends('layouts.error')

@section('title', 'Error')

@section('message')
	<div class="error-section">
		something Went wrong
		<a href="{!! url('/') !!}">Back to home</a>
	</div>
@endsection