@extends('layouts.error')

@section('title', 'Error')

@section('message')
	<div class="error-section">
	<h1>404</h1>
		<p>The page you were looking for doesn't exist</p>
		<a href="{!! url('/') !!}">Go Back</a>
	</div>
@endsection
