
<header class="navbar navbar-default">
    <div class="m-navicon"><img src="<?php echo HTTP_PATH. "public/images/menu.svg"; ?>" alt=""></div>
    <div class="overlaycus"></div>
    <!-- Left Header Navigation -->
    <ul class="nav navbar-nav-custom">
       
   </ul>
    <!-- END Left Header Navigation -->

    <div class="headermix-right">
        <ul class="desktop-allaccount">
            
            <li>
                    {!! html_entity_decode(link_to('/admin/editProfile', '<i class="fa fa-user fa-fw"></i> Edit Profile', array('escape' => false,'class'=>"btn btn-gray"))) !!}
                    <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                    
                    {!! html_entity_decode(link_to('/admin/changePassword', '<i class="fa fa-lock fa-fw"></i> Change Password', array('escape' => false,'class'=>"btn btn-gray"))) !!}
                    {!! html_entity_decode(link_to('/admin/logout', '<i class="fa fa-power-off"></i> Log Out', array('escape' => false,'class'=>"btn btn-gray"))) !!}
                </li>
        </ul>
        <div class="userlogname">
            <!--<div class="sidebar-user-avatar">
                    <a href="{{ HTTP_PATH.'admin/dashboard' }}">
                        <img src="<?php echo HTTP_PATH. "public/admin/images/logo.png" ; ?>" alt="Airtel" >
                    </a>
            </div>-->
            <div class="sidebar-user-name">Admin</div>
        </div>
    </div>

    <!-- Right Header Navigation -->
    <ul class="nav navbar-nav-custom pull-right">
      

        <!-- User Dropdown -->

        

        <li class="dropdown headtogright">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                Settings <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                <!-- <li class="dropdown-header text-center">Account</li> -->
                <li>
                    {!! html_entity_decode(link_to('/admin/editProfile', '<i class="fa fa-user fa-fw"></i> Edit Profile', array('escape' => false,'class'=>""))) !!}
                    <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                    
                    {!! html_entity_decode(link_to('/admin/changePassword', '<i class="fa fa-lock fa-fw"></i> Change Password', array('escape' => false,'class'=>""))) !!}
                </li>
                <li class="divider"></li>
                <li>
                    {!! html_entity_decode(link_to('/admin/logout', '<i class="fa fa-power-off"></i> Log Out', array('escape' => false,'class'=>""))) !!}
                </li>
                
            </ul>
        </li>
        <!-- END User Dropdown -->
    </ul>
    <!-- END Right Header Navigation -->
</header>

<script>
    if ($(window).width() < 991) {
        $(".sidebar-brand").prependTo(".navbar");
        $(".m-navicon").click(function(){
            $("#sidebar").animate({left:"0"}, 100);
            $(".overlaycus").addClass("active");
        });
        $(".overlaycus").click(function(){
            $("#sidebar").animate({left:"-200px"}, 100);
            $(this).removeClass("active");
        });
        $("#sidebar").prependTo("body");
        $(".userlogname").prependTo(".headtogright ul");
    }
</script>