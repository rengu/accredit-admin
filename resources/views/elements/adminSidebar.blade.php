<?php 
    $adminType = Session::get('admin_type_id');
    $roleId = Session::get('role_id');
?>
<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="{{ URL::to('admin/dashboard') }}" class="sidebar-brand">
               <img src="{{ asset('public/admin/images/icon32.png') }}" alt="Airtel" > <span class="sidebar-nav-mini-hide"><strong></strong></span>
            </a>
            <!-- END Brand -->

            <!-- User Info -->
           <!--  <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">
                    <a href="{{ HTTP_PATH.'admin/dashboard' }}">
                        <img src="<?php echo HTTP_PATH. "public/admin/images/logo.png" ; ?>" alt="Airtel" >
                    </a>
                </div>
                <div class="sidebar-user-name">Admin</div>
                <div class="sidebar-user-links">
                    <a href="<?php echo HTTP_PATH.'admin/editProfile'; ?>" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                    
                    {!! html_entity_decode(link_to('/admin/logout', '<i class="gi gi-exit"></i>', array('escape' => false,'class'=>"",'title'=>"Logout","data-toggle"=>"tooltip", "data-placement"=>"bottom"))) !!}
                </div>
            </div> -->
            <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
            <!-- END User Info -->



            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li>
                    <a href="{{  URL::to('admin/dashboard') }}" class="{{ Request::is('admin/dashboard')?'active':''}}"><i class="fa fa-dashboard sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                </li>
                <!-- **********************************************************
                *******Configuration Admin Left side Link Start ***************
                ********************************************************** -->
                <?php 
                    $isShowMainModule = showMainModule($adminType,'Configuration');
                      if($isShowMainModule == 1 )
                      {
                        ?>
                        <li>
                            <a href="#" class="sidebar-nav-menu {{( Request::is('admin/editProfile*') or  Request::is('admin/changePassword*')  )? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Configuration</span></a>
                            <ul style="{{( Request::is('admin/editProfile*') or  Request::is('admin/changePassword*')  )? 'display:block' : '' }}">
                                <?php 
                                    $isshowctp = isShow1($adminType,'Admin','changePassword');
                                    if($isshowctp == 1 )
                                    {
                                        ?>
                                            <li class="{{ Request::is('admin/changePassword*') ? 'active' : '' }}">
                                                <a class="{{ Request::is('admin/changePassword*') ? 'active' : '' }}" href="{{ URL::to( 'admin/changePassword') }}">
                                                    Change Password
                                                </a>
                                            </li>
                                        <?php
                                    }
                                ?>

                                 <?php 
                                    $isshowctp = isShow1($adminType,'Admin','editProfile');
                                    if($isshowctp == 1 )
                                    {
                                        ?>
                                            <li class="{{ Request::is('admin/editProfile*') ? 'active' : '' }}"> 
                                                <a class="{{ Request::is('admin/editProfile*') ? 'active' : '' }}" href="{{ URL::to( 'admin/editProfile') }}">
                                                    Edit Profile
                                                </a>
                                            </li>
                                        <?php
                                    }
                                ?>
                                <?php 
                                    $isshowctp = isShow1($adminType,'Admin','setting');
                                    if($isshowctp == 1 )
                                    {
                                        ?>
                                            <li class="{{ Request::is('admin/setting*') ? 'active' : '' }}"> 
                                                <a class="{{ Request::is('admin/setting*') ? 'active' : '' }}" href="{{ URL::to( 'admin/setting') }}">
                                                   Setting
                                                </a>
                                            </li>
                                        <?php
                                    }
                                ?>
                                
                               
                            </ul>
                            </li>
                        <?php
                      }
                ?>
             
                <!-- ******* Configuration Admin Left side Link End **************** -->
                
                <!-- *************************************************************
                *******************Users Admin Left side Link Start **************
                **************************************************************** -->
    <?php 
      $isShowMainModule = showMainModule($adminType,'Admin User/Admin Role');
      $isshowApplicationuser =showMainModule($adminType,'Application User');
      if($isShowMainModule == 1 || $isshowApplicationuser == 1)
      {
        ?>
        <li>
            <a href="#" class="sidebar-nav-menu {{ (Request::is('admin/users*') ||Request::is('admin/adminuser*')|| Request::is('admin/user-types*')) ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Users Management</span></a>
            <ul style="{{ (Request::is('admin/users*') || Request::is('admin/user-types*')) ? 'display: block;' : '' }}">
                 <?php 
                  $isshowctp = isShow($adminType,'Adminusers');
                  if($isshowctp == 1 )
                  {
                    ?>
                     <li class="{{ (Request::is('admin/adminuser/index') OR Request::is('admin/users/edit*')) ? 'active' : '' }}">{!! link_to('/admin/adminuser/index', 'Admin User List', ['escape' => false]) !!}
                     </li>
                <?php
                 }
                 ?>
                 <?php 
                  $isshowctp = isShow($adminType,'Users');
                  if($isshowctp == 1 )
                  {
                    ?>
                      <li class="{{ (Request::is('admin/users/index') OR Request::is('admin/users/edit*')) ? 'active' : '' }}">{!! link_to('/admin/users/index', 'Application User List', ['escape' => false]) !!}</li>
                 
                <?php
                 }
                 ?>
                 <?php 
                  $isshowctp = isShow1($adminType,'Adminusers','role');
                  if($isshowctp == 1 )
                  {
                    ?>
                     <li class="{{ (Request::is('admin/adminuser/role') OR Request::is('admin/adminuser/role*')) ? 'active' : '' }}">{!! link_to('/admin/adminuser/role', 'Admin Role', ['escape' => false]) !!}
                  </li>
                 
                <?php
                 }
                 ?> 
                 
            </ul>
        </li>
    <?php
     }
    ?>

     <?php 
      $isShowMainModule = showMainModule($adminType,'Loans');
      if($isShowMainModule == 1 )
      {
        ?>              
         
          <li>
            <a href="#" class="sidebar-nav-menu {{ (Request::is('admin/loans*')) ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Loans</span></a>
            <ul style="{{ (Request::is('admin/loans*') ) ? 'display: block;' : '' }}">
                <?php 
                $isshowctp = isShow1($adminType,'Loans','individualLoanEnquiries');
                if($isshowctp == 1 )
                {
                  ?>
                    <li class="{{ (Request::is('admin/loans/individualLoanEnquiries') OR Request::is('admin/loans/individualLoanEnquiries*')) ? 'active' : '' }}">{!! link_to('/admin/loans/individualLoanEnquiries', 'Individual Loan Enquiries', ['escape' => false]) !!}</li>
                <?php
                }
                ?>

                <?php 
                $isshowctp = isShow1($adminType,'Loans','companyLoanEnquiries');
                if($isshowctp == 1 )
                {
                  ?>
                     <li class="{{ (Request::is('admin/loans/companyLoanEnquiries') OR Request::is('admin/loans/companyLoanEnquiries*')) ? 'active' : '' }}">{!! link_to('/admin/loans/companyLoanEnquiries', 'Company Loan Enquiries', ['escape' => false]) !!}</li>
                <?php
                }
                ?>

               
            </ul>
        </li>
        <?php 
            }
        ?>

        <?php 
          $isShowMainModule = showMainModule($adminType,'Cms');
          if($isShowMainModule == 1 )
          {
            ?> 

                 <li>
                    <a href="#" class="sidebar-nav-menu {{ (Request::is('admin/cms*')) ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Cms </span></a>
                    <ul style="{{ (Request::is('admin/cms*') ) ? 'display: block;' : '' }}">
                      <?php 
                        $isshowctp = isShow1($adminType,'Cms','index');
                        if($isshowctp == 1 )
                        {
                          ?>
                         <li class="{{ (Request::is('admin/cms/index') OR Request::is('admin/cms/add*')) ? 'active' : '' }}">{!! link_to('/admin/cms/index', 'Faq', ['escape' => false]) !!}</li>
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Cms','about');
                        if($isshowctp == 1 )
                        {
                          ?>
                         <li class="{{ (Request::is('admin/cms/about') OR Request::is('admin/cms/about*')) ? 'active' : '' }}">{!! link_to('/admin/cms/about', 'About', ['escape' => false]) !!}</li>
                         <?php
                            }
                        ?>
                         
                    </ul>
                   
                </li>
                <?php
                  }
                ?>

        <?php 
          $isShowMainModule = showMainModule($adminType,'Master Management');
          if($isShowMainModule == 1 )
          {
            ?>    
                <li>
                    <a href="#" class="sidebar-nav-menu {{ (Request::is('admin/master*')) ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Master Management </span></a>
                    <ul style="{{ (Request::is('admin/master*') ) ? 'display: block;' : '' }}">
                        <?php 
                        $isshowctp = isShow1($adminType,'Master','index');
                        if($isshowctp == 1 )
                        {
                          ?>
                         <li class="{{ (Request::is('admin/master/index') OR Request::is('admin/master/add*')) ? 'active' : '' }}">{!! link_to('/admin/master/index', 'Form Field', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','idType');
                        if($isshowctp == 1 )
                        {
                          ?>
                          <li class="{{ (Request::is('admin/master/idType') OR Request::is('admin/master/idTypeadd*') OR Request::is('admin/master/idTypeedit*')) ? 'active' : '' }}">{!! link_to('/admin/master/idType', 'ID Type', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','educationlevel');
                        if($isshowctp == 1 )
                        {
                          ?>
                          <li class="{{ (Request::is('admin/master/educationlevel') OR Request::is('admin/master/educationlevel*') OR Request::is('admin/master/educationlevel*')) ? 'active' : '' }}">{!! link_to('/admin/master/educationlevel', 'Education Level', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','businessType');
                        if($isshowctp == 1 )
                        {
                          ?>
                           <li class="{{ (Request::is('admin/master/businessType') OR Request::is('admin/master/businessType*')) ? 'active' : '' }}">{!! link_to('/admin/master/businessType', 'Business Type', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','knowAccredit');
                        if($isshowctp == 1 )
                        {
                          ?>
                          <li class="{{ (Request::is('admin/master/knowAccredit') OR Request::is('admin/master/knowAccredit*')) ? 'active' : '' }}">{!! link_to('/admin/master/knowAccredit', 'How did I get to know Accredit? ', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                         <?php 
                        $isshowctp = isShow1($adminType,'Master','loanType');
                        if($isshowctp == 1 )
                        {
                          ?>
                          <li class="{{ (Request::is('admin/master/loanType') OR Request::is('admin/master/loanType*')) ? 'active' : '' }}">{!! link_to('/admin/master/loanType', 'Loan Type ', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','loanPurpose');
                        if($isshowctp == 1 )
                        {
                          ?>
                            <li class="{{ (Request::is('admin/master/loanPurpose') OR Request::is('admin/master/loanPurpose*')) ? 'active' : '' }}">{!! link_to('/admin/master/loanPurpose', ' Loan Purpose ', ['escape' => false]) !!}</li>
                         <?php
                            }
                        ?>

                         <?php 
                        $isshowctp = isShow1($adminType,'Master','residentialType');
                        if($isshowctp == 1 )
                        {
                          ?>
                           <li class="{{ (Request::is('admin/master/residentialType') OR Request::is('admin/master/residentialType*')) ? 'active' : '' }}">{!! link_to('/admin/master/residentialType', ' Residential Type ', ['escape' => false]) !!}</li>
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','propertyOwnership');
                        if($isshowctp == 1 )
                        {
                          ?>
                           <li class="{{ (Request::is('admin/master/propertyOwnership') OR Request::is('admin/master/propertyOwnership*')) ? 'active' : '' }}">{!! link_to('/admin/master/propertyOwnership', 'Property Ownership', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','employmentStatus');
                        if($isshowctp == 1 )
                        {
                          ?>
                           <li class="{{ (Request::is('admin/master/employmentStatus') OR Request::is('admin/master/employmentStatus*')) ? 'active' : '' }}">{!! link_to('/admin/master/employmentStatus', 'Employment Status', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','jobTitle');
                        if($isshowctp == 1 )
                        {
                          ?>
                            <li class="{{ (Request::is('admin/master/jobTitle') OR Request::is('admin/master/jobTitle*')) ? 'active' : '' }}">{!! link_to('/admin/master/jobTitle', 'Job Title/Position', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','specialization');
                        if($isshowctp == 1 )
                        {
                          ?>
                           <li class="{{ (Request::is('admin/master/specialization') OR Request::is('admin/master/specialization*')) ? 'active' : '' }}">{!! link_to('/admin/master/specialization', 'Specialization', ['escape' => false]) !!}</li>
                         <?php
                            }
                        ?>

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','officeBranch');
                        if($isshowctp == 1 )
                        {
                          ?>
                          <li class="{{ (Request::is('admin/master/officeBranch') OR Request::is('admin/master/officeBranch*')) ? 'active' : '' }}">{!! link_to('/admin/master/officeBranch', 'Office branch', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>
                            <?php 
                            $isshowctp = isShow1($adminType,'Master','bank');
                            if($isshowctp == 1 )
                            {
                              ?>
                              <li class="{{ (Request::is('admin/master/bank') OR Request::is('admin/master/bank*')) ? 'active' : '' }}">{!! link_to('/admin/master/bank', 'Bank Name', ['escape' => false]) !!}</li> 
                             <?php
                                }
                            ?>
                        <?php 
                        $isshowctp = isShow1($adminType,'Master','ReasonForLoan');
                        if($isshowctp == 1 )
                        {
                          ?>
                          <li class="{{ (Request::is('admin/master/ReasonForLoan') OR Request::is('admin/master/ReasonForLoan*')) ? 'active' : '' }}">{!! link_to('/admin/master/ReasonForLoan', 'Reason For Loan', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>    

                        <?php 
                        $isshowctp = isShow1($adminType,'Master','FaqCategories');
                        if($isshowctp == 1 )
                        {
                          ?>
                          <li class="{{ (Request::is('admin/master/FaqCategories') OR Request::is('admin/master/FaqCategories*')) ? 'active' : '' }}">{!! link_to('/admin/master/FaqCategories', 'Faq Categories', ['escape' => false]) !!}</li> 
                         <?php
                            }
                        ?>    

                    </ul>
                </li>
                <?php
                    }
                 ?>

        <?php 
          $isShowMainModule = showMainModule($adminType,'Documents');
          if($isShowMainModule == 1 )
          {
            ?> 
                
                 <li class="{{ (Request::is('admin/documents') OR Request::is('/admin/documents*')) ? 'active' : '' }}">
                    <a href="{{  URL::to('admin/documents') }}"><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Documents</span></a>
                  </li>
        <?php
          }
        ?>

         <?php 

          $isShowMainModule = showMainModule($adminType,'Qr Code');
               
          if($isShowMainModule == 1 )
          {
            ?> 
                
                 <li class="{{ Request::is('admin/QrCodes')?'active':''}}">
                    <a href="{{  URL::to('admin/QrCodes') }}" ><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">QR Code</span></a>
                  </li>
        <?php
          }
        ?>

        <?php 
          $isShowMainModule = showMainModule($adminType,'Reports');
          if($isShowMainModule == 1 )
          {
            ?> 

                 <li>
                    <a href="#" class="sidebar-nav-menu {{ (Request::is('admin/Reports*')) ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reports </span></a>
                    <ul style="{{ (Request::is('admin/Reports*') ) ? 'display: block;' : '' }}">
                      <?php 
                        $isshowctp = isShow1($adminType,'Reports','paymentReport');
                        if($isshowctp == 1 )
                        {
                          ?>
                         <li class="{{ (Request::is('admin/Reports/paymentReport') OR Request::is('admin/Reports/paymentReport*')) ? 'active' : '' }}">{!! link_to('/admin/Reports/paymentReport', 'Payment Report', ['escape' => false]) !!}</li>
                         <?php
                            }
                        ?>
                         
                    </ul>
                   
                </li>
                <?php
                  }
                ?>



                 
                 

            </ul>

            <!-- END Sidebar Navigation -->


        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>