<div class="header-top">
	<div class="container">
       <div class="row">
           <div class="col-md-7 col-sm-7">
               <div class="header-m">
                <div class="row">
                	<div class="col-md-3 col-sm-3">
                        <div class="logo">
                        <a href="{!! url('/'); !!}"><h3>Logo</h3></a>
                        </div>
                    </div>  
                    <div class="col-md-4 col-sm-4"> 
                        <div class="become-freelancer">
                            <a href="{!! url('user/signup'); !!}">Become a Freelancer</a>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5">
                        <form method="post">
                            <label>
                                <select>
                                    <option>Find Freelancers</option>
                                    <option>Find Jobs</option>
                                </select>
                                <button><i class="fa fa-search" aria-hidden="true"></i></button>
                            </label>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-5 col-sm-5">

            <div class="header-m">
               <ul>
                <li class="dropdown"><a href="#" data-toggle="dropdown">Browse</a>
                    <ul class="dropdown-menu">
                        <li><a href="#" data-toggle="collapse">Browse Categories</a></li>
                        <li><a href="#">Browse Freelancers</a></li>
                        <li><a href="#">Browse Jobs</a></li>
                        <li><a href="#">Trending skills</a></li>
                        <li><a href="#">Browse Hiring Resources</a></li>
                    </ul>
                </li>
                <li><a href="#">How It Works</a></li>
                <li><a href="{!! url('user/signup'); !!}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Sign up</a></li>
                <li><a href="{!! url('user/login'); !!}"><i class="fa fa-lock" aria-hidden="true"></i> Login</a></li>
            </ul>

        </div>
    </div>
</div>
</div>
</div>