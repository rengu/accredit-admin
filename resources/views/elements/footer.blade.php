<footer>
	<div class="footer-top">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-3 col-sm-3">
                <div class="footer-content">
                <h5>Company Info</h5>
                <ul>
            <li><a href="#">About us</a></li>
            <li><a href="#">Customer Stories</a></li>
            <li><a href="#">Press</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">khubaraa Blog</a></li>
            <li><a href="#">Terms of Service</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Trust & Safety</a></li>
            </ul>
                </div>
                </div>
                <div class="col-md-3 col-sm-3">
                <div class="footer-content">
                <h5>Company Info</h5>
                <ul>
            <li><a href="#">About us</a></li>
            <li><a href="#">Customer Stories</a></li>
            <li><a href="#">Press</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">khubaraa Blog</a></li>
            <li><a href="#">Terms of Service</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Trust & Safety</a></li>
            </ul>
                </div>
                </div>
                <div class="col-md-3 col-sm-3">
                <div class="footer-content">
                <h5>Company Info</h5>
                <ul>
            <li><a href="#">About us</a></li>
            <li><a href="#">Customer Stories</a></li>
            <li><a href="#">Press</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">khubaraa Blog</a></li>
            <li><a href="#">Terms of Service</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Trust & Safety</a></li>
            </ul>
                </div>
                </div>
                <div class="col-md-3 col-sm-3">
                <div class="footer-content">
                <h5>Company Info</h5>
                <ul>
            <li><a href="#">About us</a></li>
            <li><a href="#">Customer Stories</a></li>
            <li><a href="#">Press</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">khubaraa Blog</a></li>
            <li><a href="#">Terms of Service</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Trust & Safety</a></li>
            </ul>
                </div>
                </div>
            </div>
            <div class="footer-links">
            <div class="row">
            <div class="col-md-4 col-sm-4">
            <div class="footer-m">
            <form method="post"> 
            <label>
            <select>
            <option>US (International) \ English</option>
            <option>Arabic</option>
            </select>
            </label>
            </form>
            </div>
            </div>
            <div class="col-md-4 col-sm-4">
            <div class="footer-m">
            <h5> Follow us </h5>
            <ul>
            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube-square" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
            </div>
            </div>
            
            <div class="col-md-4 col-sm-4">
            <div class="footer-m">
            <h5> Download mobile app </h5>
            <ul>
            <li><a href="#"><i class="fa fa-apple" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-android" aria-hidden="true"></i></a></li>
            </ul>
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
    	<div class="container">
        <p>copyright © 2017 All rights reserved</p>
        </div>
    </div>
</footer>