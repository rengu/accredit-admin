<?php 
if(Session::has('success_message')) { ?>
<div class="alert alert-success">
   <a href="javascript:void(0);" class="close" onclick="closeMessage();">×</a>
   {{ session()->get('success_message') }}
</div>
<?php }
if(Session::has('error_message')) { ?>
<div class="alert alert-danger">
    <a href="javascript:void(0);" class="close" onclick="closeMessage();">×</a>
    {{ session()->get('error_message') }}
</div>
<?php } ?>
@if ($errors->any())
@foreach ($errors->all() as $error)
<div class="alert alert-danger">
    <a href="javascript:void(0);" class="close" onclick="closeMessage();">×</a>
    {{ $error }}</div>
    @endforeach
    @endif
   