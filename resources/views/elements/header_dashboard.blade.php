<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <div class="header-m dashboard-m">
                    <div class="row">
                        <div class="col-md-2 col-sm-2">
                            <div class="logo">
                                <a href="{!! url('/'); !!}"><h3><img</h3></a>
                            </div>
                        </div>
                        <div class="col-md-10 col-sm-10">
                            <nav role="navigation" class="navbar navbar-default pull-right">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div id="navbar" class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav">
                                    <?php if(Auth::user()->usertype=='freelancer') { ?>
                                      <li class="dropdown"><a href="#" data-toggle="dropdown">Find Work</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{!! url('jobs/search') !!}">Find work</a></li>
                                            <li><a href="{!! url('saved-jobs') !!}">Saved Jobs</a></li>
                                            <li><a href="#">Proposals</a></li>
                                            <li><a href="#">Profile</a></li>
                                            <li><a href="#">My Stats</a></li>
                                            <li><a href="#">Tests</a></li>
                                        </ul>
                                    </li>                                  
                                    <li class="dropdown"><a href="#" data-toggle="dropdown">My Jobs</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">My Jobs</a></li>
                                            <li><a href="#">All Contracts</a></li>
                                            <li><a href="#">Work Diary</a></li>
                                        </ul>
                                    </li>        
                                    <li class="dropdown"><a href="#" data-toggle="dropdown">Reports</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Overview</a></li>
                                            <li><a href="#">Timesheet Details</a></li>
                                            <li><a href="#">Time Analyze</a></li>
                                            <li><a href="#">Earnings By Client</a></li>
                                            <li><a href="#">Lifetime billing by clients</a></li>
                                            <li><a href="#">Weekly Timesheet</a></li>
                                            <li><a href="#">Timelogs</a></li>
                                            <li><a href="#">Connects History</a></li>
                                            <li><a href="#">Transaction History</a></li>
                                            <li><a href="#">Certificate Of Earnings</a></li>
                                        </ul>
                                    </li>
                                    <?php } ?>
                                    <?php if(Auth::user()->usertype=='client') { ?>
                                        <li class="dropdown"><a href="#" data-toggle="dropdown">Jobs</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{!! url('client/jobs-listings') !!}">My Job</a></li>
                                                <li><a href="#">All Job Postings</a></li>
                                                <li><a href="#">All Contract</a></li>
                                                <li><a href="{!! url('client/post-job') !!}">Post a Job</a></li>
                                            </ul>
                                        </li>                                  
                                        <li class="dropdown"><a href="#" data-toggle="dropdown">Freelancers</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">My Freelancers</a></li>
                                                <li><a href="{!! url('freelancer/search') !!}">Find Freelancers</a></li>
                                                <li><a href="#">Work Diary</a></li>
                                                <li><a href="#">Work Diary</a></li>
                                            </ul>
                                        </li>        
                                        <li class="dropdown"><a href="#" data-toggle="dropdown">Reports</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Weekly Summary</a></li>
                                                <li><a href="#">Budgets</a></li>
                                                <li><a href="#">Transactions</a></li>
                                                <li><a href="#">Timesheet</a></li>
                                                <li><a href="#">Time By freelancer</a></li>
                                                <li><a href="#">Time By Activity</a></li>
                                                <li><a href="#">Custom Export</a></li>
                                            </ul>
                                        </li>        
                                    <?php } ?>    

                                    <li><a href="#">Messages</a></li>
                                    <li>
                                      <form method="post">
                                        <label>
                                            <select>
                                                <option>Find Jobs</option>
                                            </select>
                                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </label>
                                    </form>                
                                </li>
                                <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-question" aria-hidden="true"></i></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="#">Help and Support</a></li>
                                    <li><a href="#">Community and forums</a></li>
                                    <li><a href="#">Disputes</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="#" data-toggle="dropdown"><i class="fa fa-bell-o" aria-hidden="true"></i></a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Security Question has been created for your account</a></li>
                                <li><a href="#">See All Notifications</a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>

        </div>
    </div>
</div>
</div>
<div class="col-md-3 col-sm-3">
    <div class="header-after-login">
        <div class="header-profile-img pull-left">
        <?php getProfileImage(Auth::user()); ?>
        </div>
        <div class="header-profile-content pull-right">
         <div class="dropdown"><a href="javascript:void(0);"onclick="myFunction()" class="dropbtn">{!! str_limit(Auth::user()->username, $limit = 10, $end = '...') !!} <span class="caret"></span></a>
             <ul id="myDropdown" class="dropdown-menu">
                <li>
                    <div class="switch-btn">
                        <?php if(Auth::user()->online_status==1){
                            $onlineStatus= "active";
                            $inactiveStatus= "";
                        } else {
                            $onlineStatus= "";
                            $inactiveStatus= "active";
                        }
                        ?>
                       <a href="javascript:void(0);" id="online" onclick="changeStatus(1,'online');" class="<?php echo $onlineStatus?> online-status"> Online</a>
                       <a href="javascript:void(0);" id="invisible" onclick="changeStatus(0,'invisible');" class="<?php echo $inactiveStatus;?> online-status"> Invisible</a>
                       <div class="loader-status" style="display: none;">{!! HTML::image('public/images/ajax-small-loader.gif','Loading...') !!}</div>
                    </div>
                 <script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-menu");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
   
                </li>
                 <?php if(Auth::user()->usertype=='freelancer') { ?>
                <li><a href="{!! url('user/profile-edit'); !!}">My Profile</a></li>
                <?php  }?>
                <li><a href="{!! url('dashboard-settings'); !!}">My Settings</a></li>
                <li><a href="{!! url('logout'); !!}">LogOut</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>