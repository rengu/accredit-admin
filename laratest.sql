-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2018 at 07:34 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laratest`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_1` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `phone_2` varchar(100) DEFAULT NULL,
  `phone_3` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `address` text NOT NULL,
  `created` datetime NOT NULL,
  `status` smallint(2) NOT NULL,
  `modified` datetime NOT NULL,
  `attempt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `email`, `email_1`, `name`, `phone`, `phone_2`, `phone_3`, `fax`, `address`, `created`, `status`, `modified`, `attempt`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com', 'admin@gmail.com', 'Laratest', '8764143625', '8764143625', '8764143625', NULL, 'Jaipur Rajasthan India', '2017-07-12 07:29:30', 1, '2018-12-04 13:42:44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `slug` varchar(254) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(254) DEFAULT NULL,
  `name_ar` varchar(254) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `parent_id`, `name`, `name_ar`, `status`, `created_at`, `updated_at`) VALUES
(1, 'designers-creatives', 0, 'Designers & Creatives', 'المصممين و التصاميم', 1, '2017-06-09 09:08:22', '2017-06-09 09:08:22'),
(2, 'programmers-developers', 0, 'Programmers & Developers', 'المبرمجين والمطورين', 1, '2017-06-09 09:08:43', '2017-06-09 09:08:43'),
(3, 'administrative-support-specialists', 0, 'Administrative Support Specialists', 'أخصائيو الدعم الإداري', 1, '2017-06-09 09:09:07', '2017-07-13 06:58:09'),
(4, 'writers-translators', 0, 'Writers & Translators', 'الكتاب والمترجمون', 1, '2017-06-09 09:09:22', '2017-06-09 09:09:22'),
(5, 'finance-professionals', 0, 'Finance Professionals', 'المهنيين الماليين', 1, '2017-06-09 09:09:40', '2017-06-09 09:09:40'),
(6, 'sales-marketing-professionals', 0, 'Sales & Marketing Professionals', 'المبيعات والتسويق المهنيين', 1, '2017-06-09 09:10:02', '2017-06-09 09:10:02'),
(7, '2d-animators', 1, '2D Animators', '2D الرسوم المتحركة', 1, '2017-06-09 09:13:26', '2017-06-09 09:13:26'),
(8, 'graphic-designers', 1, 'Graphic Designers', 'مصمم جرافيك', 1, '2017-06-09 09:14:05', '2017-06-09 09:14:05'),
(9, 'psd-to-html-experts', 1, 'PSD to HTML Experts', 'بسد إلى خبراء هتمل', 1, '2017-06-09 09:14:33', '2017-06-09 09:14:33'),
(10, 'illustrator-experts', 1, 'Illustrator Experts', 'إلوستراتور إكسيرتس', 1, '2017-06-09 09:14:55', '2017-06-09 09:14:55'),
(11, 'sharepoint-designers', 1, 'Sharepoint Designers', 'المصممين شاريبوانت', 1, '2017-06-09 09:16:01', '2017-06-09 09:16:01'),
(12, 'ui-designers', 1, 'UI Designers', 'أوي المصممين', 1, '2017-06-09 09:16:21', '2017-06-09 09:16:21'),
(13, 'logo-designers', 1, 'Logo Designers', 'شعار المصممين', 1, '2017-06-09 09:16:43', '2017-06-09 09:16:43'),
(14, 'bootstrap-designers', 1, 'Bootstrap Designers', 'المصممين بوتستراب', 1, '2017-06-09 09:18:24', '2017-06-09 09:18:24'),
(15, 'web-designers', 1, 'Web Designers', 'مصممي الويب', 1, '2017-06-09 09:18:46', '2017-06-09 09:18:46'),
(16, 'html5-developers', 2, 'HTML5 Developers', 'مطوري HTML5', 1, '2017-06-09 09:19:19', '2017-06-09 09:19:19'),
(17, 'php-developers', 2, 'PHP Developers', 'المطورين فب', 1, '2017-06-09 09:19:37', '2017-06-09 09:19:37'),
(18, 'android-developers', 2, 'Android Developers', 'الروبوت المطورين', 1, '2017-06-09 09:19:53', '2017-06-09 09:19:53'),
(20, 'java-developers', 2, 'Java Developers', 'جافا المطورين', 1, '2017-06-09 09:20:58', '2017-06-09 09:20:58'),
(21, 'javascript-developers', 2, 'JavaScript Developers', 'مطورو جافا سكريبت', 1, '2017-06-09 09:21:20', '2017-06-09 09:21:20'),
(22, 'jquery-developers', 2, 'jQuery Developers', 'جكري المطورين', 1, '2017-06-09 09:21:41', '2017-06-09 09:21:41'),
(23, 'magento-developers', 2, 'Magento Developers', 'الماجنتو المطورين', 1, '2017-06-09 09:22:02', '2017-06-09 09:22:02'),
(24, 'css-developers', 2, 'CSS Developers', 'كس المطورين', 1, '2017-06-09 09:23:50', '2017-06-09 09:23:50'),
(25, 'wordpress-developers', 2, 'Wordpress Developers', 'وورد المطورين', 1, '2017-06-09 09:24:07', '2017-06-09 09:24:07'),
(26, 'html-developers', 2, 'HTML Developers', 'مطوري هتمل', 1, '2017-06-09 09:24:22', '2017-06-09 09:24:22'),
(27, 'administrative-support-assistants', 3, 'Administrative Support Assistants', 'مساعدي الدعم الإداري', 1, '2017-06-09 09:25:01', '2017-06-09 09:25:01'),
(28, 'data-miners', 3, 'Data Miners', 'مناجم البيانات', 1, '2017-06-09 09:25:21', '2017-06-09 09:25:21'),
(29, 'appointment-setters', 3, 'Appointment Setters', 'تعيين التعيين', 1, '2017-06-09 09:25:35', '2017-06-09 09:25:35'),
(30, 'research-assistants', 3, 'Research Assistants', 'مساعدو البحوث', 1, '2017-06-09 09:25:52', '2017-06-09 09:25:52'),
(31, 'article-submission-contractors', 3, 'Article Submission Contractors', 'المقاولون تقديم المادة', 1, '2017-06-09 09:26:07', '2017-06-09 09:26:07'),
(32, 'calendar-management-assistants', 3, 'Calendar Management Assistants', 'مساعدو إدارة التقويم', 1, '2017-06-09 09:26:22', '2017-06-09 09:26:22'),
(33, 'google-sheets-experts', 3, 'Google Sheets Experts', 'غوغل شيتس إكسيرتس', 1, '2017-06-09 09:26:38', '2017-06-09 09:26:38'),
(34, 'customer-service-agents', 3, 'Customer Service Agents', 'وكلاء خدمة العملاء', 1, '2017-06-09 09:26:52', '2017-06-09 09:26:52'),
(35, 'internet-researchers', 3, 'Internet Researchers', 'باحثو الإنترنت', 1, '2017-06-09 09:27:07', '2017-06-09 09:27:07'),
(36, 'zendesk-freelancers', 3, 'Zendesk Freelancers', 'زنديسك لحسابهم الخاص', 1, '2017-06-09 09:27:23', '2017-06-09 09:27:23'),
(37, 'academic-writers', 4, 'Academic Writers', 'الكتاب الأكاديميون', 1, '2017-06-09 09:30:39', '2017-06-09 09:30:39'),
(38, 'french-writers-translators', 4, 'French Writers & Translators', 'الكتاب والمترجمون الفرنسيون', 1, '2017-06-09 09:30:58', '2017-06-09 09:30:58'),
(39, 'russian-writers-translators', 4, 'Russian Writers & Translators', 'الكتاب والمترجمون الروس', 1, '2017-06-09 09:31:20', '2017-06-09 09:31:20'),
(40, 'business-writers', 4, 'Business Writers', 'كتاب الأعمال', 1, '2017-06-09 09:32:31', '2017-06-09 09:32:31'),
(41, 'ghostwriters', 4, 'Ghostwriters', 'كاتب خفي', 1, '2017-06-09 09:34:24', '2017-06-09 09:34:24'),
(42, 'screenwriters', 4, 'Screenwriters', 'السيناريو', 1, '2017-06-09 09:34:44', '2017-06-09 09:34:44'),
(43, 'copy-editors', 4, 'Copy Editors', 'نسخ المحررين', 1, '2017-06-09 09:34:59', '2017-06-09 09:34:59'),
(44, 'journalists', 4, 'Journalists', 'الصحفيين', 1, '2017-06-09 09:35:14', '2017-06-09 09:35:14'),
(45, 'technical-writers', 4, 'Technical Writers', 'الكتاب الفنيون', 1, '2017-06-09 09:35:28', '2017-06-09 09:35:28'),
(46, 'creative-writers', 4, 'Creative Writers', 'الكتاب المبدعين', 1, '2017-06-09 09:35:52', '2017-06-09 09:35:52'),
(47, 'press-release-writers', 4, 'Press Release Writers', 'الكتاب الصحفيون', 1, '2017-06-09 09:36:08', '2017-06-09 09:36:08'),
(48, 'accounting-assistants', 5, 'Accounting Assistants', 'مساعدو المحاسبة', 1, '2017-06-09 09:36:41', '2017-06-09 09:36:41'),
(49, 'business-planning-analysts', 5, 'Business Planning Analysts', 'محللون تخطيط الأعمال', 1, '2017-06-09 09:37:00', '2017-06-09 09:37:00'),
(50, 'project-management-analysts', 5, 'Project Management Analysts', 'محللون إدارة المشاريع', 1, '2017-06-09 09:37:21', '2017-06-09 09:37:21'),
(51, 'accounts-payable-specialists', 5, 'Accounts Payable Specialists', 'خبراء الحسابات الدائنة', 1, '2017-06-09 09:37:41', '2017-06-09 09:37:41'),
(52, 'excel-experts', 5, 'Excel Experts', 'إكسيل الخبراء', 1, '2017-06-09 09:37:57', '2017-06-09 09:37:57'),
(53, 'recruiting-assistants', 5, 'Recruiting Assistants', 'مساعدو التوظيف', 1, '2017-06-09 09:38:17', '2017-06-09 09:38:17'),
(54, 'bookkeeping-assistants', 5, 'Bookkeeping Assistants', 'مساعدين مسك الدفاتر', 1, '2017-06-09 09:38:39', '2017-06-09 09:38:39'),
(55, 'financial-reporting-analysts', 5, 'Financial Reporting Analysts', 'محللون التقارير المالية', 1, '2017-06-09 09:38:57', '2017-06-09 09:38:57'),
(56, 'advertising-consultants', 6, 'Advertising Consultants', 'استشارات الإعلان', 1, '2017-06-09 09:54:37', '2017-06-09 09:54:37'),
(57, 'google-adwords-experts', 6, 'Google AdWords Experts', 'خبراء غوغل أدوردس', 1, '2017-06-09 09:54:54', '2017-06-09 09:54:54'),
(58, 'on-page-optimization-experts', 6, 'On-Page Optimization Experts', 'خبراء تحسين الصفحة', 1, '2017-06-09 09:55:10', '2017-06-09 09:55:10'),
(59, 'google-analytics-consultants', 6, 'Google Analytics Consultants', 'غوغل أناليتيكش كونسولتانتس', 1, '2017-06-09 09:55:30', '2017-06-09 09:55:30'),
(60, 'outbound-sales-consultants', 6, 'Outbound Sales Consultants', 'استشاريون المبيعات الصادرة', 1, '2017-06-09 09:55:45', '2017-06-09 09:55:45'),
(61, 'blog-writers', 6, 'Blog Writers', 'بلوق الكتاب', 1, '2017-06-09 09:56:00', '2017-06-09 09:56:00'),
(62, 'google-website-optimizer-consultant', 6, 'Google Website Optimizer Consultants', 'مستشارو محسن مواقع الويب من', 1, '2017-06-09 09:56:27', '2017-06-09 09:56:27'),
(63, 'brand-strategists', 6, 'Brand Strategists', 'استراتيجيات العلامة التجارية', 1, '2017-06-09 09:56:42', '2017-06-09 09:56:42'),
(64, 'internet-marketing-consultants', 6, 'Internet Marketing Consultants', 'استشارات التسويق عبر الإنترنت', 1, '2017-06-09 09:56:57', '2017-06-09 09:56:57'),
(65, 'sales-consultants', 6, 'Sales Consultants', 'استشارات المبيعات', 1, '2017-06-09 09:57:16', '2017-06-09 09:57:16'),
(66, 'cold-callers', 6, 'Cold Callers', 'المكالمات الباردة', 1, '2017-06-09 09:57:32', '2017-06-09 09:57:32'),
(67, 'lead-generation-specialists', 6, 'Lead Generation Specialists', 'المتخصصين في توليد الرصاص', 1, '2017-06-09 09:57:49', '2017-06-09 09:57:49'),
(68, 'link-builders', 6, 'Link Builders', 'رابط بناة', 1, '2017-06-09 09:58:04', '2017-06-09 09:58:04'),
(69, 'direct-marketers', 6, 'Direct Marketers', 'التسويق المباشر', 1, '2017-06-09 09:58:28', '2017-06-09 09:58:28'),
(70, 'market-researchers', 6, 'Market Researchers', 'باحثون في السوق', 1, '2017-06-09 09:58:43', '2017-06-09 09:58:43'),
(71, 'social-media-consultants', 6, 'Social Media Consultants', 'استشارات وسائل الإعلام', 1, '2017-06-09 09:58:57', '2017-06-09 09:58:57'),
(72, 'email-marketing-consultants', 6, 'Email Marketing Consultants', 'استشارات التسويق عبر البريد', 1, '2017-06-09 09:59:13', '2017-06-09 09:59:13'),
(73, 'marketing-strategists', 6, 'Marketing Strategists', 'استراتيجيات التسويق', 1, '2017-06-09 09:59:30', '2017-06-09 09:59:30'),
(74, 'telemarketers-see-more', 6, 'Telemarketers See More', 'الاتصالات الهاتفية', 1, '2017-06-09 09:59:44', '2017-06-09 09:59:44'),
(76, 'niraj', 1, 'niraj', 'qwertyuio', 1, '2017-07-13 06:59:10', '2017-07-13 06:59:10'),
(77, 'test', 26, 'test', 'test', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `subject` varchar(254) DEFAULT NULL,
  `message` text,
  `send_from` varchar(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `name`, `subject`, `message`, `send_from`, `created_at`, `updated_at`) VALUES
(1, 'Forgot Password', 'Reset Your Password', 'Hello [name],\r\n<br>\r\n<br>\r\nPlease click on below link for reset your password.\r\n[link]\r\n<br>\r\n<br>\r\n<br>\r\nThanks\r\n', 'info@laratest.com', '2018-12-04 05:16:17', '2018-12-04 05:16:17'),
(2, 'Signup Email', 'Welcome to Laratest', 'Hello [name],\r\n<br>\r\n<br>\r\nYou have successfully registered on Khubaraa.\r\n<br>\r\nYour login details are below:\r\n<br>\r\nEmail:[email]\r\n<br>\r\nPassword:[password]\r\n<br>\r\n<br>\r\n<br>\r\nThanks\r\n', 'info@laratest.com', '2018-12-04 05:16:17', '2018-12-04 05:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `forgot_password`
--

CREATE TABLE `forgot_password` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `token` text,
  `is_reset` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forgot_password`
--

INSERT INTO `forgot_password` (`id`, `user_id`, `token`, `is_reset`, `created_at`, `updated_at`) VALUES
(4, 1, '783deda43f1339cd1a3796805112ef56', 0, '2017-06-07 05:34:15', '2017-06-07 05:34:15'),
(5, 1, '783deda43f1339cd1a3796805112ef56', 0, '2017-06-07 06:58:46', '2017-06-07 06:58:46'),
(6, 1, '783deda43f1339cd1a3796805112ef56', 0, '2017-06-07 06:59:39', '2017-06-07 06:59:39'),
(7, 1, '783deda43f1339cd1a3796805112ef56', 0, '2017-06-07 07:00:42', '2017-06-07 07:00:42'),
(8, 1, '783deda43f1339cd1a3796805112ef56', 0, '2017-06-07 07:02:21', '2017-06-07 07:02:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE `timezones` (
  `id` int(11) NOT NULL,
  `zone` varchar(254) DEFAULT NULL,
  `name` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`id`, `zone`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Pacific/Midway', '(GMT-11:00) Midway Island', '2017-06-08 06:54:21', '2017-06-08 06:54:21'),
(2, 'Pacific/Samoa', '(GMT-11:00) Samoa', '2017-06-08 06:54:21', '2017-06-08 06:54:21'),
(3, 'Pacific/Honolulu', '(GMT-10:00) Hawaii', '2017-06-08 06:54:21', '2017-06-08 06:54:21'),
(4, 'US/Alaska', '(GMT-09:00) Alaska', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(5, 'America/Los_Angeles', '(GMT-08:00) Pacific Time (US &amp; Canada)', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(6, 'America/Tijuana', '(GMT-08:00) Tijuana', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(7, 'US/Arizona', '(GMT-07:00) Arizona', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(8, 'America/Chihuahua', '(GMT-07:00) Chihuahua', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(9, 'America/Chihuahua', '(GMT-07:00) La Paz', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(10, 'America/Mazatlan', '(GMT-07:00) Mazatlan', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(11, 'US/Mountain', '(GMT-07:00) Mountain Time (US &amp; Canada)', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(12, 'America/Managua', '(GMT-06:00) Central America', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(13, 'US/Central', '(GMT-06:00) Central Time (US &amp; Canada)', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(14, 'America/Mexico_City', '(GMT-06:00) Guadalajara', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(15, 'America/Mexico_City', '(GMT-06:00) Mexico City', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(16, 'America/Monterrey', '(GMT-06:00) Monterrey', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(17, 'Canada/Saskatchewan', '(GMT-06:00) Saskatchewan', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(18, 'America/Bogota', '(GMT-05:00) Bogota', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(19, 'US/Eastern', '(GMT-05:00) Eastern Time (US &amp; Canada)', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(20, 'US/East-Indiana', '(GMT-05:00) Indiana (East)', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(21, 'America/Lima', '(GMT-05:00) Lima', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(22, 'America/Bogota', '(GMT-05:00) Quito', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(23, 'Canada/Atlantic', '(GMT-04:00) Atlantic Time (Canada)', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(24, 'America/Caracas', '(GMT-04:30) Caracas', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(25, 'America/La_Paz', '(GMT-04:00) La Paz', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(26, 'America/Santiago', '(GMT-04:00) Santiago', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(27, 'Canada/Newfoundland', '(GMT-03:30) Newfoundland', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(28, 'America/Sao_Paulo', '(GMT-03:00) Brasilia', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(29, 'America/Argentina/Buenos_Aires', '(GMT-03:00) Buenos Aires', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(30, 'America/Argentina/Buenos_Aires', '(GMT-03:00) Georgetown', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(31, 'America/Godthab', '(GMT-03:00) Greenland', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(32, 'America/Noronha', '(GMT-02:00) Mid-Atlantic', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(33, 'Atlantic/Azores', '(GMT-01:00) Azores', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(34, 'Atlantic/Cape_Verde', '(GMT-01:00) Cape Verde Is.', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(35, 'Africa/Casablanca', '(GMT+00:00) Casablanca', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(36, 'Europe/London', '(GMT+00:00) Edinburgh', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(37, 'Etc/Greenwich', '(GMT+00:00) Greenwich Mean Time : Dublin', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(38, 'Europe/Lisbon', '(GMT+00:00) Lisbon', '2017-06-08 06:54:22', '2017-06-08 06:54:22'),
(39, 'Europe/London', '(GMT+00:00) London', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(40, 'Africa/Monrovia', '(GMT+00:00) Monrovia', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(41, 'UTC', '(GMT+00:00) UTC', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(42, 'Europe/Amsterdam', '(GMT+01:00) Amsterdam', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(43, 'Europe/Belgrade', '(GMT+01:00) Belgrade', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(44, 'Europe/Berlin', '(GMT+01:00) Berlin', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(45, 'Europe/Berlin', '(GMT+01:00) Bern', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(46, 'Europe/Bratislava', '(GMT+01:00) Bratislava', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(47, 'Europe/Brussels', '(GMT+01:00) Brussels', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(48, 'Europe/Budapest', '(GMT+01:00) Budapest', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(49, 'Europe/Copenhagen', '(GMT+01:00) Copenhagen', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(50, 'Europe/Ljubljana', '(GMT+01:00) Ljubljana', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(51, 'Europe/Madrid', '(GMT+01:00) Madrid', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(52, 'Europe/Paris', '(GMT+01:00) Paris', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(53, 'Europe/Prague', '(GMT+01:00) Prague', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(54, 'Europe/Rome', '(GMT+01:00) Rome', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(55, 'Europe/Sarajevo', '(GMT+01:00) Sarajevo', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(56, 'Europe/Skopje', '(GMT+01:00) Skopje', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(57, 'Europe/Stockholm', '(GMT+01:00) Stockholm', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(58, 'Europe/Vienna', '(GMT+01:00) Vienna', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(59, 'Europe/Warsaw', '(GMT+01:00) Warsaw', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(60, 'Africa/Lagos', '(GMT+01:00) West Central Africa', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(61, 'Europe/Zagreb', '(GMT+01:00) Zagreb', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(62, 'Europe/Athens', '(GMT+02:00) Athens', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(63, 'Europe/Bucharest', '(GMT+02:00) Bucharest', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(64, 'Africa/Cairo', '(GMT+02:00) Cairo', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(65, 'Africa/Harare', '(GMT+02:00) Harare', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(66, 'Europe/Helsinki', '(GMT+02:00) Helsinki', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(67, 'Europe/Istanbul', '(GMT+02:00) Istanbul', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(68, 'Asia/Jerusalem', '(GMT+02:00) Jerusalem', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(69, 'Europe/Helsinki', '(GMT+02:00) Kyiv', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(70, 'Africa/Johannesburg', '(GMT+02:00) Pretoria', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(71, 'Europe/Riga', '(GMT+02:00) Riga', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(72, 'Europe/Sofia', '(GMT+02:00) Sofia', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(73, 'Europe/Tallinn', '(GMT+02:00) Tallinn', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(74, 'Europe/Vilnius', '(GMT+02:00) Vilnius', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(75, 'Asia/Baghdad', '(GMT+03:00) Baghdad', '2017-06-08 06:54:23', '2017-06-08 06:54:23'),
(76, 'Asia/Kuwait', '(GMT+03:00) Kuwait', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(77, 'Europe/Minsk', '(GMT+03:00) Minsk', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(78, 'Africa/Nairobi', '(GMT+03:00) Nairobi', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(79, 'Asia/Riyadh', '(GMT+03:00) Riyadh', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(80, 'Europe/Volgograd', '(GMT+03:00) Volgograd', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(81, 'Asia/Tehran', '(GMT+03:30) Tehran', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(82, 'Asia/Muscat', '(GMT+04:00) Abu Dhabi', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(83, 'Asia/Baku', '(GMT+04:00) Baku', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(84, 'Europe/Moscow', '(GMT+04:00) Moscow', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(85, 'Asia/Muscat', '(GMT+04:00) Muscat', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(86, 'Europe/Moscow', '(GMT+04:00) St. Petersburg', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(87, 'Asia/Tbilisi', '(GMT+04:00) Tbilisi', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(88, 'Asia/Yerevan', '(GMT+04:00) Yerevan', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(89, 'Asia/Kabul', '(GMT+04:30) Kabul', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(90, 'Asia/Karachi', '(GMT+05:00) Islamabad', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(91, 'Asia/Karachi', '(GMT+05:00) Karachi', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(92, 'Asia/Tashkent', '(GMT+05:00) Tashkent', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(93, 'Asia/Calcutta', '(GMT+05:30) Chennai', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(94, 'Asia/Kolkata', '(GMT+05:30) Kolkata', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(95, 'Asia/Calcutta', '(GMT+05:30) Mumbai', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(96, 'Asia/Calcutta', '(GMT+05:30) New Delhi', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(97, 'Asia/Calcutta', '(GMT+05:30) Sri Jayawardenepura', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(98, 'Asia/Katmandu', '(GMT+05:45) Kathmandu', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(99, 'Asia/Almaty', '(GMT+06:00) Almaty', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(100, 'Asia/Dhaka', '(GMT+06:00) Astana', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(101, 'Asia/Dhaka', '(GMT+06:00) Dhaka', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(102, 'Asia/Yekaterinburg', '(GMT+06:00) Ekaterinburg', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(103, 'Asia/Rangoon', '(GMT+06:30) Rangoon', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(104, 'Asia/Bangkok', '(GMT+07:00) Bangkok', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(105, 'Asia/Bangkok', '(GMT+07:00) Hanoi', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(106, 'Asia/Jakarta', '(GMT+07:00) Jakarta', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(107, 'Asia/Novosibirsk', '(GMT+07:00) Novosibirsk', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(108, 'Asia/Hong_Kong', '(GMT+08:00) Beijing', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(109, 'Asia/Chongqing', '(GMT+08:00) Chongqing', '2017-06-08 06:54:24', '2017-06-08 06:54:24'),
(110, 'Asia/Hong_Kong', '(GMT+08:00) Hong Kong', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(111, 'Asia/Krasnoyarsk', '(GMT+08:00) Krasnoyarsk', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(112, 'Asia/Kuala_Lumpur', '(GMT+08:00) Kuala Lumpur', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(113, 'Australia/Perth', '(GMT+08:00) Perth', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(114, 'Asia/Singapore', '(GMT+08:00) Singapore', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(115, 'Asia/Taipei', '(GMT+08:00) Taipei', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(116, 'Asia/Ulan_Bator', '(GMT+08:00) Ulaan Bataar', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(117, 'Asia/Urumqi', '(GMT+08:00) Urumqi', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(118, 'Asia/Irkutsk', '(GMT+09:00) Irkutsk', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(119, 'Asia/Tokyo', '(GMT+09:00) Osaka', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(120, 'Asia/Tokyo', '(GMT+09:00) Sapporo', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(121, 'Asia/Seoul', '(GMT+09:00) Seoul', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(122, 'Asia/Tokyo', '(GMT+09:00) Tokyo', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(123, 'Australia/Adelaide', '(GMT+09:30) Adelaide', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(124, 'Australia/Darwin', '(GMT+09:30) Darwin', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(125, 'Australia/Brisbane', '(GMT+10:00) Brisbane', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(126, 'Australia/Canberra', '(GMT+10:00) Canberra', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(127, 'Pacific/Guam', '(GMT+10:00) Guam', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(128, 'Australia/Hobart', '(GMT+10:00) Hobart', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(129, 'Australia/Melbourne', '(GMT+10:00) Melbourne', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(130, 'Pacific/Port_Moresby', '(GMT+10:00) Port Moresby', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(131, 'Australia/Sydney', '(GMT+10:00) Sydney', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(132, 'Asia/Yakutsk', '(GMT+10:00) Yakutsk', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(133, 'Asia/Vladivostok', '(GMT+11:00) Vladivostok', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(134, 'Pacific/Auckland', '(GMT+12:00) Auckland', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(135, 'Pacific/Fiji', '(GMT+12:00) Fiji', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(136, 'Pacific/Kwajalein', '(GMT+12:00) International Date Line West', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(137, 'Asia/Kamchatka', '(GMT+12:00) Kamchatka', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(138, 'Asia/Magadan', '(GMT+12:00) Magadan', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(139, 'Pacific/Fiji', '(GMT+12:00) Marshall Is.', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(140, 'Asia/Magadan', '(GMT+12:00) New Caledonia', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(141, 'Asia/Magadan', '(GMT+12:00) Solomon Is.', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(142, 'Pacific/Auckland', '(GMT+12:00) Wellington', '2017-06-08 06:54:25', '2017-06-08 06:54:25'),
(143, 'Pacific/Tongatapu', '(GMT+13:00) Nuku\'alofa', '2017-06-08 06:54:25', '2017-06-08 06:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `timezone_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usertype` enum('client','freelancer') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'freelancer',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `online_status` tinyint(1) NOT NULL DEFAULT '1',
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `job_title` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hourly_rate` float DEFAULT NULL,
  `location` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `skills` text COLLATE utf8mb4_unicode_ci,
  `available` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_in_week` int(11) DEFAULT NULL,
  `available_from_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `slug`, `username`, `name`, `email`, `password`, `profile_image`, `address`, `phone`, `timezone_id`, `remember_token`, `usertype`, `status`, `online_status`, `category_id`, `subcategory_id`, `job_title`, `hourly_rate`, `location`, `description`, `skills`, `available`, `available_in_week`, `available_from_date`, `created_at`, `updated_at`) VALUES
(1, 'azadbharti26', 'azadbharti26', 'Azad Bharti', 'azadbharti26@gmail.com', '$2y$10$T1pCNUrK6XgYaeuXtts58uYAg6jWRx2FhbRxoWBVIhmVnl6oOg3P.', '1500371749Penguins.jpg', 'test', '689895656', 13, 'knzhNONp7Vcd4a5pkMR4SNZrhzrBOM4PrmCrOVbKxyvNC621sxLNvJuzIDVq', 'client', 1, 0, 2, 16, 'test title', 80, 'Jaipur', 'test', NULL, 'Yes', 1, '1970-01-01', '2018-12-04 13:42:44', '2018-12-04 13:42:44'),
(2, 'azadbharti32', 'azadbharti32', 'Azad G', 'azadbharti32@gmail.com', '$2y$10$F7tlgAprp5Va1NgkOBq37eNWDGNvRcLqCeqeq9Qczyz1YEVlhIXn.', '1500010284Koala.jpg', 'test', '95656565', 11, 'b8I2WYY82hes8Np2e24GvBkKG17s1pvUTq3pOzoAHULGeWki9wYbEqilKji9', 'freelancer', 1, 1, 1, 7, 'tet', 10, 'Jaipur', '', NULL, 'No', NULL, '2017-07-22', '2018-12-04 13:42:44', '2018-12-04 13:42:44'),
(3, 'azadbharti261', 'azadbharti261', 'Azad Bharti G', 'azadbharti261@gmail.com', '$2y$10$T1pCNUrK6XgYaeuXtts58uYAg6jWRx2FhbRxoWBVIhmVnl6oOg3P.', '1500371749Penguins.jpg', 'test', '689895656', 13, NULL, 'client', 1, 0, 2, 16, 'test title', 80, 'Jaipur', 'test', NULL, 'Yes', 1, '1970-01-01', '2018-12-04 13:42:44', '2018-12-04 13:42:44'),
(4, 'azadbharti322', 'azadbharti321', 'Azad G', 'azadbharti322@gmail.com', '$2y$10$F7tlgAprp5Va1NgkOBq37eNWDGNvRcLqCeqeq9Qczyz1YEVlhIXn.', '1500010284Koala.jpg', 'test', '95656565', 11, NULL, 'freelancer', 1, 1, 1, 8, 'tet', 10, 'Jaipur', '', NULL, 'No', NULL, '2017-07-22', '2018-12-04 13:42:44', '2018-12-04 13:42:44'),
(9, 'azadbharti321', 'azadbharti321', 'Azad 1', 'azadbharti321@gmail.com', '$2y$10$F7tlgAprp5Va1NgkOBq37eNWDGNvRcLqCeqeq9Qczyz1YEVlhIXn.', '1500010284Koala.jpg', 'test', '95656565', 11, NULL, 'freelancer', 1, 1, 1, 8, 'tet', 10, 'Jaipur', '', NULL, 'No', NULL, '2017-07-22', '2018-12-04 13:42:44', '2018-12-04 13:42:44'),
(10, 'azadbharti3222', 'azadbharti3212', 'Azad G 2', 'azadbharti3221@gmail.com', '$2y$10$F7tlgAprp5Va1NgkOBq37eNWDGNvRcLqCeqeq9Qczyz1YEVlhIXn.', '1500010284Koala.jpg', 'test', '95656565', 11, NULL, 'freelancer', 1, 1, 1, 8, 'tet', 10, 'Jaipur', '', NULL, 'No', NULL, '2017-07-22', '2018-12-04 13:42:44', '2018-12-04 13:42:44'),
(11, 'azadbharti263', 'azadbharti263', 'Azad Bharti 3', 'azadbharti263@gmail.com', '$2y$10$T1pCNUrK6XgYaeuXtts58uYAg6jWRx2FhbRxoWBVIhmVnl6oOg3P.', '1500371749Penguins.jpg', 'test', '689895656', 13, NULL, 'client', 1, 0, 2, 16, 'test title', 80, 'Jaipur', 'test', NULL, 'Yes', 1, '1970-01-01', '2018-12-04 13:42:44', '2018-12-04 13:42:44'),
(12, 'azadbharti2614', 'azadbharti2614', 'Azad Bharti G 4', 'azadbharti2614@gmail.com', '$2y$10$T1pCNUrK6XgYaeuXtts58uYAg6jWRx2FhbRxoWBVIhmVnl6oOg3P.', '1500371749Penguins.jpg', 'test', '689895656', 13, NULL, 'client', 1, 0, 2, 16, 'test title', 80, 'Jaipur', 'test', NULL, 'Yes', 1, '1970-01-01', '2018-12-04 13:42:44', '2018-12-04 13:42:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgot_password`
--
ALTER TABLE `forgot_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `forgot_password`
--
ALTER TABLE `forgot_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `timezones`
--
ALTER TABLE `timezones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
