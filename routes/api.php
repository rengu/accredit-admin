<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'WebserviceController@login');
Route::any('login1', 'WebserviceController@login1');

Route::any('get-myinfo-data', 'WebserviceController@getMyInfoData');
Route::any('getuser-type', 'WebserviceController@getUserType');
Route::any('site-issue-type-list', 'WebserviceController@siteIssueTypeList');
Route::any('surveillance-issue-type-list', 'WebserviceController@surveillanceIssueTypeList');
Route::any('preventive-type-list', 'WebserviceController@preventiveTypeList');
Route::any('save-preventive', 'WebserviceController@savePreventive');
Route::post('forgotpassword', 'WebserviceController@forgotPassword');
Route::post('save-logbook', 'WebserviceController@saveLogbook');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::post('user', 'WebserviceController@getUser');
    Route::post('rolePermission', 'WebserviceController@rolePermission');
    Route::post('change-password', 'WebserviceController@changePassword');
	Route::post('location-app', 'WebserviceController@locationApp');
    Route::post('edit-profile', 'WebserviceController@editProfile');
    Route::post('all-sites', 'WebserviceController@allSites');
    Route::post('save-search', 'WebserviceController@saveSearch');
    Route::post('recent-search', 'WebserviceController@recentSearch');
    Route::post('save-site-estate-issue', 'WebserviceController@saveSiteIssue');
    Route::post('site-issue-tracker', 'WebserviceController@siteIssueTracker');
    Route::post('site-issue-status-change', 'WebserviceController@siteIssueStatusChange');
    Route::post('save-surveillance-issue', 'WebserviceController@saveSurveillanceIssue');
    Route::post('surveillance-tracker', 'WebserviceController@surveillanceIssueTracker');
    Route::post('surveillance-status-change', 'WebserviceController@surveillanceIssueStatusChange');
    Route::post('add-user', 'WebserviceController@addUser');
    Route::post('edit-user', 'WebserviceController@editUser');
    Route::post('manage-users', 'WebserviceController@manageUsers');
    Route::post('search-site', 'WebserviceController@searchSite');
    Route::post('site-users', 'WebserviceController@siteUsers');
    Route::post('save-message', 'WebserviceController@saveMessage');
    Route::post('messages', 'WebserviceController@messages');
    Route::post('bpv-approvar', 'WebserviceController@bpvApprovar');
    Route::post('change-beat-value-approve-status', 'WebserviceController@changeBeatValueApproveStatus');
    Route::any('get-old-diesel-balance', 'WebserviceController@getOldDieselBalance');
    Route::any('update-fcm-token', 'WebserviceController@updateFcmToken');
});

URL::forceScheme('https');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
