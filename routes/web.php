<?php

use Illuminate\Support\Facades\Config;
//localhost
// define('SITE_URLI', $_SERVER['DOCUMENT_ROOT'].'/accredit-admin');
//if (defined('SITE_URLI')) {
  //  echo SITE_URLI;
//}

if (!defined('SITE_URLI'))
define('SITE_URLI', base_path());

if (!defined('SITE_URL'))
 define('SITE_URL','https://mobile.sivren.org/'.Config::get('app.name'));
//server



// define('SITE_URLI', $_SERVER['DOCUMENT_ROOT']);
// define('SITE_URL',Config::get('app.url'));

if (!defined('loanApproved'))
define('loanApproved','Congratulation your loan [loan_no] is approved by Accredit');

if (!defined('loanUnApproved'))
define('loanUnApproved','Your loan [loan_no] is un-approved by Accredit');

if (!defined('loanActive'))
define('loanActive','Your loan [loan_no] is activated by Accredit');

if (!defined('loanInActive'))
define('loanInActive','Your loan [loan_no] is in-activated by Accredit');

if (!defined('loanPayment'))
define('loanPayment','Your loan [loan_no] [installment_name] Payment [emi] Recieved');

if (!defined('installmentDue'))
define('installmentDue','Your [installment_name] is due on DATE - : [date]');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/************* Set Admin URL ***************************/
Route::any('/admin/login', 'Admin\AdminController@login');
Route::any('/', 'Admin\AdminController@login');
Route::any('admin/logout', 'Admin\AdminController@logout');
Route::any('admin/dashboard', 'Admin\AdminController@dashboard');
Route::any('admin/editProfile', array('as' => 'admin.editProfile', 'uses' => 'Admin\AdminController@editProfile'));
Route::any('admin/setting', array('as' => 'admin.setting', 'uses' => 'Admin\AdminController@setting'));
Route::any('admin/changePassword', array('as' => 'admin.changePassword', 'uses' => 'Admin\AdminController@changePassword'));
Route::any('admin/forgotPassword', 'Admin\AdminController@forgotPassword');
Route::any('admin/reset-password/{id}/{ids}', 'Admin\AdminController@resetPassword');
Route::any('admin/reset-password-create', 'Admin\AdminController@resetpasswordCreate');
Route::get('/captcha', 'HomeController@showCapcha');

// admin users routes
Route::any('/admin/users/activate/{id}', 'Admin\UsersController@activate');
Route::any('/admin/users/deactivate/{id}', 'Admin\UsersController@deactivate');
Route::any('/admin/users/delete/{id}', array('as' => 'users.delete', 'uses' => 'Admin\UsersController@delete'));
Route::any('/admin/users/edit/{id}', 'Admin\UsersController@edit');
Route::any('/admin/users/change_password/{id}', 'Admin\UsersController@change_password');
Route::any('/admin/users/index', array('as' => 'users.users', 'uses' => 'Admin\UsersController@index'));
Route::any('/admin/users/index/{role_id}', array('as' => 'users.users', 'uses' => 'Admin\UsersController@index'));

Route::any('/admin/users/add', 'Admin\UsersController@add');
Route::any('/admin/users/download-sample-excel', 'Admin\UsersController@downloadSampleExcel');
Route::any('admin/users/upload-users', 'Admin\UsersController@uploadUsers');
Route::any('/admin/users/view/{id}', 'Admin\UsersController@view');
Route::any('/admin/users/session_delete/{id}', 'Admin\UsersController@session_delete');
Route::any('/admin/users/admin', array('as' => 'users.view', 'uses' => 'Admin\UsersController@admin'));

// Backend user Management
Route::any('/admin/adminuser/index', array('as' => 'admin.users', 'uses' => 'Admin\AdminusersController@index'));
Route::any('/admin/adminuser/add', array('as' => 'admin.add', 'uses' => 'Admin\AdminusersController@add'));
Route::any('/admin/adminuser/activate/{id}', 'Admin\AdminusersController@activate');
Route::any('/admin/adminuser/deactivate/{id}', 'Admin\AdminusersController@deactivate');
Route::any('/admin/adminuser/delete/{id}', array('as' => 'admin.delete', 'uses' => 'Admin\AdminusersController@delete'));
Route::any('/admin/adminuser/edit/{id}', array('as' => 'admin.edit', 'uses' => 'Admin\AdminusersController@edit'));
Route::any('/admin/adminuser/change_password/{id}',  array('as' => 'admin.change_password', 'uses' => 'Admin\AdminusersController@change_password'));
Route::any('/admin/adminuser/role', array('as' => 'admin.role', 'uses' => 'Admin\AdminusersController@role'));

Route::any('/admin/adminuser/assignRoles/{id}','Admin\AdminusersController@assignRoles');

//Route::any('/admin/users/admin','Admin\UsersController@admin');
Route::any('/admin/question/index','Admin\QuestionsController@index');
Route::any('/admin/question/add','Admin\QuestionsController@create');
Route::any('/admin/question/edit/{id}','Admin\QuestionsController@edit');
Route::any('/admin/question/delete/{id}','Admin\QuestionsController@delete');
// admin loan applications

Route::any('/admin/loans/individualLoanEnquiries', array('as' => 'loans.individualLoanEnquiries', 'uses' => 'Admin\LoansController@individualLoanEnquiries'));
Route::any('/admin/loans/deactivate/{id}','Admin\LoansController@deactivate');
Route::any('/admin/loans/activate/{id}','Admin\LoansController@activate');
Route::any('/admin/loans/delete/{id}','Admin\LoansController@delete');
Route::any('/admin/loans/viewIndividual/{id}','Admin\LoansController@viewIndividual');
Route::any('/admin/loans/approved/{id}','Admin\LoansController@approved');
Route::any('/admin/loans/unapproved/{id}','Admin\LoansController@unapproved');
Route::any('/admin/loans/active/{id}','Admin\LoansController@active');
Route::any('/admin/loans/inactive/{id}','Admin\LoansController@inactive');
Route::any('/admin/loans/payments/{id}','Admin\LoansController@payments');
Route::any('/admin/loans/completed/{id}','Admin\LoansController@completed');
Route::any('/admin/loans/incompleted/{id}','Admin\LoansController@incompleted');
Route::any('/admin/loans/loanCompleted/{id}','Admin\LoansController@loanCompleted');
Route::any('/admin/loans/loanInCompleted/{id}','Admin\LoansController@loanInCompleted');

Route::any('/admin/loans/companyLoanEnquiries', array('as' => 'loans.companyLoanEnquiries', 'uses' => 'Admin\LoansController@companyLoanEnquiries'));
Route::any('/admin/loans/cdeactivate/{id}','Admin\LoansController@cdeactivate');
Route::any('/admin/loans/cactivate/{id}','Admin\LoansController@cactivate');
Route::any('/admin/loans/cdelete/{id}','Admin\LoansController@cdelete');
Route::any('/admin/loans/viewCompany/{id}','Admin\LoansController@viewCompany');
Route::any('/admin/loans/capproved/{id}','Admin\LoansController@capproved');
Route::any('/admin/loans/cunapproved/{id}','Admin\LoansController@cunapproved');

// admin CMS
Route::any('/admin/cms/index', array('as' => 'admin.cms', 'uses' => 'Admin\CmsController@index'));
Route::any('/admin/cms/about', array('as' => 'cms.about', 'uses' => 'Admin\CmsController@about'));
Route::any('/admin/cms/add', array('as' => 'cms.add', 'uses' => 'Admin\CmsController@add'));
Route::any('/admin/cms/delete/{id}',array('as' => 'cms.delete', 'uses' => 'Admin\CmsController@delete'));
Route::any('/admin/cms/activate/{id}', 'Admin\CmsController@activate');
Route::any('/admin/cms/deactivate/{id}', 'Admin\CmsController@deactivate');
Route::any('/admin/cms/edit/{id}', array('as' => 'cms.edit', 'uses' => 'Admin\CmsController@edit'));

// admin documents
Route::any('/admin/documents','Admin\DocumentsController@index');
Route::any('/admin/documents/add', array('as' => 'documents.add', 'uses' => 'Admin\DocumentsController@add'));
Route::any('/admin/documents/delete', array('as' => 'documents.add', 'uses' => 'Admin\DocumentsController@delete'));
Route::any('/admin/documents/activate/{id}', array('as' => 'documents.activate', 'uses' => 'Admin\DocumentsController@activate'));
Route::any('/admin/documents/deactivate/{id}', array('as' => 'documents.activate', 'uses' => 'Admin\DocumentsController@deactivate'));
Route::any('/admin/documents/delete/{id}', array('as' => 'documents.activate', 'uses' => 'Admin\DocumentsController@delete'));
Route::any('/admin/documents/view/{id}', array('as' => 'documents.activate', 'uses' => 'Admin\DocumentsController@view'));

//admin qr codes
Route::any('/admin/QrCodes','Admin\QrCodesController@index');
Route::any('/admin/QrCodes/generateQrCode','Admin\QrCodesController@generateQrCode');



// master management
// admin CMS
Route::any('/admin/master/index', array('as' => 'master.index', 'uses' => 'Admin\MasterController@index'));
Route::any('/admin/master/activate/{id}', 'Admin\MasterController@activate');
Route::any('/admin/master/deactivate/{id}', 'Admin\MasterController@deactivate');
Route::any('/admin/master/FaqCategories', array('as' => 'master.FaqCategories', 'uses' => 'Admin\MasterController@FaqCategories'));
Route::any('/admin/master/required/{id}', 'Admin\MasterController@required');
Route::any('/admin/master/optional/{id}', 'Admin\MasterController@optional');
Route::any('/admin/master/idType', array('as' => 'master.idType', 'uses' => 'Admin\MasterController@idType'));
Route::any('/admin/master/educationlevel', array('as' => 'master.educationlevel', 'uses' => 'Admin\MasterController@educationlevel'));
Route::any('/admin/master/businessType', array('as' => 'master.businessType', 'uses' => 'Admin\MasterController@businessType'));
Route::any('/admin/master/knowAccredit', array('as' => 'master.knowAccredit', 'uses' => 'Admin\MasterController@knowAccredit'));
Route::any('/admin/master/loanType', array('as' => 'master.loanType', 'uses' => 'Admin\MasterController@loanType'));
Route::any('/admin/master/loanPurpose', array('as' => 'master.loanPurpose', 'uses' => 'Admin\MasterController@loanPurpose'));
Route::any('/admin/master/residentialType', array('as' => 'master.residentialType', 'uses' => 'Admin\MasterController@residentialType'));
Route::any('/admin/master/propertyOwnership', array('as' => 'master.propertyOwnership', 'uses' => 'Admin\MasterController@propertyOwnership'));
Route::any('/admin/master/employmentStatus', array('as' => 'master.employmentStatus', 'uses' => 'Admin\MasterController@employmentStatus'));
Route::any('/admin/master/jobTitle', array('as' => 'master.jobTitle', 'uses' => 'Admin\MasterController@jobTitle'));
Route::any('/admin/master/specialization', array('as' => 'master.specialization', 'uses' => 'Admin\MasterController@specialization'));
Route::any('/admin/master/officeBranch', array('as' => 'master.officeBranch', 'uses' => 'Admin\MasterController@officeBranch'));
Route::any('/admin/master/bank', array('as' => 'master.bank', 'uses' => 'Admin\MasterController@bank'));
Route::any('/admin/master/ReasonForLoan', array('as' => 'master.ReasonForLoan', 'uses' => 'Admin\MasterController@ReasonForLoan'));

//Payments
Route::any('/admin/Reports/paymentReport', array('as' => 'paymentReport.paymentReport', 'uses' => 'Admin\ReportsController@paymentReport'));
Route::any('/admin/Reports/downloadReports', array('as' => 'paymentReport.downloadReports', 'uses' => 'Admin\ReportsController@downloadReports'));

//Route::any('admin/Reports/paymentReport','Admin\ReportsController@paymentReport');



//webservices


Route::any('/Webservice/checkdb', 'WebserviceController@checkDB');
Route::any('/Webservice/registertest', 'WebserviceController@registertest');
Route::any('/Webservice/logintest', 'WebserviceController@logintest');
Route::any('/Webservice/book', 'WebserviceController@book');
Route::any('/Webservice/getdummydata', 'WebserviceController@bookAuth');
Route::any('/Webservice/user', 'WebserviceController@getAuthenticatedUserTest')->middleware('jwt.auth');
Route::any('/Webservice/login','WebserviceController@login');
Route::any('/Webservice/sendOtp','WebserviceController@sendOtp');
Route::any('/Webservice/getFormFields','WebserviceController@getFormFields');
Route::any('/Webservice/registration','WebserviceController@registration');
Route::any('/Webservice/getMyInfo','WebserviceController@getMyInfo');

Route::any('/Webservice/forgotpassword','WebserviceController@forgotpassword');

Route::any('/Webservice/getIdType','WebserviceController@getIdType');
Route::any('/Webservice/getEducationLevel','WebserviceController@getEducationLevel');
Route::any('/Webservice/getBusinessType','WebserviceController@getBusinessType');
Route::any('/Webservice/getKnowAccredit','WebserviceController@getKnowAccredit');
Route::any('/Webservice/getLoanType','WebserviceController@getLoanType');
Route::any('/Webservice/getLoanPurpose','WebserviceController@getLoanPurpose');
Route::any('/Webservice/getResidentialType','WebserviceController@getResidentialType');
Route::any('/Webservice/getEmploymentStatus','WebserviceController@getEmploymentStatus');
Route::any('/Webservice/getJobTitle','WebserviceController@getJobTitle');
Route::any('/Webservice/getSpecilization','WebserviceController@getSpecilization');
Route::any('/Webservice/getOfficeBranch','WebserviceController@getOfficeBranch');
Route::any('/Webservice/getAdminDocument','WebserviceController@getAdminDocument');
Route::any('/Webservice/getAboutContent','WebserviceController@getAboutContent');
Route::any('/Webservice/verifyOtp','WebserviceController@verifyOtp');
Route::any('/Webservice/resendOtp','WebserviceController@resendOtp');
Route::any('/Webservice/getFaq','WebserviceController@getFaq');
Route::any('/Webservice/loanCalculator','WebserviceController@loanCalculator');
Route::any('/Webservice/pmt','WebserviceController@pmt');
Route::any('/Webservice/changePassword','WebserviceController@changePassword');
Route::any('/Webservice/checkEmail','WebserviceController@checkEmail');
Route::any('/Webservice/checkPhone','WebserviceController@checkPhone');
Route::any('/Webservice/checkUserDuplicate','WebserviceController@checkUserDuplicate');
Route::any('/Webservice/updateProfile','WebserviceController@updateProfile');
Route::any('/Webservice/downloadPDF','WebserviceController@downloadPDF');
Route::any('/Webservice/downloadBulkPDF','WebserviceController@downloadBulkPDF');
Route::any('/Webservice/getNotifications','WebserviceController@getNotifications');
Route::any('/Webservice/updateNotification','WebserviceController@updateNotification');
Route::any('/Webservice/getFrontQRCode','WebserviceController@getFrontQRCode');

Route::middleware(['jwt.auth'])->group(function () {

	Route::any('/Webservice/applyLoanIndividual','WebserviceController@applyLoanIndividual');
	Route::any('/Webservice/applyLoanCompany','WebserviceController@applyLoanCompany');  
	Route::any('/Webservice/getPaymentScheduleList','WebserviceController@getPaymentScheduleListNew');
	Route::any('/Webservice/getPaymentScheduleDetails','WebserviceController@getPaymentScheduleDetails');
	Route::any('/Webservice/getRePaymentScheduleList','WebserviceController@getRePaymentScheduleList');
	Route::any('/Webservice/getRePaymentScheduleDetails','WebserviceController@getRePaymentScheduleDetails');
});

Route::any('/Cron/reminder','CronController@reminder');
// Set Front End
//Route::any('/', 'HomeController@index');
Route::get('/changelocate/{locate}', 'TranslationController@changeLocale');


Route::any('reset-password/{hashstring1?}/{hashstring2?}', 'UserController@resetpassword');
Route::any('reset-password-create', 'UserController@resetpasswordCreate');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
	Artisan::call('config:clear');
	// Artisan::call('config:cache'); 
	Artisan::call('view:clear');
	return "Cache is cleared";
});


$questionType=[1=>'Single choice',2=>'Multiple Choice',3=>'Text Box'];

if (!defined('questionType'))
 define('questionType', $questionType);

// define variable
if (!defined('CURR'))
define('CURR','$');

if (!defined('TITLE_FOR_SITE'))
define('TITLE_FOR_SITE','Accredit');

if (!defined('SITE_TITLE'))
define('SITE_TITLE','Accredit');

if (!defined('TITLE_FOR_PAGES'))
define('TITLE_FOR_PAGES','Accredit :: ');

if (!defined('HTTP_PATH'))
define('HTTP_PATH','https://mobile.sivren.org/');



//  * ******************  user images ************************ 
if (!defined('UPLOAD_FULL_USER_IMAGE_PATH'))
define('UPLOAD_FULL_USER_IMAGE_PATH', 'public/uploads/users/');

if (!defined('DISPLAY_FULL_USER_IMAGE_PATH'))
define('DISPLAY_FULL_USER_IMAGE_PATH', 'public/uploads/users/');




// * ******************  Document ************************ 
if (!defined('UPLOAD_FULL_DOCUMENT_PATH'))
define('UPLOAD_FULL_DOCUMENT_PATH', 'public/uploads/documents/');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//****** smtp host **********
if (!defined('SMTP_HOST'))
define('SMTP_HOST', 'smtp.gmail.com');

if (!defined('SMTP_USERNAME'))
define('SMTP_USERNAME', 'info@accreditloan.com');

if (!defined('SMTP_PASSWORD'))
define('SMTP_PASSWORD', 'Accreditloan!');

if (!defined('SMTP_SECURE'))
define('SMTP_SECURE', 'tls');

if (!defined('PORT'))
define('PORT', '587');

URL::forceScheme('https');

global $years;
$years = array();
for($i = 1950;$i<= date('Y');$i++)
    $years["$i"] = $i;
