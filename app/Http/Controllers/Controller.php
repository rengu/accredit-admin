<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   

    // create unique slug
    public function createUniqueSlug($string = null, $model = null) {
        $string = substr(strtolower($string), 0, 35);
        $old_pattern = array("/[^a-zA-Z0-9]/", "/_+/", "/_$/");
        $new_pattern = array("-", "-", "");
        $string = strtolower(preg_replace($old_pattern, $new_pattern, $string));
        $findData = DB::table($model)
                ->where('slug', $string)
                ->orderBy('id', 'desc')
                ->first();
        if (!empty($findData)) {
            $uniqueSlug = $string . '-' . time();
        } else {
            $uniqueSlug = $string;
        }
        return $uniqueSlug;
    }
}
