<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ForgotPassword;
use App\Models\EmailTemplates;
use Illuminate\Http\Request;
use Response;
use Illuminate\Notifications\Notifiable;
use Redirect;
use Validator;
use Hash;
use Auth;
use Mail;
use Input;
use DB;
use Cookie;
use JWTAuth;
use JWTAuthException;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;


class WebserviceController extends Controller
{
	public function __construct(){
	}

	/***************** get request from json a *****************/
	public function getJson($jData){
		$data = json_decode($jData,true);
		if(isset($data) && !empty($data)){
			$_REQUEST = $data;
		} else {
			$data = $_POST;
		}
		return $data;
	}

	/***************** null value filter from array *****************/
	public function filterNullValueFromArray($arr){
		$resultArr = array();
		foreach($arr as $key=>$val){
			if($val===NULL){
				$resultArr[$key] = '';
			} else {
				$resultArr[$key] = $val;
			}
		}
		return $resultArr;
	}
	
	/************** Login user ***********************/
	public function login(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$email = $data['email'];
		$password = $data['password'];
		// check login with username password
		$credentials = array('email'=>$email, 'password'=>$password);
		$token = null;
		try {
		   if (!$token = JWTAuth::attempt($credentials)) {
			return response()->json(array('status'=>false,'message'=>'Invalid username or password'));
		   }
		} catch (JWTAuthException $e) {
			return response()->json(array('status'=>false,'message'=>'failed to create token'));
		}

		// get user data
		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;
		$username = $user->username;
		// check user active or not
		if($user->status!=1){
			return response()->json(array('status'=>false,'message'=>'You are not deactivated from admin. Please contact to admin.'));   
		}
		return response()->json(array(
								'status'=>true,
								'message'=>'user token create successfully.',
								'username'=>$username,
								'token'=>$token
							));

	}

	/************** Get User Profile ***********************/
	public function getUser(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];

		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;
		$userData = DB::table('users')->where('id',$user_id)->first();
		return response()->json(array(
									'status'=>true,
									'message'=>'user login successfully',
									'result'=>$userData
								));
	}

	/************** get list of user types ***********************/
	public function getUserType(Request $request){
		$usertypeData = DB::table('user_types')->select('name','id')->get();
		$resultArr = array();
		foreach($usertypeData as $data){
			$dataArr['id'] = $data->id;
			$dataArr['name'] = $data->name;
			$resultArr[] = $dataArr;
			unset($dataArr);
		}
		return response()->json(array(
								'status'=>true,
								'message'=>'role data',
								'result'=>$resultArr
							));
	}

 


	 /************** Forgot Password ***********************/
	public function forgotpassword(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);

		$email = $data['email'];

		$userData = User::where('email',$email)->first();
		if(!empty($userData)) {

			$password = rand(18973824, 989721389);
			DB::table('users')
							->where('id', $userData->id)
							->update(array('password' => bcrypt($password)));

			// send and email 
			$emailTemplate = EmailTemplates::find(1);
			$messageBody = $emailTemplate->message;
			$messageBody = str_replace('[name]', $userData->username, $messageBody);
			$messageBody = str_replace('[password]', $password, $messageBody);

		   // return view('emails.message')->with('data',$messageBody);   // check email before sending
			$data = ['message' => $messageBody];
			// Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$userData)
			// {
			// 	$m->to($userData->email,$userData->username)
			// 	 ->subject($emailTemplate->subject)
			// 	 ->from($emailTemplate->send_from);
			//    });
			/************* PHP Mailer *************************/
			$mail = new PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
			try {
				//Server settings
				$mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = SMTP_USERNAME;                 // SMTP username
				$mail->Password = SMTP_PASSWORD;                           // SMTP password
				$mail->SMTPSecure = SMTP_SECURE;                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = PORT;                                    // TCP port to connect to

				//Recipients
				$mail->setFrom($emailTemplate->send_from);
				$mail->addAddress($userData->email);     // Add a recipient
				//Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $emailTemplate->subject;
				$mail->Body    = $messageBody;

				$mail->send();
			} catch (Exception $e) {
				   // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
			}
			return response()->json(array('status'=>true,'message'=>'An email sent you with new password on your registered email. Please check.'));
		} else {
			 return response()->json(array('status'=>false,'message'=>"We cannot find an account for contains this email. Please enter your correct email."));  
		}

	}


	

	/************** Change Password 1***********************/
	public function changePassword(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];
		$old_password = $data['opassword'];
		$new_password = $data['password'];

		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;
		// check old password
		if (Hash::check($old_password, $user->password)) { 
			if (Hash::check($new_password, $user->password)) { 
				return Response::json(array('status'=>false,'message'=>"Old password and new password cannot same."));
			} else {
				$password = Hash::make($new_password);
				// update password in DB
				User::where('id', $user_id)->update(array('password' => $password,'change_flag'=>1));
				return Response::json(array('status'=>true,'message'=>"Password updated successfully."));
			}
		} else {
			return Response::json(array('status'=>false,'message'=>"Old password not correct."));
		}

	}
	
	/************** Location App 1***********************/
	public function locationApp(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		#print_r($data); die;
		$token = $data['token'];
		$app = $data['app'];
		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;
		// check old password
		if (!empty($app)) { 
			User::where('id', $user_id)->update(array('app' => $app));
			return Response::json(array('status'=>true,'message'=>"save data successfully."));
		
		} else {
			return Response::json(array('status'=>false,'message'=>"save not data successfully."));
		}

	}


	  /************** Edit Profile ***********************/
	public function editProfile(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];
		$name = $data['name'];
		$username = $data['username'];
		$email = $data['email'];
		$phone = $data['phone'];
		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;
		
		// check username unique
		$userNameUnique = DB::table('users')->where('username',$username)->where('id','!=',$user_id)->first();
		if(!empty($userNameUnique)){
			 return Response::json(array('status'=>false,'message'=>"Username not unique."));
		}
		// check email unique
		$userEmailUnique = DB::table('users')->where('email',$email)->where('id','!=',$user_id)->first();
		if(!empty($userEmailUnique)){
			 return Response::json(array('status'=>true,'message'=>"Email not unique."));
		}

		// update edit basic info
		$userObj = User::find($user_id);
		$userObj->name= trim($name);
		$userObj->email= trim($email);
		$userObj->username= trim($username);
		$userObj->phone= trim($phone);
		$userObj->save();
		return Response::json(array('status'=>true,'message'=>"Profile details updated successfully."));
	}



	/*************** get days form current date ***************/
	public function getDaysFromDate($date){
		$days = (strtotime(date('Y-m-d H:i:s')) - strtotime($date)) / (60 * 60 * 24);
		$daysRound = round($days);
		if($daysRound==0 || $daysRound==1){
			$txt = 'day';
		} else {
			$txt = 'days';
		}
		return $daysRound." ".$txt;
	}





	/************** Update FCM  ***********************/
	public function updateFcmToken(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];
		$fcm_token = $data['fcm_token'];

		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;

		$updateUser = array(
			'fcm_token' => $fcm_token,
		);
		DB::table('users')->where('id', $user_id)->update($updateUser);

		return response()->json(array('status'=>true,'message'=>'FCM Token updated successfully'));
	}
	
	/**-----------------Firebase Notification ------------**/
	
	function SendNotification( $deviceData, $notification,$type){
		/* print_r($deviceData);
		print_r($notification);
		 die; */
		/* Check device data not empty */ 
		if( empty( $deviceData ) ){
			return true;
		}else{
			$androidDevice = array();
			$iosDevice = array();
			
			 $androidDevice = $deviceData;
			if( count( $androidDevice ) > 0 ){
				  $SERVER_KEY = 'AAAA3TdTsbQ:APA91bEceyvNApclNMD7tzHQkZezwKxdpMsAYyPV8GX42U-_d8EU4vSNS4SzcsYmTf4jzbG3Vz74RvumlkssdKzkBye9JT2qR5MfGp-fEbDgyd8_itBqGmMHau1scLlOvdzT1R-oySui'; 
				$jsonString = $this->sendPushNotificationToGCMSever( $androidDevice, $notification, $SERVER_KEY,$type);
				return true;
				// $jsonObject = json_decode($jsonString);


								
			} 
			
		}
	}
		
	/**-----------------Firebase Notification Send ------------**/	
	function sendPushNotificationToGCMSever($token, $notification, $SERVER_KEY,$type){
		$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
		// send notification when send message  
		$fields = array();
		if($type=='saveMessage') {
			 $fields = array(
				'registration_ids' => array_values($token),
				'data'=>array(
					'siteId'=>$notification['siteId'],
					'issueId'=>$notification['issueId'],
					'message'=>$notification['msg'],
					'title'=>$notification['title'],
					'name'=>$notification['username'],
					'dateTime'=>date('d M | h:i A'),
					'vibrate'=>1,
					'sound'=>1,
					'icon'=>HTTP_PATH.'public/admin/images/notification.png'
				),
				'priority'=>'high',
				'notification'=>array(
				'title'=>$notification['title'],
				'body'=>$notification['msg']
				)
			); 
		}
		
		 $jsonData = json_encode($fields);
		  /* echo 'dffd ';
		 print_r($jsonData); die; */
		$headers = array(
			'Authorization:key=' . $SERVER_KEY,
			'Content-Type:application/json'
		);      
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		$result = curl_exec($ch);
		// echo "<pre>"; print_r($result);die;
		curl_close($ch);
		return $result;
	}

	
}
