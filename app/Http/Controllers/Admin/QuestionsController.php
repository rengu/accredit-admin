<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session, Input, HTML, Validator, DB, Redirect, View, Mail;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = array();
        $questions = DB::table('feedback_questions')->get(); 


        // it will get the entire table
      
        // return View::make("admin/packages/index");

         return View::make('admin/question/index', compact('questions'));
         // ->with('search_keyword', $search_keyword)
         //                ->with('user_type_id', $user_type_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        $input = Input::all();
        // echo '<pre>';print_r($input);
      // die;
        if (!empty($input)) {
            $rules = array(
                'question_text' => 'required',
            );

            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            #print_r($input); die;
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {

                return Redirect::to('/admin/question/add')
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {
                if(isset($input['option']) && !empty($input['option']))
                {
                    $newarrayOption=[];
                     $i=1;
                    foreach ($input['option'] as $key => $value)
                    {
                        $newarrayOption[$i]=$value;
                        $i++;
                    }
                    print_r($newarrayOption);
                    $optiondata=json_encode($newarrayOption,JSON_FORCE_OBJECT);
                }
                else{
                     $optiondata='';
                }
                $data = array(
                    'question_text' => $input['question_text'],
                    'question_type' => $input['question_type'],
                    'option' => $optiondata,
                    'status' => $input['status'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                );

                //print_r($data) ; die;
                DB::table('feedback_questions')->insert($data);
                return Redirect::to('/admin/question/index')->with('success_message', 'Question Create successfully.');
            }
        } else {
            //echo 'sfds'; die;
            return View::make('/admin/question/add');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $input = Input::all();
        $questions = DB::table("feedback_questions")
            ->where("id",$id)
            ->first();

        if(!empty($input)){
            $rules = array(
                'question_text' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()){
                return Redirect::to('/admin/question/edit/'.$id)
                                ->withErrors($validator)
                                ->withInput(Input::all());
            }else{
                if(isset($input['option']) && !empty($input['option']))
                {
                    $newarrayOption=[];
                     $i=1;
                    foreach ($input['option'] as $key => $value)
                    {
                        $newarrayOption[$i]=$value;
                        $i++;
                    }
                    $optiondata=json_encode($newarrayOption,JSON_FORCE_OBJECT);
                }
                else{
                     $optiondata='';
                }


               $data = array(
                  
                    'question_text' => $input['question_text'],
                    'question_type' => $input['question_type'],
                    'option' => $optiondata,
                    'status' => $input['status'],
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                );
                DB::table("feedback_questions")->where( "id" ,$id )->update($data);
                return Redirect::to("admin/question/index")->with('success_message', "Question  updated successfully.");
            }
        }else{
                

            return View::make('/admin/question/edit',compact('questions'))->with('detail', $questions);
            // dd($id);
            // return Redirect::to("admin/package/edit/".$id);
                // , compact("package"))->with('detail', $package);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
      
        if(DB::table("feedback_questions")->where("id", $id)->delete()){
            return Redirect::to('admin/question/index')->with('success_message',"Question deleted successfully.");
        }else{
            return Redirect::to('admin/question/index')->with('error_message',"Incorrect question ID.");
        }
    }
}
