<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\AdminDocuments;
use App\Models\Admin;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class DocumentsController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Loans Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */
    public function index() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }

        $query = AdminDocuments::sortable(); 
       
        $data = $query->orderBy('id', 'desc')->sortable()->paginate(10);
		 $input = input::all();
         //print_r($input); die;
       if (!empty($input['action'])) {
            $action = $input['action'];
            $idList = $input['chkRecordId'];

            switch ($action) {
                case "Activate":
                    DB::table('admin_documents')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 1));
                    return Redirect::back()->with('success_message', 'Record(s) Activated Successfully.');                  
                    break;
                case "Deactivate":
                    DB::table('admin_documents')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 0));
                    return Redirect::back()->with('success_message', 'Record(s) Deactivate Successfully.');
                   
                    break;
                case "Delete":
                    DB::table('admin_documents')
                            ->whereIn('id', $idList)
                            ->delete();
                    return Redirect::back()->with('success_message', 'Record(s) Deleted Successfully.');
                   
                    break;
            }
        }
        return View::make('admin/documents/index', compact('data'));
    }
	
  
    public function deactivate($id = null) {
        if (!empty($id)) {
            DB::table('admin_documents')
                    ->where('id', $id)
                    ->update(['status' => 0]);
            return Redirect::back()->with('success_message', 'Record deactivated successfully');
        }
    }

     public function activate($id = null) {
        if (!empty($id)) {
            DB::table('admin_documents')
                    ->where('id', $id)
                    ->update(['status' => 1]);
            return Redirect::back()->with('success_message', 'Record deactivated successfully');
        }
    }

    public function add()
    {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
         $input = Input::all();
         if(!empty($input))
         {
            $adminDocuments = new AdminDocuments;
            $adminDocuments->title = $input['title'];
            $adminDocuments->priority = $input['priority'];
            $adminDocuments->status = 1;

            if(input::hasFile('document'))
            {
                $file = Input::file('document');                  
                $filename = microtime() . '.' .$file->getClientOriginalName();
                $file->move(SITE_URLI.'/public/files/admindocuments/',$filename);
                $adminDocuments->document = $filename;
            }
            else
            {
              $adminDocuments->document = '';  
            }
            if($adminDocuments->save())
            {
                return Redirect::to('/admin/documents')->with('success_message', 'Document Added successfully.');
            }
            else
            {
               return Redirect::to('/admin/documents')->with('error_message', 'Document Not Added !');
            }
         }
        return View::make('admin/documents/add');
    }
	
    public function delete($id = null) {
        if (!empty($id)) {
            DB::table('admin_documents')->where('id', $id)->delete();
            return Redirect::to('/admin/documents')->with('success_message', 'Record deleted successfully');
        }
    }

    public function view($id  = null)
    {
        if (!empty($id)) {
            $data = DB::table('admin_documents')->where('id', $id)->first();
            
        }
         
          return View::make('admin/documents/view')->with('data',$data);
    }

    public function checkPriority()
    {
      $input = input::all();
      
      if($input['value'] > 0)
      {
         $data = DB::table('admin_documents')->where(['priority'=>$input['value']])->first();
        
          if(!empty($data))
          {
            echo 0; die;
          }
          else
          {
            echo 1; die;
          }
      }
      else
      {
        echo 2; die;
      }
    }



}
