<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Loan;
use App\Models\Faq;
use App\Models\LoanRepayment;
use App\Models\Admin;
use App\Models\EmailTemplates;
use App\Models\FaqCategories;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class CmsController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Loans Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */
    public function index() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }


        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $user_type_id = "";
        $searchByDateTo = "";
        $separator = array();
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }

        $query = Faq::sortable()->with('faqcategory')
                ->where(function ($query) use ($search_keyword) {
            $query->where('id', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('question', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('answer', 'LIKE', '%' . $search_keyword . '%');
        });

       
        if (!empty($input['action'])) {
            $action = $input['action'];
            $idList = $input['chkRecordId'];
            switch ($action) {
                case "Activate":
                    DB::table('loan_application')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 1));
                    Session::put('success_message', "Record(s) Activated Successfully."); // set activate session message
                    break;
                case "Deactivate":
                    DB::table('loan_application')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 0));
                    Session::put('success_message', "Record(s) Deactivate Successfully."); // set deactivate session message
                    break;
                case "Delete":
                    DB::table('loan_application')
                            ->whereIn('id', $idList)
                            ->delete();
                    Session::put('success_message', "Record(s) Deleted Successfully."); // set delete session message
                    break;
            }
        }
        
        $separator = implode("/", $separator);
        // Get all the loans
        $faq = $query->orderBy('id', 'desc')->sortable()->paginate(10);
		
        // Show the page
        return View::make('admin/cms/index', compact('faq'))->with('search_keyword', $search_keyword);
    }
    public function add() {

        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $input = Input::all();
        $dataCategory = DB::table('faq_categories')->where('status',1)->get();
        if (!empty($input)) {
         
            $rules = array(
                'question' => 'required', // make sure the first name field is not empty
                'answer' => 'required', 
                'faq_category_id' => 'required'          
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/cms/add/')
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {           
                $data = array(
                    'faq_category_id'=>$input['faq_category_id'],
                    'priority'=>isset($input['priority'])?$input['priority']:'',
                    'question' => $input['question'],
                    'answer' => $input['answer'],
                    'status'=>1,
                    'created_by'=>Session::get('adminid'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'modified_at' => date('Y-m-d H:i:s'),
                );

                DB::table('Faq')->insert($data);
                return Redirect::to('/admin/cms/index')->with('success_message', 'FAQ Added successfully.');
            }
        } else {

            return View::make('admin/cms/add')->with('dataCategory',$dataCategory);
        }
    }
    
    public function edit($id = null) {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $input = Input::all();
        $question = FAQ::with('faqcategory')->where('id', $id)->first();
        $dataCategory = DB::table('faq_categories')->where('status',1)->get();
        if (!empty($input)) {
             $rules = array(
                'question' => 'required', // make sure the first name field is not empty
                'answer' => 'required',           
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/cms/edit/' . $user->id)
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {
                
               $data = array(
                    'faq_category_id'=>$input['faq_category_id'],
                    'priority'=>isset($input['priority'])?$input['priority']:'',
                    'question' => $input['question'],
                    'answer' => $input['answer'],
                    'status'=>1,
                    'created_by'=>Session::get('adminid'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'modified_at' => date('Y-m-d H:i:s'),
                );
               
                DB::table('Faq')
                        ->where('id', $id)
                        ->update($data);
                return Redirect::to('/admin/cms/index')->with('success_message', 'FAQ details updated successfully.');
            }
        } else {
            return View::make('/admin/cms/edit')->with('detail', $question)->with('dataCategory',$dataCategory);
        }
    }
	public function activate($id = null) {
        if (!empty($id)) {
            DB::table('Faq')
                    ->where('id', $id)
                    ->update(['status' => 1]);
            return Redirect::back()->with('success_message', 'Faq activated successfully');
        }
    }
    public function deactivate($id = null) {
        if (!empty($id)) {
            DB::table('Faq')
                    ->where('id', $id)
                    ->update(['status' => 0]);
            return Redirect::back()->with('success_message', 'Faq deactivated successfully');
        }
    }
    public function delete($id = null) {
        if (!empty($id)) {
            DB::table('Faq')->where('id', $id)->delete();
            return Redirect::to('/admin/cms/index')->with('success_message', 'Faq deleted successfully');
        }
    }
    public function about(){
            if (!Session::has('adminid')) 
            {
            return Redirect::to('/admin/login');
            }
         
              $about = DB::table('about_content')
                        ->where('id', 1)->first();
            $input = Input::all();
            if (!empty($input)) 
            {
                $rules = array(
                'content' => 'required', 
               );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->fails()) {
                    return Redirect::to('/admin/cms/about')->withErrors($validator)->withInput(Input::all());
                } else 
                {
                  
                    $saveUser = array(
                    'content' => $input['content'],
                    'created_by'=>Session::get('adminid'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'modified_at' => date('Y-m-d H:i:s'),
                    );
                    
                    if(empty($about))
                    {
                         DB::table('about_content')->insert($saveUser);
                    }
                    else{
                      
                        DB::table('about_content')
                            ->where('id', 1)
                            ->update($saveUser);
                    }
                }
                 $about = DB::table('about_content')
                        ->where('id', 1)->first();
                return View::make('admin/cms/about')->with('about',$about);
                }
            
            return View::make('admin/cms/about')->with('about',$about);
    }

    public function checkPriority()
    {
      $input = input::all();
      //print_r($input); die;
      if($input['value'] > 0)
      {
         $data = DB::table('Faq')->where(['faq_category_id'=>$input['faq_category_id'],'priority'=>$input['value']])->first();
        
          if(!empty($data))
          {
            echo 0; die;
          }
          else
          {
            echo 1; die;
          }
      }
      else
      {
        echo 2; die;
      }
    }

}
