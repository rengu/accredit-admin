<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Loan;
use App\Models\Faq;
use App\Models\LoanRepayment;
use App\Models\Payments;
use App\Models\LoanEnquiryIndividual;
use App\Models\LoanEnquiryCompany;
use App\Models\Admin;
use App\Models\EmailTemplates;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class ReportsController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Loans Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */
    public function paymentReport() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }

        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $user_type_id = "";
        $todayDate = date('Y-m-d');
        $separator = array();
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }

        if($search_keyword == 1)
        {
            
            $query = Payments::sortable()
                ->where(function ($query) use ($todayDate) {
            $query->where('date_of_payment',$todayDate);
           
             });
        }
        else if($search_keyword == 7)
        {   
            $date = date('Y-m-d',strtotime($todayDate.' -7 days'));
            $query = Payments::sortable()
                ->where(function ($query) use ($date) {
            $query->whereBetween('date_of_payment',array($date, date('Y-m-d')));
           
             });


        }
        else if($search_keyword == 30)
        {
            $date = date('Y-m-d',strtotime($todayDate.'-1 month'));
            $query = Payments::sortable()
                ->where(function ($query) use ($date) {
            $query->whereBetween('date_of_payment',array($date, date('Y-m-d')));
           
             });
        }
        else if($search_keyword == 90)
        {
           $date = date('Y-m-d',strtotime($todayDate.'-3 months'));
            $query = Payments::sortable()
                ->where(function ($query) use ($date) {
            $query->whereBetween('date_of_payment',array($date, date('Y-m-d')));
           
             });
        }
        else if($search_keyword == 180)
        {
            $date = date('Y-m-d',strtotime($todayDate.'-6 months'));
            $query = Payments::sortable()
                ->where(function ($query) use ($date) {
            $query->whereBetween('date_of_payment',array($date, date('Y-m-d')));
           
             });
        }
        else if($search_keyword == 365)
        {
            $date = date('Y-m-d',strtotime($todayDate.'-1 year'));
            $query = Payments::sortable()
                ->where(function ($query) use ($date) {
            $query->whereBetween('date_of_payment',array($date, date('Y-m-d')));
           
             });
        }
        else
        {
            $a = date('Y-m-d');
            $date = date('Y-m-d',strtotime($todayDate.' -1 month'));            
            $query = Payments::sortable()
                ->where(function ($query) use ($date) {
            $query->whereBetween('date_of_payment',array($date, date('Y-m-d')));
           
             });
        }
        
        
       
        $data = $query->with('userinfo')->orderBy('id', 'desc')->sortable()->paginate(10);
		//print_r($data); die;
        return View::make('admin/Reports/paymentReport', compact('data'))->with('search_keyword', $search_keyword);
    }

    public function downloadReports()
    {
        
    }
    
}
