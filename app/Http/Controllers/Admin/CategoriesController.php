<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;

class CategoriesController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | categories Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin categories listing to the admin.
     *
     * @return Response
     */
    public function index() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $searchByDateTo = "";
        $separator = array();
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }
        $query = Category::sortable()
                ->where(function ($query) use ($search_keyword) {
            $query->where('name', 'LIKE', '%' . $search_keyword . '%');
        });
        if (!empty($input['action'])) {
            $action = $input['action'];
            $idList = $input['chkRecordId'];
            switch ($action) {
                case "Activate":
                    DB::table('categories')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 1));
                    Session::put('success_message', "Record(s) Activated Successfully."); // set activate session message
                    break;
                case "Deactivate":
                    DB::table('categories')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 0));
                    Session::put('success_message', "Record(s) Deactivate Successfully."); // set deactivate session message
                    break;
                case "Delete":
                    DB::table('categories')
                            ->whereIn('id', $idList)
                            ->delete();
                    Session::put('success_message', "Record(s) Deleted Successfully."); // set delete session message
                    break;
            }
        }
        $separator = implode("/", $separator);
        // Get all the users
        $categories = $query->where('parent_id',0)->orderBy('name', 'asc')->sortable()->paginate(20);
        // Show the page
        return View::make('admin/categories/index', compact('categories'))->with('search_keyword', $search_keyword)
                        ->with('searchByDateFrom', $searchByDateFrom)
                        ->with('searchByDateTo', $searchByDateTo);
    }

    /**
     * Show the application admin add category.
     *
     * @return Response
     */
    public function add() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $input = Input::all();
        if (!empty($input)) {
            $name = trim($input['name']);
            $name_ar = trim($input['name_ar']);
            $rules = array(
                'name' => 'required|unique:categories', // make sure the name field is not empty and unique
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/categories/add')->withErrors($validator)->withInput(Input::all());
            } else {

                $slug = $this->createUniqueSlug($name, 'categories');
                $saveCategory = array(
                    'name' => $name,
                    'name_ar' => $name_ar,
                    'status' => '1',
                    'slug' => $slug,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                DB::table('categories')->insert(
                        $saveCategory
                );
                return Redirect::to('/admin/categories/index')->with('success_message', 'Category saved successfully.');
            }
        } else {
            return View::make('/admin/categories/add');
        }
    }

    /**
     * Show the application admin edit Category.
     *
     * @return Response
     */
    public function edit($slug = null) {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $input = Input::all();
        $category = DB::table('categories')
                        ->where('slug', $slug)->first();
        $category_id = $category->id;
        if (!empty($input)) {
            $rules = array(
                'name' => 'required|unique:categories,name,'.$category_id, // make sure the name field is not empty and unique
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/categories/edit/' . $category->slug)
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {
                $name_ar = trim($input['name_ar']);
                $data = array(
                    'name' => $input['name'],
                    'name_ar' => $name_ar,
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                DB::table('categories')
                        ->where('id', $category_id)
                        ->update($data);
                return Redirect::to('/admin/categories/index')->with('success_message', 'Category updated successfully.');
            }
        } else {
            return View::make('/admin/categories/edit')->with('detail', $category);
        }
    }

    /**
     * This function use for activate a Category in admin panel.
     *
     * @return Response
     */
    public function activate($slug = null) {
        if (!empty($slug)) {
            DB::table('categories')
                    ->where('slug', $slug)
                    ->update(['status' => 1]);
            return Redirect::back()->with('success_message', 'Category activated successfully');
        }
    }

    /**
     * This function use for deactivate a Category in admin panel.
     *
     * @return Response
     */
    public function deactivate($slug = null) {
        if (!empty($slug)) {
            DB::table('categories')
                    ->where('slug', $slug)
                    ->update(['status' => 0]);
            return Redirect::back()->with('success_message', 'Category deactivated successfully');
        }
    }

    /**
     * This function use for delete a Category in admin panel.
     *
     * @return Response
     */
    public function delete($slug = null) {
        if (!empty($slug)) {
            DB::table('categories')->where('slug', $slug)->delete();
            return Redirect::back()->with('success_message', 'Category deleted successfully');
        }
    }

     /**
     * Show the application admin add sub category.
     *
     * @return Response
     */
    public function addSubCategory($parent_slug = null) {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $parentCategoryData = Category::where('slug',$parent_slug)->first();
        $parentId = $parentCategoryData['id'];

        $input = Input::all();
        if (!empty($input)) {
            $name = trim($input['name']);
            $name_ar = trim($input['name_ar']);
            $rules = array(
                'name' => 'required|unique:categories', // make sure the name field is not empty and unique
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/categories/add-subcategory/'.$parent_slug)->withErrors($validator)->withInput(Input::all());
            } else {

                $slug = $this->createUniqueSlug($name, 'categories');
                $saveCategory = array(
                    'name' => $name,
                    'name_ar' => $name_ar,
                    'parent_id'=>$parentId,
                    'status' => '1',
                    'slug' => $slug,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                DB::table('categories')->insert(
                        $saveCategory
                );
                return Redirect::to('/admin/subcategories/'.$parent_slug)->with('success_message', 'Sub Category saved successfully.');
            }
        } else {
            return View::make('/admin/categories/add-subcategory')->with('parentCategoryData',$parentCategoryData);
        }
    }

      /**
     * Show the application admin sub categories listing to the admin.
     *
     * @return Response
     */
    public function subcategories($parent_slug = null) {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }

        $parentCategoryData = Category::where('slug',$parent_slug)->first();
        $parentId = $parentCategoryData['id'];

        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $searchByDateTo = "";
        $separator = array();
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }
        $query = Category::sortable()
                ->where(function ($query) use ($search_keyword) {
            $query->where('name', 'LIKE', '%' . $search_keyword . '%');
        });
        if (!empty($input['action'])) {
            $action = $input['action'];
            $idList = $input['chkRecordId'];
            switch ($action) {
                case "Activate":
                    DB::table('categories')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 1));
                    Session::put('success_message', "Record(s) Activated Successfully."); // set activate session message
                    break;
                case "Deactivate":
                    DB::table('categories')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 0));
                    Session::put('success_message', "Record(s) Deactivate Successfully."); // set deactivate session message
                    break;
                case "Delete":
                    DB::table('categories')
                            ->whereIn('id', $idList)
                            ->delete();
                    Session::put('success_message', "Record(s) Deleted Successfully."); // set delete session message
                    break;
            }
        }
        $separator = implode("/", $separator);
        // Get all the users
        $categories = $query->where('parent_id',$parentId)->orderBy('id', 'desc')->sortable()->paginate(20);
        // Show the page
        return View::make('admin/categories/subcategories', compact('categories'))->with('search_keyword', $search_keyword)
                        ->with('searchByDateFrom', $searchByDateFrom)
                        ->with('searchByDateTo', $searchByDateTo)->with('parentCategoryData', $parentCategoryData);
    }

    /**
     * Show the application admin edit sub Category.
     *
     * @return Response
     */
    public function editSubCategory($parent_slug = null,$slug = null) {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
         $parentCategoryData = Category::where('slug',$parent_slug)->first();
        $parentId = $parentCategoryData['id'];

        $input = Input::all();
        $category = DB::table('categories')
                        ->where('slug', $slug)->first();
        $category_id = $category->id;
        if (!empty($input)) {
            $rules = array(
                'name' => 'required|unique:categories,name,'.$category_id, // make sure the name field is not empty and unique
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/subcategories/edit/'.$parent_slug.'/' . $category->slug)
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {
                $name_ar = trim($input['name_ar']);
                $data = array(
                    'name' => $input['name'],
                    'name_ar'=>$name_ar,
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                DB::table('categories')
                        ->where('id', $category_id)
                        ->update($data);
                return Redirect::to('/admin/subcategories/'.$parent_slug)->with('success_message', 'Category updated successfully.');
            }
        } else {
            return View::make('/admin/categories/edit-subcategory')->with('detail', $category)->with('parentCategoryData',$parentCategoryData);
        }
    }

}
