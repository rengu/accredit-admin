<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\LoanEnquiryIndividual;
use App\Models\LoanEnquiryCompany;
use App\Models\LoanRepayment;
use App\Models\Admin;
use App\Models\EmailTemplates;
use App\Models\Payments; 
use App\Models\Notifications; 
use Illuminate\Http\Request;

use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class LoansController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Loans Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */
    public function individualLoanEnquiries() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }


        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $user_type_id = "";
        $searchByDateTo = "";
        $separator = array();
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }

        $query = LoanEnquiryIndividual::sortable()
                ->where(function ($query) use ($search_keyword) {
            $query->where('id', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('name', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('email', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('loan_amount_requested', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('phone', 'LIKE', '%' . $search_keyword . '%');
        });

       
        if (!empty($input['action'])) {
            $action = $input['action'];
            $idList = $input['chkRecordId'];
              switch ($action) {
                case "Activate":
                    DB::table('loan_enquiry_individual')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 1));
                    return Redirect::back()->with('success_message', 'Record(s) Activated Successfully.');                  
                    break;
                case "Deactivate":
                    DB::table('loan_enquiry_individual')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 0));
                    return Redirect::back()->with('success_message', 'Record(s) Deactivate Successfully.');
                   
                    break;
                case "Delete":
                    DB::table('loan_enquiry_individual')
                            ->whereIn('id', $idList)
                            ->delete();
                    return Redirect::back()->with('success_message', 'Record(s) Deleted Successfully.');
                   
                    break;
            }
        }
        
        $separator = implode("/", $separator);
        // Get all the loans
        $data = $query->orderBy('id', 'desc')->sortable()->paginate(10);
        //$data = json_decode(json_encode($data), true);
		
        // Show the page
        return View::make('admin/loans/individualLoanEnquiries', compact('data'))->with('search_keyword', $search_keyword);
    }


    public function companyLoanEnquiries() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }


        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $user_type_id = "";
        $searchByDateTo = "";
        $separator = array();
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }

        $query = LoanEnquiryCompany::sortable()
                ->where(function ($query) use ($search_keyword) {
            $query->where('id', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('name', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('email', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('loan_amount_requested', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('office_number', 'LIKE', '%' . $search_keyword . '%');
        });

       
        if (!empty($input['action'])) {
            $action = $input['action'];
            $idList = $input['chkRecordId'];
              switch ($action) {
                case "Activate":
                    DB::table('loan_enquiry_company')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 1));
                    return Redirect::back()->with('success_message', 'Record(s) Activated Successfully.');                  
                    break;
                case "Deactivate":
                    DB::table('loan_enquiry_company')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 0));
                    return Redirect::back()->with('success_message', 'Record(s) Deactivate Successfully.');
                   
                    break;
                case "Delete":
                    DB::table('loan_enquiry_company')
                            ->whereIn('id', $idList)
                            ->delete();
                    return Redirect::back()->with('success_message', 'Record(s) Deleted Successfully.');
                   
                    break;
            }
        }
        
        $separator = implode("/", $separator);
        // Get all the loans
        $data = $query->orderBy('id', 'desc')->sortable()->paginate(10);
        //$data = json_decode(json_encode($data), true);
        
        // Show the page
        return View::make('admin/loans/companyLoanEnquiries', compact('data'))->with('search_keyword', $search_keyword);
    }



    /**
     * Show the application admin edit user.
     *
     * @return Response
     */
    
	 public function deactivate($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_individual')
                    ->where('id', $id)
                    ->update(['status' => 0]);
            return Redirect::back()->with('success_message', 'Record deactivated successfully');
        }
    }

     public function activate($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_individual')
                    ->where('id', $id)
                    ->update(['status' => 1]);
            return Redirect::back()->with('success_message', 'Record deactivated successfully');
        }
    }

    public function delete($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_individual')->where('id', $id)->delete();
            return Redirect::back()->with('success_message', 'Record deleted successfully');            
        }
    }


     public function cdeactivate($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_company')
                    ->where('id', $id)
                    ->update(['status' => 0]);
            return Redirect::back()->with('success_message', 'Record deactivated successfully');
        }
    }

     public function cactivate($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_company')
                    ->where('id', $id)
                    ->update(['status' => 1]);
            return Redirect::back()->with('success_message', 'Record deactivated successfully');
        }
    }

    public function cdelete($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_company')->where('id', $id)->delete();
            return Redirect::back()->with('success_message', 'Record deleted successfully');            
        }
    }

    public function viewIndividual($id  = null)
    {
        if (!empty($id)) {
            $data = LoanEnquiryIndividual::where('id', $id)->first();
         	$office_branch_Name='';
            if(!empty($data->office_branch)){
                $office_branch_Name=DB::table('office_branch')->where('id', $data->office_branch)->first();
            }   
        }

        //print_r($data->knowaccrdit); die;
         
          return View::make('admin/loans/viewIndividual',compact('office_branch_Name'))->with('data',$data);
    }

    public function viewCompany($id  = null)
    {
        if (!empty($id)) {
            $data = LoanEnquiryCompany::where('id', $id)->first();
            
        }

        //print_r($data); die;
         
          return View::make('admin/loans/viewCompany')->with('data',$data);
    }

    

     public function approved(Request $request,$id = null) {
        $type = $request->input('type');
        if ($id > 0 && $type > 0) {
            if($type == 3)
            {
                $dataLoanEnq = LoanEnquiryIndividual::where('id',$id)->first();
            }
            else if($type == 4)
            {
                $dataLoanEnq = LoanEnquiryCompany::where('id',$id)->first();
            }
            
            if(!empty($dataLoanEnq))
            {
                $dataUser = DB::table('users')->where('id',$dataLoanEnq->user_id)->first();
                $post22 = new Notifications;
                $msg = str_replace('[loan_no]',$dataLoanEnq->loan_no, loanApproved);
                $post22->user_id = $dataLoanEnq->user_id;
                $post22->loan_no = $dataLoanEnq->loan_no;
                $post22->message = $msg;
                $post22->status = 1;
                $post22->save();
                 if(!empty($dataUser->firebase_token))
                 {
                    $tokan = $dataUser->firebase_token;
                    $data = array(
                            'title'=>'Accredit',
                            'loan_no'=>$dataLoanEnq->loan_no,
                            'msg'=>$msg
                    );
                    $this->sendPushNotification($tokan,$data);
                 }
                

                $emailTemplate = EmailTemplates::find(5);
                //print_r( $emailTemplate);die;
                $messageBody = $emailTemplate->message;
                $messageBody = str_replace('[name]', $dataLoanEnq->name, $messageBody);
                $messageBody = str_replace('[status]', 'Approved', $messageBody);
                $mail = new PHPMailer\PHPMailer(true);                             
                try {
                   
                    $mail->SMTPDebug = 0;                                 // Enable 
                    $mail->isSMTP();                                      // Set 
                    $mail->Host = SMTP_HOST; 
                    $mail->SMTPAuth = true;                               // Enable 
                    $mail->Username = SMTP_USERNAME;                 // SMTP username
                    $mail->Password = SMTP_PASSWORD;                           // SMTP 
                    $mail->SMTPSecure = SMTP_SECURE;                            // 
                    $mail->Port = PORT;                                    // TCP port 
                    $mail->setFrom($emailTemplate->send_from, $emailTemplate->send_from);
                    $mail->addAddress($dataLoanEnq->email);             
                    $mail->isHTML(true);                                  // Set email 
                    $mail->Subject = $emailTemplate->subject;
                    $mail->Body    = $messageBody;
                    $mail->send();
                } catch (Exception $e) {
                       // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
            }
           
            if($type == 3)
            {
                DB::table('loan_enquiry_individual')->where('id', $id)->update(['approved_status' => 2]);
            }
            else if($type == 4)
            {
                 DB::table('loan_enquiry_company')->where('id', $id)->update(['approved_status' => 2]);
            }
           
            return Redirect::back()->with('success_message', 'Record updated successfully');            
        }
        else
        {
             return Redirect::back()->with('error_message', 'Record not found');
        }
    }

     public function unapproved(Request $request,$id = null) {
        $type = $request->input('type');
        if ($id > 0 && $type > 0) {
            
            if($type == 3)
            {
                $dataLoanEnq = LoanEnquiryIndividual::where('id',$id)->first();
            }
            else if($type == 4)
            {
                $dataLoanEnq = LoanEnquiryCompany::where('id',$id)->first();
            }
            if($dataLoanEnq->id > 0)
            {
                $dataUser = DB::table('users')->where('id',$dataLoanEnq->user_id)->first();
                $post22 = new Notifications;
                $msg = str_replace('[loan_no]',$dataLoanEnq->loan_no, loanUnApproved);
                $post22->user_id = $dataLoanEnq->user_id;
                $post22->loan_no = $dataLoanEnq->loan_no;
                $post22->message = $msg;
                $post22->status = 1;
                $post22->save();
                 if(!empty($dataUser->firebase_token))
                 {
                    $tokan = $dataUser->firebase_token;
                    $data = array(
                            'title'=>'Accredit',
                            'loan_no'=>$dataLoanEnq->loan_no,
                            'msg'=>$msg
                    );
                    $this->sendPushNotification($tokan,$data);
                 }

                $emailTemplate = EmailTemplates::find(5);
                $messageBody = $emailTemplate->message;
                $messageBody = str_replace('[name]', $dataLoanEnq->name, $messageBody);
                $messageBody = str_replace('[status]', 'Un-Approved', $messageBody);
                $mail = new PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                    $mail->isSMTP();                                      // Set mailer to use SMTP
                    $mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = SMTP_USERNAME;                 // SMTP username
                    $mail->Password = SMTP_PASSWORD;                           // SMTP password
                    $mail->SMTPSecure = SMTP_SECURE;                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = PORT;                                    // TCP port to connect to

                    //Recipients
                    $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
                    $mail->addAddress($dataLoanEnq->email);     // Add a recipient
                    //Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = $emailTemplate->subject;
                    $mail->Body    = $messageBody;

                    $mail->send();
                } catch (Exception $e) {
                       // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
            }
            
            if($type == 3)
            {
                 DB::table('loan_enquiry_individual')->where('id', $id)->update(['approved_status' => 1]);;
            }
            else if($type == 4)
            {
                  DB::table('loan_enquiry_company')->where('id', $id)->update(['approved_status' => 1]);;
            }
           
            return Redirect::back()->with('success_message', 'Record updated successfully');            
        }
        else
        {
             return Redirect::back()->with('error_message', 'Record not found');
        }
    }
    public function pmt($interest, $paymentFrequency, $loan,$tenure) 
    {
        //echo $interest; die;
        $intr =$interest/ ($paymentFrequency*100); //get percentage
        $EMI= $loan * $intr / (1 - (pow(1/(1 + $intr),$tenure))); # This for 
        return $EMI;
    }

    public function active(Request $request,$id = null) {
         $type = $request->input('type');
        if ($id > 0 && $type > 0) 
        {
            if($type == 3)
            {
                $dataLoanEnq = LoanEnquiryIndividual::where('id',$id)->first();
            }
            else if($type == 4)
            {
                $dataLoanEnq = LoanEnquiryCompany::where('id',$id)->first();
            }            
            if(!empty($dataLoanEnq) && $dataLoanEnq->approved_status == 2)
            {
                if($type == 3)
                {
                    $dataPayment = DB::table('payments')->where('loan_enquiry_individual_id',$dataLoanEnq->id)->where('loan_type',$type)->get();
                }
                else if($type == 4)
                {
                   $dataPayment = DB::table('payments')->where('loan_enquiry_company_id',$dataLoanEnq->id)->where('loan_type',$type)->get();
                }  
                
                if(!$dataPayment->isEmpty())
                {
                    if($type == 3)
                    {
                        DB::table('payments')->where('loan_enquiry_individual_id',$dataLoanEnq->id)->where('loan_type',$type)->delete();
                    }
                    else if($type == 4)
                    {
                      DB::table('payments')->where('loan_enquiry_company_id',$dataLoanEnq->id)->where('loan_type',$type)->delete();
                    }  
                    
                }

                $loanAmount = $dataLoanEnq->loan_amount_requested;
                $tenure = $dataLoanEnq->tenure;
                $interestRate = $dataLoanEnq->loantype->interest_rate;
                $paymentFrequency = $dataLoanEnq->payment_frequency;
                if($paymentFrequency == 0)
                {
                    $paymentFrequency = 12;
                }
                $emi  = $this->pmt($interestRate,$paymentFrequency,$loanAmount,$tenure);
                $totalPaidAmont = $emi*$tenure;
                

                $currentDate = date('Y-m-d');
                $dueDate= date('Y-m-d');                
                //echo $dueDate; die;
                $k = 1;
                $dueBalance = $loanAmount;
                for ($i=1; $i <= $tenure; $i++)
                { 
                   
                    if($paymentFrequency == 365)
                    {
                        $dueDate = date('Y-m-d',strtotime($dueDate.' +1 day'));
                    }
                    else if($paymentFrequency == 52)
                    {                       
                       $dueDate = date('Y-m-d',strtotime($dueDate.' +7 days'));
                    }
                    else if($paymentFrequency == 12)
                    {
                        $dueDate = date('Y-m-d',strtotime($dueDate.' +1 month'));
                    }
                    else if($paymentFrequency == 1)
                    {
                        $dueDate = date('Y-m-d',strtotime($dueDate.' +1 year'));
                    }
                   
                   $rateInt = $interestRate/$paymentFrequency;
                    $k = $k-1;
                    $interestAmount = number_format($dueBalance*$rateInt/100,2,'.','');    
                    $principalAmount = number_format($emi-$interestAmount,2,'.','');
                   //echo $dueBalance; die;
                    $dueBalance = number_format($dueBalance-$principalAmount,2,'.','');
                  $post = new Payments();
                  if($type == 3)
                  {
                    $post->loan_enquiry_individual_id = $dataLoanEnq->id;
                  }
                  else if($type == 4)
                  {
                    $post->loan_enquiry_company_id = $dataLoanEnq->id;  
                  }
                  $post->user_id = $dataLoanEnq->user_id;
                  $post->installment_name = 'Installment'.' '.$i;
                  $post->due_date = $dueDate;
                  $post->principal_amount = $principalAmount;
                  $post->interest_amount = $interestAmount;
                  $post->total_emi = number_format($emi,2,'.','');
                  $post->due_balance = $dueBalance;
                  $post->status = 1;
                  $post->loan_type = $type;
                  $post->save(); 
                  $k++;        
                } 

                $dataUser = DB::table('users')->where('id',$dataLoanEnq->user_id)->first();
                $post22 = new Notifications;
                $msg = str_replace('[loan_no]',$dataLoanEnq->loan_no, loanActive);
                $post22->user_id = $dataLoanEnq->user_id;
                $post22->loan_no = $dataLoanEnq->loan_no;
                $post22->message = $msg;
                $post22->status = 1;
                $post22->save();
               // print_r($dataUser->firebase_token);die;
                 if(!empty($dataUser->firebase_token))
                 {
                    $tokan = $dataUser->firebase_token;
                    $data = array(
                            'title'=>'Accredit',
                            'loan_no'=>$dataLoanEnq->loan_no,
                            'msg'=>$msg
                    );
                    $this->sendPushNotification($tokan,$data);
                 }              
                
                $emailTemplate = EmailTemplates::find(5);
                $messageBody = $emailTemplate->message;
                $messageBody = str_replace('[name]', $dataLoanEnq->name, $messageBody);
                $messageBody = str_replace('[status]', 'Activated', $messageBody);
                $mail = new PHPMailer\PHPMailer(true);                             
                try {
                   
                    $mail->SMTPDebug = 0;                                 // Enable 
                    $mail->isSMTP();                                      // Set 
                    $mail->Host = SMTP_HOST; 
                    $mail->SMTPAuth = true;                               // Enable 
                    $mail->Username = SMTP_USERNAME;                 // SMTP username
                    $mail->Password = SMTP_PASSWORD;                           // SMTP 
                    $mail->SMTPSecure = SMTP_SECURE;                            // 
                    $mail->Port = PORT;                                    // TCP port 
                    $mail->setFrom($emailTemplate->send_from, $emailTemplate->send_from);
                    $mail->addAddress($dataLoanEnq->email);             
                    $mail->isHTML(true);                                  // Set email 
                    $mail->Subject = $emailTemplate->subject;
                    $mail->Body    = $messageBody;
                    $mail->send();
                } catch (Exception $e) {
                       // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
            }
            else
            {
                return Redirect::back()->with('error_message', 'Please approved loan first');
            }
           
            if($type == 3)
            {
                DB::table('loan_enquiry_individual')->where('id', $id)->update(['active_status' => 2]);
            }
            else if($type == 4)
            {
               DB::table('loan_enquiry_company')->where('id', $id)->update(['active_status' => 2]);
            }  
           
            return Redirect::back()->with('success_message', 'Loan activated successfully');            
        }
        else
        {
             return Redirect::back()->with('error_message', 'Record not found');
        }
    }

    public function inactive(Request $request,$id = null) {
        $type = $request->input('type');      
        if ($id > 0 && $type > 0) 
        {
            if($type == 3)
            {
                $dataLoanEnq = LoanEnquiryIndividual::where('id',$id)->first();
            }
            else if($type == 4)
            {
                $dataLoanEnq = LoanEnquiryCompany::where('id',$id)->first();
            }  
            if(!empty($dataLoanEnq))
            {
                if($type == 3)
                {
                  $dataPayment = DB::table('payments')->where('loan_enquiry_individual_id',$dataLoanEnq->id)->where('loan_type',$type)->get();
                }
                else if($type == 4)
                {
                    $dataPayment = DB::table('payments')->where('loan_enquiry_company_id',$dataLoanEnq->id)->where('loan_type',$type)->get();
                }
                
                if(!$dataPayment->isEmpty())
                {   
                    if($type == 3)
                    {
                      DB::table('payments')->where('loan_type',$type)->where('loan_enquiry_individual_id',$dataLoanEnq->id)->delete();
                    }
                    else if($type == 4)
                    {
                        DB::table('payments')->where('loan_type',$type)->where('loan_enquiry_company_id',$dataLoanEnq->id)->delete();
                    }
                    
                }

                $dataUser = DB::table('users')->where('id',$dataLoanEnq->user_id)->first();
                $post22 = new Notifications;
                $msg = str_replace('[loan_no]',$dataLoanEnq->loan_no, loanInActive);
                $post22->user_id = $dataLoanEnq->user_id;
                $post22->loan_no = $dataLoanEnq->loan_no;
                $post22->message = $msg;
                $post22->status = 1;
                $post22->save();
                if(!empty($dataUser->firebase_token))
                {
                    $tokan = $dataUser->firebase_token;
                    $data = array(
                            'title'=>'Accredit',
                            'loan_no'=>$dataLoanEnq->loan_no,
                            'msg'=>$msg
                    );
                    $this->sendPushNotification($tokan,$data);
                }      
               
                $emailTemplate = EmailTemplates::find(5);
                $messageBody = $emailTemplate->message;
                $messageBody = str_replace('[name]', $dataLoanEnq->name, $messageBody);
                $messageBody = str_replace('[status]', 'In-Activated', $messageBody);
                $mail = new PHPMailer\PHPMailer(true);                             
                try {
                   
                    $mail->SMTPDebug = 0;                                 // Enable 
                    $mail->isSMTP();                                      // Set 
                    $mail->Host = SMTP_HOST; 
                    $mail->SMTPAuth = true;                               // Enable 
                    $mail->Username = SMTP_USERNAME;                 // SMTP username
                    $mail->Password = SMTP_PASSWORD;                           // SMTP 
                    $mail->SMTPSecure = SMTP_SECURE;                            // 
                    $mail->Port = PORT;                                    // TCP port 
                    $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
                    $mail->addAddress($dataLoanEnq->email);             
                    $mail->isHTML(true);                                  // Set email 
                    $mail->Subject = $emailTemplate->subject;
                    $mail->Body    = $messageBody;
                    $mail->send();
                } catch (Exception $e) {
                       // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
            }
           
            if($type == 3)
            {
               DB::table('loan_enquiry_individual')->where('id', $id)->update(['active_status' => 1]);
            }
            else if($type == 4)
            {
                DB::table('loan_enquiry_company')->where('id', $id)->update(['active_status' => 1]);
            }
            
            return Redirect::back()->with('success_message', 'Loan deactivated successfully');            
        }
        else
        {
             return Redirect::back()->with('error_message', 'Record not found');
        }
    }
    public function payments(Request $request,$id = null)
    {
      $type = $request->input('type');
      $id = base64_decode($id);
      if($id>0 && $type > 0)
      {
         if($type  == 3)
         {
            $data = DB::table('payments')->where('loan_enquiry_individual_id',$id)->where('loan_type',$type)->get();
         }
         else if($type == 4)
         {
            $data = DB::table('payments')->where('loan_enquiry_company_id',$id)->where('loan_type',$type)->get();
         }
         
      }
     else
     {
         return Redirect::back()->with('error_message', 'Record not found');
     }

      return View::make('admin/loans/payments', compact('data'));

    }

    public function completed(Request $request,$id = null) {
        $type = $request->input('type');
        $date = date('Y-m-d');
        $recNo = rand(18973824, 989721389);
        $recNo = '#Rec'.$recNo.$id;
        if ($id > 0 && $type > 0) {

            /* $dataPayment = DB::table('payments')->where('id',$id)->first();
             if(!$dataPayment->isEmpty())
             {
                if($type == 3)
                {
                    $dataLoanEnq = LoanEnquiryIndividual::where('id',$dataPayment->loan_enquiry_individual_id)->first();
                }
                else if($type == 4)
                {
                    $dataLoanEnq = LoanEnquiryCompany::where('id',$dataPayment->loan_enquiry_company_id)->first();
                }  
                if(!$dataLoanEnq->isEmpty())
                {
                    $dataUser = DB::table('users')->where('id',$dataLoanEnq->user_id)->first();
                    $post22 = new Notifications;
                    $msg = str_replace('[loan_no]',$dataLoanEnq->loan_no, loanPayment);
                    $msg = str_replace('[installment_name]',$dataPayment->installment_name, loanPayment);
                    $msg = str_replace('[emi]',$dataPayment->total_emi, loanPayment);
                    
                    $post22->user_id = $dataLoanEnq->user_id;
                    $post22->loan_no = $dataLoanEnq->loan_no;
                    $post22->message = $msg;
                    $post22->installment_name = $dataPayment->installment_name;
                    $post22->receipt_no = $dataPayment->receipt_no;
                    $post22->status = 1;
                    $post22->save();
                    if(!empty($dataUser->firebase_token))
                    {
                        $tokan = $dataUser->firebase_token;
                        $data = array(
                                'title'=>'Accredit',
                                'loan_no'=>$dataLoanEnq->loan_no,
                                'msg'=>$msg
                        );
                        $this->sendPushNotification($tokan,$data);
                    }      
                   
                    $emailTemplate = EmailTemplates::find(5);
                    $messageBody = $emailTemplate->message;
                    $messageBody = str_replace('[name]', $dataLoanEnq->name, $messageBody);
                    $messageBody = str_replace('[status]', 'In-Activated', $messageBody);
                    $mail = new PHPMailer\PHPMailer(true);                             
                    try {
                       
                        $mail->SMTPDebug = 0;                                 // Enable 
                        $mail->isSMTP();                                      // Set 
                        $mail->Host = SMTP_HOST; 
                        $mail->SMTPAuth = true;                               // Enable 
                        $mail->Username = SMTP_USERNAME;                 // SMTP username
                        $mail->Password = SMTP_PASSWORD;                           // SMTP 
                        $mail->SMTPSecure = SMTP_SECURE;                            // 
                        $mail->Port = PORT;                                    // TCP port 
                        $mail->setFrom($emailTemplate->send_from);
                        $mail->addAddress($dataLoanEnq->email);             
                        $mail->isHTML(true);                                  // Set email 
                        $mail->Subject = $emailTemplate->subject;
                        $mail->Body    = $messageBody;
                        $mail->send();
                    } catch (Exception $e) {
                           // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                    }
                }
             }*/
             

            DB::table('payments')->where('loan_type',$type)->where('id', $id)->update(['is_completed' => 1,'date_of_payment'=>$date,'receipt_no'=>$recNo]);

            return Redirect::back()->with('success_message', 'Record updated successfully');            
        }
    }

     public function incompleted(Request $request,$id = null) {
        $type = $request->input('type');
        if ($id > 0 && $type > 0) {
            DB::table('payments')->where('loan_type',$type)->where('id', $id)->update(['is_completed' => 0,'receipt_no'=>'']);;
            return Redirect::back()->with('success_message', 'Record updated successfully');            
        }
    }

    public function loanCompleted(Request $request,$id = null)
    {
        $type = $request->input('type');
        if ($id > 0 && $type > 0) {
            if($type == 3)
            {
                 DB::table('loan_enquiry_individual')->where('id', $id)->update(['is_completed' => 1]);;
            return Redirect::back()->with('success_message', 'Record updated successfully');
            }
            else if($type == 4)
            {
                 DB::table('loan_enquiry_company')->where('id', $id)->update(['is_completed' => 1]);;
            return Redirect::back()->with('success_message', 'Record updated successfully');
            }
                       
        }
    }

    public function loanInCompleted(Request $request,$id = null)
    {
        $type = $request->input('type');
        if ($id > 0 && $type > 0) {
            if($type == 3)
            {
                 DB::table('loan_enquiry_individual')->where('id', $id)->update(['is_completed' => 0]);;
            return Redirect::back()->with('success_message', 'Record updated successfully');
            }
            else if($type == 4)
            {
                 DB::table('loan_enquiry_company')->where('id', $id)->update(['is_completed' => 0]);;
            return Redirect::back()->with('success_message', 'Record updated successfully');
            }
                       
        }
    }



    public function capproved($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_company')->where('id', $id)->update(['approved_status' => 2]);;
            return Redirect::back()->with('success_message', 'Record updated successfully');            
        }
    }

     public function cunapproved($id = null) {
        if (!empty($id)) {
            DB::table('loan_enquiry_company')->where('id', $id)->update(['approved_status' => 1]);;
            return Redirect::back()->with('success_message', 'Record updated successfully');            
        }
    }

  function sendPushNotification($token,$data)
  {
   /*   $apiKey = 'AAAAd-2CpeU:APA91bFnXIb9l6a4e3-8oMIzIbwk_7YUw5v0nJz7uhoEiBEnGkN1F851Ar0rph8AE7Qj7Qqy3rq5lI_IGo6ivRkFIfppCc-q9El433spA8NHuxwgS-ypmKYkAyvFXRTg9PcovX9MLOzq';*/
      $apiKey='AIzaSyDqkuG-TRzNwkZ-dPkt5iFtwsW2eJsHe8U';

      $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
      // send notification when send message  
      
      $fields = array(

                  'registration_ids' =>array($token),
                  'data'=>array(                  
                  'loan_no'=> $data['loan_no'],
                  'message'=>$data['msg'],
                  'title'=>$data['title'],
                  'dateTime'=>date('d-M-Y'),
                  'vibrate'=>1,
                  'sound'=>1,
                  'force-start'=> 1,
                  'icon'=>'notification_icon'
                  ),
                  'priority'=>'high',
                  'notification'=>array(
                  'title'=>$data['title'],
                  'body'=>$data['msg'],
                  "click_action"=>"FCM_PLUGIN_ACTIVITY",
                 'force-start'=> 1
                  )
      );

    $jsonData = json_encode($fields);
   
    $headers = array(
    'Authorization:key=' . $apiKey,
    'Content-Type:application/json'
    );      
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    $result = curl_exec($ch);
    curl_close($ch);
   // print_r($result);die;
    return $result;
 }

}
