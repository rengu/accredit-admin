<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Admin;
use App\Models\EmailTemplates;
use App\Models\SubadminAuths;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class AdminusersController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Users Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */

    /**
     * Show the application admin add user.
     *
     * @return Response
     */
        public function index() {

        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }

        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $user_type_id = "";
        $searchByDateTo = "";
        $separator = array();
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }
        $query = Admin::sortable()
                ->where(function ($query) use ($search_keyword) {
            $query->where('username', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('email', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('name', 'LIKE', '%' . $search_keyword . '%');
        });
        if (!empty($input['action'])) {
            $action = $input['action'];
            $idList = $input['chkRecordId'];
            switch ($action) {
                case "Activate":
                    DB::table('admins')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 1));
                    Session::put('success_message', "Record(s) Activated Successfully."); // set activate session message
                    break;
                case "Deactivate":
                    DB::table('admins')
                            ->whereIn('id', $idList)
                            ->update(array('status' => 0));
                    Session::put('success_message', "Record(s) Deactivate Successfully."); // set deactivate session message
                    break;
                case "Delete":
                    DB::table('admins')
                            ->whereIn('id', $idList)
                            ->delete();
                    Session::put('success_message', "Record(s) Deleted Successfully."); // set delete session message
                    break;
            }
        }
        $separator = implode("/", $separator);
        // Get all the users
        $users = $query->orderBy('id', 'desc')->sortable()->paginate(10);
    
        // Show the page
    #print_r($allSitesResult); die;
   
        return View::make('admin/adminuser/index', compact('users'))->with('search_keyword', $search_keyword);
    }
    public function add() {

        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }

        $input = Input::all();
        $roles = DB::table('admin_type')->orderBy('name','asc')->pluck('name','id')->toArray();


        if (!empty($input)) 
        {
          
            $email_address = trim($input['email']);
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
                'email' => 'required|unique:admins,email', // make sure the email address field is not empty
                'username' => 'required|unique:admins,username', // make sure the username field is not empty
		'phone' => 'required',
		'admin_type_id' => 'required'
            );
	    $messages = array(
            	'admin_type_id.required' => 'Role is required'
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/adminuser/add')->withErrors($validator)->withInput(Input::all());
            } else {

                $password=$this->password_generate(7);
                 // print_r(); die;
                $slug = $this->createUniqueSlug($input['username'], 'admins');
                //print_r($slug); die;
                $saveUser = array(
                    'name' => $input['name'],
                    'phone' => $input['phone'],
                    'username' => $input['username'],
                    'email' => $input['email'],
                    'status' => '1',
                    'slug' => $slug,
                    'password'=>md5($password),
                    'admin_type_id' => $input['admin_type_id'],
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                );
                
                DB::table('admins')->insert($saveUser);
                $id = DB::getPdo()->lastInsertId();
                $userEmail = $input['email'];

                // send and email 
                $emailTemplate = EmailTemplates::find(2);
                $messageBody = $emailTemplate->message;
                $messageBody = str_replace('[name]', $input['name'], $messageBody);
                $messageBody = str_replace('[email]', $input['username'], $messageBody);
                $messageBody = str_replace('[password]',$password, $messageBody);

                
                //return view('emails.message')->with('data',$messageBody);   // check email before sending
                $data = ['message' => $messageBody];
                // Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$userEmail)
                // {
                //     $m->to($userEmail)
                //     ->subject($emailTemplate->subject)
                //     ->from($emailTemplate->send_from);
                // });


                /************* PHP Mailer *************************/
                $mail = new PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                    $mail->isSMTP();                                      // Set mailer to use SMTP
                    $mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = SMTP_USERNAME;                 // SMTP username
                    $mail->Password = SMTP_PASSWORD;                           // SMTP password
                    $mail->SMTPSecure = SMTP_SECURE;                            // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = PORT;                                    // TCP port to connect to

                    //Recipients
                    $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
                    $mail->addAddress($userEmail);     // Add a recipient
                    //Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = $emailTemplate->subject;
                    $mail->Body    = $messageBody;

                    $mail->send();
                } catch (Exception $e) {
                   // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
                

                return Redirect::to('/admin/adminuser/index')->with('success_message', 'User saved successfully.');
            }
        } else {
   
            return View::make('/admin/adminuser/add')->with('roles',$roles);
        }
    }

    /**
     * Show the application admin edit user.
     *
     * @return Response
     */
    public function edit($slug = null) {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $input = Input::all();
        $user = DB::table('admins')
                        ->where('slug', $slug)->first();
        $roles = DB::table('admin_type')->orderBy('name','asc')->pluck('name','id')->toArray();
        $user_id = $user->id;
        if (!empty($input)) {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
                'email' => 'required|unique:admins,email,'.$user_id,
                'username' => 'required|unique:admins,username,'.$user_id,
		'phone' => 'required',
		'admin_type_id' => 'required'
	    );
	    $messages = array(
            	'admin_type_id.required' => 'Role is required'
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/adminuser/edit/' . $user->slug)
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {
             
                $data = array(
                    'name' => $input['name'],
                    'phone' => $input['phone'],
                    'username' => $input['username'],
                    'email' => $input['email'],
                    'admin_type_id' => $input['admin_type_id'],
                    'modified' => date('Y-m-d H:i:s'),
                );

                DB::table('admins')
                        ->where('id', $user_id)
                        ->update($data);
                return Redirect::to('/admin/adminuser/index')->with('success_message', 'User profile details updated successfully.');
            }
        } else {
            return View::make('/admin/adminuser/edit',compact('roles'))->with('detail', $user);
        }
    }
	
	public function change_password($slug = null) {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $input = Input::all();
        $user = DB::table('admins')
                        ->where('slug', $slug)->first();
        $user_id = $user->id;
        
        if (!empty($input)) {
            $rules = array(
                'password' => 'required', // make sure the first name field is not empty
                'cpassword' => 'required', // make sure the first name field is not empty
                
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/adminuser/change_password/' . $user->slug)
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {
				
                $data = array(
                    'password' => md5($input['cpassword']),
                    'modified' => date('Y-m-d H:i:s'),
                );
                
                DB::table('admins')
                        ->where('id', $user_id)
                        ->update($data);
                return Redirect::to('/admin/adminuser/index')->with('success_message', 'User password updated successfully.');
            }
        } else {
            return View::make('/admin/adminuser/change_password',compact('user'))->with('detail', $user);
        }
    }
    /**
     * This function use for activate a user in admin panel.
     *
     * @return Response
     */
    public function activate($slug = null) {
        if (!empty($slug)) {
            DB::table('admins')
                    ->where('slug', $slug)
                    ->update(['status' => 1]);
            return Redirect::back()->with('success_message', 'User(s) activated successfully');
        }
    }

    /**
     * This function use for deactivate a user in admin panel.
     *
     * @return Response
     */
    public function deactivate($slug = null) {
        if (!empty($slug)) {
            DB::table('admins')
                    ->where('slug', $slug)
                    ->update(['status' => 0]);
            return Redirect::back()->with('success_message', 'User(s) deactivated successfully');
        }
    }
	
	/**
     * This function use for session Delete a user in admin panel.
     *
     * @return Response
     */
    public function session_delete($slug = null) {
        if (!empty($slug)) {
			DB::table('users')
                    ->where('slug', $slug)
                    ->update(['device_id' => '','fcm_token'=>'','change_flag'=>1]);
            return Redirect::back()->with('success_message', 'User session has been  killed successfully.');
        }
    }

    /**
     * This function use for delete a user in admin panel.
     *
     * @return Response
     */
    public function delete($slug = null) {
        if (!empty($slug)) {
            DB::table('admins')->where('slug', $slug)->delete();
            return Redirect::to('/admin/adminuser/index')->with('success_message', 'User deleted successfully');
        }
    }


     /**
     * This function use for view a user in admin panel.
     *
     * @return Response
     */
    public function view($slug = null) {
      if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
      }
      if (!empty($slug)) {
        $userData = User::where('slug',$slug)->first();
         // get site name from site id
        return View::make('admin/users/view', compact('userData','siteNameValues'));
      }
    }


    /**
     * This function use for download sample excel
     *
     * @return Response
     */

    public function downloadSampleExcel() {
      Excel::create('UserSample'.date('Y-m-d-H-i-s'), function($excel) {
          $excel->sheet('Users', function($sheet) {
            $compArray = array('name','username','email','phone','password','user_type_id','site_id');
              $sheet->row(1, array_values($compArray));

          });
          // Set the title
          $excel->setTitle('Our new awesome title');

          // Chain the setters
          $excel->setCreator('AirtelVendor')
                ->setCompany('AirtelVendor');

          // Call them separately
          $excel->setDescription('A demonstration to change the file properties');
      })->download('xls');
      //store('xls',UPLOAD_FULL_DOCUMENT_PATH);
    }


    // upload users via excels 
    public function uploadUsers(){
        $input = Input::all();
        $sites = DB::table('sites')->where('status',1)->orderBy('site_id','asc')->pluck('id','site_id')->toArray();
        $userTypeArr = DB::table('user_types')->where('status',1)->get();
        if (!empty($input)) {
            $rules = array(
                //'document' => 'mimes:xml',
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/users/upload-users/')
                                ->withErrors($validator) // send back all errors
                                ->withInput(Input::all());
            } else {
                if (Input::hasFile('document')) {
                    
                    $file = Input::file('document');
                    $documentName = time() . $file->getClientOriginalName();
                    $file->move(UPLOAD_FULL_DOCUMENT_PATH, $documentName);

                    $dataUsers = Excel::load(UPLOAD_FULL_DOCUMENT_PATH. $documentName, function($reader) {
                                })->get();

                    $headerRow = $dataUsers->getHeading();

                    $excelColumns = array('name','username','email','phone','password','user_type_id','site_id');
                     $arrDiff = array_diff($excelColumns,$headerRow);
                     // echo "<pre>";
                     // print_r($arrDiff);
                     // exit;
                     if(!empty($arrDiff)){
                        return Redirect::to('/admin/users/upload-users')->with('error_message', "Excel not have proper fields as sample xls template.");
                     }
                     
                     $errorMsg = array();
                     $errorShowNum = 0;
                    if(!empty($dataUsers)){
                        foreach($dataUsers->toArray() as $data){
                          
                          if($data['username']!="" && $data['email']!="")
                          {
                              $username = $data['username']; 
                              $email = $data['email'];
                              // check email exists
                              $notUnique = DB::table('users')->where("username" , $username)->orWhere('email' , $email)->first();
                              if(!empty($notUnique))  {
                                $errorShowNum = 1;
                                $errorMsg[] = $notUnique->username;
                                return Redirect::to('/admin/users/upload-users')->with('error_message','Usernames with '.$notUnique->username.' have not unique email or username!');

                              } 
                              // if(strlen($data['password'])<8){
                              //   return Redirect::to('/admin/users/upload-users')->with('error_message','Usernames with '.$username.' have not 8 characters lenght in password');
                              // }
                              
                          }

                        }

                          

                          foreach($dataUsers->toArray() as $data){
                          
                        if($data['username']!="" && $data['email']!="")
                        {
                            $username = $data['username']; 
                            $email = $data['email'];
                            // check email exists
                            $notUnique = DB::table('users')->where("username" , $username)->orWhere('email' , $email)->first();
                            if(!empty($notUnique))  {
                              $errorShowNum = 1;
                              $errorMsg[] = $notUnique->username;
                            } else {
                             $slug = $this->createUniqueSlug($data['username'], 'users');
                             // create site id 
                              $sArr = array();
                             if(!empty($data['site_id'])){
                              $explodedSiteArr = explode(',',$data['site_id']);
                              foreach($explodedSiteArr as $sData){
                                if(isset($sites[$sData])) {
                                  $sArr[] = $sites[$sData];
                                }
                              }
                             }
                             $saveSiteVal = implode(',',$sArr);
                              $saveUser = array(
                                  'name' => $data['name'],
                                  'phone' => $data['phone'],
                                  'username' => $data['username'],
                                  'email' => $data['email'],
                                  'password' => bcrypt($data['password']),
                                  'status' => '1',
                                  'slug' => $slug,
                                  'user_type_id' => $data['user_type_id'],
                                  'site_id' => $saveSiteVal,
                                  'created_at' => date('Y-m-d H:i:s'),
                                  'updated_at' => date('Y-m-d H:i:s'),
                              );
                              try{
                                DB::table('users')->insert(
                                      $saveUser
                                );
                              } catch ( Illuminate\Database\QueryException $e) {
                                  //var_dump($e->errorInfo);
                                return Redirect::to('/admin/users/upload-users')->with('error_message', "Excel not have proper fields values as defined format.");
                              }catch (\Exception $e) {
                                  return Redirect::to('/admin/users/upload-users')->with('error_message', "Excel not have proper fields values as defined format.");
                              }  
                              
                           }
                           }

                            }
    
                              
                           
                        }
                    }            
                    
                    
                      return Redirect::to('/admin/users/upload-users')->with('success_message', "Users Uploaded Successfully.");
                    
                }
            }
       
         return View::make('admin.users.users-upload',compact('userTypeArr'));
    }
 
    function password_generate($chars) 
    {
      $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
      return substr(str_shuffle($data), 0, $chars);
    }
    public function role() {
      if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
      }
        $input = Input::all();
        if (!empty($input)) 
	{
	    $rules = array(
                'name' => 'required|unique:admin_type', // make sure the first name field is not empty
            );
	    $messages = array(
		    'name.required' => 'The Role Name field is required',
                'name.unique' => 'This User Role already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/adminuser/role')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                );
                DB::table('admin_type')->insert($saveUser);
             

                return Redirect::to('/admin/adminuser/role')->with('success_message', 'User Role saved successfully.');
            }
        }
        $roles = DB::table('admin_type')->get();
        return View::make('/admin/adminuser/role')->with('roles',$roles);
        
      
    } 

    public function assignRoles($id = null)
    {

         if (!Session::has('adminid')) 
         {
            return Redirect::to('/admin/login');
         }
         $input = input::all();
         $dataModule = array();
         $subAuths = array();
          $dataModule11 = DB::table('modules')->get();
          //$dataModule11 = array($dataModule11,true);
          if(!empty($dataModule11))
          {
             foreach ($dataModule11 as $key => $value)
             {
                $dataModule[$value->controller][] = array('name'=>$value->name,'id'=>$value->id,'controller'=>$value->controller,'action'=>$value->action,'action_title'=>$value->action_title); 
             }
          }
         // print_r($dataModule); die;
          $dataSubAuths = DB::table('SubadminAuths')->where('admin_type_id',base64_decode($id))->get();
          if(!empty($dataSubAuths))
          {
            foreach ($dataSubAuths as $key11 => $value11)
            {
               $subAuths[$value11->controller]['action'][] =$value11->action;
               $subAuths[$value11->controller]['controller'] = $value11->controller;
            }
          }
          //print_r($subAuths); die;
          if(!empty($input))
          {
            $authData = isset($input['data'])?$input['data']:'';            
            DB::table('SubadminAuths')->where('admin_type_id',base64_decode($id))->delete();
            if(!empty($authData))
            {
                foreach ($authData as $keyMain => $valueMain)
                {
                    $module = $keyMain;
                    if(!empty($valueMain))
                    {
                        foreach ($valueMain as $keyController => $valueController)
                        {
                          $controller = $keyController;
                          if(!empty($valueController))
                          {
                            foreach ($valueController as $key => $value)
                            {
                               $SubadminAuths = new SubadminAuths();
                               $SubadminAuths->module = $module;
                               $SubadminAuths->controller = $controller;
                               $SubadminAuths->action = $key;
                               $SubadminAuths->admin_type_id  = base64_decode($id);
                               $SubadminAuths->save();
                              
                            }
                             
                          }
                        }
                    }
                }
                return redirect('/admin/adminuser/assignRoles/'.$id)->with('id',$id);
            }
          }

          //print_r($dataModule); exit();          
         return View::make('/admin/adminuser/assignRoles')->with('dataModule',$dataModule)->with('id',$id)->with('subAuths',$subAuths);
    }

}
