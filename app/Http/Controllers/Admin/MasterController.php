<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\EducationLevel;
use App\Models\Field;
use App\Models\IdType;
use App\Models\Admin;
use App\Models\KnowAccredit;
use App\Models\BusinessType;
use App\Models\LoanType;
use App\Models\EmailTemplates;
use App\Models\LoanPurpose;
use App\Models\ResidentialType;
use App\Models\PropertyOwnership;
use App\Models\EmploymentStatus;
use App\Models\JobTitle;
use App\Models\Specialization;
use App\Models\OfficeBranch;
use App\Models\Bank;
use App\Models\ReasonForLoan;
use App\Models\FaqCategories;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class MasterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Loans Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */
    public function index() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
     

        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $user_type_id = "";
        $searchByDateTo = "";
        $separator = array();
       
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
            $ids = [11,12,33,34];
            if(!in_array($id,$ids))
            {
              if (!empty($id)) 
              {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
              } 
            }
               
        }
        if(isset($input['active']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['required']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['is_required' => 1]);
               
            }
        }
        if(isset($input['optional']))
        {
            $id=$input['id'];
            $ids = [11,12,33,34];
            if(!in_array($id,$ids))
            {
               if (!empty($id)) {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['is_required' => 2]);
               
            }  
          }
        }
        $query = Field::sortable()
                ->where(function ($query) use ($search_keyword) {
            $query->where('id', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('name', 'LIKE', '%' . $search_keyword . '%');
        });
        if(!empty($input['form_type_id']))
        {
           $query = $query->where('form_type_id',$input['form_type_id']);
        }
        $separator = implode("/", $separator);
        // Get all the loans
        $Field = $query->orderBy('id', 'desc')->sortable()->paginate(10);
		$form_type = DB::table('form_type')->orderBy('name','asc')->where('status', 1)->pluck('name','id')->toArray();
   
        return View::make('admin/master/index', compact('Field','form_type'))->with('search_keyword', $search_keyword);
    }
/*    public function idType()
    {

        $idTypeData = DB::table('Id_type')
                        ->get();
        return View::make('admin/master/idType')->with('idTypeData',$idTypeData);
    }*/
     public function idType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
        $editdata='';
        $input = Input::all();
        // print_r($input);exit();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('Id_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('Id_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $id = $input['id'];
            if (!empty($id)) {
               $editdata = DB::table('Id_type')
                        ->where('id', $id)
                        ->first();
            }
            $editMode = true;
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('Id_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
        // if (isset($input['name'])) 
        if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:Id_type', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:Id_type,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The ID Type field is required',
                'name.unique' => 'This ID Type already exists'
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/idType';
                if ($editMode) {
                    $route = 'admin/master/idType?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput($input);
            } else {
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('Id_type')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('Id_type')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/idType')->with('success_message',$msg);
            }
        }
        $query = IdType::sortable();
       $idTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/idType',compact('editdata'))->with('idTypeData',$idTypeData);    
     } 
      public function educationlevel() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
        $editdata='';
        $input = Input::all();

        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('education_level')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('education_level')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id = $input['id'];
            if (!empty($id)) {
               $editdata=DB::table('education_level')
                        ->where('id', $id)
                        ->first();

            }
        }
       if(isset($input['delete']) )
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('education_level')
                        ->where('id', $id)
                        ->delete();
            }
        }

       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
          if(isset($input['edit']))
          {
               $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('education_level')
                        ->where('id', $id)
                        ->first();    
              }
          }
          
            $rules = array(
                'name' => 'required|unique:education_level', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules = array(
                    'name' => 'required|unique:education_level,name,'.$input['id'].',id', // make sure the first name field is not empty
                );
            }
            $messages = array(
                'name.required' => 'The Education Level field is required',
                'name.unique' => 'This Education Level already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/educationlevel';
                if ($editMode) {
                    $route = 'admin/master/educationlevel?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('education_level')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('education_level')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/educationlevel')->with('success_message',$msg);
            }
        }
        $query = EducationLevel::sortable();
       $educationlevelData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/educationlevel',compact('editdata'))->with('educationlevelData',$educationlevelData);    
     } 
     public function businessType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
        $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('business_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('business_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id = $input['id'];
            if (!empty($id)) {
               $editdata=DB::table('business_type')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('business_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:business_type', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules = array(
                    'name' => 'required|unique:business_type,name,'.$input['id'].',id', // make sure the first name field is not empty
                );
            }
            $messages = array(
                'name.required' => 'The Business Type field is required',
                'name.unique' => 'This Business Type already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/businessType';
                if ($editMode) {
                    $route = 'admin/master/businessType?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());

            } else {
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if (isset($input['edit']) && !empty($input['id'])) {
                    DB::table('business_type')->where('id',$input['id'])->update($saveUser);
                    $msg = 'Successfully updated';
                } else {
                    DB::table('business_type')->insert($saveUser);
                    $msg = 'Successfully added';
                }
                return Redirect::to('admin/master/businessType')->with('success_message',$msg);
            }
        }
        $query = BusinessType::sortable();
       $BusinessTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/businessType',compact('editdata'))->with('BusinessTypeData',$BusinessTypeData);    
     } 
    public function knowAccredit() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('know_accredit')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('know_accredit')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('know_accredit')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('know_accredit')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:know_accredit', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules = array(
                    'name' => 'required|unique:know_accredit,name,'.$input['id'].',id', // make sure the first name field is not empty
                );
            }
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/knowAccredit';
                if ($editMode) {
                    $route = 'admin/master/knowAccredit?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('know_accredit')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('know_accredit')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/knowAccredit')->with('success_message',$msg);
            }
        }
        $query = KnowAccredit::sortable();
       $KnowAccreditData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/knowAccredit',compact('editdata'))->with('KnowAccreditData',$KnowAccreditData);    
     } 
    public function loanType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('loan_type')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('loan_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:loan_type',
                'interest_rate'=>'required' // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:loan_type,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Loan Type field is required',
                'name.unique' => 'This Loan Type already exists', // make sure the first name field is not empty
                'interest_rate.required' => 'The Interest Rate field is required',
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/loanType';
                if ($editMode) {
                    $route = 'admin/master/loanType?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                     'interest_rate' => $input['interest_rate'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('loan_type')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('loan_type')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/loanType')->with('success_message',$msg);
            }
        }
        $query = LoanType::sortable();
       $LoanTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/loanType',compact('editdata'))->with('LoanTypeData',$LoanTypeData);    
     } 
     public function loanPurpose() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_purpose')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_purpose')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('loan_purpose')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('loan_purpose')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:loan_purpose', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:loan_purpose,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Loan Purpose field is required',
                'name.unique' => 'This Loan Purpose already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/loanPurpose';
                if ($editMode) {
                    $route = 'admin/master/loanPurpose?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('loan_purpose')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('loan_purpose')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/loanPurpose')->with('success_message',$msg);
            }
        }
        $query = LoanPurpose::sortable();
       $LoanPurposeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/loanPurpose',compact('editdata'))->with('LoanPurposeData',$LoanPurposeData);    
     }

     public function residentialType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
        $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('residential_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('residential_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('residential_type')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('residential_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:residential_type', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:residential_type,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Residential Type field is required',
                'name.unique' => 'This Residential Type already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/residentialType';
                if ($editMode) {
                    $route = 'admin/master/residentialType?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('residential_type')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('residential_type')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/residentialType')->with('success_message',$msg);
            }
        }
        $query = ResidentialType::sortable();
       $residentialTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/residentialType',compact('editdata'))->with('residentialTypeData',$residentialTypeData);    
     }
     public function propertyOwnership() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
        $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('property_ownership')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('property_ownership')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id = $input['id'];
            if (!empty($id)) {
               $editdata=DB::table('property_ownership')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('property_ownership')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:property_ownership', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:property_ownership,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Property Ownership field is required',
                'name.unique' => 'This Property Ownership already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/propertyOwnership';
                if ($editMode) {
                    $route = 'admin/master/propertyOwnership?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('property_ownership')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('property_ownership')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/propertyOwnership')->with('success_message',$msg);
            }
        }
        $query = PropertyOwnership::sortable();
       $PropertyOwnershipData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/propertyOwnership',compact('editdata'))->with('PropertyOwnershipData',$PropertyOwnershipData);    
     }
     public function employmentStatus() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('employment_status')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('employment_status')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('employment_status')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('employment_status')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:employment_status', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:employment_status,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Employment Status field is required',
                'name.unique' => 'This Employment Status already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/employmentStatus';
                if ($editMode) {
                    $route = 'admin/master/employmentStatus?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('employment_status')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('employment_status')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/employmentStatus')->with('success_message',$msg);
            }
        }
        $query = EmploymentStatus::sortable();
       $EmploymentStatusData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/employmentStatus',compact('editdata'))->with('EmploymentStatusData',$EmploymentStatusData);    
     }
     public function jobTitle() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('job_title')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('job_title')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('job_title')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('job_title')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:job_title', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:job_title,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Job Title field is required',
                'name.unique' => 'This Job Title already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/jobTitle';
                if ($editMode) {
                    $route = 'admin/master/jobTitle?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('job_title')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('job_title')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/jobTitle')->with('success_message',$msg);
            }
        }
        $query = JobTitle::sortable();
       $jobTitleData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/jobTitle',compact('editdata'))->with('jobTitleData',$jobTitleData);    
     }
     public function Specialization() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('specialization')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('specialization')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('specialization')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('specialization')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:specialization', // make sure the first name field is not empty
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:specialization,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Specialization field is required',
                'name.unique' => 'This Specialization already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/specialization';
                if ($editMode) {
                    $route = 'admin/master/specialization?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('specialization')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('specialization')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/specialization')->with('success_message',$msg);
            }
        }
        $query = Specialization::sortable();
       $specializationData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/specialization',compact('editdata'))->with('specializationData',$specializationData);    
     }
     public function officeBranch() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('office_branch')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('office_branch')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('office_branch')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('office_branch')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:office_branch',
                'phone'=>'required',
                'address'=>'required'
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:office_branch,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Office Branch Name is required',
                'name.unique' => 'This Office Branch Name already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/officeBranch';
                if ($editMode) {
                    $route = 'admin/master/officeBranch?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'phone'=>$input['phone'],
                    'address'=>$input['address'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('office_branch')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('office_branch')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/officeBranch')->with('success_message',$msg);
            }
        }
        $query = OfficeBranch::sortable();
       $officeBranchData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/officeBranch',compact('editdata'))->with('officeBranchData',$officeBranchData);    
     }
      public function bank() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('bank')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('bank')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        $editMode = false;
        if(isset($input['edit']))
        {
            $editMode = true;
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('bank')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('bank')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required|unique:bank'
            );
            if ($editMode) {
                $rules['name'] = 'required|unique:bank,name,'.$input['id'].',id';
            }
            $messages = array(
                'name.required' => 'The Bank Name is required',
                'name.unique' => 'This Bank Name already exists', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $route = 'admin/master/bank';
                if ($editMode) {
                    $route = 'admin/master/bank?edit=1&id=' . $input['id'];
                }
                return Redirect::to($route)->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('bank')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('bank')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/bank')->with('success_message',$msg);
            }
        }
        $query = Bank::sortable();
       $bankData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/bank',compact('editdata'))->with('bankData',$bankData);    
     }
     public function ReasonForLoan() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            //echo "elkj"; die;
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/reasonForLoan')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s'),                
                    'status'=>1
                   
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('reason_for_loan')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('reason_for_loan')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/ReasonForLoan')->with('success_message',$msg);
            }
        }
        $query = ReasonForLoan::sortable();
       $data = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/reasonForLoan',compact('editdata'))->with('data',$data);    
      }


     public function FaqCategories() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';      
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('faq_categories')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('faq_categories')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('faq_categories')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('faq_categories')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/FaqCategories')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'priority'=>isset($input['priority'])?$input['priority']:0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('faq_categories')->where('id',$input['id'])->update($saveUser);
                        $msg='Successfully updated';
                }
                else{
                          DB::table('faq_categories')->insert($saveUser);
                          $msg='Successfully added';
                }
                return Redirect::to('admin/master/FaqCategories')->with('success_message',$msg);
            }
        }
        $query = FaqCategories::sortable();
       $FaqCategoryData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/FaqCategories',compact('editdata'))->with('FaqCategoryData',$FaqCategoryData);    
     } 

    public function checkPriority()
    {
      $input = input::all();
      
      if($input['value'] > 0)
      {
         $data = DB::table('faq_categories')->where(['priority'=>$input['value']])->first();
        
          if(!empty($data))
          {
            echo 0; die;
          }
          else
          {
            echo 1; die;
          }
      }
      else
      {
        echo 2; die;
      }
    }
     
}
