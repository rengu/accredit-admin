<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\QrCodes;
use App\Models\Admin;
use App\Models\AdminQrCode;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class QrcodesController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Loans Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */
    public function index() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $data = AdminQrCode::where('status',1)->first();
       
        return View::make('admin/QrCodes/index', compact('data'));
    }
  
  
    

    public function generateQrCode()
    {
      $qrCode='';
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        $id = Session::get('adminid');
   
        $data = Admin::where('admin_type_id',1)->where('id',$id)->first();

        $data = json_decode(json_encode($data), true);
        if(!empty($data))
        {
            $secretKey = $data['secret_key'];
            $email = $data['email'];
            $qrCode = $this->getQRCodeGoogleUrl(1,$email, $secretKey,'Accredit');
            $qrCode1 = $this->getQRCodeGoogleUrl(2,$email, $secretKey,'Accredit');
            Session::put('qrcode', $qrCode);
            $dataAdminQr = DB::table('admin_qr_code')->first();            
            if(!empty($dataAdminQr))
            {
              DB::table('admin_qr_code')->where('id',1)->update(array('url'=>$qrCode,'url_ios'=>$qrCode1));
            }
            else
            {
              $post = new AdminQrCode;
              $post->url = $qrCode;
              $post->status = 1;              
              $post->save();  
            }
           
        }

        return Redirect::back()->with('qrcode',$qrCode);
         
    }

     public function getQRCodeGoogleUrl($name, $secret, $title = null, $params = array())
    {
        
        // print_r('otpauth://totp/'.$name.'?secret='.$secret.'&issuer='.$title.''); die;
        $width = !empty($params['width']) && (int) $params['width'] > 0 ? (int) $params['width'] : 200;
        $height = !empty($params['height']) && (int) $params['height'] > 0 ? (int) $params['height'] : 200;
        $level = !empty($params['level']) && array_search($params['level'], array('L', 'M', 'Q', 'H')) !== false ? $params['level'] : 'M';

        //$urlencoded = urlencode('otpauth://totp/'.$name.'?secret='.$secret.'&issuer='.$title.'');
        if($name == 1){
        $urlencoded = urlencode('https://play.google.com/store/apps/details?id=com.accredit.app');
        }
        if($name == 2){
        $urlencoded = urlencode('https://apps.apple.com/in/app/accredit/id1518945361');
        }

      return  'https://chart.googleapis.com/chart?chs='.$width.'x'.$height.'&chld='.$level.'|0&cht=qr&chl='.$urlencoded.'';
        
    }




}
