<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ForgotPassword;
use App\Models\EmailTemplates;
use Illuminate\Http\Request;
use Session,
    Input,
    Validator,
    DB,
    Redirect,
    View,Mail;


use PHPMailer\PHPMailer;
use PHPMailer\Exception;    

class AdminController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Admin Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin login form to the admin.
     *
     * @return Response
     */
    public function login() {

        if (Session::has('adminid')) {
            return Redirect::to('/admin/dashboard');
        }
        $input = Input::all();
        Session::forget('is_forgot');
        if (!empty($input)) {
            
            $username = $input['username'];
            $password = md5($input['password']);
            //$captcha = $input['captcha'];

            $rules = array(
                'username' => 'required', // make sure the username field is not empty
                'password' => 'required' // make sure the password field is not empty
            );

         /*   $securityCapcha = Session::get('security_number');
            if($securityCapcha!=$captcha){
                Session::forget('is_forgot');
                Session::put('error_message', "Security code incorrect.");
                return Redirect::to('/admin/login');
            }*/

            // run the validation rules on the inputs from the form
            $validator = Validator::make($input, $rules);

            // if the validator fails, redirect back to the form
            if ($validator->fails()) {

                return Redirect::to('/admin/login')
                                ->withErrors($validator) // send back all errors to the login form
                                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
            } else {
                
                // create our user data for the authentication
                $adminuser = DB::table('admins')
                        ->where('username', $username)
                        ->where('password', $password)
                        ->first();
                        
                if (!empty($adminuser)) {

                    // destroy captcha from login
                    //Session::forget('captcha');

                    // return to dashboard page
                    Session::put('adminid', $adminuser->id);
                    Session::put('admin_type_id', $adminuser->admin_type_id);
                    Session::put('role_id', $adminuser->role_id);
                    return Redirect::to('/admin/dashboard');
                } else {

                    // return error message
                    //Session::put('captcha', 1);
                    Session::put('error_message', "Invalid username or password");
                    return Redirect::to('/admin/login');
                }
            }
            return view('admin.admins.login');
        } else {
            return View::make('admin.admins.login');
        }
    }

    // admin dashboard page
    public function dashboard() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
        // total users
        $totalPayment = 0;
        $totalUsers = DB::table('users')->count();
        $totalCompanyUsers = DB::table('users')->where('role_id',2)->count();
        $totalIndividualUsers = DB::table('users')->where('role_id',1)->count();
        $totalActiveLoanIndividuals = DB::table('loan_enquiry_individual')->where('active_status',2)->count();
        $totalActiveLoanCompany = DB::table('loan_enquiry_company')->where('active_status',2)->count();

        $totalApprovedLoanIndividuals = DB::table('loan_enquiry_individual')->where('approved_status',2)->count();

        $totalApprovedLoanCompany = DB::table('loan_enquiry_company')->where('approved_status',2)->count();

        $totalActiveLoan = $totalActiveLoanIndividuals+$totalActiveLoanCompany;
        $totalApprovedLoan = $totalApprovedLoanIndividuals+$totalApprovedLoanCompany;
        $dataPayments = DB::table('payments')->where('is_completed',1)->get();
        if(!$dataPayments->isEmpty())
        {
            foreach ($dataPayments as $key => $value)
            {
               $totalPayment = $totalPayment+$value->total_emi;
            }
        }

        return View::make('admin/admins/dashboard')
                ->with(compact('totalUsers','totalCompanyUsers','totalIndividualUsers','totalActiveLoan','totalApprovedLoan','totalPayment'));
    }

    // admin change password page
    public function changePassword() {

        if (!Session::has('adminid')) {
            return Redirect::to('/admin/admins/login');
        }
        $input = Input::all();
        if (!empty($input)) {
            $opassword = md5($input['opassword']);
            $password = md5($input['password']);
            $rules = array(
                'opassword' => 'required', // make sure the old password field is not empty
                'password' => 'required', // make sure the password field is not empty
                'cpassword' => 'required' // make sure the confirm password field is not empty
            );

            // run the validation rules on the inputs from the form
            $validator = Validator::make($input, $rules);

            // if the validator fails, redirect back to the form
            if ($validator->fails()) {

                return Redirect::to('/admin/admins/changePassword')
                                ->withErrors($validator) // send back all errors to the login form
                                ->withInput(Input::except('opassword'))
                                ->withInput(Input::except('password'))
                                ->withInput(Input::except('cpassword')); // send back the input (not the password) so that we can repopulate the form
            } else {
                // create our user data for the authentication
                $adminuser = DB::table('admins')
                        ->where('password', $opassword)
                        ->first();

                if (!empty($adminuser)) {
                    // check new password with old password
                    if ($password == $opassword) {
                        // return error message
                        Session::put('error_message', "You cannot put your old password for the new password!");
                        return Redirect::to('/admin/changePassword');
                    }

                    // update admin password
                    DB::table('admins')
                            ->where('id', Session::get("adminid"))
                            ->update(array('password' => $password));
                    Session::put('success_message', "Password successfully changed");
                    return Redirect::to('/admin/changePassword');
                } else {

                    // return error message
                    Session::put('error_message', "Please enter correct old password");
                    return Redirect::to('/admin/changePassword');
                }
            }
        } else {
            return View::make('admin.admins.changePassword');
        }
    }

    // admin edit profile page
    public function editProfile() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
         $roles = DB::table('admin_type')->orderBy('name','asc')->pluck('name','id')->toArray();
        // create our user data for the authentication
        $adminuser = DB::table('admins')
                ->where('id', Session::get('adminid'))
                ->first();

        $input = Input::all();
        if (!empty($input)) {

            // set validatin rules
            $rules = array(
                'name' => 'required',
                'email' => 'required|email',
                'username' => 'required|alpha_num',
                'phone' => 'required',
                'address' => 'required',
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make($input, $rules);

            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/editProfile')
                                ->withErrors($validator)->withInput(Input::all());
            } else {

                // update admin profile
                $data = array(
                    'name' => trim($input['name']),
                    'email' => trim($input['email']),
                    'email_1' => trim($input['email_1']),
                    'username' => trim($input['username']),
                    'phone' => trim($input['phone']),
                    'phone_2' => trim($input['phone_2']),
                    'phone_3' => trim($input['phone_3']),
                    'fax' => trim($input['fax']),
                    'address' => trim($input['address']),
                );
                DB::table('admins')
                        ->where('id', Session::get("adminid"))
                        ->update($data);
                Session::put('success_message', "Profile Information successfully updated.");
                return Redirect::to('/admin/editProfile');
            }
        } else {
            return View::make('admin.admins.editProfile',compact('roles'))->with('detail', $adminuser);
        }
    }
    //admin setting
     public function setting() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
     
         $emailTemplate = EmailTemplates::find(4);
         $settingdata =  DB::table('setting')->where('id',1)->first();
         if(isset($settingdata->is_individual_loan_show))
         {
           $emailTemplate->Individual= $settingdata->is_individual_loan_show;
          $emailTemplate->Company= $settingdata->is_company_loan_show; 
         }
         
                  
            $send_from =  $emailTemplate->send_from;
        $input = Input::all();
        if (!empty($input)) {

            // set validatin rules
            $rules = array(
                'send_from' => 'required|email'
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make($input, $rules);

            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/setting')
                                ->withErrors($validator)->withInput(Input::all());
            } else {
               if(isset($input['Individual']) || isset($input['Company']))
               {
                $datasetting = array(
                    'is_individual_loan_show' => $input['Individual'],
                     'is_company_loan_show' => $input['Company']
                );
                DB::table('setting')->where('id',1)
                        ->update($datasetting);
               }
                // update admin profile
                $data = array(
                    'send_from' => trim($input['send_from'])
                );
                DB::table('email_templates')
                        ->update($data);
                Session::put('success_message', "Configration setting successfully updated.");
                return Redirect::to('/admin/setting');
            }
        } else {
            return View::make('admin.admins.setting')->with('detail', $emailTemplate);
        }
    }
    // admin forgot password
    public function forgotpassword() {
        $this->layout = false;
        $input = Input::all();
       
        if (!empty($input)) {
            $email = $input['email'];
            $rules = array(
                'email' => 'required'
            );

            // run the validation rules on the inputs from the form
            $validator = Validator::make($input, $rules);

            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/admin/login')
                                ->withErrors($validator)->withInput(Input::all());
            } else {

                // create our user data for the authentication
                $adminuser = DB::table('admins')
                        ->where('email', $email)
                        ->first();

                if (!empty($adminuser)) {

                    $hashString = md5($adminuser->id).'/'.md5($adminuser->email);
                    $link = "<a href=".HTTP_PATH.'admin/reset-password/'.$hashString.">click here</a>";

                    // save data in forgot password
                    $forgotPasswordObj = new ForgotPassword();
                    $forgotPasswordObj->user_id = $adminuser->id;
                    $forgotPasswordObj->token = md5($adminuser->email);
                    $forgotPasswordObj->created_at = date('Y-m-d H:i:s');
                    $forgotPasswordObj->updated_at = date('Y-m-d H:i:s');
                    $forgotPasswordObj->save();

                    // send and email 
                    $emailTemplate = EmailTemplates::find(4);
                    $messageBody = $emailTemplate->message;
                    $messageBody = str_replace('[name]', $adminuser->username, $messageBody);
                    $messageBody = str_replace('[link]', $link, $messageBody);

                   // return view('emails.message')->with('data',$messageBody);   // check email before sending
                    $data = ['message' => $messageBody];
                    // Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$adminuser)
                    // {
                    //     $m->to($adminuser->email,$adminuser->username)
                    //     ->subject($emailTemplate->subject)
                    //     ->from($emailTemplate->send_from);
                    // });
                    /************* PHP Mailer *************************/
                    $mail = new PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
                    try {
                        //Server settings
                        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                        $mail->isSMTP();                                      // Set mailer to use SMTP
                        $mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true;                               // Enable SMTP authentication
                        $mail->Username = SMTP_USERNAME;                 // SMTP username
                        $mail->Password = SMTP_PASSWORD;                           // SMTP password
                        $mail->SMTPSecure = SMTP_SECURE;                            // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = PORT;                                    // TCP port to connect to

                        //Recipients
                        $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
                        $mail->addAddress($adminuser->email);     // Add a recipient
                        //Content
                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = $emailTemplate->subject;
                        $mail->Body    = $messageBody;

                        $mail->send();
                    } catch (Exception $e) {
                           // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                    }
                     return Redirect::back()->with('success_message','An email sent you with reset link on your registered email. Please check.');
                    return Redirect::to('/admin/login');
                } else {

                    // return error message
                    Session::put('error_message', "Please enter correct email address");
                    Session::put('is_forgot', "1");
                    return Redirect::to('/admin/login');
                }
            }
        } else {
            
        }
    }

    // goto admin login page
    public function logout() {
        Session::forget('adminid');
        return Redirect::to('/admin/login');
    }

      /************** Reset Password ***********************/
    public function resetpassword(Request $request, $params1 = null, $params2 = null){

        if(empty($params1) || empty($params2)){
            return Redirect::to('admin/login')->with('error_message','Something went wrong in URL');
        }  else {
            $userData = ForgotPassword::where('token',$params2)->first();

            if(empty($userData)) {
                return Redirect::to('admin/login')->with('error_message','Something went wrong in URL');
            } else {
                return View('admin.admins.resetpassword')->with('id',$userData->user_id);
            }
        }        
    }

     public function resetpasswordCreate(Request $request){

         $validator = Validator::make($request->all(), [
            'password' => 'required',
            'cpassword' => 'required',
            ]);
        
        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        } else {
             $user_id = $request->get('id');
           
             // create our user data for the authentication
                $adminuser = DB::table('admins')
                        ->where('id', $user_id)
                        ->first();

            if (empty($adminuser)) {
                 return redirect('admin/login')->with('error_message',"Something went wrong!");
            }
            $password = md5($request->get('password'));
            // update password
            DB::table('admins')
                        ->where('id',$user_id)
                        ->update(array('password' => $password));
                        
            return redirect('admin/login')->with('success_message',"Password successfully updated. Please login.");  
        }

    }


}
