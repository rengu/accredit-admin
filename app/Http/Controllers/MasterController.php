<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\EducationLevel;
use App\Models\Field;
use App\Models\IdType;
use App\Models\Admin;
use App\Models\KnowAccredit;
use App\Models\BusinessType;
use App\Models\LoanType;
use App\Models\EmailTemplates;
use App\Models\LoanPurpose;
use App\Models\ResidentialType;
use App\Models\PropertyOwnership;
use App\Models\EmploymentStatus;
use App\Models\JobTitle;
use App\Models\Specialization;
use App\Models\OfficeBranch;
use App\Models\ReasonForLoan;
use Session,
    Input,
    HTML,
    Validator,
    DB,
    Redirect,
    View,
    Mail;
use Excel;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

class MasterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Loans Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "login" for admin that
      | are authenticated.
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application admin users listing to the admin.
     *
     * @return Response
     */
    public function index() {
        if (!Session::has('adminid')) {
            return Redirect::to('/admin/login');
        }
     

        $input = Input::all();
        $search_keyword = "";
        $searchByDateFrom = "";
        $user_type_id = "";
        $searchByDateTo = "";
        $separator = array();
       
        if (!empty($input['search'])) {
            $search_keyword = trim($input['search']);
        }
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['active']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['required']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['is_required' => 1]);
               
            }
        }
        if(isset($input['optional']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('field')
                        ->where('id', $id)
                        ->update(['is_required' => 2]);
               
            }  
        }
        $query = Field::sortable()
                ->where(function ($query) use ($search_keyword) {
            $query->where('id', 'LIKE', '%' . $search_keyword . '%')
            ->orwhere('name', 'LIKE', '%' . $search_keyword . '%');
        });
        if(!empty($input['form_type_id']))
        {
           $query = $query->where('form_type_id',$input['form_type_id']);
        }
        $separator = implode("/", $separator);
        // Get all the loans
        $Field = $query->orderBy('id', 'desc')->sortable()->paginate(10);
		$form_type = DB::table('form_type')->orderBy('name','asc')->where('status', 1)->pluck('name','id')->toArray();
   
        return View::make('admin/master/index', compact('Field','form_type'))->with('search_keyword', $search_keyword);
    }
/*    public function idType()
    {

        $idTypeData = DB::table('Id_type')
                        ->get();
        return View::make('admin/master/idType')->with('idTypeData',$idTypeData);
    }*/
     public function idType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('Id_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('Id_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('Id_type')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('Id_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
        if (isset($input['name'])) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/idType')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('Id_type')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('Id_type')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/idType')->with('success_message',$msg);
            }
        }
        $query = IdType::sortable();
       $idTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/idType',compact('editdata'))->with('idTypeData',$idTypeData);    
     } 
      public function educationlevel() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
        $editdata='';
        $input = Input::all();

        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('education_level')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('education_level')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('education_level')
                        ->where('id', $id)
                        ->first();    
            }
        }
       if(isset($input['delete']) )
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('education_level')
                        ->where('id', $id)
                        ->delete();
            }
        }

       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
          if(isset($input['edit']))
          {
               $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('education_level')
                        ->where('id', $id)
                        ->first();    
              }
          }
          
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/educationlevel')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('education_level')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('education_level')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/educationlevel')->with('success_message',$msg);
            }
        }
        $query = EducationLevel::sortable();
       $educationlevelData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/educationlevel',compact('editdata'))->with('educationlevelData',$educationlevelData);    
     } 
     public function businessType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('business_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('business_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('business_type')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('business_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/businessType')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('business_type')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('business_type')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/businessType')->with('success_message',$msg);
            }
        }
        $query = BusinessType::sortable();
       $BusinessTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/businessType',compact('editdata'))->with('BusinessTypeData',$BusinessTypeData);    
     } 
    public function knowAccredit() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('know_accredit')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('know_accredit')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('know_accredit')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('know_accredit')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/knowAccredit')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('know_accredit')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('know_accredit')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/knowAccredit')->with('success_message',$msg);
            }
        }
        $query = KnowAccredit::sortable();
       $KnowAccreditData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/knowAccredit',compact('editdata'))->with('KnowAccreditData',$KnowAccreditData);    
     } 
    public function loanType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('loan_type')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('loan_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required',
                'interest_rate'=>'required' // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/loanType')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                     'interest_rate' => $input['interest_rate'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('loan_type')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('loan_type')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/loanType')->with('success_message',$msg);
            }
        }
        $query = LoanType::sortable();
       $LoanTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/loanType',compact('editdata'))->with('LoanTypeData',$LoanTypeData);    
     } 
     public function loanPurpose() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_purpose')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('loan_purpose')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('loan_purpose')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('loan_purpose')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/loanPurpose')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('loan_purpose')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('loan_purpose')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/loanPurpose')->with('success_message',$msg);
            }
        }
        $query = LoanPurpose::sortable();
       $LoanPurposeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/loanPurpose',compact('editdata'))->with('LoanPurposeData',$LoanPurposeData);    
     }
     public function residentialType() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('residential_type')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('residential_type')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('residential_type')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('residential_type')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/residentialType')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('residential_type')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('residential_type')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/residentialType')->with('success_message',$msg);
            }
        }
        $query = ResidentialType::sortable();
       $residentialTypeData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/residentialType',compact('editdata'))->with('residentialTypeData',$residentialTypeData);    
     }
     public function propertyOwnership() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('property_ownership')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('property_ownership')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('property_ownership')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('property_ownership')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/propertyOwnership')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('property_ownership')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('property_ownership')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/propertyOwnership')->with('success_message',$msg);
            }
        }
        $query = PropertyOwnership::sortable();
       $PropertyOwnershipData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/propertyOwnership',compact('editdata'))->with('PropertyOwnershipData',$PropertyOwnershipData);    
     }
     public function employmentStatus() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('employment_status')
                        ->where('id', $id)
                        ->update(['status' => 2]);
               
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('employment_status')
                        ->where('id', $id)
                        ->update(['status' => 1]);
               
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('employment_status')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('employment_status')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/employmentStatus')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('employment_status')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('employment_status')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/employmentStatus')->with('success_message',$msg);
            }
        }
        $query = EmploymentStatus::sortable();
       $EmploymentStatusData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/employmentStatus',compact('editdata'))->with('EmploymentStatusData',$EmploymentStatusData);    
     }
     public function jobTitle() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('job_title')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('job_title')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('job_title')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('job_title')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/jobTitle')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('job_title')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('job_title')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/jobTitle')->with('success_message',$msg);
            }
        }
        $query = JobTitle::sortable();
       $jobTitleData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/jobTitle',compact('editdata'))->with('jobTitleData',$jobTitleData);    
     }
     public function Specialization() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('specialization')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('specialization')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('specialization')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('specialization')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/specialization')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('specialization')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('specialization')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/specialization')->with('success_message',$msg);
            }
        }
        $query = Specialization::sortable();
       $specializationData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/specialization',compact('editdata'))->with('specializationData',$specializationData);    
     }
     public function officeBranch() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('office_branch')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('office_branch')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('office_branch')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('office_branch')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/officeBranch')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],
                    'created' => date('Y-m-d H:i:s'),
                    'status'=>1,
                    'modified' => date('Y-m-d H:i:s'),
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('office_branch')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('office_branch')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/officeBranch')->with('success_message',$msg);
            }
        }
        $query = OfficeBranch::sortable();
       $officeBranchData = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/officeBranch',compact('editdata'))->with('officeBranchData',$officeBranchData);    
     }

     public function ReasonForLoan() 
     {
        if (!Session::has('adminid')) {
              return Redirect::to('/admin/login');
        }
       $editdata='';
        $input = Input::all();
        if(isset($input['deactivate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->update(['status' => 2]);
            } 
        }
        if(isset($input['activate']))
        {
            $id=$input['id'];
               if (!empty($id)) {
                DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->update(['status' => 1]);
            }
        }
        if(isset($input['edit']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               $editdata=DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->first();    
            }
        }
        if(isset($input['delete']))
        {
            $id=$input['id'];
               if (!empty($id)) {
               DB::table('reason_for_loan')
                        ->where('id', $id)
                        ->delete();
            }
        }
       if(isset($input['formsubmit']) && $input['formsubmit'] ==1) 
        {
            //echo "elkj"; die;
            $rules = array(
                'name' => 'required', // make sure the first name field is not empty
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('admin/master/reasonForLoan')->withErrors($validator)->withInput(Input::all());
            } else {
               
                $saveUser = array(
                    'name' => $input['name'],                    
                    'status'=>1
                   
                );

                if(isset($input['edit']) && !empty($input['id']))
                {
                        DB::table('reason_for_loan')->where('id',$input['id'])->update($saveUser);
                        $msg='successfully update';
                }
                else{
                          DB::table('reason_for_loan')->insert($saveUser);
                          $msg='successfully Add';
                }
                return Redirect::to('admin/master/ReasonForLoan')->with('success_message',$msg);
            }
        }
        $query = ReasonForLoan::sortable();
       $data = $query->orderBy('id', 'desc')->sortable()->paginate(10);
          return View::make('admin/master/reasonForLoan',compact('editdata'))->with('data',$data);    
     }
     
}
