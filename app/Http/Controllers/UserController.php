<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ForgotPassword;
use App\Models\EmailTemplates;
use Illuminate\Http\Request;
use Response;
use Illuminate\Notifications\Notifiable;
use Redirect;
use Validator;
use Hash;
use Auth;
use Mail;
use Input;
use DB;
use Cookie;
use JWTAuth;
use JWTAuthException;



class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['except' => array('login','createLogin','signup','createUser','forgotpassword','resetpassword','resetpasswordCreate','getAuthUser')]);
    }
    
    /************** signup ***********************/
    public function signup(){
    	return view('user.signup');

    }

    /************** create User ***********************/
    public function createUser(Request $request){
    	 $validator = Validator::make($request->all(), [
        	'email' => 'required|unique:users',
    		'username' => 'required|unique:users',
    		'password' => 'required',
    		]);
    	
    	if($validator->fails()){
    		return Redirect::back()->withErrors($validator);
    	} else {
            $slug = $this->createUniqueSlug(trim($request->get('username')), 'users');
    		$type = $request->get('type');
    		if($type=='Hire'){
    			$usertype = "client";
    		} else {
    			$usertype = "freelancer";
    		}
    		$password = Hash::make($request->get('password'));

    		// save user data
    		$userInfo = new User();
            $userInfo->slug = $slug;
    		$userInfo->email = trim($request->get('email'));
    		$userInfo->username = trim($request->get('username'));
    		$userInfo->password = $password;
    		$userInfo->usertype = $usertype;
    		$userInfo->created_at = date('Y-m-d H:i:s');
    		$userInfo->updated_at = date('Y-m-d H:i:s');
    		$userInfo->save();
    		
            // send and email 
            $emailTemplate = EmailTemplates::find(2);
            $messageBody = $emailTemplate->message;
            $messageBody = str_replace('[name]', $request->get('username'), $messageBody);
            $messageBody = str_replace('[email]', trim($request->get('email')), $messageBody);
            $messageBody = str_replace('[password]', trim($request->get('password')), $messageBody);

                //return view('emails.message')->with('data',$messageBody);   // check email before sending
            $data = ['message' => $messageBody];
            $email = $request->get('email');
            Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$email)
            {
                $m->to($email)
                ->subject($emailTemplate->subject)
                ->from($emailTemplate->send_from);
            });

    	}
    	return redirect('user/login')->with("success_message","Your have successfully registered. Please login here.");

    }

    //  /************** login ***********************/
    // public function login(){
    // 	return view('user.login');

    // }

     public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['invalid_email_or_password'], 422);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        // $new_user_id = $request->user_id;
        // $user = User::find($new_user_id);
        return response()->json(['result' => $user]);
    }

     /************** login User ***********************/
    public function createLogin(Request $request){
         $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
            ]);
        
        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        } else {
            
            $email = trim($request->get('email'));
            $password = $request->get('password');
            $remember = $request->get('remember');
            
            if(null !== $request->get('remember')) {
                Cookie::queue('email', $email, 60);
                Cookie::queue('password', $password, 60);
                Cookie::queue('remember', $password, 60);
            } else {
                Cookie::queue('email', '', 60);
                Cookie::queue('password', '', 60);
                Cookie::queue('remember', '', 60);
            }
            
            $userdata = array(
                    'email' =>$email,
                    'password' =>$password
                );
            if (Auth::attempt($userdata, $remember)) {
                $userType= Auth::user()->usertype;
                if($userType=='client') {
                    return redirect('/dashboard-settings');
                } else {
                    return redirect('/user/profile-edit');
                }
                
            } else {
                return Redirect::back()->with('error_message',"Invalid Email or Password!");  
            }
        }
    }

   

    /************** Logout ***********************/
    public function logout(){
        Auth::logout(); // logging out user
        return Redirect::to('user/login'); // redirection to login screen

    }

     /************** Forgot Password ***********************/
    public function forgotpassword(Request $request){
        if(!empty($request->all())){
            $email = trim($request->get('email'));
            $userData = User::where('email',$email)->first();
            if(!empty($userData)) {
                $hashString = md5($userData->id).'/'.md5($userData->email);
                $link = "<a href=".HTTP_PATH.'reset-password/'.$hashString.">click here</a>";

                // save data in forgot password
                $forgotPasswordObj = new ForgotPassword();
                $forgotPasswordObj->user_id = $userData->id;
                $forgotPasswordObj->token = md5($userData->email);
                $forgotPasswordObj->created_at = date('Y-m-d H:i:s');
                $forgotPasswordObj->updated_at = date('Y-m-d H:i:s');
                $forgotPasswordObj->save();

                // send and email 
                $emailTemplate = EmailTemplates::find(1);
                $messageBody = $emailTemplate->message;
                $messageBody = str_replace('[name]', $userData->username, $messageBody);
                $messageBody = str_replace('[link]', $link, $messageBody);

                //return view('emails.message')->with('data',$messageBody);   // check email before sending
                $data = ['message' => $messageBody];
                Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$userData)
                {
                    $m->to($userData->email,$userData->username)
                    ->subject($emailTemplate->subject)
                    ->from($emailTemplate->send_from);
                });
                 return Redirect::back()->with('success_message','An email sent you with reset link on your registered email. Please check.');


            } else {
                return Redirect::back()->with('error_message',"We cannot find an account for contains this email. Please enter your correct email.");  
            }
        }
        return view('user.forgotpassword');

    }

     /************** Reset Password ***********************/
    public function resetpassword(Request $request, $params1 = null, $params2 = null){
        if(empty($params1) || empty($params2)){
            return Redirect::to('user/login')->with('error_message','Something went wrong in URL');
        }  else {
            $userData = ForgotPassword::where('token',$params2)->first();
            if(empty($userData)) {
                return Redirect::to('user/login')->with('error_message','Something went wrong in URL');
            } else {
                return View('user.resetpassword')->with('user_id',$userData->user_id);
            }
        }        
    }

     /************** Reset Create Password ***********************/
    public function resetpasswordCreate(Request $request){
         $validator = Validator::make($request->all(), [
            'password' => 'required',
            'cpassword' => 'required',
            ]);
        
        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        } else {
            $user_id = $request->get('user_id');
            $password = Hash::make($request->get('password'));
            // update password
            User::where('id', $user_id)->update(array('password' => $password));
                        
            return redirect('user/login')->with('success_message',"Password successfully updated. Please login.");  
        }

    }


   

     /************** Change Password ***********************/
    public function changePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'opassword' => 'required',
            'password' => 'required',
            'cpassword' => 'required',
            ]);
        
        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        } else {

            $user_id = Auth::user()->id;
            $old_password = $request->get('opassword');
           
            $new_password = $request->get('password');

            // check old password
            if (Hash::check($old_password, Auth::user()->password)) { 
                if (Hash::check($new_password, Auth::user()->password)) { 
                    return Response::json(array('success' => false,'message'=>"Old password and new password cannot same."));
                } else {
                    $password = Hash::make($new_password);
                    // update password
                    User::where('id', $user_id)->update(array('password' => $password));
                    return Response::json(array('success' => true,'message'=>"Password updated successfully."));
                }
            } else {
                return Response::json(array('success' => false,'message'=>"Old password not correct."));
            }
        }
    }

    /************** Edit basic info ***********************/
    public function editBasicInfo(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'profile_image' => 'mimes:jpeg,bmp,png,jpg',
            ]);
        
        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        } else {
            $old_profile_image = $request->get('old_profile_image');
            if (Input::hasFile('profile_image')) {
                $file = Input::file('profile_image');
                $imageName = time() . $file->getClientOriginalName();
                $file->move(UPLOAD_FULL_USER_IMAGE_PATH, time() . $file->getClientOriginalName());
                @unlink(UPLOAD_FULL_USER_IMAGE_PATH . $old_profile_image);
            } else {
                $imageName = $old_profile_image;
            }

            // update edit basic info
            $userObj = User::find(Auth::user()->id);
            $userObj->name= trim($request->get('name'));
            $userObj->address= trim($request->get('address'));
            $userObj->timezone_id= $request->get('timezone_id');
            $userObj->phone= trim($request->get('phone'));
            $userObj->profile_image= $imageName;
            $userObj->save();
            $userType= Auth::user()->usertype;
            if($userType=='client') {
                return redirect('client/dashboard')->with('success_message',"Profile successfully updated.");
            } else {
                return redirect('user/dashboard')->with('success_message',"Profile successfully updated.");
            }
        }
    }


    
}
