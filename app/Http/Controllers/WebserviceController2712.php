<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ForgotPassword;
use App\Models\EmailTemplates;
use Illuminate\Http\Request;
use Response;
use Illuminate\Notifications\Notifiable;
use Redirect;
use Validator;
use Hash;
use Auth;
use Mail;
use Input;
use DB;
use Cookie;
use JWTAuth;
use JWTAuthException;



class WebserviceController extends Controller
{
    public function __construct(){
    }

    /***************** get request from json a *****************/
    public function getJson($jData){
        $data = json_decode($jData,true);
        if(isset($data) && !empty($data)){
            $_REQUEST = $data;
        } else {
            $data = $_POST;
        }
        return $data;
    }

    /***************** null value filter from array *****************/
    public function filterNullValueFromArray($arr){
        $resultArr = array();
        foreach($arr as $key=>$val){
            if($val===NULL){
                $resultArr[$key] = '';
            } else {
                $resultArr[$key] = $val;
            }
        }
        return $resultArr;
    }
    
    /************** Login user ***********************/
    public function login(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        

        $username = $data['username'];
        $password = $data['password'];
        $user_type_id = $data['user_type_id'];
        // check login with username password
        $credentials = array('username'=>$username, 'password'=>$password);
        $token = null;

        try {
           if (!$token = JWTAuth::attempt($credentials)) {
           
            return response()->json(array('status'=>false,'message'=>'Invalid username or password'));
           }
        } catch (JWTAuthException $e) {
            return response()->json(array('status'=>false,'message'=>'failed to create token'));
        }
        // get user data
        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $user_type = $user->user_type_id;
         // check usertype match
        if($user_type_id!=$user_type){
            return response()->json(array('status'=>false,'message'=>'This user not belong to selected user type.'));
        }
        // check user active or not
        if($user->status!=1){
            return response()->json(array('status'=>false,'message'=>'You are not deactivated from admin. Please contact to admin.'));   
        }
        // check user role active
        $userTypeData = DB::table('user_types')->where('id',$user_type)->first();
        if($userTypeData->status!=1){
            return response()->json(array('status'=>false,'message'=>'Role Deactivated from admin.'));      
        }
        
        return response()->json(array('status'=>true,'message'=>'user token create successfully.','token'=>$token));
    }

    /************** Get User Profile ***********************/
    public function getUser(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $userData = DB::table('users')->where('id',$user_id)->first();
        return response()->json(array('status'=>true,'message'=>'user login successfully','result'=>$userData));
    }

    /************** get list of user types ***********************/
    public function getUserType(Request $request){

        $usertypeData = DB::table('user_types')->select('name','id')->get();
        $resultArr = array();
        foreach($usertypeData as $data){
            $dataArr['id'] = $data->id;
            $dataArr['name'] = $data->name;
            $resultArr[] = $dataArr;
            unset($dataArr);
        }
        return response()->json(array('status'=>true,'message'=>'role data','result'=>$resultArr));
    }

     /************** Get All Preventive Type List  ***********************/
    public function preventiveTypeList(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $allPreventiveType = DB::table('preventive')->where('status',1)->orderBy('name','asc')->select('id','name')->get();
        $resultArr = array();
        foreach($allPreventiveType as $data){
            $dataArr['id'] = $data->id;
            $dataArr['name'] = $data->name;
            $resultArr[] = $dataArr;
            unset($dataArr);
        }
        return response()->json(array('status'=>true,'message'=>"all preventive type data.",'result'=>$resultArr));
    }


    /************** get roles based on user type  ***********************/
    public function rolePermission(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $user_type_id = $user->user_type_id;
        $userTypeData = DB::table('user_types')->where('id',$user_type_id)->first();
        $permissons = explode(',',$userTypeData->permissions);
        $userRolePermissions = DB::table('roles')->whereIn('id',$permissons)->select('id','name','slug')->get();
        $resultArrR = array();
        $resultArr['id'] = $userTypeData->id;
        $resultArr['slug'] = $userTypeData->slug;
        $resultArr['name'] = $userTypeData->name;
        $resultArr['status'] = $userTypeData->status;
        $resultArr['surveillance_permission'] = $userTypeData->spermissons;
        $resultArr['site_estate_permission'] = $userTypeData->sspermissons;
        $resultArr['search_type'] = $userTypeData->search_type;
        $resultArr['is_approver'] = $userTypeData->approver;
        $resultArr['approver_hierachy'] = $userTypeData->approver_value;


        foreach($userRolePermissions as $data){
            $dataArr['id'] = $data->id;
            $dataArr['slug'] = $data->slug;
            $dataArr['name'] = $data->name;
            $resultArrR[] = $dataArr;
            unset($dataArr);
        }
        $resultArr['permissons'] =$resultArrR;
        return response()->json(array('status'=>true,'message'=>'role permissons','result'=>$resultArr));
    }
 


     /************** Forgot Password ***********************/
    public function forgotpassword(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);

        $email = $data['email'];

        $userData = User::where('email',$email)->first();
        if(!empty($userData)) {

            $password = rand(18973824, 989721389);
            DB::table('users')
                            ->where('id', $userData->id)
                            ->update(array('password' => bcrypt($password)));

            // send and email 
            $emailTemplate = EmailTemplates::find(1);
            $messageBody = $emailTemplate->message;
            $messageBody = str_replace('[name]', $userData->username, $messageBody);
            $messageBody = str_replace('[password]', $password, $messageBody);

           // return view('emails.message')->with('data',$messageBody);   // check email before sending
            $data = ['message' => $messageBody];
            Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$userData)
            {
                $m->to($userData->email,$userData->username)
                 ->subject($emailTemplate->subject)
                 ->from($emailTemplate->send_from);
               });
            return response()->json(array('status'=>true,'message'=>'An email sent you with new password on your registered email. Please check.'));
        } else {
             return response()->json(array('status'=>false,'message'=>"We cannot find an account for contains this email. Please enter your correct email."));  
        }

    }

    /************** Change Password 1***********************/
    public function changePassword(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $old_password = $data['opassword'];
        $new_password = $data['password'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        // check old password
        if (Hash::check($old_password, $user->password)) { 
            if (Hash::check($new_password, $user->password)) { 
                return Response::json(array('status'=>false,'message'=>"Old password and new password cannot same."));
            } else {
                $password = Hash::make($new_password);
                // update password
                User::where('id', $user_id)->update(array('password' => $password));
                return Response::json(array('status'=>true,'message'=>"Password updated successfully."));
            }
        } else {
            return Response::json(array('status'=>false,'message'=>"Old password not correct."));
        }

    }

      /************** Edit Profile ***********************/
    public function editProfile(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $name = $data['name'];
        $username = $data['username'];
        $email = $data['email'];
        $phone = $data['phone'];
        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        
        // check username unique
        $userNameUnique = DB::table('users')->where('username',$username)->where('id','!=',$user_id)->first();
        if(!empty($userNameUnique)){
             return Response::json(array('status'=>false,'message'=>"Username not unique."));
        }
        // check email unique
        $userEmailUnique = DB::table('users')->where('email',$email)->where('id','!=',$user_id)->first();
        if(!empty($userEmailUnique)){
             return Response::json(array('status'=>true,'message'=>"Email not unique."));
        }

        // update edit basic info
        $userObj = User::find($user_id);
        $userObj->name= trim($name);
        $userObj->email= trim($email);
        $userObj->username= trim($username);
        $userObj->phone= trim($phone);
        $userObj->save();
        return Response::json(array('status'=>true,'message'=>"Profile details updated successfully."));
    }


   
    /************** Get All Sites  ***********************/
    public function allSites(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $user_type_id = $user->user_type_id;
        $allSites = DB::table('sites')->where('status',1)->orderBy('name','asc')->select('id','name','')->get();
        $resultArr = array();
        foreach($allSites as $data){
            $dataArr['id'] = $data->id;
            $dataArr['name'] = $data->name;
            $resultArr[] = $dataArr;
            unset($dataArr);
        }
        return response()->json(array('status'=>true,'message'=>"all site data.",'result'=>$resultArr));
    }

     /************** Get All site Issue Type List  ***********************/
    public function siteIssueTypeList(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $allIsuesType = DB::table('site_estate_issue_types')->where('status',1)->orderBy('name','asc')->select('id','name')->get();
        $resultArr = array();
        foreach($allIsuesType as $data){
            $dataArr['id'] = $data->id;
            $dataArr['name'] = $data->name;
            $resultArr[] = $dataArr;
            unset($dataArr);
        }
        return response()->json(array('status'=>true,'message'=>"all site issue type data.",'result'=>$resultArr));
    }

     /************** Get All surveillance Issue Type List  ***********************/
    public function surveillanceIssueTypeList(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $allIsuesType = DB::table('surveillance_issue_types')->where('status',1)->orderBy('name','asc')->select('id','name')->get();
        $resultArr = array();
        foreach($allIsuesType as $data){
            $dataArr['id'] = $data->id;
            $dataArr['name'] = $data->name;
            $resultArr[] = $dataArr;
            unset($dataArr);
        }
        return response()->json(array('status'=>true,'message'=>"all surveillance issue type data.",'result'=>$resultArr));
    }


     /************** Save Recent Search  ***********************/
    public function saveSearch(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $site_id = $data['site_id'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $saveData = array(
            'user_id' => $user_id,
            'site_id' => $site_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        // Save Data
        DB::table('recent_searches')->insert(
            $saveData
        );
        return response()->json(array('status'=>true,'message'=>'search saved successfully'));
    }

     /************** Get Recent Search  ***********************/
    public function recentSearch(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;

        $allsites = DB::table('recent_searches')
                ->where('user_id',$user_id)
                ->leftJoin('sites', 'sites.id', '=', 'recent_searches.site_id')
                ->select('sites.name','sites.id')
                ->limit(5)
                ->get();

        $allSitesResult = array();        
        // create sites array
        if(!empty($allsites)){
           foreach($allsites as $data){
            $dataArr['id'] = $data->id;
            $dataArr['name'] = $data->name;
            $allSitesResult[] = $dataArr;
            unset($dataArr);
            }
        }  
        return response()->json(array('status'=>true,'message'=>'recent search site data','result'=>$allSitesResult));
    }



    /************** Save Site Estate Issue  ***********************/
    public function saveSiteIssue(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);

        $token = $data['token'];
        $site_id = $data['site_id'];
        $site_estate_issue_type_id = $data['site_estate_issue_type_id'];
        $other = $data['other'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $saveData = array(
            'user_id' => $user_id,
            'site_id' => $site_id,
            'site_estate_issue_type_id' => $site_estate_issue_type_id,
            'other' => $other,
            'status' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        // Save Data
        DB::table('site_estate_issues')->insert(
            $saveData
        );
        return response()->json(array('status'=>true,'message'=>'Site Issue saved successfully.'));
    }


    /************** Get site Issue Tracker  ***********************/
    public function siteIssueTracker(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $site_id = $data['site_id'];
        $status_type = $data['status_type'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;

        $query = DB::table('site_estate_issues')->select('site_estate_issue_types.name',
                    'site_estate_issues.id',
                    'site_estate_issues.status',
                    'site_estate_issues.other',
                    'site_estate_issues.created_at');
        if($status_type==0){
            //get open or recheck status
            $query = $query->whereIn('site_estate_issues.status',array(0,1));
        }  else{
            //get close status
            $query = $query->where('site_estate_issues.status',2);
        }
        $allIssues =$query->where('user_id',$user_id)
                ->where('site_id',$site_id)
                ->leftJoin('site_estate_issue_types', 'site_estate_issue_types.id', '=', 'site_estate_issues.site_estate_issue_type_id')
                ->orderBy('site_estate_issues.id','desc')
                ->get();
        

        $allIssuesResult = array();        
        // create issues array
        if(!empty($allIssues)){
            foreach($allIssues as $data){
                if($data->status==0){
                    $status = 'Open';
                } elseif($data->status==1){
                    $status = 'Recheck';
                } else{
                    $status = 'Closed';
                }
                $dataArr['id'] = $data->id;
                $dataArr['name'] = $data->name;
                $dataArr['other'] = $data->other;
                $dataArr['status'] = $status;
                $dataArr['age'] = $this->getDaysFromDate($data->created_at);
                $allIssuesResult[] = $dataArr;
                unset($dataArr);
            }
        }  
        
        return response()->json(array('status'=>true,'message'=>'site issuess data','result'=>$allIssuesResult));
    }


    /*************** get days form current date ***************/
    public function getDaysFromDate($date){
        $days = (strtotime(date('Y-m-d H:i:s')) - strtotime($date)) / (60 * 60 * 24);
        $daysRound = round($days);
        if($daysRound==0 || $daysRound==1){
            $txt = 'day';
        } else {
            $txt = 'days';
        }
        return $daysRound." ".$txt;
    }

     /************** Save Site Issue Status Change  ***********************/
    public function siteIssueStatusChange(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $issue_id = $data['issue_id'];
        $status = $data['status'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        // Update Data
        DB::table('site_estate_issues')->where('id', $issue_id)
                            ->update(array('status' => $status));
        return response()->json(array('status'=>true,'message'=>'Issue status changed successfully'));
    }

     /************** Save Surveillance Issue  ***********************/
    public function saveSurveillanceIssue(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);

        $token = $data['token'];
        $site_id = $data['site_id'];
        $surveillance_issue_type_id = $data['surveillance_issue_type_id'];
        $description = $data['description'];
        $other = $data['other'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $saveData = array(
            'user_id' => $user_id,
            'site_id' => $site_id,
            'surveillance_issue_type_id' => $surveillance_issue_type_id,
            'other' => $other,
            'description'=>$description,
            'status' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        // Save Data
        DB::table('surveillance_issues')->insert(
            $saveData
        );
        return response()->json(array('status'=>true,'message'=>'Surveillance Issue saved successfully.'));
    }


    /************** Get Surveillance Issue Tracker  ***********************/
    public function surveillanceIssueTracker(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $site_id = $data['site_id'];
        $status_type = $data['status_type'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;

        $query = DB::table('surveillance_issues')->select('surveillance_issue_types.name',
                    'surveillance_issues.id',
                    'surveillance_issues.status',
                    'surveillance_issues.other',
                    'surveillance_issues.description',
                    'surveillance_issues.created_at');
        if($status_type==0){
            //get open or recheck status
            $query = $query->whereIn('surveillance_issues.status',array(0,1));
        }  else{
            //get close status
            $query = $query->where('surveillance_issues.status',2);
        }
        $allIssues =$query->where('site_id',$site_id)
                ->leftJoin('surveillance_issue_types', 'surveillance_issue_types.id', '=', 'surveillance_issues.surveillance_issue_type_id')
                ->orderBy('surveillance_issues.id','desc')
                ->get();
        

        $allIssuesResult = array();        
        // create issues array
        if(!empty($allIssues)){
            foreach($allIssues as $data){
                if($data->status==0){
                    $status = 'Open';
                } elseif($data->status==1){
                    $status = 'Recheck';
                } else{
                    $status = 'Closed';
                }
                $dataArr['id'] = $data->id;
                $dataArr['name'] = $data->name;
                $dataArr['other'] = $data->other;
                $dataArr['description'] = $data->description;
                $dataArr['status'] = $status;
                $dataArr['age'] = $this->getDaysFromDate($data->created_at);
                $allIssuesResult[] = $dataArr;
                unset($dataArr);
            }
        }  
        
        return response()->json(array('status'=>true,'message'=>'surveillance issues data','result'=>$allIssuesResult));
    }



     /************** Save Surveillance Status Change  ***********************/
    public function surveillanceIssueStatusChange(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $issue_id = $data['issue_id'];
        $status = $data['status'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        // Update Data
        DB::table('surveillance_issues')->where('id', $issue_id)
                            ->update(array('status' => $status));
        return response()->json(array('status'=>true,'message'=>'Issue status changed successfully'));
    }

    /************** Add User  ***********************/
    public function addUser(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $username = $data['username'];
        $name = $data['name'];
        $email = $data['email'];
        $password = $data['password'];
        $phone = $data['phone'];
        $site_id = $data['site_id'];
        $status = $data['status'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $vendor_name = $user->username;

         // check username unique
        $usernameCheck = DB::table('users')->select('username')->where('username',$username)->first();
        if(!empty($usernameCheck)){
            return response()->json(array('status'=>false,'message'=>'Username not unique.'));      
        }
         // check email unique
        $emailCheck = DB::table('users')->select('email')->where('email',$email)->first();
        if(!empty($emailCheck)){
            return response()->json(array('status'=>false,'message'=>'Email not unique.'));      
        }

        $slug = $this->createUniqueSlug($username, 'users');
        $saveUser = array(
            'name' => $name,
            'phone' => $phone,
            'username' => $username,
            'email' => $email,
            'password' => bcrypt($password),
            'status' => $status,
            'slug' => $slug,
            'user_type_id' => 4,
            'vendor_id' => $user_id,
            'site_id' => $site_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        // Save Data
        DB::table('users')->insert(
            $saveUser
        );
        $id = DB::getPdo()->lastInsertId();

        $userEmail = $email;

        // send email to user
        $emailTemplate = EmailTemplates::find(5);
        $messageBody = $emailTemplate->message;
        $messageBody = str_replace('[name]', $username, $messageBody);
        $messageBody = str_replace('[email]', $email, $messageBody);
        $messageBody = str_replace('[password]', $password, $messageBody);
        $messageBody = str_replace('[vendor_name]', $vendor_name, $messageBody);

        //return view('emails.message')->with('data',$messageBody);   // check email before sending
        $data = ['message' => $messageBody];
        Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$userEmail)
        {
            $m->to($userEmail)
            ->subject($emailTemplate->subject)
            ->from($emailTemplate->send_from);
        });

        // send email to admin
        $adminData = DB::table('admins')->where('id',1)->first();
        $adminEmail =$adminData->email;
        $emailTemplate = EmailTemplates::find(6);
        $messageBody = $emailTemplate->message;
        $messageBody = str_replace('[name]', $username, $messageBody);
        $messageBody = str_replace('[email]', $email, $messageBody);
        $messageBody = str_replace('[password]', $password, $messageBody);
        $messageBody = str_replace('[vendor_name]', $vendor_name, $messageBody);

        //return view('emails.message')->with('data',$messageBody);   // check email before sending
        $data = ['message' => $messageBody];
        Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$adminEmail)
        {
            $m->to($adminEmail)
            ->subject($emailTemplate->subject)
            ->from($emailTemplate->send_from);
        });
        return response()->json(array('status'=>true,'message'=>'User saved successfully'));
    }


    /************** Edit User  ***********************/
    public function editUser(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $username = $data['username'];
        $name = $data['name'];
        $email = $data['email'];
        $password = $data['password'];
        $phone = $data['phone'];
        $site_id = $data['site_id'];
        $new_user_id = $data['new_user_id'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $vendor_name = $user->username;


        // check username unique
        $usernameCheck = DB::table('users')->select('username')->where('username',$username)->where('id','!=',$new_user_id)->first();
        if(!empty($usernameCheck)){
            return response()->json(array('status'=>false,'message'=>'Username not unique.'));      
        }
        // check email unique
        $emailCheck = DB::table('users')->select('email')->where('email',$email)->where('id','!=',$new_user_id)->first();
        if(!empty($emailCheck)){
            return response()->json(array('status'=>false,'message'=>'Email not unique.'));      
        }
		//$date= date('Y-m-d H:i:s');
        $updateUser = array(
            'name' => "'$name'",
            'phone' => "'$phone'",
            'username' => "'$username'",
            'email' => "'$email'",
            'site_id' => $site_id,
            //'updated_at' => "'$date'",
        );
        if($password!='') {
			$pass = bcrypt($password);
            $updateUser['password'] ="'$pass'";
        }
		//print_r($updateUser); die;
       // Update Data
        DB::table('users')->where('id', $new_user_id)->update($updateUser);

        return response()->json(array('status'=>true,'message'=>'User updated successfully'));
    }


     /************** Search Sites  ***********************/
    public function searchSite(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $keyword = $data['keyword'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $user_type_id = $user->user_type_id;
        $user_sites = explode(',',$user->site_id);
        $userTypeData = DB::table('user_types')->where('id',$user_type_id)->first();

        $allSites = DB::table('sites')
                    ->where('name', 'LIKE', '%' . $keyword . '%')
                    ->whereIn('id',$user_sites)
                    ->select('id','site_id','name','latitude','longitude')
                    ->where('status',1)
                    ->orderBy('name','asc')
                    ->get();
        $resultArr = array();
        if(!$allSites->isEmpty()) {            
            foreach($allSites as $data) {
                $dataArr['id'] = $data->id;
                $dataArr['site_id'] = $data->site_id;
                $dataArr['name'] = $data->name;
                $dataArr['latitude'] = $data->latitude;
                $dataArr['longitude'] = $data->longitude;
                $resultArr[] = $dataArr;
                unset($dataArr);
            }
            $message = 'all sites data';
        } else {
            $message = 'No site found';
        }
        return response()->json(array('status'=>true,'message'=>$message,'is_location'=>$userTypeData->search_type,'result'=>$resultArr));
    }


     /************** site related users  ***********************/
    public function siteUsers(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $site_id = $data['site_id'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;

        $allUsers = DB::table('users')
                    ->where('id','!=', $user_id)
                     ->whereRaw('FIND_IN_SET(?,site_id)', [$site_id])
                    ->select('id','username','name','site_id')
                    ->where('status',1)
                    ->orderBy('name','asc')
                    ->get();
        $resultArr = array();
        if(!$allUsers->isEmpty()) {            
            foreach($allUsers as $data){
                $dataArr['id'] = $data->id;
                $dataArr['username'] = $data->username;
                $dataArr['name'] = $data->name;
                $resultArr[] = $dataArr;
                unset($dataArr);
            }
            $message = 'all user data';
        } else {
            $message = 'No user found';
        }
        return response()->json(array('status'=>true,'message'=>$message,'result'=>$resultArr));
    }


     /************** Save Message  ***********************/
    public function saveMessage(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $site_id = $data['site_id'];
        $surveillance_issue_id = $data['surveillance_issue_id'];
        $message = $data['message'];

        $user = JWTAuth::toUser($token);  // authorization JWT Token
        $user_id = $user->id;
        $saveData = array(
            'user_id' => $user_id,
            'site_id' => $site_id,
            'surveillance_issue_id' => $surveillance_issue_id,
            'message' => $message,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        // Save Data
        DB::table('messages')->insert(
            $saveData
        );
        return response()->json(array('status'=>true,'message'=>'message saved successfully'));
    }

    /***************Get Survelleance Message**********************/
    public function messages(Request $request){
        $jData = file_get_contents('php://input');
        $data = $this->getJson($jData);
        $token = $data['token'];
        $surveillance_issue_id = $data['surveillance_issue_id'];
        
        $pageCurrent = 0;
        if(isset($data['pageCurrent'])&& !empty($data['pageCurrent'])){
        $pageCurrent = $data['pageCurrent']; 
        }

       
        $user = JWTAuth::toUser($token); // authorization JWT Token
		$User_Id =$user->id;
		
        //---------------Count------------------//

        $query = DB::table('messages')
                            ->select('messages.id','messages.user_id','users.username','messages.created_at','messages.message')
                            ->leftJoin('users', 'users.id', '=', 'messages.user_id')
                            ->where('surveillance_issue_id',$surveillance_issue_id);
        $query2 = DB::table('messages')
                            ->select('id')
                            ->where('surveillance_issue_id',$surveillance_issue_id);
        $messageCount = $query2->count();

        $limit =3;
        $pageNo = ceil($messageCount/$limit); 
        $start = $limit*$pageCurrent; 
        $messages = $query->skip($start)->take($limit)
                    ->orderBy('id','desc')
                    ->get();     
       // $messages = $messages->reverse();
        $resultArr = array();        
        // create messages array
        if(!empty($messages)){
			
            foreach($messages as $data){
				$my_message = false;
                $dataArr['id'] = $data->id;
                $dataArr['username'] = $data->username;
				if($User_Id ==$data->user_id){
					$my_message = true;
				}
                $dataArr['my_message'] = $my_message;
                $dataArr['user_id'] = $data->user_id;
                $dataArr['message'] = $data->message;
                $dataArr['created_at'] = date('d M | h:i A', strtotime($data->created_at));
                $resultArr[] = $this->filterNullValueFromArray($dataArr);
                unset($dataArr);
            }
        } 
        return response()->json(array(
                                'status'=>true,
                                'message'=>'all messages',
                                'result'=>$resultArr,
                                'pageNo'=>$pageNo,
								'pageCurrent'=>$pageCurrent
                            ));
    }

    /************** Save Logbook  ***********************/
    public function saveLogbook(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $site_id = $data['site_id'];
        $diesel_filled = $data['diesel_filled'];
        $balance_diesel = $data['balance_diesel'];
        $eb_meter_reading = $data['eb_meter_reading'];
        $eb_runs_hrs_as_per_piu = $data['eb_runs_hrs_as_per_piu'];
        $battery_run_hrs_as_per_piu = $data['battery_run_hrs_as_per_piu'];
        $dg_run_hrs_in_manual = $data['dg_run_hrs_in_manual'];
        $dg_run_hrs_in_auto = $data['dg_run_hrs_in_auto'];
        $solor_run_hrs_piu = $data['solor_run_hrs_piu'];
        $total_dg_run_hrs = $data['total_dg_run_hrs'];
        $total_eb_solar_dg = $data['total_eb_solar_dg'];
        $total_dg_hrs_as_per_piu = $data['total_dg_hrs_as_per_piu'];
        $dg_hour_meter_reading = $data['dg_hour_meter_reading'];
        $meter_mdi = $data['meter_mdi'];
        $bb_charging_current_set_smps = $data['bb_charging_current_set_smps'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $saveData = array(
            'user_id' => $user_id,
            'site_id' => $site_id,
            'date' => date('Y-m-d'),
            'balance_diesel' => $balance_diesel,
            'eb_meter_reading' => $eb_meter_reading,
            'eb_runs_hrs_as_per_piu' => $eb_runs_hrs_as_per_piu,
            'battery_run_hrs_as_per_piu' => $battery_run_hrs_as_per_piu,
            'dg_run_hrs_in_manual' => $dg_run_hrs_in_manual,
            'dg_run_hrs_in_auto' => $dg_run_hrs_in_auto,
            'solor_run_hrs_piu' => $solor_run_hrs_piu,
            'total_dg_run_hrs' => $total_dg_run_hrs,
            'total_eb_solar_dg' => $total_eb_solar_dg,
            'total_dg_hrs_as_per_piu' => $total_dg_hrs_as_per_piu,
            'dg_hour_meter_reading' => $dg_hour_meter_reading,
            'meter_mdi' => $meter_mdi,
            'bb_charging_current_set_smps' => $bb_charging_current_set_smps,
            'diesel_filled' => $diesel_filled,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        // Save Data
        DB::table('logbook')->insert(
            $saveData
        );
        $logbook_id = DB::getPdo()->lastInsertId();
         // gat max diesel filled limit
        $siteDataDiesel = DB::table('sites')->where('id',$site_id)->select('id','max_diesel_filled_limit')->first();
        if(!empty($siteDataDiesel)) {
            $max_diesel_filled_limit = $siteDataDiesel->max_diesel_filled_limit;
            $totalDieselFilled = $diesel_filled+$balance_diesel;
            //check if diesel filled more from limit
            if($totalDieselFilled>$max_diesel_filled_limit){
                $diffLimit = $totalDieselFilled-$max_diesel_filled_limit;
                 $updateBpv = array(
                    'is_bpv_above' => 1,
                    'bpv'=>$max_diesel_filled_limit,
                    'extra_diesel_filled'=>$diffLimit,
                );
                // update logbook Data
                DB::table('logbook')->where('id',$logbook_id)->update(
                    $updateBpv
                );
                $approval_diesel_id = DB::getPdo()->lastInsertId();
                // count total approval
                $approvalData = DB::table('user_types')->where('approver','!=',0)->orderBy('approver_value','asc')->get();
                if(!empty($approvalData)){
                    foreach($approvalData as $data){
                        $saveData = array(
                            'user_id' => $user_id,
                            'site_id' => $site_id,
                            'logbook_id' => $logbook_id,
                            'approval_order' => $data->approver_value,
                            'user_type_id' => $data->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        );
                        // Save Data
                        DB::table('bpv_approvar')->insert(
                            $saveData
                        );
                    }
                }

            }
        }
        

        return response()->json(array('status'=>true,'message'=>'Logbook saved successfully'));
    }


    /***************Manage Users**********************/
    public function manageUsers(Request $request){
        $jData = file_get_contents('php://input');
        $data = $this->getJson($jData);
        $token = $data['token'];
        
        $pageCurrent = 0;
        if(isset($data['pageCurrent'])&& !empty($data['pageCurrent'])){
            $pageCurrent = $data['pageCurrent']; 
        }


        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        //---------------Count------------------//
        $query = DB::table('users')
                            ->select('users.id','users.slug','users.username','users.name','users.email','users.phone','users.created_at','users.site_id','users.status','users.is_approved')
                            ->where('vendor_id',$user_id);
        $query2 = DB::table('users')
                            ->select('id')
                             ->where('vendor_id',$user_id);
        $usersCount = $query2->count();

        $limit=3;

        $pageNo = ceil($usersCount/$limit); 
        $start = $limit*$pageCurrent; 
        $users = $query->skip($start)->take($limit)
                    ->orderBy('id','desc')
                    ->get();     
        $resultArr = array();        
        // create users array
        if(!empty($users)){
            foreach($users as $data){
                $dataArr['id'] = $data->id;
                $dataArr['slug'] = $data->slug;
                $dataArr['name'] = $data->name;
                $dataArr['username'] = $data->username;
                $dataArr['email'] = $data->email;
                $dataArr['phone'] = $data->phone;
                $dataArr['status'] = $data->status;
                $dataArr['is_approved'] = $data->is_approved;
                $dataArr['created_at'] = $data->created_at;
                // get site name from site id
				#print_r($data->site_id).'</br>'; 
                $sitesData = DB::table('sites')->whereIn('id',explode(',',$data->site_id))->pluck('name','id')->toArray();
				/* echo '23423';
				print_r($sitesData); die; */
					$siteIdValues =array();
					$siteNameValues =array();
                if(!empty($sitesData)){
					foreach($sitesData as $key => $site){
				    	$siteIdValues[] = $key;
                        $siteNameValues[] = $site;
					}
                } 
                $dataArr['site_id'] = implode(",",$siteIdValues);
                $dataArr['site_name'] = implode(",",$siteNameValues);

                $resultArr[] = $this->filterNullValueFromArray($dataArr);
                unset($dataArr);
            }
        } 
        return response()->json(array(
                                'status'=>true,
                                'message'=>'all users',
                                'result'=>$resultArr,
                                'pageNo'=>$pageNo,
                                'pageCurrent'=>$pageCurrent
                            ));
    }


     /***************approval diesel list**********************/
    public function bpvApprovar(Request $request){
        $jData = file_get_contents('php://input');
        $data = $this->getJson($jData);
        $token = $data['token'];
        
        $pageCurrent = 0;
        if(isset($data['pageCurrent'])&& !empty($data['pageCurrent'])){
            $pageCurrent = $data['pageCurrent']; 
        }


        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $userTypeData = DB::table('user_types')->where('id',$user->user_type_id)->first();
        $ifLoggedUserapprovar = $userTypeData->approver_value;
        

        //---------------Count------------------//
        $query = DB::table('logbook')
                            ->select('logbook.id','logbook.bpv','logbook.diesel_filled','logbook.extra_diesel_filled','logbook.site_id','logbook.created_at')
                            ->where('is_bpv_above',1);
        $usersCount = DB::table('logbook')
                            ->select('id')
                            ->where('is_bpv_above',1)
                            ->count();

        $limit =3;
        $pageNo = ceil($usersCount/$limit); 
        $start = $limit*$pageCurrent; 
        $resultData = $query->skip($start)->take($limit)
                    ->orderBy('id','desc')
                    ->get();     
                  
        $resultArr = array();        
        // create users array
        if(!empty($resultData)){
            foreach($resultData as $data){
                //check if previous user approved 
                $bpvApproverDatas = DB::table('bpv_approvar')->where('logbook_id',$data->id)->where('approval_order',"<",$ifLoggedUserapprovar)->where('approval_id',0)->orderBy('approval_order','desc')->first();
               
                if(empty($bpvApproverDatas)){
                    $approver = '1';
                } else{
                    $approver = '0';
                }
                $dataArr['id'] = $data->id;
                $dataArr['site_id'] = $data->site_id;
                $dataArr['diesel_filled'] = $data->diesel_filled;
                $dataArr['bpv'] = $data->bpv;
                $dataArr['extra_diesel_filled'] = $data->extra_diesel_filled;
                $dataArr['created_at'] = $data->created_at;
                $dataArr['approver'] = $approver;
                // create approvar array
                $dbApprovar = DB::table('bpv_approvar')
                                ->select('bpv_approvar.id','bpv_approvar.approval_order','bpv_approvar.is_approved','user_types.name')
                                ->where('logbook_id',$data->id)
                                ->leftJoin('user_types', 'user_types.id', '=', 'bpv_approvar.user_type_id')
                                ->orderBy('bpv_approvar.approval_order','asc')
                                ->get();
                if(!empty($dbApprovar)) {
                    $bprResultArr = array();
                    foreach($dbApprovar as $dVal) {
                        $bpv['id'] = $dVal->id;
                        $bpv['approval_order'] = $dVal->approval_order;
                        $bpv['is_approved'] = $dVal->is_approved;
                        $bpv['name'] = $dVal->name;
                        $bprResultArr[] = $bpv;
                    }
                    $dataArr['bpv_approvar'] = $bprResultArr;
                }
                $resultArr[] = $this->filterNullValueFromArray($dataArr);
                unset($dataArr); // unset data array
            }
        } 
        return response()->json(array(
                                'status'=>true,
                                'message'=>'all approvar',
                                'result'=>$resultArr,
                                'pageNo'=>$pageNo,
                                'pageCurrent'=>$pageCurrent
                            ));
    }

     /************** Change Beat Value Approve Status  ***********************/
    public function changeBeatValueApproveStatus(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $bpv_approvar_id = $data['bpv_approvar_id'];
        $status = $data['status'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $updateData = array(
            'is_approved' => $status,
            'approval_id' => $user_id,
            'updated_at'=>date('Y-m-d H:i:s')
        );
        // update Data
        DB::table('bpv_approvar')
                ->where('id',$bpv_approvar_id)
                ->update($updateData);
        return response()->json(array('status'=>true,'message'=>'Status Change Successfully'));
    }


    /************** Save Prementive  ***********************/
    public function savePreventive(Request $request){
        $token = $_REQUEST['token'];
        $site_id = $_REQUEST['site_id'];
        $checklist_data = $_REQUEST['checklist_data'];
        $observation = $_REQUEST['observation'];
        $recommendation = $_REQUEST['recommendation'];
        $filesNameArr = array();

        // get files
        if(isset($_FILES) && !empty($_FILES)) {
                foreach($_FILES as $imgData) {
                    if($imgData['error']==0) {
                        $filename = $imgData['name'];
                        $tmp_name = $imgData['tmp_name'];
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        $newFileName = strtotime(date('Y-m-d h:i:s')).'.'.$ext;
                        @move_uploaded_file($tmp_name, UPLOAD_FULL_PREVENTIVE_IMAGE_PATH.$newFileName);
                        $filesNameArr[] = $newFileName;
                    }
                }
        }


        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $saveData = array(
            'user_id' => $user_id,
            'site_id' => $site_id,
            'checklist_data' => $checklist_data,
            'observation' => $observation,
            'recommendation' => $recommendation,
            'images' => implode(',',$filesNameArr),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        // Save Data
        DB::table('preventive_maintenance')->insert(
            $saveData
        );

        return response()->json(array('status'=>true,'message'=>'Prementive saved successfully'));
    }

     /************** Get Old Diesel Balance ***********************/
    public function getOldDieselBalance(Request $request){
        $jData = file_get_contents('php://input');
        // get decode json data
        $data = $this->getJson($jData);
        $token = $data['token'];
        $site_id = $data['site_id'];

        $user = JWTAuth::toUser($token); // authorization JWT Token
        $user_id = $user->id;
        $logbookData = DB::table('logbook')->select('site_id','balance_diesel')
                        ->where('site_id',$site_id)
                        ->orderBy('id','desc')
                        ->first();
        if(!empty($logbookData)) {
            $balanceDeisel = $logbookData->balance_diesel;
        } else {
            $balanceDeisel = 0;
        }                
        return response()->json(array('status'=>true,'message'=>'balance deisel','balance_deisel'=>$balanceDeisel));
    }

    
}
