<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ForgotPassword;
use App\Models\EmailTemplates;
use App\Models\LoanEnquiryIndividual;
use App\Models\LoanEnquiryCompany;
use App\Models\IdType;
use App\Models\EducationLevel;
use App\Models\BusinessType;
use App\Models\KnowAccredit;
use App\Models\LoanType;
use App\Models\LoanPurpose;
use App\Models\ResidentialType;
use App\Models\PropertyOwnership;
use App\Models\EmploymentStatus;
use App\Models\JobTitle;
use App\Models\Specialization;
use App\Models\OfficeBranch;
use App\Models\AdminDocuments;
use App\Models\AboutContent;
use App\Models\ReasonForLoan;
use App\Models\Payments;
use App\Models\Notifications;
use App\Models\FaqCategories;
use Illuminate\Http\Request;
use Response;
use Illuminate\Notifications\Notifiable;
use Redirect;
use Validator;
use Hash;
use Auth;
use Mail;
use Input;
use DB;
use Cookie;
use JWTAuth;
use JWTAuthException;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

require_once SITE_URLI. '/vendor/mpdf/vendor/autoload.php';
//use \Mpdf\Mpdf;

class WebserviceController extends Controller
{
	public function __construct(){
	}

	/***************** get request from json a *****************/
	public function getJson($jData){
		$data = json_decode($jData,true);		
		if(isset($data) && !empty($data)){
			$_REQUEST = $data;
		} else {
			$data = $_POST;
		}
		return $data;
	}

	/***************** null value filter from array *****************/
	public function filterNullValueFromArray($arr){
		$resultArr = array();
		foreach($arr as $key=>$val){
			if($val===NULL){
				$resultArr[$key] = '';
			} else {
				$resultArr[$key] = $val;
			}
		}
		return $resultArr;
	}
	
	/************** Login user ***********************/
	
	public function login()
	{
	  $result = array();
	  $response = array();
	  $getReqData = file_get_contents('php://input');
	  $getData = $this->getJson($getReqData);	
	
	  if(!empty($getData))
	  {
	  	try
	  	{
         if(filter_var($getData['email'],FILTER_VALIDATE_EMAIL))
         {
           $credentials = array('email'=>$getData['email'],'password'=>$getData['password']);
         }
         else
         {
            $credentials = array('phone'=>$getData['email'],'password'=>$getData['password']);
         }
	  	   
	  	   if(!$token = JWTAuth::attempt($credentials))
	  	   {
              $response['status'] = 0;
      			  $response['msg']  = 'Invalid email or password !';
      			  $response['data'] = [];
      			  return Response::json($response);
	  	   }
	  	   else
	  	   {
	  	   	  $user = JWTAuth::toUser($token); // authorization JWT Token
    			  $user_id = $user->id;
    			  $username = $user->username;
    			  $name = $user->name;
    			  $phone = $user->phone;
    			  $email = $user->email;
            $isLoginFirst = $user->is_loginfirst;
            if(!empty($user->image))
            {
              $profileImage = SITE_URL.'/public/files/profileImage/'.$user->image;
            }
            else
            {
              $profileImage = '';
            }
            
    			  $result11 = $this->sendOtp('+65'.$phone,$user_id);

    			  if($result11)
    			  {
              
    			  	$result['user_id'] = $user_id;
    				  $result['email'] = $email;
    				  $result['name'] = $name;
    				  $result['token'] = $token;
               $result['phone'] = $phone;
               $result['is_loginfirst'] = $isLoginFirst;
                $result['registration_type'] = $user->role_id;
               $result['otp'] = $result11;
               $result['profile_image'] = $profileImage;
    				  $response['status'] = 1;
    				  $response['msg'] = 'Token created succussfully';
    				  $response['data'] = $result;
    				  return Response::json($response);
    			  }	
    			  else
    			  {
    			  	  $result['user_id'] = $user_id;
    				  $result['email'] = $email;
    				  $result['name'] = $name;
    				  $result['token'] = $token;
    				   $result['phone'] = $phone;
               $result['is_loginfirst'] = $isLoginFirst;
               $result['otp'] = '';
               $result['profile_image'] = $profileImage;
    				  $response['status'] = 1;
    				  $response['msg'] = 'Otp not sent !';
    				  $response['data'] = $result;
    				  return Response::json($response);
    			  }		  
	  	   }
	  	}
	  	catch(JWTAuthException $e)
	  	{
	  		$response['status'] = 0;
		  	$response['msg']  = 'Failed to create token !';
		  	$response['data'] = [];
		  	return Response::json($response);
	  	}
	  }
	  else
	  {	  	
	  	$response['status'] = 0;
	  	$response['msg']  = 'Please enter email or password !';
	  	$response['data'] = [];
	  	return Response::json($response);
	  }
	}

	/**************  Send Otp  ******************************/

	public function sendOtp($phone = null,$userId = null)
    {
      
      $digits = 4;
      $otp =  rand(pow(10, $digits-1), pow(10, $digits)-1);      
      //file_get_contents("http://trans.masssms.tk/api.php?username=srcjpr&password=test@123&sender=SRCJPR&sendto=".$phone."&message=".$otp);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://api.transmitsms.com/send-sms.json?format=json&to=".$phone."&message=".$otp."");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,false);  //Post Fields
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $headers = array();
      $headers[] = 'X-abc-AUTH: 123456789';
      $headers[] = 'Accept:  */*';
      $headers[] = 'Cache-Control: no-cache';
      $headers[] = 'Content-Type: application/json';
      $headers[] = 'Authorization: Basic ZTJmYjRlMGY0ZDQ1MTkwYjM2MTc1ODJiMjJlZGYzZWE6anNkaGtqc2hmanNoZmpzZmhqZHM=';

      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $data = curl_exec ($ch);
      //print_r($data); die;
      curl_close ($ch);
      DB::table('users')
              ->where('id', $userId)
              ->update(array('otp' =>$otp));
      if($data)
      {
        return $otp;
      }
      else
      {
        return $data;
      }
      die;
    }

    /************** Resend Otp ***********************/
       public function resendOtp()
       {
         $getReqData = file_get_contents('php://input');
         $getData = $this->getJson($getReqData);
         $token = $getData['token'];
         $user = JWTAuth::toUser($token);
         $user_id = $user->id;
         if(!empty($user_id > 0))
         {
            $data = DB::table('users')->where('id',$user_id)->first();
            if($data->id > 0)
            {
              $phone = $data->phone;
              $user_id = $data->id;
              $result11 = $this->sendOtp('+65'.$phone,$user_id);
              if($result11)
              {
                $response['status'] = 1;
                $response['msg']  = 'OTP send successfully !';
                $response['data'] = [];
                return Response::json($response);
              }
              else
              {
                $response['status'] = 0;
                $response['msg']  = 'OTP not send !';
                $response['data'] = [];
                return Response::json($response);
              }
            }
            else
            {
               $response['status'] = 0;
               $response['msg']  = 'User Data Not Found !';
               $response['data'] = [];
               return Response::json($response);
            }
            
         }
       }
       

    /************** Verify Otp ***********************/

    public function verifyOtp()
    {
       $getReqData = file_get_contents('php://input');
       $getData = $this->getJson($getReqData);
       $token = $getData['token'];
       $user = JWTAuth::toUser($token);       
       $user_id = $user->id;
       if(!empty($user_id > 0))
       {
          $otp = $getData['otp'];
          $firebaseToken = isset($getData['firebase_token'])?$getData['firebase_token']:'';
          $data = DB::table('users')->where('id',$user_id)->first();
          if($otp == $data->otp)
          {
             DB::table('users')->where('id',$user_id)->update(array('firebase_token'=>$firebaseToken));
             $response['status'] = 1;
             $response['msg']  = 'Otp Verified Successfully';
             $response['data'] = [];
             return Response::json($response);
          }
          else
          {
             $response['status'] = 0;
             $response['msg']  = 'Otp Not Verified !';
             $response['data'] = [];
             return Response::json($response);
          }

       }
       else
       {
          $response['status'] = 0;
          $response['msg']  = 'Request parament not found !';
          $response['data'] = [];
          return Response::json($response);
       }
    }


   /************** Get Form Filed ***********************/
   	public function getFormFields()
   	{   	
   	  $getReqData = file_get_contents('php://input');
   	  $getData = $this->getJson($getReqData);
      $isToken = isset($getData['is_token'])?$getData['is_token']:0;
      if($isToken == 1)
      {
        $token = $getData['token'];
        $user = JWTAuth::toUser($token);
        $user_id = $user->id;
      }
   	  $result = array();
   	  $response = array();
      $data = array();
      $modal = '';
      $temp = array();
      if(!empty($getData))
      {
      	  $registrationType = $getData['registration_type'];
      	  $dataForms = DB::table('field')->where(['form_type_id'=>$registrationType,'status'=>1])->orderBy('position', 'ASC')->get();
      	  if(!empty($dataForms))
      	  {
      	  	 $i = 0;
      	  	foreach ($dataForms as $key => $value)
      	  	{
              $data = [];
              $modal = '';
              if($value->slug == 'id_type')
              {
                $data = $this->getIdType();
              }
              if($value->slug == 'education_level')
              {
                $data = $this->getEducationLevel();
              }
              if($value->slug == 'business_type')
              {
               $data = $this->getBusinessType();
              }     
              if($value->slug == 'know_accredit')
              {
                $data = $this->getKnowAccredit();                
              } 
              if($value->slug == 'loan_type')
              {
               $data = $this->getLoanType();
              
              } 
              if($value->slug == 'loan_purpose')
              {
                $data = $this->getLoanPurpose();
                
              } 

              if($value->slug == 'office_branch')
              {
                $data = $this->getOfficeBranch();
              }
              /*if($value->slug == 'reason_for_loan')
              {
                $data = $this->getReasonForLoan();
                
              } */
              if($value->slug == 'residential_type')
              {
                $data = $this->getResidentialType();
                
              } 
              if($value->slug == 'property_ownership')
              {
                $data = $this->getPropertyOwnership();
                
              } 
              if($value->slug == 'employment_status')
              {
                $data = $this->getEmploymentStatus();
                
              } 
              if($value->slug == 'job_title')
              {
                $data = $this->getJobTitle();
              
              } 
              /*if($value->slug == 'specialization')
              {
                $data = $this->getSpecilization();
                
              }*/
              if($value->slug == 'nationality')
              {
                $nationality[] =array('id'=>1,'name'=>'Singaporean');
                $nationality[] =array('id'=>2,'name'=>'Malaysian');
                $nationality[] =array('id'=>3,'name'=>'Chinese');
                $nationality[] =array('id'=>4,'name'=>'Indian');
                
                $data = $nationality;
              }
              if($value->slug == 'gender')
              {
                $gender[] = array('id'=>1,"name"=>'Male');
                $gender[] = array('id'=>2,"name"=>'Female');
                $data = $gender;
                
              }
              if($value->slug == 'marital_status')
              {
                $maritalStatus[] = array('id'=>1,"name"=>'Not Married');
                $maritalStatus[] = array('id'=>2,"name"=>'Married');
                $data = $maritalStatus;
                
              }
              if($value->slug == 'salution')
              {
                $salution[] = array('id'=>1,"name"=>'Mr.');
                $salution[] = array('id'=>2,"name"=>'Mrs.');
                $salution[] = array('id'=>3,"name"=>'Ms.');
                $salution[] = array('id'=>4,"name"=>'Miss.');
                $data = $salution;
                
              }
              if($value->slug == 'salary_paid')
              {
                $salaryPaid[] = array('id'=>10000,"name"=>10000);
                $salaryPaid[] = array('id'=>20000,"name"=>20000);
                $salaryPaid[] = array('id'=>30000,"name"=>30000);
                $salaryPaid[] = array('id'=>40000,"name"=>40000);
                $data = $salaryPaid;
                
              }
              if($value->slug == 'next_of_kin_relationship')
              {
                $reletionShip[] = array('id'=>1,"name"=>'Immediate Supervisor');
                $reletionShip[] = array('id'=>2,"name"=>'Next of Kins');
                $reletionShip[] = array('id'=>3,"name"=>'Colleague or Friend');
                $reletionShip[] = array('id'=>4,"name"=>'Other');                
                $data = $reletionShip;
                
              }
              if($value->slug == 'payment_frequency')
              {
                $paymentFrequency[] = array('id'=>365,"name"=>'Daily');
                $paymentFrequency[] = array('id'=>52,"name"=>'Weekly');
                $paymentFrequency[] = array('id'=>12,"name"=>'Monthly');
                $paymentFrequency[] = array('id'=>1,"name"=>'Yearly');   
                $data = $paymentFrequency;
                
              }
              if($value->slug == 'length_of_current_employment_years')
              {
                $empYear[] = array('id'=>1,"name"=>'1 Year');
                $empYear[] = array('id'=>2,"name"=>'2 Years');
                $empYear[] = array('id'=>3,"name"=>'3 Years');
                $empYear[] = array('id'=>4,"name"=>'4 Years');
                $empYear[] = array('id'=>5,"name"=>'5 Years');
                $empYear[] = array('id'=>6,"name"=>'6 Years');
                $empYear[] = array('id'=>7,"name"=>'7 Years');
                $empYear[] = array('id'=>8,"name"=>'8 years');
                $empYear[] = array('id'=>9,"name"=>'9 Years');
                $empYear[] = array('id'=>10,"name"=>'10 Years');
                $data = $empYear;
                
              }
              if($value->slug == 'length_of_current_employment_months')
              {
                $empMonth[] = array('id'=>1,"name"=>'1 Month');
                $empMonth[] = array('id'=>2,"name"=>'2 Months');
                $empMonth[] = array('id'=>3,"name"=>'3 Months');
                $empMonth[] = array('id'=>4,"name"=>'4 Months');
                $empMonth[] = array('id'=>5,"name"=>'5 Months');
                $empMonth[] = array('id'=>6,"name"=>'6 Months');
                $empMonth[] = array('id'=>7,"name"=>'7 Months');
                $empMonth[] = array('id'=>8,"name"=>'8 Months');
                $empMonth[] = array('id'=>9,"name"=>'9 Months');
                $empMonth[] = array('id'=>10,"name"=>'10 Months');
                $empMonth[] = array('id'=>11,"name"=>'11 Months');
                $empMonth[] = array('id'=>12,"name"=>'12 Months');
                $data = $empMonth;
                
              }
              if($value->slug == 'payment_frequency')
              {
                $paymentFrequency[] = array('id'=>1,"name"=>'1 Month');
                $paymentFrequency[] = array('id'=>2,"name"=>'2 Months');
                $paymentFrequency[] = array('id'=>3,"name"=>'3 Months');
                $paymentFrequency[] = array('id'=>4,"name"=>'4 Months');
                $paymentFrequency[] = array('id'=>5,"name"=>'5 Months'); 
                $data = $paymentFrequency;                
              }
              if($value->slug == 'number_of_dependents')
              {
                $dependents[] = array('id'=>1,"name"=>'1');
                $dependents[] = array('id'=>2,"name"=>'2');
                $dependents[] = array('id'=>3,"name"=>'3');
                $dependents[] = array('id'=>4,"name"=>'4');
                $dependents[] = array('id'=>5,"name"=>'5'); 
                $data = $dependents;                
              }
              if($isToken == 1)
              {
                if($value->slug == 'name')
                {
                  $modal = $user->name;               
                }
                if($value->slug == 'email')
                {
                  $modal = $user->email;               
                }
                if($value->slug == 'office_number')
                {
                  $modal = $user->office_number;               
                }
                if($value->slug == 'loan_amount')
                {
                  $modal = $user->loan_amount; 
                }
              }
              else
              {
                $modal = '';
              }

              if($value->is_required == 1)
              {
                $isRequired = 'true';
              }
              else
              {
                $isRequired = 'false';
              }
              


      	  		$result[$i]['title'] = $value->name;
              $result[$i]['type'] = $value->type;
      	  		$result[$i]['slug'] = $value->slug;
      	  		$result[$i]['is_required'] = $isRequired;
              $result[$i]['value'] = $data;
      	  		$result[$i]['modal'] = $modal;              
      	  		$i++;
      	  	}
            /*if($registrationType == 3)
            {
              $employementLengthMonthArray = $result[40];  
              $officeNumberArray = $result[39];

              array_unshift($result[21], $employementLengthMonthArray); 
              $result[21] = $employementLengthMonthArray;

              array_unshift($result[27], $officeNumberArray); 
              $result[27] = $officeNumberArray;
            }*/
            

            
            //print_r($result); die;            
            

      	  	$response['status'] = 1;
		  	$response['msg']  = 'Form Fields List';
		  	$response['data'] = $result;
		  	return Response::json($response);
      	  }
      	  else
      	  {
      	  	$response['status'] = 0;
		  	$response['msg']  = 'No record found for this registration type !';
		  	$response['data'] = [];
		  	return Response::json($response);
      	  }
      }
      else
      {
      	$response['status'] = 0;
	  	$response['msg']  = 'Please supply registration type !';
	  	$response['data'] = [];
	  	return Response::json($response);
      }     

   	}

    public function moveElement($array, $a, $b)
    {
      $out = array_splice($array, $a, 1);
      array_splice($array, $b, 0, $out);
    }

   	/************** Registration ***********************/

   	public function registration()
   	{
   		//pr('hello'); die;
   		$getReqData = file_get_contents('php://input');
   		$getData = $this->getJson($getReqData);
      $isToken = isset($getData['is_token'])?$getData['is_token']:0;
      if($isToken == 1)
      {
        $token = $getData['token'];
        $user = JWTAuth::toUser($token);
        $user_id = $user->id;
      }
   		$result = array();
   		$response = array();
      //print_r($getData); die;
   		if(!empty($getData))
   		{

   			$email = $getData['email'];
   			$phone = $getData['phone'];
        if($isToken == 1)
        {
           $data = DB::table('users')->where(['email'=>$email,'phone'=>$phone])->where('id','!=',$user->id)->get()->toArray();
        } 
        else
        {
          $data = DB::table('users')->where(['email'=>$email,'phone'=>$phone])->get()->toArray();  
        }           
   			
   			if(empty($data))
        { 
   			  $registrationType = $getData['registration_type'];
   			  if($registrationType == 1)
   			  {
            if($isToken == 0)
            {
       			  $users = new User;
       			  $users->salution = isset($getData['salution'])?$getData['salution']:'';
       			  $users->name = isset($getData['name'])?$getData['name']:'';
       			  $users->role_id = 1;
              $users->id_type = isset($getData['id_type'])?$getData['id_type']:0;
              $users->nationality = isset($getData['nationality'])?$getData['nationality']:0;
              $users->id_number = isset($getData['id_number'])?$getData['id_number']:0;
              if(!empty($getData['dob']))
              {
              	  $users->dob = $getData['dob'];
              }
              $users->gender = isset($getData['gender'])?$getData['gender']:0;
              $users->marital_status = isset($getData['marital_status'])?$getData['marital_status']:0;
              $users->residential_address_one = isset($getData['residential_address_one'])?$getData['residential_address_one']:'';
              $users->residential_address_two = isset($getData['residential_address_two'])?$getData['residential_address_two']:'';
              $users->phone = isset($getData['phone'])?$getData['phone']:'';
              $users->email = isset($getData['email'])?$getData['email']:'';
              if(!empty($getData['password']))
              {
              	$users->password = bcrypt($getData['password']);	
              }
              else
              {
              	$users->password = '';
              }              
              $users->home_number = isset($getData['home_number'])?$getData['home_number']:'';
              $users->office_number = isset($getData['office_number'])?$getData['office_number']:'';
              $users->education_level = isset($getData['education_level'])?$getData['education_level']:0;
              $users->monthly_income = isset($getData['monthly_income'])?$getData['monthly_income']:0.00;
              $users->loan_amount = isset($getData['loan_amount'])?$getData['loan_amount']:0.0;
              $users->years_of_stay = isset($getData['years_of_stay'])?$getData['years_of_stay']:0;
              $users->status = 1;
              $users->is_loginfirst = 0;
             if($users->save())
             {
             	
             	$result['email'] = $getData['email'];
             	$result['phone'] = $getData['phone'];
             	$result['name'] = $getData['name'];
             	$response['status'] = 1;
    			  	$response['msg']  = 'Registration completed successfully';
    			  	$response['data'] = $result;
    			  	return Response::json($response);
             }
             else
             {
             	  $response['status'] = 0;
      			  	$response['msg']  = 'Registration not completed successfully !';
      			  	$response['data'] = [];
      			  	return Response::json($response);
             }
        }
        else
        {   
           unset($getData['is_token']); 
           unset($getData['token']);
           unset($getData['registration_type']);
           if(!empty($getData['password']))
           {
              $getData['password'] = bcrypt($getData['password']);  
           }
           else
           {
              $getData['password']= '';
           }
           $getData['status'] = 1;
           $getData['role_id'] = 1;
           $getData['is_loginfirst'] = 0;
           DB::table('users')
                        ->where('id',$user->id)
                        ->update($getData);

          $response['status'] = 1;
          $response['msg']  = 'Registration completed successfully !';
          $response['data'] = $getData;
          return Response::json($response);

        }           

   			}
   			else if($registrationType == 2)
   			{
          if($isToken == 0)
          {
            $users = new User;
   			    $users->name = isset($getData['name'])?$getData['name']:'';
   			    $users->uen_no = isset($getData['uen_no'])?$getData['uen_no']:'';
   			    $users->role_id = 2;
              $users->business_type = isset($getData['business_type'])?$getData['business_type']:0;
              $users->paid_up_capital = isset($getData['paid_up_capital'])?$getData['paid_up_capital']:0.00;
              if(!empty($getData['date_of_registration']))
              {
              	$users->date_of_registration = $getData['date_of_registration'];
              }      
              $users->registered_address_one = isset($getData['registered_address_one'])?$getData['registered_address_one']:'';
              $users->registered_address_two = isset($getData['registered_address_two'])?$getData['registered_address_two']:'';
              $users->business_address_one = isset($getData['business_address_one'])?$getData['business_address_one']:'';
              $users->business_address_two = isset($getData['business_address_two'])?$getData['business_address_two']:'';
              $users->office_number = isset($getData['office_number'])?$getData['office_number']:'';
              $users->contact_person = isset($getData['contact_person'])?$getData['contact_person']:'';
              $users->email = isset($getData['email'])?$getData['email']:'';
              $users->phone = isset($getData['phone'])?$getData['phone']:'';
              if(!empty($getData['password']))
              {
                $users->password = bcrypt($getData['password']);
              }
              else
              {
                $users->password = '';
              }
              $users->status = 1;
              
             if($users->save())
             {
             	
             	$result['email'] = $getData['email'];
             	$result['phone'] = $getData['phone'];
             	$result['name'] = $getData['name'];
             	$response['status'] = 1;
			  	    $response['msg']  = 'Registration completed successfully';
			  	    $response['data'] = $result;
			  	    return Response::json($response);
             }
             else
             {
              	$response['status'] = 0;
			  	      $response['msg']  = 'Registration not completed successfully !';
			  	      $response['data'] = [];
			  	      return Response::json($response);
             } 
   			 }
         else
         {
            unset($getData['is_token']); 
           unset($getData['token']);
           unset($getData['registration_type']);
           if(!empty($getData['password']))
           {
              $getData['password'] = bcrypt($getData['password']);  
           }
           else
           {
              $getData['password']= '';
           }
           $getData['status'] = 1;
           $getData['role_id'] = 2;
           $getData['is_loginfirst'] = 0;
           DB::table('users')
                        ->where('id',$user->id)
                        ->update($getData);
           $response['status'] = 1;
           $response['msg']  = 'Registration completed successfully !';
           $response['data'] = $getData;
           return Response::json($response);
         }     
        }
   		 }
   		  else
   		  {
   		  	$response['status'] = 0;
		  	$response['msg']  = 'This email or phone no. already exits ! Please try another email or phone no.';
		  	$response['data'] = [];
		  	return Response::json($response);
   		  }
   		}
   		else
   		{
   			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];
		  	return Response::json($response);
   		}
   	}

   	/**************  Apply Loan Individual ***********************/

   	public function applyLoanIndividual()
   	{
      //echo SITE_URL; die;
   		$response = array();
   		$result = array();
   		$getReqData = Input::all();
   		$getData = json_decode($getReqData['Body'],true);
      $isToken = isset($getData['is_token'])?$getData['is_token']:0;
      if($isToken == 1)
      {
        $token = $getData['token'];
        $user = JWTAuth::toUser($token);
        $user_id = $user->id;
      }
   		if(!empty($getData))
   		{
        if($isToken == 0)
        {
          $post = new User;
          $post->name = $getData['name'];
          $post->email = $getData['email'];
          $post->role_id = 1;
          $post->phone = $getData['phone'];
          $post->loan_amount = $getData['loan_amount_requested'];
          $post->office_number = $getData['office_number'];
          $post->marital_status = $getData['marital_status'];
          $post->residential_address_one = $getData['residential_address_one'];
          $post->residential_address_two = $getData['residential_address_two'];
          $post->education_level = $getData['education_level'];
          $post->monthly_income = $getData['monthly_income'];          
          $post->is_loginfirst = 1;
          $post->save();

          $email = $getData['email'];          
          $password = rand(18973824, 989721389);
          DB::table('users')
                  ->where('id', $post->id)
                  ->update(array('password' => bcrypt($password)));
          $emailTemplate = EmailTemplates::find(1);
          $messageBody = $emailTemplate->message;
          $messageBody = str_replace('[name]', $post->name, $messageBody);
          $messageBody = str_replace('[password]', $password, $messageBody);
          $data = ['message' => $messageBody];
        
          /************* PHP Mailer *************************/
          $mail = new PHPMailer\PHPMailer(true);                              
          try {
           
            $mail->SMTPDebug = 0;                                 
            $mail->isSMTP();                                      
            $mail->Host = SMTP_HOST;  
            $mail->SMTPAuth = true;                               
            $mail->Username = SMTP_USERNAME;                 
            $mail->Password = SMTP_PASSWORD;                           
            $mail->SMTPSecure = SMTP_SECURE;                            
            $mail->Port = PORT;                                    
            $mail->setFrom($emailTemplate->send_from);
            $mail->addAddress($post->email);
            $mail->isHTML(true);
            $mail->Subject = $emailTemplate->subject;
            $mail->Body    = $messageBody;
            $mail->send();
          } catch (Exception $e) {
               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
          }
        }           
        
     			$appyLoan = new LoanEnquiryIndividual();
          if($isToken == 0)
          {
            $appyLoan->user_id = $post->id;  
          }
          else
          {
            $appyLoan->user_id = $user_id;
          }
     			
     			$appyLoan->name = isset($getData['name'])?$getData['name']:'';
     			$appyLoan->email = isset($getData['email'])?$getData['email']:'';
     			$appyLoan->phone = isset($getData['phone'])?$getData['phone']:'';
     			$appyLoan->know_accredit = isset($getData['know_accredit'])?$getData['know_accredit']:0;
     			$appyLoan->loan_type = isset($getData['loan_type'])?$getData['loan_type']:0;
     			$appyLoan->loan_amount_requested = isset($getData['loan_amount_requested'])?$getData['loan_amount_requested']:0.00;

          $appyLoan->tenure = isset($getData['tenure'])?$getData['tenure']:0;

     			$appyLoan->loan_purpose = isset($getData['loan_purpose'])?$getData['loan_purpose']:0;
     			$appyLoan->reason_for_loan = isset($getData['reason_for_loan'])?$getData['reason_for_loan']:'';
     			$appyLoan->marital_status = isset($getData['marital_status'])?$getData['marital_status']:0;
     			$appyLoan->residential_address_one = isset($getData['residential_address_one'])?$getData['residential_address_one']:'';
     			$appyLoan->residential_address_two = isset($getData['residential_address_two'])?$getData['residential_address_two']:'';
     			$appyLoan->residential_type = isset($getData['residential_type'])?$getData['residential_type']:0;
     			$appyLoan->property_ownership = isset($getData['property_ownership'])?$getData['property_ownership']:0;
     			if(!empty($getData['length_of_stay']))
     			{
     			  $appyLoan->length_of_stay = $getData['length_of_stay'];	
     			}
          else
          {
            $appyLoan->length_of_stay = 0; 
          }
     			
     			$appyLoan->education_level = isset($getData['education_level'])?$getData['education_level']:0;
     			$appyLoan->employment_status = isset($getData['employment_status'])?$getData['employment_status']:0;
     			$appyLoan->company_name = isset($getData['company_name'])?$getData['company_name']:'';
     			$appyLoan->job_title = isset($getData['job_title'])?$getData['job_title']:0;
     			$appyLoan->specialization = isset($getData['specialization'])?$getData['specialization']:'';
     			if(!empty($getData['start_date_of_employment']))
     			{
     			  $appyLoan->start_date_of_employment = $getData['start_date_of_employment'];	
     			}
     			
     			  $appyLoan->length_of_current_employment_years = isset($getData['length_of_current_employment_years'])?$getData['length_of_current_employment_years']:'';	

            $appyLoan->length_of_current_employment_months = isset($getData['length_of_current_employment_months'])?$getData['length_of_current_employment_months']:'';  
     		
     			
     			$appyLoan->salary_paid = isset($getData['salary_paid'])?$getData['salary_paid']:0;
     			$appyLoan->annual_income = isset($getData['annual_income'])?$getData['annual_income']:0.00;
     			$appyLoan->monthly_income = isset($getData['monthly_income'])?$getData['monthly_income']:0.00;
     			if(!empty($getData['pay_date_of_salary']))
     			{
     				$appyLoan->pay_date_of_salary = $getData['pay_date_of_salary'];
     			}
     			$appyLoan->office_address_one = isset($getData['office_address_one'])?$getData['office_address_one']:'';
     			$appyLoan->office_address_two = isset($getData['office_address_two'])?$getData['office_address_two']:'';
     			$appyLoan->office_number = isset($getData['office_number'])?$getData['office_number']:'';
          if($getData['outstanding_loan'] == 'no')
          {
            $appyLoan->outstanding_loan = 'no';          
            $appyLoan->name_of_license_moneylender = '';
            $appyLoan->loan_amount = 0.00;
            $appyLoan->payment_frequency = 0;
          }
          else
          {
            $appyLoan->outstanding_loan = isset($getData['outstanding_loan'])?$getData['outstanding_loan']:'';          
            $appyLoan->name_of_license_moneylender = isset($getData['name_of_license_moneylender'])?$getData['name_of_license_moneylender']:'';
            $appyLoan->loan_amount = isset($getData['loan_amount'])?$getData['loan_amount']:0.00;
            $appyLoan->payment_frequency = isset($getData['payment_frequency'])?$getData['payment_frequency']:0;
          }
     			
     			$appyLoan->bank_name = isset($getData['bank_name'])?$getData['bank_name']:'';
     			$appyLoan->account_number = isset($getData['account_number'])?$getData['account_number']:'';
     			$appyLoan->number_of_dependents = isset($getData['number_of_dependents'])?$getData['number_of_dependents']:'';
     			$appyLoan->next_of_kin_contact_number = isset($getData['next_of_kin_contact_number'])?$getData['next_of_kin_contact_number']:0;
     			$appyLoan->next_of_kin_relationship = isset($getData['next_of_kin_relationship'])?$getData['next_of_kin_relationship']:0;
     			$appyLoan->approved_status = 1;
          $appyLoan->active_status = 1;
          $appyLoan->office_branch = isset($getData['office_branch'])?$getData['office_branch']:'';
     			/*$appyLoan->created_at = date('Y-m-d');
     			$appyLoan->modified_at = date('Y-m-d');*/

     			$proofOfIncome = isset($getReqData['proof_of_income'])?$getReqData['proof_of_income']:'';
     			if(input::hasFile('proof_of_income'))
     			{
     				 //echo SITE_URLI.'/public/files/proofofincome/'; die;
     				$file = Input::file('proof_of_income');
     				
        			$filename = microtime() . '.' .$file->getClientOriginalName();
        			$file->move(SITE_URLI.'/public/files/proofofincome/',$filename);
        			$appyLoan->proof_of_income = $filename;
     			}
     			$otherDocument = isset($getReqData['other_document'])?$getReqData['other_document']:'';
     			if(input::hasFile('other_document'))
     			{
     				$file = Input::file('other_document');   				
        			$filename = microtime() . '.' .$file->getClientOriginalName();
        			$file->move(SITE_URLI.'/public/files/otherdocument/',$filename);
        			$appyLoan->other_document = $filename;
     			}

     			if($appyLoan->save())
     			{
             $loanNo = rand(18973824, 989721389);
             $loanNo = '#ind'.$loanNo.$appyLoan->id;
             DB::table('loan_enquiry_individual')
                  ->where('id', $appyLoan->id)
                    ->update(array('loan_no'=>$loanNo));
     				$response['status'] = 1;
  			  	$response['msg']  = 'Loan enquiry submitted successfully';
  			  	$response['data'] = [];
  			  	return Response::json($response);
     			}
     			else
     			{
     				$response['status'] = 0;
  			  	$response['msg']  = 'Loan enquiry not submitted !';
  			  	$response['data'] = [];
  			  	return Response::json($response);
     			}   			

   		}
   		else
   		{
   			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];
		  	return Response::json($response);
   		}
   	}


   	/**************  Apply Loan Company ***********************/

   	public function applyLoanCompany()
   	{
   		$result = array();
   		$response = array();
   		$getReqData = Input::all();
      $getData = json_decode($getReqData['Body'],true);
      $isToken = isset($getData['is_token'])?$getData['is_token']:0;
      if($isToken == 1)
      {
        $token = $getData['token'];
        $user = JWTAuth::toUser($token);
        $user_id = $user->id;
      }  		
   		if(!empty($getData))
   		{
         $appyLoan = new LoanEnquiryCompany();
         if($isToken == 0)
         {
            $post = new User;
            $post->name = $getData['name'];
            $post->email = $getData['email'];
            $post->role_id = 2;          
            $post->loan_amount = $getData['loan_amount_requested'];
            $post->office_number = $getData['office_number'];          
            $post->is_loginfirst = 1;
            $post->save();

            $email = $getData['email'];          
            $password = rand(18973824, 989721389);
             DB::table('users')
                  ->where('id', $post->id)
                    ->update(array('password' => bcrypt($password)));
            $emailTemplate = EmailTemplates::find(1);
            $messageBody = $emailTemplate->message;
            $messageBody = str_replace('[name]', $post->name, $messageBody);
            $messageBody = str_replace('[password]', $password, $messageBody);
            $data = ['message' => $messageBody];
        
            /*********** PHP Mailer *************************/
            $mail = new PHPMailer\PHPMailer(true);                              
            try {
             
              $mail->SMTPDebug = 0;                                 
              $mail->isSMTP();                                      
              $mail->Host = SMTP_HOST;  
              $mail->SMTPAuth = true;                               
              $mail->Username = SMTP_USERNAME;                 
              $mail->Password = SMTP_PASSWORD;                           
              $mail->SMTPSecure = SMTP_SECURE;                            
              $mail->Port = PORT;                                    
              $mail->setFrom($emailTemplate->send_from);
              $mail->addAddress($post->email);
              $mail->isHTML(true);
              $mail->Subject = $emailTemplate->subject;
              $mail->Body    = $messageBody;
              $mail->send();
            } catch (Exception $e) {
                 // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }           

         }
         if($isToken == 0)
         {
           $appyLoan->user_id = $post->id;          
         }
         else
         {
            $appyLoan->user_id = $user_id;
         }
   			
   			
   			$appyLoan->name = isset($getData['name'])?$getData['name']:'';
   			$appyLoan->office_number = isset($getData['office_number'])?$getData['office_number']:'';
   			$appyLoan->email = isset($getData['email'])?$getData['email']:'';
   			$appyLoan->know_accredit = isset($getData['know_accredit'])?$getData['know_accredit']:0;
   			$appyLoan->loan_type = isset($getData['loan_type'])?$getData['loan_type']:0;

   			$appyLoan->loan_amount_requested = isset($getData['loan_amount_requested'])?$getData['loan_amount_requested']:0.00;

        $appyLoan->tenure = isset($getData['tenure'])?$getData['tenure']:0;
        
   			$appyLoan->loan_purpose = isset($getData['loan_purpose'])?$getData['loan_purpose']:0;
   			$appyLoan->reason_for_loan = isset($getData['reason_for_loan'])?$getData['reason_for_loan']:'';
   			if(!empty($getData['financial_year_end_as_at']))
   			{
   				$appyLoan->financial_year_end_as_at = $getData['financial_year_end_as_at'];
   			}
   			
   			$appyLoan->annual_gross_profit = isset($getData['annual_gross_profit'])?$getData['annual_gross_profit']:0.00;
   			$appyLoan->annual_net_profit = isset($getData['annual_net_profit'])?$getData['annual_net_profit']:0.00;
   			$appyLoan->total_asset = isset($getData['total_asset'])?$getData['total_asset']:0.0;
   			$appyLoan->total_current_asset = isset($getData['total_current_asset'])?$getData['total_current_asset']:0.00;
   			
   			$appyLoan->total_liabilities = isset($getData['total_liabilities'])?$getData['total_liabilities']:0.00;
   			$appyLoan->total_current_liabilities = isset($getData['total_current_liabilities'])?$getData['total_current_liabilities']:0.00;
   			$appyLoan->company_office_address_one = isset($getData['company_office_address_one'])?$getData['company_office_address_one']:'';
   			$appyLoan->company_office_address_two = isset($getData['company_office_address_two'])?$getData['company_office_address_two']:0;
   			$appyLoan->residential_type = isset($getData['residential_type'])?$getData['residential_type']:0;
   			
   			
   			$appyLoan->property_ownership = isset($getData['property_ownership'])?$getData['property_ownership']:0;

   			$appyLoan->annual_profit = isset($getData['annual_profit'])?$getData['annual_profit']:0.00;
   			$appyLoan->outstanding_loan = isset($getData['outstanding_loan'])?$getData['outstanding_loan']:'';
   			
   			$appyLoan->name_of_license_moneylender = isset($getData['name_of_license_moneylender'])?$getData['name_of_license_moneylender']:'';
   			$appyLoan->loan_amount = isset($getData['loan_amount'])?$getData['loan_amount']:0.00;
   			$appyLoan->payment_frequency = isset($getData['payment_frequency'])?$getData['payment_frequency']:'';
   			$appyLoan->bank_name = isset($getData['bank_name'])?$getData['bank_name']:'';
   			$appyLoan->account_number = isset($getData['account_number'])?$getData['account_number']:'';
          $appyLoan->approved_status = 1;
        $appyLoan->active_status = 1;
        $appyLoan->office_branch = isset($getData['office_branch'])?$getData['office_branch']:'';

        $proofOfIncome = isset($getReqData['proof_of_income'])?$getReqData['proof_of_income']:'';
        if(input::hasFile('proof_of_income'))
        {
           //echo SITE_URLI.'/public/files/proofofincome/'; die;
          $file = Input::file('proof_of_income');
          
            $filename = microtime() . '.' .$file->getClientOriginalName();
            $file->move(SITE_URLI.'/public/files/proofofincome/',$filename);
            $appyLoan->proof_of_income = $filename;
        }
        $otherDocument = isset($getReqData['other_document'])?$getReqData['other_document']:'';
        if(input::hasFile('other_document'))
        {
          $file = Input::file('other_document');          
            $filename = microtime() . '.' .$file->getClientOriginalName();
            $file->move(SITE_URLI.'/public/files/otherdocument/',$filename);
            $appyLoan->other_document = $filename;
        }

   			if($appyLoan->save())
   			{
          $loanNo = rand(18973824, 989721389);
             $loanNo = '#comp'.$loanNo.$appyLoan->id;
             DB::table('loan_enquiry_company')
                  ->where('id', $appyLoan->id)
                    ->update(array('loan_no'=>$loanNo));

   				$response['status'] = 1;
			  	$response['msg']  = 'Loan enquiry submitted successfully';
			  	$response['data'] = [];
			  	return Response::json($response);
   			}
   			else
   			{
   				$response['status'] = 0;
			  	$response['msg']  = 'Loan enquiry not submitted !';
			  	$response['data'] = [];
			  	return Response::json($response);
   			}
   			

   		}
   		else
   		{
   			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];
		  	return Response::json($response);
   		}
   	}



   	/**************  Get Myinfo data ***********************/

   	public function getMyInfo()
   	{
   		$getReqData = file_get_contents('php://input');
   		$getData = $this->getJson($getReqData);
   		if(!empty($getData))
   		{
   		  
   		}
   		else
   		{
			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];
		  	return Response::json($response);
   		}
   	}


	
	

	 /************** Forgot Password ***********************/
	public function forgotpassword(){
		$response = array();
		$result = array();
		$getReqData = file_get_contents('php://input');
		// get decode json data
		$getData = $this->getJson($getReqData);

		$email = $getData['email'];

		$userData = User::where('email',$email)->first();
		if(!empty($userData)) {

			$password = rand(18973824, 989721389);
			DB::table('users')
							->where('id', $userData->id)
							->update(array('password' => bcrypt($password)));

			// send and email 
			$emailTemplate = EmailTemplates::find(1);
			$messageBody = $emailTemplate->message;
			$messageBody = str_replace('[name]', $userData->username, $messageBody);
			$messageBody = str_replace('[password]', $password, $messageBody);

		   // return view('emails.message')->with('data',$messageBody);   // check email before sending
			$data = ['message' => $messageBody];
			// Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$userData)
			// {
			// 	$m->to($userData->email,$userData->username)
			// 	 ->subject($emailTemplate->subject)
			// 	 ->from($emailTemplate->send_from);
			//    });
			/************* PHP Mailer *************************/
			$mail = new PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
			try {
				//Server settings
				$mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = SMTP_USERNAME;                 // SMTP username
				$mail->Password = SMTP_PASSWORD;                           // SMTP password
				$mail->SMTPSecure = SMTP_SECURE;                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = PORT;                                    // TCP port to connect to

				//Recipients
				$mail->setFrom($emailTemplate->send_from);
				$mail->addAddress($userData->email);     // Add a recipient
				//Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $emailTemplate->subject;
				$mail->Body    = $messageBody;

				$mail->send();
			} catch (Exception $e) {
				   // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
			}
			 $response['status'] = 1;
		  	$response['msg']  = 'An email sent you with new password on your registered email. Please check.';
		  	$response['data'] = [];
		  	return $response; die();			
		}
		else
		{
			 $response['status'] = 0;
		  	$response['msg']  = 'We cannot find an account for contains this email. Please enter your correct email.';
		  	$response['data'] = [];
		  	return Response::json($response); 
		}

	}


	

	/************** Change Password 1***********************/
	public function changePassword(Request $request){
		$response = array();
		$result = array();
		$getReqData = file_get_contents('php://input');
		// get decode json data
		$getData = $this->getJson($getReqData);		
		$token = $getData['token'];
		$old_password = $getData['opassword'];
		$new_password = $getData['password'];

		$user = JWTAuth::toUser($token); // authorization JWT Token

		$user_id = $user->id;
		// check old password
		if (Hash::check($old_password, $user->password))
		{ 
			if (Hash::check($new_password, $user->password))
			{ 
				$response['status'] = 0;
		  		$response['msg']  = 'Old password and new password cannot same.';
		  		$response['data'] = [];
				return Response::json($response);
			}
			else
			{
				$password = Hash::make($new_password);
				// update password in DB
				User::where('id', $user_id)->update(array('password' => $password));

				$response['status'] = 1;
		  	    $response['msg']  = 'Password updated successfully.';
			  	$response['data'] = [];
			  	return Response::json($response);

			}
		 }
		 else
		 {
		 	$response['status'] = 0;
		  	$response['msg']  = 'Old password not correct.';
		  	$response['data'] = [];
			return Response::json($response);
		}

	}
	
	

	  /************** Edit Profile ***********************/
	public function editProfile(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];
		$name = $data['name'];
		$username = $data['username'];
		$email = $data['email'];
		$phone = $data['phone'];
		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;
		
		// check username unique
		$userNameUnique = DB::table('users')->where('username',$username)->where('id','!=',$user_id)->first();
		if(!empty($userNameUnique)){
			 return Response::json(array('status'=>false,'message'=>"Username not unique."));
		}
		// check email unique
		$userEmailUnique = DB::table('users')->where('email',$email)->where('id','!=',$user_id)->first();
		if(!empty($userEmailUnique)){
			 return Response::json(array('status'=>true,'message'=>"Email not unique."));
		}

		// update edit basic info
		$userObj = User::find($user_id);
		$userObj->name= trim($name);
		$userObj->email= trim($email);
		$userObj->username= trim($username);
		$userObj->phone= trim($phone);
		$userObj->save();
		return Response::json(array('status'=>true,'message'=>"Profile details updated successfully."));
	}


 


	 /************** Get Id Type ***********************/

	 public function getIdType()
	 {
	 	$result = array();
	 	$data = IdType::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);	 
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
          	$result[$i]['id'] = $value['id'];
            $result[$i]['name'] = $value['name'];
          	$i++;
          }          
		      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	 /************** Get Education Level ***********************/

	 public function getEducationLevel()
	 {
	 	$result = array();

	 	$data = EducationLevel::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
          
		  return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }

    /************** Get Reason For Loan ***********************/

   public function getReasonForLoan()
   {
    $result = array();

    $data = ReasonForLoan::where('status',1)->get();
    $data = json_decode(json_encode($data), true);
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
            $result[$i]['name'] = $value['name'];
            $i++;
          }
          
      return $result;
    }
    else
    {
      $result = '';
      return $result;
    }

   }

	 /************** Get Business Type ***********************/

	 public function getBusinessType()
	 {
	 	$result = array();
	 
	 	$data = BusinessType::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
       
       return $result;
	 	}
	 	else
	 	{
	 	    $return = '';
        return $result;
	 	}

	 }

	  /************** Get Know Accredit ***********************/

	 public function getKnowAccredit()
	 {
	 	$result = array();
	 
	 	$data = KnowAccredit::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
       return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }

	  /************** Get Loan Type ***********************/

	 public function getLoanType()
	 {
	 	$result = array();
	 
	 	$data = LoanType::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
         return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	  /************** Get Loan Type ***********************/

	 public function getLoanPurpose()
	 {
	 	$result = array();
	 
	 	$data = LoanPurpose::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
         
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	  /************** Get Residential ***********************/

	 public function getResidentialType()
	 {
	 	$result = array();
	 
	 	$data = ResidentialType::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
         
      return $result;
	 	}
	 	else
	 	{
	 	   $result = '';
      return $result;
	 	}

	 }

	  /************** Get Property Ownership ***********************/

	 public function getPropertyOwnership()
	 {
	 	$result = array();
	 	
	 	$data = PropertyOwnership::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
        
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	 /************** Get Employment Status ***********************/

	 public function getEmploymentStatus()
	 {
	 	$result = array();
	 
	 	$data = EmploymentStatus::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
          
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }

	 /************** Get job Title List ***********************/

	 public function getJobTitle()
	 {
	 	$result = array();
	 
	 	$data = JobTitle::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
        
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	  /************** Get Specilization ***********************/

	 public function getSpecilization()
	 {
	 	$result = array();
	 
	 	$data = Specialization::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
        
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }



	 /************** Get Office Branch ***********************/

	 public function getOfficeBranch()
	 {
	 	$result = array();
	 
	 	$data = OfficeBranch::where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);	 
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
          return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }

	  /************** Get Admin Document ***********************/

	 public function getAdminDocument()
	 {
	 	$result = array();
	 	$response = array();
    $text = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      Why do we use it?

    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
    ';
	 	/*$getReqData = file_get_contents('php://input');		
		$getData = $this->getJson($getReqData);		
		$token = $getData['token'];
		$user = JWTAuth::toUser($token);*/


	 	$data = AdminDocuments::orderByRaw('priority = 0, priority')->where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);	 
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
          	$result[$i]['title'] = $value['title'];
          	$result[$i]['url'] = SITE_URL. '/public/files/admindocuments/'.$value['document'];
            $result[$i]['priority'] = $value['priority'];
            $result[$i]['text'] = $text;
          	$i++;
          }
          $response['status'] = 1;
		  $response['msg']  = 'Document List';
		  $response['data'] = $result;
		  return Response::json($response);
	 	}
	 	else
	 	{
	 		$response['status'] = 0;
		  	$response['msg']  = 'Record not found !';
		  	$response['data'] = [];
			return Response::json($response);
	 	}

	 }


	 /************** Get About Us ***********************/

	 public function getAboutContent()
	 {
	 	$result = array();
	 	$response = array();

	 /*	$getReqData = file_get_contents('php://input');		
		$getData = $this->getJson($getReqData);		
		$token = $getData['token'];
		$user = JWTAuth::toUser($token);*/


	 	$data = DB::table('about_content')->get();
	 	$data = json_decode(json_encode($data), true);	 	
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
          	$result[$i]['content'] = $value['content'];          	
          	$i++;
          }
          $response['status'] = 1;
		  $response['msg']  = 'About Content';
		  $response['data'] = $result;
		  return Response::json($response);
	 	}
	 	else
	 	{
	 		$response['status'] = 0;
		  	$response['msg']  = 'Record not found !';
		  	$response['data'] = [];
			return Response::json($response);
	 	}

	 }


   /************** Get FAQ ***********************/

   public function getFaq()
   {
    $result = array();
    $response = array();
    $faq = array();
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);

    $data = DB::table('Faq')->where('status',1)->get();
    $dataFaqCategory = FaqCategories::with('faqlist')->where('status',1)->get();
    
    $dataFaqCategory = json_decode(json_encode($dataFaqCategory),true);  
    //print_r($dataFaqCategory[0]['faqlist'][0]['name']); die;  
    if(!empty($dataFaqCategory))
    {
      $i = 0;
      foreach ($dataFaqCategory as $key => $value)
      {
        if(!empty($value['faqlist']))
        {
          $j = 0;
          foreach ($value['faqlist'] as $key11 => $value11)
          {
             $faq[$j]['question'] = $value11['question'];           
             $faq[$j]['answer'] = $value11['answer'];
             $j++;
          }
          $result[$i]['category']['name'] = $value['name'];
          $result[$i]['category']['value'] = $faq;
          
        }
        $i++;
      }
      $response['status'] = 1;
      $response['msg']  = 'Faq';
      $response['data'] = $result;
      return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }
   /* $data = json_decode(json_encode($data), true);    
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['question'] = $value['question'];           
            $result[$i]['answer'] = $value['answer'];
            $i++;
          }
      $response['status'] = 1;
      $response['msg']  = 'Faq';
      $response['data'] = $result;  
      return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }*/

   }


   /************** Loan Calculator ***********************/

   public function loanCalculator()
   {
    $result = array();
    $response = array();

    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    
   $loanAmount = $getData['loan_amount'];
   $tenure = $getData['tenure'];
   $interestRate = $getData['interest'];
   $paymentFrequency = $getData['payment_frequency'];
   $dueDate= date('Y-m-d'); 
   $emi  = $this->pmt($interestRate,$paymentFrequency,$loanAmount,$tenure); 
   $totalPaidAmont = $emi*$tenure;
   $dueBalance = $loanAmount;

    if(!empty($emi) || $emi > 0)
    {
      $j=0;
      $k = 1;
      for ($i=1; $i <= $tenure; $i++)
      {
        if($paymentFrequency == 365)
        {
            $dueDate = date('Y-m-d',strtotime($dueDate.' +1 day'));
        }
        else if($paymentFrequency == 52)
        {                       
           $dueDate = date('Y-m-d',strtotime($dueDate.' +7 days'));
        }
        else if($paymentFrequency == 12)
        {
            $dueDate = date('Y-m-d',strtotime($dueDate.' +1 month'));
        }
        else if($paymentFrequency == 1)
        {
            $dueDate = date('Y-m-d',strtotime($dueDate.' +1 year'));
        }
           
        $rateInt = $interestRate/$paymentFrequency;
        $k = $k-1;
        $interestAmount = number_format($dueBalance*$rateInt/100,2,'.','');    
        $principalAmount = number_format($emi-$interestAmount,2,'.','');
       //echo $dueBalance; die;
        $dueBalance = number_format($dueBalance-$principalAmount,2,'.','');
        $result[$j]['installment_name'] = 'Installment '.$i;
        $result[$j]['due_date'] = $dueDate;
        $result[$j]['principal_amount'] = $principalAmount;
        $result[$j]['interest_amount'] = $interestAmount;
        $result[$j]['total_emi'] = number_format($emi,2,'.','');
        $result[$j]['due_balance'] = $dueBalance;
        $j++;
        $k++;
      } 
      $response['status'] = 1;
      $response['msg']  = 'Loan Calculator';
      $response['total_amount'] = number_format($totalPaidAmont,2,'.','');
      $response['data'] = $result;
      return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }

   }

   public function pmt($interest, $paymentFrequency, $loan,$tenure) 
    {
        //echo $tenure; die;
        $intr =$interest/($paymentFrequency*100); //get percentage
        $EMI= $loan * $intr / (1 - (pow(1/(1 + $intr),$tenure))); # This for 
        return $EMI;
    }







/************** Check Email  ***********************/
public function checkEmail()
{
    $result = array();
    $response = array();
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);
    if(!empty($getData))
    {
       $dataUser = DB::table('users')->where('email',$getData['email'])->first();
       $data = json_decode(json_encode($dataUser), true); 
       if(!empty($data))
       {
          $response['status'] = 0;
          $response['msg']  = 'This email is exist.Please login with your credentials';
          $response['data'] = [];
          return Response::json($response);
       }
       else
       {
         $response['status'] = 1;
          $response['msg']  = 'Successfully';
          $response['data'] = [];
          return Response::json($response);
       }

    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }
}



/************** Check Phone No.  ***********************/
public function checkPhone()
{
    $result = array();
    $response = array();
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);
    if(!empty($getData))
    {
       $dataUser = DB::table('users')->where('phone',$getData['phone'])->first();
       $data = json_decode(json_encode($dataUser), true); 
       if(!empty($data))
       {
          $response['status'] = 0;
          $response['msg']  = 'This phone number is exist.Please try another phone number !';
          $response['data'] = [];
          return Response::json($response);
       }
       else
       {
         $response['status'] = 1;
          $response['msg']  = 'Successfully';
          $response['data'] = [];
          return Response::json($response);
       }

    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }
}



/************** Get Front QR Code ***********************/

   public function getFrontQRCode()
   {
    $result = array();
    $response = array();
    $imagePath = SITE_URL. '/public/files/frontqrcode/paymentqrcode.png';
      $extension = pathinfo($imagePath, PATHINFO_EXTENSION);
      $img = file_get_contents($imagePath);
      $dataEncoded = base64_encode($img);
      $base64Str = 'data:image/' . $extension . ';base64,' . $dataEncoded;
      $result['url'] =   $base64Str;  
      $response['status'] = 1;
      $response['msg']  = 'QR Code';
      $response['data'] = $result;
       echo json_encode($response,JSON_UNESCAPED_SLASHES); die;
   }


   /************** Payment Schedule ***********************/

   public function getPaymentScheduleList()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    $i = 0;
    //print_r($user); die;
    $dataLoanInd = LoanEnquiryIndividual::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>0])->get();    
    $dataLoanInd = json_decode(json_encode($dataLoanInd), true);

    $dataLoanComp = LoanEnquiryCompany::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>0])->get();   
    $dataLoanComp = json_decode(json_encode($dataLoanComp), true);

    
    if(!empty($dataLoanInd))
    {
      
          foreach ($dataLoanInd as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
            $result[$i]['loan_no'] = $value['loan_no'];            
            $result[$i]['loan_type'] = 3;
            $i++;
          }
    }

    if(!empty($dataLoanComp))
    {
          $j = $i;
          foreach ($dataLoanComp as $key => $value)
          {
            $result[$j]['id'] = $value['id'];
            $result[$j]['loan_no'] = $value['loan_no'];
            $result[$j]['loan_type'] = 4;            
            $j++;
          }
    }

    if(!empty($result))
    {
       $response['status'] = 1;
        $response['msg']  = 'Payment Schedule List';
        $response['data'] = $result;
        return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }

   }


    /************** Payment Schedule Details ***********************/

   public function getPaymentScheduleDetails()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    $loanId = $getData['loan_id'];
    $loanType = $getData['loan_type'];
    if($loanType == 3)
    {
      //echo $user->id; die;
      $data = Payments::with('loanenquiryindividual')->where(['user_id'=>$user->id,'loan_enquiry_individual_id'=>$loanId,'is_completed'=>0])->get();    
    }
    else if($loanType == 4)
    {
      $data = Payments::with('loanenquiryindividual')->where(['user_id'=>$user->id,'loan_enquiry_company_id'=>$loanId,'is_completed'=>0])->get();
    }
    
    $data = json_decode(json_encode($data), true);
    $totalDueAmount = 0;
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            
            $totalDueAmount = $totalDueAmount+$value['total_emi'];           
            $result[$i]['installment_name'] = $value['installment_name'];
            $result[$i]['due_on'] = date('d-M-Y',strtotime($value['due_date']));
            $result[$i]['principal_amount'] = $value['principal_amount'];
            $result[$i]['interest_amount'] = $value['interest_amount'];
            $result[$i]['balance_due'] = $value['due_balance'];
            $result[$i]['late_fee'] = $value['late_fee'];
            if($value['status'] == 1)
            {
              $result[$i]['status'] = 'pending';  
            }
            else if($value['status'] == 2)
            {
              $result[$i]['status'] = 'Over Due';
            }
            
            $i++;
          }
          $response['status'] = 1;
          $response['msg']  = 'Payment Schedule Details';
          $response['account_number']  = $data[0]['loanenquiryindividual']['account_number'];
          $response['total_due']  = number_format($totalDueAmount,2,'.','');
          $response['data'] = $result;
          return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
      $response['msg']  = 'Payment schedule details not found !';
      $response['account_number']  = '';
      $response['total_due']  = '';
      $response['data'] = $result;
      return Response::json($response);
    }

   }


   /************** Re-Payment Schedule ***********************/

   public function getRePaymentScheduleList()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    
    $dataLoanInd = LoanEnquiryIndividual::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>1])->get();    
    $dataLoanInd = json_decode(json_encode($dataLoanInd), true);
   
    $dataLoanComp = LoanEnquiryCompany::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>1])->get();   
    $dataLoanComp = json_decode(json_encode($dataLoanComp), true);

    //print_r($dataLoanInd); die;
    if(!empty($dataLoanInd))
    {
      $i = 0;
          foreach ($dataLoanInd as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
            $result[$i]['loan_no'] = $value['loan_no'];            
            $result[$i]['loan_type'] = 3;
            $i++;
          }
    }

    if(!empty($dataLoanComp))
    {
      $j = $i;
          foreach ($dataLoanComp as $key => $value)
          {
            $result[$j]['id'] = $value['id'];
            $result[$j]['loan_no'] = $value['loanenquiryindividual']['loan_no'];
            $result[$j]['loan_type'] = 4;            
            $j++;
          }
    }

    if(!empty($result))
    {
       $response['status'] = 1;
        $response['msg']  = 'Re-Payment Schedule List';
        $response['data'] = $result;
        return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }

   }


    /************** Re-Payment Schedule Details ***********************/

   public function getRePaymentScheduleDetails()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    $loanId = $getData['loan_id'];
    $loanType = $getData['loan_type'];
    if($loanType == 3)
    {
      //echo $user->id; die;
      $data = Payments::where(['user_id'=>$user->id,'loan_enquiry_individual_id'=>$loanId,'is_completed'=>1])->get();    
    }
    else if($loanType == 4)
    {
      $data = Payments::where(['user_id'=>$user->id,'loan_enquiry_company_id'=>$loanId,'is_completed'=>1])->get();
    }
    
    $data = json_decode(json_encode($data), true);
    $totalDueAmount = 0;
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {            
            $result[$i]['payment_id'] = $value['id'];         
            $result[$i]['date_of_payment'] = date('d-M-Y',strtotime($value['date_of_payment']));
            $result[$i]['amount'] = $value['total_emi'];
            $result[$i]['date_of_receipt'] = date('d-M-Y',strtotime($value['date_of_payment']));
            $result[$i]['receipt_no'] = $value['receipt_no'];   
            $i++;
          }
          $response['status'] = 1;
          $response['msg']  = 'Re Payment Schedule Details';          
          $response['data'] = $result;
          return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
      $response['msg']  = 'Re Payment schedule details not found !';      
      $response['data'] = $result;
      return Response::json($response);
    }

   }


    /************** Download PDF ***********************/

   public function downloadPDF()
   {
    //phpinfo();die;
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];    
    $user = JWTAuth::toUser($token);
    $paymentId = $getData['payment_id'];
    $officeBranchId = 0;
    $data = Payments::where(['id'=>$paymentId,'is_completed'=>1])->first();
    if($data->loan_type == 3)
    {      
      $loan_no = isset($data->loanenquiryindividual->loan_no)?$data->loanenquiryindividual->loan_no:'';
      $officeBranchId = isset($data->loanenquiryindividual->office_branch)?$data->loanenquiryindividual->office_branch:'';
      ;
     
    }
    else if($data->loan_type == 4)
    {
      $loan_no = isset($data->loanenquirycompany->loan_no)?$data->loanenquirycompany->loan_no:'';
       $officeBranchId = $data->loanenquirycompany->office_branch;
      ;
    }

    $dataOfficeBranch = OfficeBranch::where('id',$officeBranchId)->first();
    
    $data = json_decode(json_encode($data), true);
   
    if(!empty($data))
    {
        //echo SITE_URLI; die;
    
         if(empty($data->url))
         {
            $url = SITE_URLI.'/public/files/receiptSingle/singreceipt'.$paymentId.'.pdf';

             $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8', 
            'format' => [220, 236], 
            'orientation' => 'L'
              ]);

            $html = file_get_contents(SITE_URL. '/public/reciept.html');
            $html = str_replace('[receipt_no]',$data['receipt_no'],
                        $html);
            $html = str_replace('[date]',date('d-M-Y'),
                        $html);
            $html = str_replace('[loan_no]',$loan_no,
                        $html);
            $html = str_replace('[installment_name]',$data['installment_name'],
                        $html);
            $html = str_replace('[principal_amount]','$'.$data['principal_amount'],
                        $html);
            $html = str_replace('[interest_amount]','$'.$data['interest_amount'],
                        $html);
            $html = str_replace('[interest_amount]','$'.$data['interest_amount'],
                        $html);
            $html = str_replace('[outstanding_amount]','$'.$data['due_balance'],
                        $html);
            $html = str_replace('[total_emi]','$'.$data['total_emi'],
                        $html);
            $html = str_replace('[late_fee]','$'.$data['late_fee'],
                        $html);
            if(!empty($dataOfficeBranch))
            {
              $html= str_replace('[office_branch]',$dataOfficeBranch['name'],$html);
            }
            
            //print_r($html); die;
            $mpdf->WriteHTML($html);
            $mpdf->Output($url,'F');
            $pdfUrl = SITE_URL.'/public/files/receiptSingle/singreceipt'.$paymentId.'.pdf';

            DB::table('payments')
              ->where('id', $paymentId)
              ->update(array('url' =>$pdfUrl));
         }
         else
         {
           $pdfUrl = $data->url;
         }

          $imagePath = $pdfUrl;
          $extension = pathinfo($imagePath, PATHINFO_EXTENSION);
          $img = file_get_contents($imagePath);
          $dataEncoded = base64_encode($img);
          $base64Str = 'data:application/pdf'.';base64,' . $dataEncoded;

        $result['date_of_payment'] = date('d-M-Y',strtotime($data['due_date']));
        $result['amount'] = $data['total_emi'];
        $result['date_of_receipt'] = date('d-M-Y',strtotime($data['date_of_payment']));
        $result['receipt_no'] = $data['receipt_no'];   
        $result['url'] = $base64Str;
          $response['status'] = 1;
          $response['msg']  = 'Download PDF';          
          $response['data'] = $result;
          return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
      $response['msg']  = 'Record not found !';      
      $response['data'] = $result;
      return Response::json($response);
    }

   }


   /************** Download Bulk PDF ***********************/

   public function downloadBulkPDF()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    $dateFrom = $getData['date_from'];
    $dateTo = $getData['date_to'];
    $loanId = $getData['loan_id'];
    $loanType = $getData['loan_type'];
    $html11 = '';
    $html = '';
    $totalPricipalAmount = 0;
    $totalInterestAmount = 0;
    $totalAmountRequested = 0;   
    $totalPayableAmount  = 0;
    $pricipalAmount = 0; 
    $interestAmount = 0;
    $lateFee = 0;
    $payableAmount = 0;
    $isData = 0;
    if($loanType == 3)
    {
      $data = Payments::where(['loan_enquiry_individual_id'=>$loanId])->get();   
    }
    else if($loanType == 4)
    {
      $data = Payments::whereBetween('date_of_payment',[$dateFrom,$dateTo])->where(['loan_enquiry_company_id'=>$loanId])->get();
    }  

    if(!$data->isEmpty())
    {
       
      if($loanType == 3)
      {      
        $loan_no = isset($data[0]->loanenquiryindividual->loan_no)?$data[0]->loanenquiryindividual->loan_no:'';  
        $totalAmountRequested = isset($data[0]->loanenquiryindividual->loan_amount_requested)?$data[0]->loanenquiryindividual->loan_amount_requested:'';    
      }
      else if($loanType == 4)
      {
        $loan_no = isset($data[0]->loanenquirycompany->loan_no)?$data[0]->loanenquirycompany->loan_no:'';
        $totalAmountRequested = isset($data[0]->loanenquirycompany->loan_amount_requested)?$data[0]->loanenquirycompany->loan_amount_requested:'';
      }   
       $customerName = isset($data[0]->userinfo->name)?$data[0]->userinfo->name:'';
       $customerAddress = isset($data[0]->userinfo->residential_address_one)?$data[0]->userinfo->residential_address_one:'';
       $customerPhone = isset($data[0]->userinfo->phone)?$data[0]->userinfo->phone:'';
       $customerEmail = isset($data[0]->userinfo->email)?$data[0]->userinfo->email:''; 
    }    
    $data = json_decode(json_encode($data), true); 

    if(!empty($data))
    { 
       $officeBranchId = $data[0]['loanenquiryindividual']['office_branch'];        
       $dataOfficeBranch = OfficeBranch::where('id',$officeBranchId)->first();
       $branchName = isset($dataOfficeBranch['name'])?$dataOfficeBranch['name']:'';
       $branchAddress = isset($dataOfficeBranch['address'])?$dataOfficeBranch['address']:'';
       $branchPhone = isset($dataOfficeBranch['phone'])?$dataOfficeBranch['phone']:'';

       $html = file_get_contents(SITE_URL. '/public/bulkReciept.html');
       $html = str_replace(array('date_from','date_to','branch_name','branch_address','branch_phone','customer_name','customer_address','customer_phone','customer_email'),array($dateFrom,$dateTo,$branchName,$branchAddress,$branchPhone,$customerName,$customerAddress,$customerPhone,$customerEmail),$html);

       $mpdf = new \Mpdf\Mpdf([
          'mode' => 'utf-8',
          'format' => [220, 236], 
          'orientation' => 'L'
            ]);
        $digits = 4;
         $randNo = rand(pow(10, $digits-1), pow(10, $digits)-1);       
         $url = SITE_URLI.'/public/files/receiptBulk/bulkreceipt'.$randNo.'.pdf';
            
         $i = 0;
        foreach ($data as $key => $value)
        {
          if($value['is_completed'] == 1 && $value['date_of_payment'] >= $dateFrom && $value['date_of_payment'] <= $dateTo){
            $isData = 1;
          $html11 .= '<tr>
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">'. date('d-M-Y',strtotime($value["date_of_payment"])) .'</td>
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["principal_amount"] .'</td>
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["interest_amount"] .'</td>
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["late_fee"] .'</td>
                      
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["total_emi"] .'</td>
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["principal_amount"] .'</td>
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["interest_amount"] .'</td>
                      <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["late_fee"] .'</td>
                      
                  </tr>';
          $result[$i]['date_of_payment'] = date('d-M-Y',strtotime($value['due_date']));
          $result[$i]['amount'] = $value['total_emi'];
          $result[$i]['date_of_receipt'] = date('d-M-Y',strtotime($value['date_of_payment']));
          $result[$i]['receipt_no'] = $value['receipt_no'];          
          $pricipalAmount = $pricipalAmount+$value['principal_amount'];
          if($value['interest_amount'] > 0)
          {
            $interestAmount = number_format($interestAmount+$value['interest_amount'],2,'.','');
          }
          
          $payableAmount = $payableAmount+$value['total_emi'];
          
          $lateFee = number_format($lateFee+$value['late_fee'],2,'.','');
          $i++;
        }
        $totalPricipalAmount = number_format($totalPricipalAmount+$value['principal_amount'],2,'.','');
        $totalInterestAmount = number_format($totalInterestAmount+$value['interest_amount'],2,'.','');
        $totalPayableAmount = $totalPayableAmount+$value['total_emi'];
        }
        if($isData == 1)
        {
          $totaloutstanigAmount = number_format($totalPayableAmount -$payableAmount,2,'.',''); 
         $totalOutStandingPriciplaAmount = number_format($totalPricipalAmount -$pricipalAmount,2,'.','');
         //echo $totalOutStandingPriciplaAmount; die; 
         $html = str_replace(array('loan_no','loan_date','loan_amount_requested','principal_amount','total_pricipal_amount','total_interest_amount','interest_amount','total_payable_amount','payable_amount','late_fee','total_outstanding_amount','total_outstanding_pricipal_amount'),array($loan_no,date('d-M-Y',strtotime($data[0]['created_at'])),'$'.$totalAmountRequested,'$'.$pricipalAmount,'$'.$totalPricipalAmount,'$'.$totalInterestAmount,'$'.$interestAmount,'$'.$totalPayableAmount,'$'.$payableAmount,'$'.$lateFee,'$'.$totaloutstanigAmount,$totalOutStandingPriciplaAmount),$html);
            $html = str_replace('[replace_tr]',$html11, $html);
          //print_r($html); die;
          $mpdf->WriteHTML($html);        
          $mpdf->Output($url,'F');
          $pdfUrl = SITE_URL.'/public/files/receiptBulk/bulkreceipt'.$randNo.'.pdf';
          

          $imagePath = $pdfUrl;
          $extension = pathinfo($imagePath, PATHINFO_EXTENSION);
          $img = file_get_contents($imagePath);
          $dataEncoded = base64_encode($img);
          $base64Str = 'data:application/pdf'.';base64,' . $dataEncoded;

          $response['status'] = 1;
          $response['msg']  = 'Download Bulk PDF';          
          $response['data'] = $result;
          $response['url'] = $base64Str;
          return Response::json($response);
        }  
        else
        {
          $response['status'] = 0;
          $response['msg']  = 'Download Bulk PDF';          
          $response['data'] = $result;
          $response['url'] = '';
          return Response::json($response);
        }
         

        
    }
    else
    {
      $response['status'] = 0;
      $response['msg']  = 'Record not found !';      
      $response['data'] = [];
      return Response::json($response);
    }

   }



   /************** Get Notifications ***********************/

   public function getNotifications()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    //print_r($user); die;
    $data = Notifications::where('user_id',$user->id)->get();
    $data = json_decode(json_encode($data), true);
   // print_r($data); die;
    $curDate = date('Y-m-d H:i:s');
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            $seconds = time()-strtotime($value['created_at']);

            //echo $seconds; die;
            //$curDate).getTime()) / 1000;
            if($seconds < 60)
            {
              $time = round($seconds).' Sec Ago';
            }
            else if($seconds >= 60 && $seconds < 3600)
            {
              $time = round($seconds/60);
              $time = $time.' Mins Ago';
            }
            else if($seconds >= 3600 && $seconds < 86400)
            {
              $time  = round($seconds/3600);
              $time = $time.' Hours Ago';
            }
            else if($seconds >= 86400 && $seconds < 172800)
            {
              $time = 'Today';
            }
            else if($seconds >= 172800 && $seconds < 259200)
            {
              $time = 'Yesterday';
            }
            else
            {
              $time = date('d-M-Y',strtotime($value['created_at']));
            }

            $result[$i]['id'] = $value['id'];
            $result[$i]['is_read'] = $value['is_read'];
            $result[$i]['loan_no'] = $value['loan_no'];
            $result[$i]['message'] = $value['message'];
            $result[$i]['time'] = $time;
            $i++;
          }
       
        $response['status'] = 1;
        $response['msg']  = 'Notification List';          
        $response['data'] = $result;
        return Response::json($response);
    }
    else
    {
        $response['status'] = 0;
        $response['msg']  = 'Notification not found !';          
        $response['data'] = $result;
        return Response::json($response);
    }

   }



/************** updateNotification ***********************/

  public function updateNotification()
  {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);   
    $isRead = $getData['is_read']; //1 for unread, 0 for read
    $id = $getData['id'];   
    if($id > 0)
    {
      Notifications::where('id',$id)->update(array('is_read'=>$isRead)); 
       $response['status'] = 1;
       $response['msg']  = 'Record updated successfully';          
       $response['data'] = [];
       return Response::json($response);
      
    }
    else
    {
       $response['status'] = 0;
       $response['msg']  = 'Record not found !';      
       $response['data'] = [];
       return Response::json($response);
    }
    
    
  }

  public function updateProfile()
  {
    $result = array();
    $response = array();    
    $getReqData = Input::all();
    $getData = json_decode($getReqData['Body'],true);  
    $token = $getData['token']; 
    $user = JWTAuth::toUser($token);
    $userId = $user->id;
    $name = isset($getData['name'])?$getData['name']:'';
    $phone = isset($getData['phone'])?$getData['phone']:'';    
    if($userId > 0)
    {
       if(input::hasFile('image'))
          {
             //echo SITE_URLI.'/public/files/proofofincome/'; die;
              $file = Input::file('image');            
              $filename = microtime() . '.' .$file->getClientOriginalName();
              $file->move(SITE_URLI.'/public/files/profileImage/',$filename);
              
          }
          else
          {
            $filename = '';
          }
          if(!empty($filename))
          {
            DB::table('users')->where('id',$userId)->update(array('name'=>$name,'phone'=>$phone,'image'=>$filename));
            $result['image'] = SITE_URL.'/public/files/profileImage/'.$filename;
          }
          else
          {
            DB::table('users')->where('id',$userId)->update(array('name'=>$name,'phone'=>$phone));
            if(!empty($user->image))
            {
              $result['image'] = SITE_URL.'/public/files/profileImage/'.$user->image;
            }
            else
            {
              $result['image'] = '';
            }
            
          }
          

          $result['name'] = $name;
          $result['phone'] = $phone;
          

           $response['status'] = 1;
           $response['msg']  = 'Record updated successfully';          
           $response['data'] = $result;
           return Response::json($response);

    }
    else
    {
       $response['status'] = 0;
       $response['msg']  = 'Record not found !';      
       $response['data'] = [];
       return Response::json($response);
    }

  }

 

	
	/************** Update FCM  ***********************/
	public function updateFcmToken(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];
		$fcm_token = $data['fcm_token'];

		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;

		$updateUser = array(
			'fcm_token' => $fcm_token,
		);
		DB::table('users')->where('id', $user_id)->update($updateUser);

		return response()->json(array('status'=>true,'message'=>'FCM Token updated successfully'));
	}
	
	/**-----------------Firebase Notification ------------**/
	
	function SendNotification( $deviceData, $notification,$type){
		/* print_r($deviceData);
		print_r($notification);
		 die; */
		/* Check device data not empty */ 
		if( empty( $deviceData ) ){
			return true;
		}else{
			$androidDevice = array();
			$iosDevice = array();
			
			 $androidDevice = $deviceData;
			if( count( $androidDevice ) > 0 ){
				  $SERVER_KEY = 'd3apoI51_ig:APA91bERt35X-4PbVWx3GMEWBcH7KND_xWdFNtIQXupmVjmmwYXLqyPYXim1Xonaf9PT3toOqLi8A3EglJdp5dBC94ZLsV8YYHblkdbgx69Tc48STBWwLuda8qvYSy739uT3iNx9APPJ'; 
				$jsonString = $this->sendPushNotificationToGCMSever( $androidDevice, $notification, $SERVER_KEY,$type);
				return true;
				// $jsonObject = json_decode($jsonString);


								
			} 
			
		}
	}
		
	/**-----------------Firebase Notification Send ------------**/	
	function sendPushNotificationToGCMSever($token, $notification, $SERVER_KEY,$type){
		$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
		// send notification when send message  
		$fields = array();
		if($type=='saveMessage') {
			 $fields = array(
				'registration_ids' => array_values($token),
				'data'=>array(
					'siteId'=>$notification['siteId'],
					'issueId'=>$notification['issueId'],
					'message'=>$notification['msg'],
					'title'=>$notification['title'],
					'name'=>$notification['username'],
					'dateTime'=>date('d M | h:i A'),
					'vibrate'=>1,
					'sound'=>1,
					'icon'=>HTTP_PATH.'public/admin/images/notification.png'
				),
				'priority'=>'high',
				'notification'=>array(
				'title'=>$notification['title'],
				'body'=>$notification['msg']
				)
			); 
		}
		
		 $jsonData = json_encode($fields);
		  /* echo 'dffd ';
		 print_r($jsonData); die; */
		$headers = array(
			'Authorization:key=' . $SERVER_KEY,
			'Content-Type:application/json'
		);      
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		$result = curl_exec($ch);
		// echo "<pre>"; print_r($result);die;
		curl_close($ch);
		return $result;
	}

	
}
