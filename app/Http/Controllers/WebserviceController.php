<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\ForgotPassword;
use App\Models\EmailTemplates;
use App\Models\LoanEnquiryIndividual;
use App\Models\LoanEnquiryCompany;
use App\Models\IdType;
use App\Models\EducationLevel;
use App\Models\BusinessType;
use App\Models\KnowAccredit;
use App\Models\LoanType;
use App\Models\LoanPurpose;
use App\Models\ResidentialType;
use App\Models\PropertyOwnership;
use App\Models\EmploymentStatus;
use App\Models\JobTitle;
use App\Models\Specialization;
use App\Models\OfficeBranch;
use App\Models\Bank;
use App\Models\AdminDocuments;
use App\Models\AboutContent;
use App\Models\ReasonForLoan;
use App\Models\Payments;
use App\Models\Notifications;
use App\Models\FaqCategories;
use Illuminate\Http\Request;
use Response;
use Illuminate\Notifications\Notifiable;
use Redirect;
use Validator;
use Hash;
use Auth;
use Mail;
use Input;
use DB;
use Cookie;
use JWTAuth;
use JWTAuthException;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;

require_once SITE_URLI. '/vendor/autoload.php';
use \Mpdf\Mpdf;
// $mpdf = new \Mpdf\Mpdf();

class WebserviceController extends Controller
{
	public function __construct(){
	}

  public function checkDB() {

        try {
              DB::connection()->getPdo();
              $data = "Connected";
          } catch (\Exception $e) {
              die("Could not connect to the database.  Please check your configuration. error:" . $e );
          }
        return response()->json($data, 200);
    }

    public function book() {
        $data = "Data All Book";
        return response()->json($data, 200);
    }

    public function bookAuth(Request $request) {
      // print_r($request->input('token'));
      $getReqData = file_get_contents('php://input');
      $getData = $this->getJson($getReqData); 
      // $credentials1 = array('email'=>$getData['email'],'password'=>$getData['password']);
      //print_r($credentials1);
      // $token = JWTAuth::attempt($credentials1);
      // print_r($user_id);
     

      $data = "Welcome to the API ";
      return response()->json($data, 200);
    }

  public function logintest(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function registertest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    public function getAuthenticatedUserTest()
    {
         // grab credentials from the request
        $credentials = Input::only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return Response::json(['error' => 'invalid_credentials'], 401);
            }

            $user = JWTAuth::toUser($token);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return Response::json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return Response::json(compact('user'));
    }

	/***************** get request from json a *****************/
	public function getJson($jData){
		$data = json_decode($jData,true);		
		if(isset($data) && !empty($data)){
			$_REQUEST = $data;
		} else {
			$data = $_POST;
		}
		return $data;
	}

	/***************** null value filter from array *****************/
	public function filterNullValueFromArray($arr){
		$resultArr = array();
		foreach($arr as $key=>$val){
			if($val===NULL){
				$resultArr[$key] = '';
			} else {
				$resultArr[$key] = $val;
			}
		}
		return $resultArr;
	} 
	//priyanka work start for website login 
    function httpPost($url,$params)
  {

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      curl_close($ch);
       return $output;

  }
  function httpGet($url)
  {
    // echo $url;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      curl_close($ch);
      return $output;
  }
  function savewebsiterUser($user,$formdata)
  {
  	$digits = 4;
  	 $otp =  rand(pow(10, $digits-1), pow(10, $digits)-1);   
     $users = new User;
     $users->name = $user['name'];
     $users->email = $formdata['email'];
     $users->role_id = 1;
     $users->password = bcrypt($formdata['password']); 
     $users->phone= $user['hand_phone'];  
     $users->status = 1;
    // $users->otp=1234; 
  		$users->otp=$otp;
     $users->is_loginfirst = 0;
     $users->is_website_user=1;
     $users->website_user_id=$user['user_id']; 
     $users->website_token=$user['token']; 
     $users->website_password=$formdata['password']; 
     $users->save();
     $insertedId = $users->id;
     return $users;

  }

  public function common_erp_download($getUrl,$token)
   {
    // echo $user->website_token;
            $header = array(
            'Authorization: bearer '. $token->website_token);
         
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
         
            curl_setopt($ch, CURLOPT_URL, $getUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
	    curl_close($ch);

	// echo "<pre>".print_r($output,1)."</pre>";exit;

            return $output;
   }
  //priyanka work end
  public function login1()
  {
    $params = array();
    
     $api_url = \Config::get('values.api_url');

      $url=    $api_url .'api/myinfoLogin'; 

 // $url='https://mobile.sivren.org/api/index.php/api/myinfoLogin'; 
      // echo $url;  
      $result12=$this->httpGet($url);
      $result12 =str_replace('\/','/', $result12);
      $resultjson =json_decode($result12,true);
      // var_dump($result12);
      if(!empty($result12))
      {
          $response['status'] = 1;
          $response['msg']  = 'Successfully login to My info';          
          $response['data'] = $resultjson['myinfo_login_url'];
      }
      else{
         $response['status'] = 0;
          $response['msg']  = 'Something went wrong';          
          $response['data'] = '';
          $response['url'] = $url;
      }
   
    
   
    return Response::json($response);

  }
	/************** Login user ***********************/
	
	public function login()
	{
	  $result = array();
	  $response = array();
	  $getReqData = file_get_contents('php://input');
	  $getData = $this->getJson($getReqData);	
	 $settingdata =  DB::table('setting')->where('id',1)->first();
	  if(!empty($getData))
	  {
	  	try
	  	{
         if(filter_var($getData['email'],FILTER_VALIDATE_EMAIL))
         {
           $credentials = array('email'=>$getData['email'],'password'=>$getData['password']);
         }
         else
         {
            $credentials = array('phone'=>$getData['email'],'password'=>$getData['password']);
         }
   
	  	  if(!$token = JWTAuth::attempt($credentials))
         {
             $data = DB::table('users')->where('email',$getData['email'])->first();
             $data1 = DB::table('users')->where('phone',$getData['email'])->first();
         
             if(!empty($data) || !empty($data1))
             {
                $response['status'] = 0;
                $response['msg']  = 'Invalid password !';
                $response['data'] = [];
                return Response::json($response);
             }
             else
             {
              if (filter_var($getData['email'], FILTER_VALIDATE_EMAIL)) 
              {
                  $params = array(
                     "email" => $getData['email'],
                     "password" => $getData['password']
                  );
$api_url = \Config::get('values.api_url');

                  $result12=$this->httpPost($api_url. '/api/login',$params);
//env('API_URL')."api/login",$params);
                  $manage = json_decode($result12, true);
                  if(isset($manage['success']) && $manage['success'] == 1)
                  { 
                        $responssedata=$this->savewebsiterUser($manage,$params);
                        $credentials1 = array('email'=>$getData['email'],'password'=>$getData['password']);
                        //print_r($credentials1);
                        $token = JWTAuth::attempt($credentials1);
                       // print_r( $token);die;
                        if($responssedata->id)
                        {
                          
				$user_id = $responssedata->id;
				$uid = $manage['uid'];
                          $name = $responssedata->name;
                          $phone = $responssedata->phone;
                          $email = $responssedata->email;
                          $isLoginFirst = $responssedata->is_loginfirst;
                          $profileImage = '';
                         // $result11 = $this->sendOtp('+65'.$phone,$user_id);
			  $result['user_id'] = $user_id;
			  $result['uid'] = $uid;
                          $result['email'] = $email;
                          $result['name'] = $name;
                          $result['token'] = $token;
                          $result['phone'] = $phone;
                          $result['is_loginfirst'] = $isLoginFirst;
                          $result['registration_type'] = $responssedata->role_id;
                          //$result['otp'] = $responssedata->otp;
                          $result['profile_image'] = $profileImage;
                          $response['status'] = 1;
                          $response['msg'] = 'Token created succussfully';
                          $response['data'] = $result;
                          $response['is_website_user'] = $responssedata->is_website_user;
                          $response['website_user_id'] = $responssedata->website_user_id;
                          if(isset($settingdata))
                          {
                            $response['data']['is_individual_loan_show'] = $settingdata->is_individual_loan_show;  
                            $response['data']['is_company_loan_show'] = $settingdata->is_company_loan_show;                        
                          }
                         
                          return Response::json($response);
                        }
                    
                      
                  }
                  else{
                    $response['status'] = 0;
                    $response['msg']  = 'Invalid email or password !';
                    $response['data'] = [];
                    return Response::json($response);
                  }
              }
               else 
               {
               $response['status'] = 0;
                $response['msg']  = 'Please proceed to sign up an account. Thank you!';
                $response['data'] = [];
                return Response::json($response);
              }
            }
         }
	  	   else
	  	   {
                        //    $user = auth()->user();
	  	   	  $user = JWTAuth::toUser($token); // authorization JWT Token
            // if($user->is_website_user == 1)
            // {
                 $params = array(
                 "email" => $user->email,
                 "password" => $getData['password']
                );
$api_url = \Config::get('values.api_url');
                $result12=$this->httpPost($api_url.'api/login',$params);
                $manage = json_decode($result12, true);

		$uid = "";
                if(isset($manage['success']) && $manage['success'] == 1)
                { 
                  //update token
                      DB::table('users')
                      ->where('id', $user->id)
                      ->update(array('website_token' =>$manage['token'],'is_website_user'=> 1));
                  $uid = $manage['uid'];
                }
                // }else{
                // 	$response['status'] = 0;
	               //  $response['msg']  = $manage['message'];
	               //  $response['data'] = [];
	               //  return Response::json($response);
                // }
            // }
    			  /*$user_id = $user->id;
    			  $username = $user->username;
    			  $name = $user->name;
    			  $phone = $user->phone;
    			  $email = $user->email;
            $isLoginFirst = $user->is_loginfirst;
            if(!empty($user->image))
            {
              $profileImage = SITE_URL.'/public/files/profileImage/'.$user->image;
            }
            else
            {
              $profileImage = '';
            }
            
            
              $result11 = $this->sendOtp('+65'.$phone,$user_id);
            
    			  

    			  if($result11)
    			  {
              
    			  	$result['user_id'] = $user_id;
    				  $result['email'] = $email;
    				  $result['name'] = $name;
    				  $result['token'] = $token;
               $result['phone'] = $phone;
               $result['is_loginfirst'] = $isLoginFirst;
                $result['registration_type'] = $user->role_id;
               //$result['otp'] = $result11;
               $result['profile_image'] = $profileImage;
    				  $response['status'] = 1;
    				  $response['msg'] = 'Token created succussfully';
    				  $response['data'] = $result;
              if(isset($settingdata))
              {
                $response['data']['is_individual_loan_show'] = $settingdata->is_individual_loan_show;  
                $response['data']['is_company_loan_show'] = $settingdata->is_company_loan_show;                        
              }
    				  return Response::json($response);
    			  }	
    			  else
    			  {
    			  	  $result['user_id'] = $user_id;
    				  $result['email'] = $email;
    				  $result['name'] = $name;
    				  $result['token'] = $token;
    				   $result['phone'] = $phone;
               $result['is_loginfirst'] = $isLoginFirst;
               //$result['otp'] = '';
               $result['profile_image'] = $profileImage;
    				  $response['status'] = 1;
    				  $response['msg'] = 'Otp not sent !';
    				  $response['data'] = $result;
              if(isset($settingdata))
              {
                $response['data']['is_individual_loan_show'] = $settingdata->is_individual_loan_show;  
                $response['data']['is_company_loan_show'] = $settingdata->is_company_loan_show;                        
              }

    				  return Response::json($response);
			  }		 */ 

		$user_id = $user->id;
		$result['uid'] = $uid;
    			    $username = $user->username;
    			    $name = $user->name;
    			    $phone = $user->phone;
    			    $email = $user->email;
                    $isLoginFirst = $user->is_loginfirst;

                    $profileImage = '';
		    if (!empty($user->image)) {
			$baseUrl = str_replace("/index.php", "", url('/'));
                        $profileImage = $baseUrl . '/public/files/profileImage/' . $user->image;
                    }

                    $result['user_id'] = $user_id;
                    $result['email'] = $email;
                    $result['name'] = $name;
                    $result['token'] = $token;
                    $result['phone'] = $phone;
                    $result['is_loginfirst'] = $isLoginFirst;
                    $result['profile_image'] = $profileImage;

                    if (isset($settingdata)) {
                        $response['data']['is_individual_loan_show'] = $settingdata->is_individual_loan_show;
                        $response['data']['is_company_loan_show'] = $settingdata->is_company_loan_show;
                    }

                    $isLocalHP = $this->checkIsLocalHP($phone);

                    if ($isLocalHP) {
                        $newPhoneNo = $phone;
                        if (strpos($newPhoneNo, '+65') !== false) {
                            $newPhoneNo = str_replace("+65", "", $phone);
                        }
                        $result11 = $this->sendOtp('+65'.$newPhoneNo, $user_id);
    			        if ($result11) {
                            $result['registration_type'] = $user->role_id;

        				    $response['status'] = 1;
        				    $response['msg'] = 'Token created succussfully';

                        } else {
    				        $response['status'] = 0;
        				    $response['msg'] = 'OTP not sent!';
                        }
                    } else {
                        $response['status'] = 0;
                        $response['msg'] = 'We are not able to verify your mobile number';
                    }

                    $response['data'] = $result;
                    return Response::json($response);



	  	   }
	  	}
	  	catch(JWTAuthException $e)
	  	{
	  		$response['status'] = 0;
		  	$response['msg']  = 'Failed to create token !';
		  	$response['data'] = [];
		  	return Response::json($response);
	  	}
	  }
	  else
	  {	  	
	  	$response['status'] = 0;
	  	$response['msg']  = 'Please enter email or password !';
	  	$response['data'] = [];
	  	return Response::json($response);
	  }
	}

public function checkIsLocalHP($phone)
    {
        $localHP = false;

        $newPhoneNo = trim($phone);
        $phoneNoLength = strlen($newPhoneNo);

        if (strpos($newPhoneNo, '+65') !== false && $phoneNoLength == 11) {
            $localHP = true;
        } else if ($phoneNoLength == 8) {
            $localHP = true;
        }

        return $localHP;
    }

	/**************  Send Otp  ******************************/

    public function sendOtp($phone = null,$userId = null)
    {
    	$digits = 4;
      	$otp = rand(pow(10, $digits-1), pow(10, $digits)-1);      
        if ( $userId == 1352 && userId==2922 && userId==2933) {
             $otp = 1234;
        }
 
	//file_get_contents("http://trans.masssms.tk/api.php?username=srcjpr&password=test@123&sender=SRCJPR&sendto=".$phone."&message=".$otp);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://api.transmitsms.com/send-sms.json?format=json&to=".$phone."&message=".$otp."");
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,false);  //Post Fields
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $headers = array();
      $headers[] = 'X-abc-AUTH: 123456789';
      $headers[] = 'Accept:  */*';
      $headers[] = 'Cache-Control: no-cache';
      $headers[] = 'Content-Type: application/json';
      $headers[] = 'Authorization: Basic ZTJmYjRlMGY0ZDQ1MTkwYjM2MTc1ODJiMjJlZGYzZWE6anNkaGtqc2hmanNoZmpzZmhqZHM=';

      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $data = curl_exec ($ch);
      //$data = array('OTP sent');
      //print_r($data); die;
      curl_close ($ch);

      DB::table('users')
              ->where('id', $userId)
              ->update(array('otp' =>$otp));
      if ($data) {
        return $otp;
      } else {
        return $data;
      }
      die;
    }

    /************** Resend Otp ***********************/
       public function resendOtp()
       {
         $getReqData = file_get_contents('php://input');
         $getData = $this->getJson($getReqData);
         $token = $getData['token'];
         $user = JWTAuth::toUser($token);
         $user_id = $user->id;
         if(!empty($user_id > 0))
         {
            $data = DB::table('users')->where('id',$user_id)->first();
            if($data->id > 0)
            {
              $phone = $data->phone;
              $user_id = $data->id;
              $result11 = $this->sendOtp('+65'.$phone,$user_id);
              if($result11)
              {
                $response['status'] = 1;
                $response['msg']  = 'OTP send successfully !';
                $response['data'] = [];
                return Response::json($response);
              }
              else
              {
                $response['status'] = 0;
                $response['msg']  = 'OTP not send !';
                $response['data'] = [];
                return Response::json($response);
              }
            }
            else
            {
               $response['status'] = 0;
               $response['msg']  = 'User Data Not Found !';
               $response['data'] = [];
               return Response::json($response);
            }
            
         }
       }
       

    /************** Verify Otp ***********************/

    public function verifyOtp()
    {
       $getReqData = file_get_contents('php://input');
       $getData = $this->getJson($getReqData);
       $token = $getData['token'];
       $user = JWTAuth::toUser($token);       
       //$user = auth()->userByToken($token);
      // print_r($user);exit;
           
       $user_id = $user->id;
       if(!empty($user_id > 0))
       {
          $otp = $getData['otp'];
          $firebaseToken = isset($getData['firebase_token'])?$getData['firebase_token']:'';
          $data = DB::table('users')->where('id',$user_id)->first();
          if($otp == $data->otp)
          {
          	//Remove duplicate FCM token from Database
          	if(!empty($firebaseToken))
          	{
          		 $firebasetokenUsers = DB::table('users')->where('firebase_token',$firebaseToken)->where('id','!=',$user_id)->get()->toArray();
          		 if(!empty($firebasetokenUsers))
          		 {
          		 	foreach ($firebasetokenUsers as $key => $value) {
          		 			DB::table('users')->where('id',$value->id)->update(array('firebase_token'=>''));
          		 	}
          		 
          		 }
          	}
             DB::table('users')->where('id',$user_id)->update(array('firebase_token'=>$firebaseToken));
             $response['status'] = 1;
             $response['msg']  = 'Otp Verified Successfully';
             $response['data'] = [];
             return Response::json($response);
          }
          else
          {
             $response['status'] = 0;
             $response['msg']  = 'Otp Not Verified !';
             $response['data'] = [];
             return Response::json($response);
          }

       }
       else
       {
          $response['status'] = 0;
          $response['msg']  = 'Request parament not found !';
          $response['data'] = [];
          return Response::json($response);
       }
    }


   /************** Get Form Filed ***********************/
   	public function getFormFields()
   	{   	
   	  $getReqData = file_get_contents('php://input');
   	  $getData = $this->getJson($getReqData);
      $isToken = isset($getData['is_token'])?$getData['is_token']:0;
      if($isToken == 1)
      {
        $token = $getData['token'];
        $user = JWTAuth::toUser($token);
        $user_id = $user->id;
      }
   	  $result = array();
   	  $response = array();
      $data = array();
      $modal = '';
      $temp = array();
      if(!empty($getData))
      {
      	  $registrationType = $getData['registration_type'];
      	  $dataForms = DB::table('field')->where(['form_type_id'=>$registrationType,'status'=>1])->orderBy('position', 'ASC')->get();

      	  if(!empty($dataForms))
      	  {
      	  	 $i = 0;
      	  	foreach ($dataForms as $key => $value)
      	  	{
              $data = [];
              $modal = '';
               if($value->slug == 'id_type')
              {
                $data = $this->getIdType();
              }
              if($value->slug == 'education_level')
              {
                $data = $this->getEducationLevel();
              }
              if($value->slug == 'business_type')
              {
               $data = $this->getBusinessType();
              }     
              if($value->slug == 'know_accredit')
              {
                $data = $this->getKnowAccredit();                
              } 
              if($value->slug == 'loan_type')
              {
               $data = $this->getLoanType();
              
              } 
              if($value->slug == 'loan_purpose')
              {
                $data = $this->getLoanPurpose();
                
              } 

              if($value->slug == 'office_branch')
              {
                $data = $this->getOfficeBranch();
              }
              if($value->slug == 'bank_name')
              {
                $data = $this->getbankname();
                
              } 
              if($value->slug == 'residential_type')
              {
                $data = $this->getResidentialType();
                
              } 
              if($value->slug == 'property_ownership')
              {
                $data = $this->getPropertyOwnership();
                
              } 
              if($value->slug == 'employment_status')
              {
                $data = $this->getEmploymentStatus();
                
              } 
              if($value->slug == 'job_title')
              {
                $data = $this->getJobTitle();
              
              } 
              /*if($value->slug == 'specialization')
              {
                $data = $this->getSpecilization();
                
              }*/
              if($value->slug == 'nationality')
              {
                $nationality[] =array('id'=>3,'name'=>'Chinese');
                $nationality[] =array('id'=>4,'name'=>'Indian');
                $nationality[] =array('id'=>2,'name'=>'Malaysian');
                $nationality[] =array('id'=>1,'name'=>'Singaporean');
                $data = $nationality;
              }
              if($value->slug == 'gender' || $value->slug == 'sex')
              {
                $gender[] = array('id'=>"Male","name"=>'Male');
                $gender[] = array('id'=>"Female","name"=>'Female');
                $data = $gender;
                
              }
              if($value->slug == 'marital_status')
              {
                $maritalStatus[] = array('id'=>3,"name"=>'Divorced');
                $maritalStatus[] = array('id'=>2,"name"=>'Married');
                $maritalStatus[] = array('id'=>1,"name"=>'Single');
                $maritalStatus[] = array('id'=>4,"name"=>'Widowed');
                $data = $maritalStatus;
                
              }
              if($value->slug == 'salution')
              {
                $salution[] = array('id'=>1,"name"=>'Mr.');
                $salution[] = array('id'=>2,"name"=>'Mrs.');
                $salution[] = array('id'=>3,"name"=>'Ms.');
                $salution[] = array('id'=>4,"name"=>'Miss.');
                $data = $salution;
                
              }
              if($value->slug == 'salary_paid')
              {
                $salaryPaid[] = array('id'=>10000,"name"=>10000);
                $salaryPaid[] = array('id'=>20000,"name"=>20000);
                $salaryPaid[] = array('id'=>30000,"name"=>30000);
                $salaryPaid[] = array('id'=>40000,"name"=>40000);
                $data = $salaryPaid;
                
              }
              if($value->slug == 'next_of_kin_relationship')
              {
                $reletionShip[] = array('id'=>3,"name"=>'Colleague or Friend');
                $reletionShip[] = array('id'=>1,"name"=>'Immediate Supervisor');
                $reletionShip[] = array('id'=>2,"name"=>'Next of Kins');
                $reletionShip[] = array('id'=>4,"name"=>'Other');                
                $data = $reletionShip;
                
              }
              if($value->slug == 'payment_frequency')
              {
                $paymentFrequency[] = array('id'=>365,"name"=>'Daily');
                $paymentFrequency[] = array('id'=>52,"name"=>'Weekly');
                $paymentFrequency[] = array('id'=>12,"name"=>'Monthly');
                $paymentFrequency[] = array('id'=>1,"name"=>'Yearly');   
                $data = $paymentFrequency;
                
              }
              if($value->slug == 'length_of_current_employment_years')
              {
                $empYear[] = array('id'=>1,"name"=>'1 Year');
                $empYear[] = array('id'=>2,"name"=>'2 Years');
                $empYear[] = array('id'=>3,"name"=>'3 Years');
                $empYear[] = array('id'=>4,"name"=>'4 Years');
                $empYear[] = array('id'=>5,"name"=>'5 Years');
                $empYear[] = array('id'=>6,"name"=>'6 Years');
                $empYear[] = array('id'=>7,"name"=>'7 Years');
                $empYear[] = array('id'=>8,"name"=>'8 years');
                $empYear[] = array('id'=>9,"name"=>'9 Years');
                $empYear[] = array('id'=>10,"name"=>'10 Years');
                $data = $empYear;
                
              }
              if($value->slug == 'length_of_current_employment_months')
              {
                $empMonth[] = array('id'=>1,"name"=>'1 Month');
                $empMonth[] = array('id'=>2,"name"=>'2 Months');
                $empMonth[] = array('id'=>3,"name"=>'3 Months');
                $empMonth[] = array('id'=>4,"name"=>'4 Months');
                $empMonth[] = array('id'=>5,"name"=>'5 Months');
                $empMonth[] = array('id'=>6,"name"=>'6 Months');
                $empMonth[] = array('id'=>7,"name"=>'7 Months');
                $empMonth[] = array('id'=>8,"name"=>'8 Months');
                $empMonth[] = array('id'=>9,"name"=>'9 Months');
                $empMonth[] = array('id'=>10,"name"=>'10 Months');
                $empMonth[] = array('id'=>11,"name"=>'11 Months');
                $empMonth[] = array('id'=>12,"name"=>'12 Months');
                $data = $empMonth;
                
              }
              if($value->slug == 'payment_frequency')
              {
                $paymentFrequency[] = array('id'=>1,"name"=>'1 Month');
                $paymentFrequency[] = array('id'=>2,"name"=>'2 Months');
                $paymentFrequency[] = array('id'=>3,"name"=>'3 Months');
                $paymentFrequency[] = array('id'=>4,"name"=>'4 Months');
                $paymentFrequency[] = array('id'=>5,"name"=>'5 Months'); 
                $data = $paymentFrequency;                
              }
              if($value->slug == 'number_of_dependents')
              {
                $dependents[] = array('id'=>0,"name"=>'0');
                $dependents[] = array('id'=>1,"name"=>'1');
                $dependents[] = array('id'=>2,"name"=>'2');
                $dependents[] = array('id'=>3,"name"=>'3');
                $dependents[] = array('id'=>4,"name"=>'4');
                $dependents[] = array('id'=>5,"name"=>'5'); 
                $data = $dependents;                
              }
              if($value->slug == 'pay_date_of_salary')
              {
                for ($x = 31; $x >= 1; $x--) {
                  $pay_date_of_salary[]=array('id'=>$x,"name"=>$x);
              }
                
                $data = $pay_date_of_salary;                
              }
              if($isToken == 1)
              {
                if($value->slug == 'know_accredit')
                {
                  $loansdata = LoanEnquiryIndividual::where(['user_id'=>$user->id,'is_completed'=>1])->pluck('id')->toArray();
                  $knowaccreditdata = KnowAccredit::where('name','Repeat Customer')->pluck('id')->toArray();
                if(isset($loansdata) && !empty($loansdata))
                {
                  if(isset($knowaccreditdata[0]) && !empty($knowaccreditdata[0]))
                  {
                    $modal = $knowaccreditdata[0];
                  }
                }
                  
                  
                }
                if ($value->slug == 'id_number') {
                  $modal = $user->id_number;
                }
                if($value->slug == 'name')
                {
                  $modal = $user->name;               
                }
                if($value->slug == 'email')
                {
                  $modal = $user->email;               
                }
                if($value->slug == 'phone')
                {
                  $modal = $user->phone;               
                }
                if($value->slug == 'office_number')
                {
                  $modal = $user->office_number;               
                }
                if($value->slug == 'loan_amount')
                {
                  $modal = $user->loan_amount; 
                }
               
                 

              }
              else
              {
                $modal = '';

               if ($value->slug == 'id_number')
               {
                       $modal = $getData['uid'];
               }



              }
                if ($value->slug == 'id_number')
                {
                     //$modal = $getData['uid'];
                }
              	if($value->slug == 'payment_frequency')
                {
                  $modal = 12; 
                }
                if($value->slug == 'loan_type')
                {
                  $loandata = LoanType::where('name','Unsecured')->pluck('id')->toArray();
               
                  if(isset($loandata[0]) && !empty($loandata[0]))
                  {
                    $modal = $loandata[0];
                  }
                  
                }
                
                
              if($value->is_required == 1)
              {
                $isRequired = 'true';
              }
              else
              {
                $isRequired = 'false';
              }
              


      	  		$result[$i]['title'] = $value->name;
              $result[$i]['type'] = $value->type;
      	  		$result[$i]['slug'] = $value->slug;
      	  		$result[$i]['is_required'] = $isRequired;
              $result[$i]['value'] = $data;
      	  		$result[$i]['modal'] = $modal;              
      	  		$i++;
      	  	}
            /*if($registrationType == 3)
            {
              $employementLengthMonthArray = $result[40];  
              $officeNumberArray = $result[39];

              array_unshift($result[21], $employementLengthMonthArray); 
              $result[21] = $employementLengthMonthArray;

              array_unshift($result[27], $officeNumberArray); 
              $result[27] = $officeNumberArray;
            }*/
            

            
            //print_r($result); die;            
            

      	  	$response['status'] = 1;
		  	$response['msg']  = 'Form Fields List';
		  	$response['data'] = $result;
        if(!empty($getData['uid']))
        {
           $response['data']= $this->getDataMYINFO($result,$getData['uid']);
           return Response::json($response);
        }
        else{
            return Response::json($response);
        }
      
		  	
      	  }
      	  else
      	  {
      	  	$response['status'] = 0;
		  	$response['msg']  = 'No record found for this registration type !';
		  	$response['data'] = [];
		  	return Response::json($response);
      	  }
      }
      else
      {
      	$response['status'] = 0;
	  	$response['msg']  = 'Please supply registration type !';
	  	$response['data'] = [];
	  	return Response::json($response);
      }     

   	}

    public function moveElement($array, $a, $b)
    {
      $out = array_splice($array, $a, 1);
      array_splice($array, $b, 0, $out);
    }

   	/************** Registration ***********************/

   	public function registration()
   	{
   		//pr('hello'); die;
   		$getReqData = file_get_contents('php://input');
   		$getData = $this->getJson($getReqData);
      $isToken = isset($getData['is_token'])?$getData['is_token']:0;
      if($isToken == 1)
      {
        $token = $getData['token'];
        $user = JWTAuth::toUser($token);
        $user_id = $user->id;
      }
   		$result = array();
   		$response = array();
      //print_r($getData); die;
   		if(!empty($getData))
   		{

   			$email = $getData['email'];
   			$phone = $getData['phone'];
        if($isToken == 1)
        {
           $data = DB::table('users')->where(['email'=>$email,'phone'=>$phone])->where('id','!=',$user->id)->get()->toArray();
        } 
        else
        {
          $data = DB::table('users')->where(['email'=>$email,'phone'=>$phone])->get()->toArray();  
        }   
   			//if(empty($data))
       // { 
   			  $registrationType = $getData['registration_type'];

   			  if($registrationType == 1)
   			  {
            if(empty($data))
            {
       			  $users = new User;
       			  $users->salution = isset($getData['salution'])?$getData['salution']:'';
       			  $users->name = isset($getData['name'])?$getData['name']:'';
       			  $users->role_id = 1;
              $users->id_type = isset($getData['id_type'])?$getData['id_type']:0;
              $users->nationality = isset($getData['nationality'])?$getData['nationality']:0;
              $users->id_number = isset($getData['id_number'])?$getData['id_number']:0;
              if(!empty($getData['dob']))
              {
              	  $getData['dob'] = date("Y-m-d", strtotime($getData['dob']));
                  $users->dob = $getData['dob'];
              }
              $users->gender = isset($getData['gender'])?$getData['gender']:0;
              $users->marital_status = isset($getData['marital_status'])?$getData['marital_status']:0;
              $users->residential_address_one = isset($getData['residential_address_one'])?$getData['residential_address_one']:'';
              $users->residential_address_two = isset($getData['residential_address_two'])?$getData['residential_address_two']:'';
              $users->phone = isset($getData['phone'])?$getData['phone']:'';
              $users->email = isset($getData['email'])?$getData['email']:'';
              if(!empty($getData['password']))
              {
              	$users->password = bcrypt($getData['password']);	
              }
              else
              {
              	$users->password = '';
              }              
              $users->home_number = isset($getData['home_number'])?$getData['home_number']:'';
              $users->office_number = isset($getData['office_number'])?$getData['office_number']:'';
              $users->education_level = isset($getData['education_level'])?$getData['education_level']:0;
              $users->monthly_income = isset($getData['monthly_income'])?$getData['monthly_income']:0.00;
              $users->loan_amount = isset($getData['loan_amount'])?$getData['loan_amount']:0.0;
              $users->years_of_stay = isset($getData['years_of_stay'])?$getData['years_of_stay']:0;
              $users->status = 1;
              $users->is_loginfirst = 0;
             if($users->save())
             {
             	
             	$result['email'] = $getData['email'];
             	$result['phone'] = $getData['phone'];
             	$result['name'] = $getData['name'];
             	$response['status'] = 1;
    			  	$response['msg']  = 'Registration completed successfully';
    			  	$response['data'] = $result;
    			  	return Response::json($response);
             }
             else
             {
             	  $response['status'] = 0;
      			  	$response['msg']  = 'Registration not completed successfully !';
      			  	$response['data'] = [];
      			  	return Response::json($response);
             }
            }
            else
            {  
              if(isset($data[0])){
                 $alradyuser= $data[0];
              }
             
               unset($getData['is_token']); 
               unset($getData['token']);
               unset($getData['registration_type']);
               if(!empty($getData['password']))
               {
                  $getData['password'] = bcrypt($getData['password']);  
               }
               else
               {
                  $getData['password']= '';
               }
               $getData['status'] = 1;
               $getData['role_id'] = 1;
               $getData['is_loginfirst'] = 0;
               DB::table('users')
                            ->where('id',$alradyuser->id)
                            ->update($getData);

              $response['status'] = 1;
              $response['msg']  = 'Registration completed successfully !';
              $response['data'] = $getData;
              return Response::json($response);

            }           

   			  }
   			else if($registrationType == 2)
   			{
          if($isToken == 0)
          {
            $users = new User;
   			    $users->name = isset($getData['name'])?$getData['name']:'';
   			    $users->uen_no = isset($getData['uen_no'])?$getData['uen_no']:'';
   			    $users->role_id = 2;
              $users->business_type = isset($getData['business_type'])?$getData['business_type']:0;
              $users->paid_up_capital = isset($getData['paid_up_capital'])?$getData['paid_up_capital']:0.00;
              if(!empty($getData['date_of_registration']))
              {
              	$getData['date_of_registration'] = date("Y-m-d", strtotime($getData['date_of_registration']));
                $users->date_of_registration = $getData['date_of_registration'];
              }      
              $users->registered_address_one = isset($getData['registered_address_one'])?$getData['registered_address_one']:'';
              $users->registered_address_two = isset($getData['registered_address_two'])?$getData['registered_address_two']:'';
              $users->business_address_one = isset($getData['business_address_one'])?$getData['business_address_one']:'';
              $users->business_address_two = isset($getData['business_address_two'])?$getData['business_address_two']:'';
              $users->office_number = isset($getData['office_number'])?$getData['office_number']:'';
              $users->contact_person = isset($getData['contact_person'])?$getData['contact_person']:'';
              $users->email = isset($getData['email'])?$getData['email']:'';
              $users->phone = isset($getData['phone'])?$getData['phone']:'';
              if(!empty($getData['password']))
              {
                $users->password = bcrypt($getData['password']);
              }
              else
              {
                $users->password = '';
              }
              $users->status = 1;
              
             if($users->save())
             {
             	
             	$result['email'] = $getData['email'];
             	$result['phone'] = $getData['phone'];
             	$result['name'] = $getData['name'];
             	$response['status'] = 1;
			  	    $response['msg']  = 'Registration completed successfully';
			  	    $response['data'] = $result;
			  	    return Response::json($response);
             }
             else
             {
              	$response['status'] = 0;
			  	      $response['msg']  = 'Registration not completed successfully !';
			  	      $response['data'] = [];
			  	      return Response::json($response);
             } 
   			 }
         else
         {
            unset($getData['is_token']); 
           unset($getData['token']);
           unset($getData['registration_type']);
           if(!empty($getData['password']))
           {
              $getData['password'] = bcrypt($getData['password']);  
           }
           else
           {
              $getData['password']= '';
           }
           $getData['status'] = 1;
           $getData['role_id'] = 2;
           $getData['is_loginfirst'] = 0;
           DB::table('users')
                        ->where('id',$user->id)
                        ->update($getData);
           $response['status'] = 1;
           $response['msg']  = 'Registration completed successfully !';
           $response['data'] = $getData;
           return Response::json($response);
         }     
        }
   		// }
   		 /* else
   		  {
   		  	$response['status'] = 0;
		  	$response['msg']  = 'This email or phone no. already exits ! Please try another email or phone no.';
		  	$response['data'] = [];
		  	return Response::json($response);
   		  }*/
   		}
   		else
   		{
   			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];
		  	return Response::json($response);
   		}
   	}

   	/**************  Apply Loan Individual ***********************/

	public function applyLoanIndividual()
   	{
	      	//echo SITE_URL; die;
   		$response = array();
   		$result = array();
   		$getReqData = Input::all();
   		$getData = json_decode($getReqData['Body'],true);
   		
   		//print_r($getData);die;
      		$isToken = isset($getData['is_token'])?$getData['is_token']:0;
      		if ($isToken == 1) {
      			$completeuser = json_decode($getData['token'],true);
      		
        		$token = $completeuser['token'];
           
        		$user = JWTAuth::toUser($token);

        		$user_id = $user->id;
      			//  print_r($user);die;
      		}

   		if (!empty($getData)) {
        	    if ($isToken == 0) {
        			// check for existing user
        			$oldUser = DB::table('users')
        				->where('status',1)
        				->where('email', $getData['email'])
        				->orWhere('phone', $getData['phone'])
					->first();
				// echo "<pre>". print_r($oldUser, 1) ."</pre>";exit;
				if (!$oldUser) {
					$post = new User;
					$post->name = isset($getData['name'])?$getData['name']:'';
					$post->email = $getData['email'];
					$post->role_id = 1;
					$post->phone = isset($getData['phone'])?$getData['phone']:0;   
					$post->loan_amount = isset($getData['loan_amount_requested'])?$getData['loan_amount_requested']:0;  
					$post->office_number = isset($getData['office_number'])?$getData['office_number']:0; 
					$post->marital_status = isset($getData['marital_status'])?$getData['marital_status']:0;
					$post->residential_address_one = isset($getData['residential_address_one'])?$getData['residential_address_one']:0; 
					$post->residential_address_two = isset($getData['residential_address_two'])?$getData['residential_address_two']:0; 
					$post->education_level =  isset($getData['education_level'])?$getData['education_level']:0;  
					$post->monthly_income = isset($getData['monthly_income'])?$getData['monthly_income']:0;         
					$post->is_loginfirst = 1;
					// $post->save();
					$post = User::updateOrCreate(['email'=>$getData['email']],json_decode(json_encode($post),true));
					$userId = $post->id;
          				$email = $getData['email'];          
          				$password = rand(18973824, 989721389);
         				// DB::table('users')
           				//       ->where('id', $post->id)
             				//     ->update(array('password' => bcrypt($password)));
					$emailTemplate = EmailTemplates::find(1);
					$messageBody = $emailTemplate->message;
					$messageBody = str_replace('[name]', $post->name, $messageBody);
					$messageBody = str_replace('[password]', $password, $messageBody);
					$data = ['message' => $messageBody];

          		/************* PHP Mailer *************************/
          		$mail = new PHPMailer\PHPMailer(true);                              
          		try {
		            $mail->SMTPDebug = 0;                                 
		            $mail->isSMTP();                                      
		            $mail->Host = SMTP_HOST;  
		            $mail->SMTPAuth = true;                               
		            $mail->Username = SMTP_USERNAME;                 
		            $mail->Password = SMTP_PASSWORD;                           
		            $mail->SMTPSecure = SMTP_SECURE;                            
		            $mail->Port = PORT;                                    
		            $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
		            $mail->addAddress($post->email);
		            $mail->isHTML(true);
		            $mail->Subject = $emailTemplate->subject;
		            $mail->Body    = $messageBody;
		            //$mail->send();
	          	} catch (Exception $e) {
	               	// echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
		        }

          		// Second Email Start
			$emailTemplate1 = EmailTemplates::find(8);
			$messageBody1 = $emailTemplate1->message;
			$messageBody1 = str_replace('[name]', $post->name, $messageBody1);
			// $messageBody = str_replace('[name]', $password, $messageBody);
			$data1 = ['message' => $messageBody1];
			/************* PHP Mailer *************************/
			$mail1 = new PHPMailer\PHPMailer(true);                              
          		try {
		            $mail1->SMTPDebug = 0;                                 
		            $mail1->isSMTP();                                      
		            $mail1->Host = SMTP_HOST;  
		            $mail1->SMTPAuth = true;                               
		            $mail1->Username = SMTP_USERNAME;                 
		            $mail1->Password = SMTP_PASSWORD;                           
		            $mail1->SMTPSecure = SMTP_SECURE;                            
		            $mail1->Port = PORT;                                    
		            $mail1->setFrom($emailTemplate1->send_from,$emailTemplate1->send_from);
		            $mail1->addAddress($emailTemplate1->send_from);
		            $mail1->isHTML(true);
		            $mail1->Subject = $emailTemplate1->subject;
		            $mail1->Body    = $messageBody1;
		           	// $mail1->send();
	          	} catch (Exception $e) {
		           // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	          	}

          		// third email start.
          		$emailTemplate2 = EmailTemplates::find(9);
          		$messageBody2 = $emailTemplate2->message;
          		$messageBody2 = str_replace('[name]', $post->name, $messageBody2);
          		//$messageBody = str_replace('[name]', $password, $messageBody);
          		$data2 = ['message' => $messageBody2];
          		/************* PHP Mailer *************************/
          		$mail2 = new PHPMailer\PHPMailer(true);                              
          		try {
		            $mail2->SMTPDebug = 0;                                 
		            $mail2->isSMTP();                                      
		            $mail2->Host = SMTP_HOST;  
		            $mail2->SMTPAuth = true;                               
		            $mail2->Username = SMTP_USERNAME;                 
		            $mail2->Password = SMTP_PASSWORD;                           
		            $mail2->SMTPSecure = SMTP_SECURE;                            
		            $mail2->Port = PORT;                                    
		            $mail2->setFrom($emailTemplate2->send_from,$emailTemplate2->send_from);
		            $mail2->addAddress($post->email);
		            $mail2->isHTML(true);
		            $mail2->Subject = $emailTemplate2->subject;
		            $mail2->Body    = $messageBody2;
            		// $mail2->send();
          		} catch (Exception $e) {
               		// echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
          		}
		    } else {
			$userId = $oldUser->id;
		    }
		}
        
 		$appyLoan = new LoanEnquiryIndividual();
          	if ($isToken == 0) {
            		$appyLoan->user_id = $userId;
          	} else {
            		$appyLoan->user_id = $user_id;
          	}

			$appyLoan->uid = isset($_REQUEST['uid']) ? $_REQUEST['uid'] : $getData['id_number'];
			$appyLoan->name = isset($getData['name'])?$getData['name']:'';
			$appyLoan->email = isset($getData['email'])?$getData['email']:'';
			$appyLoan->phone = isset($getData['phone'])?$getData['phone']:'';
			$appyLoan->know_accredit = isset($getData['know_accredit'])?$getData['know_accredit']:0;
			$appyLoan->loan_type = isset($getData['loan_type'])?$getData['loan_type']:0;
			$appyLoan->loan_amount_requested = isset($getData['loan_amount_requested'])?$getData['loan_amount_requested']:0.00;

          	$appyLoan->tenure = isset($getData['tenure'])?$getData['tenure']:0;

			$appyLoan->loan_purpose = isset($getData['loan_purpose'])?$getData['loan_purpose']:0;
			$appyLoan->reason_for_loan = isset($getData['reason_for_loan'])?$getData['reason_for_loan']:'';
			$appyLoan->marital_status = isset($getData['marital_status'])?$getData['marital_status']:0;
			$appyLoan->residential_address_one = isset($getData['residential_address_one'])?$getData['residential_address_one']:'';
			$appyLoan->residential_address_two = isset($getData['residential_address_two'])?$getData['residential_address_two']:'';
			$appyLoan->residential_type = isset($getData['residential_type'])?$getData['residential_type']:0;
			$appyLoan->property_ownership = isset($getData['property_ownership'])?$getData['property_ownership']:0;

 			if (!empty($getData['length_of_stay'])) {
 			  	$appyLoan->length_of_stay = $getData['length_of_stay'];	
			} else {
            			$appyLoan->length_of_stay = 0; 
          		}

			$appyLoan->education_level = isset($getData['education_level'])?$getData['education_level']:0;
			$appyLoan->employment_status = isset($getData['employment_status'])?$getData['employment_status']:0;
			$appyLoan->company_name = isset($getData['company_name'])?$getData['company_name']:'';
			$appyLoan->job_title = isset($getData['job_title'])?$getData['job_title']:0;
			$appyLoan->specialization = isset($getData['specialization'])?$getData['specialization']:'';

 			if (!empty($getData['start_date_of_employment'])) {
            			$getData['start_date_of_employment'] = date("Y-m-d", strtotime($getData['start_date_of_employment']));
 			  	$appyLoan->start_date_of_employment = $getData['start_date_of_employment'];	
 			}

 			if (isset($getData['length_of_current_employment_years'])) {
 				if ($getData['length_of_current_employment_years'] != '') {
 					$appyLoan->length_of_current_employment_years=$getData['length_of_current_employment_years'];
 				} else {
 					$appyLoan->length_of_current_employment_years=0;
 				}
 			} else {
 				$appyLoan->length_of_current_employment_years=0;
 			}

 			/*  $appyLoan->length_of_current_employment_years = isset($getData['length_of_current_employment_years'])?$getData['length_of_current_employment_years']:0;*/	

			if (isset($getData['length_of_current_employment_months'])) {
 				if ($getData['length_of_current_employment_months'] != '') {
 					$appyLoan->length_of_current_employment_months=$getData['length_of_current_employment_months'];
 				} else {
 					$appyLoan->length_of_current_employment_months=0;
 				}
 			} else {
 				$appyLoan->length_of_current_employment_months=0;
 			}

          	/*  $appyLoan->length_of_current_employment_months = isset($getData['length_of_current_employment_months'])?$getData['length_of_current_employment_months']:0;  */

 			$appyLoan->salary_paid = isset($getData['salary_paid'])?$getData['salary_paid']:0;
 			$appyLoan->annual_income = isset($getData['annual_income'])?$getData['annual_income']:0.00;
 			$appyLoan->monthly_income = isset($getData['monthly_income'])?$getData['monthly_income']:0.00;

 			if (!empty($getData['pay_date_of_salary'])) {
 				//$getData['pay_date_of_salary'] = date("Y-m-d", strtotime($getData['pay_date_of_salary']));
            			$appyLoan->pay_date_of_salary = $getData['pay_date_of_salary'];
 			}

 			$appyLoan->office_address_one = isset($getData['office_address_one'])?$getData['office_address_one']:'';
 			$appyLoan->office_address_two = isset($getData['office_address_two'])?$getData['office_address_two']:'';
 			$appyLoan->office_number = isset($getData['office_number'])?$getData['office_number']:'';

          	if (isset($getData['outstanding_loan'])) {
            	    if ($getData['outstanding_loan'] == 'no') {
              		$appyLoan->outstanding_loan = 'no';
              		$appyLoan->name_of_license_moneylender = '';
              		$appyLoan->loan_amount = 0.00;
              		$appyLoan->payment_frequency = 0;
            	    }
          	} else {
	            $appyLoan->outstanding_loan = isset($getData['outstanding_loan'])?$getData['outstanding_loan']:'';          
	            $appyLoan->name_of_license_moneylender = isset($getData['name_of_license_moneylender'])?$getData['name_of_license_moneylender']:'';
	            $appyLoan->loan_amount = isset($getData['loan_amount'])?$getData['loan_amount']:0.00;
	            $appyLoan->payment_frequency = isset($getData['payment_frequency'])?$getData['payment_frequency']:0;
          	}


 			$appyLoan->bank_name = isset($getData['bank_name'])?$getData['bank_name']:'';
 			$appyLoan->account_number = isset($getData['account_number'])?$getData['account_number']:'';
 			$appyLoan->number_of_dependents = isset($getData['number_of_dependents'])?$getData['number_of_dependents']:'';
 			$appyLoan->next_of_kin_name = isset($getData['next_of_kin_name'])?$getData['next_of_kin_name']:0;
                        $appyLoan->next_of_kin_contact_number = isset($getData['next_of_kin_contact_number'])?$getData['next_of_kin_contact_number']:0;
 			$appyLoan->next_of_kin_relationship = isset($getData['next_of_kin_relationship'])?$getData['next_of_kin_relationship']:0;
 			$appyLoan->approved_status = 1;
          		$appyLoan->active_status = 1;
          		$appyLoan->office_branch = isset($getData['office_branch'])?$getData['office_branch']:4;
 			/*$appyLoan->created_at = date('Y-m-d');
 			$appyLoan->modified_at = date('Y-m-d');*/

 			$proofOfIncome = isset($getReqData['proof_of_income'])?$getReqData['proof_of_income']:'';
 			if (input::hasFile('proof_of_income')) {
 				//echo SITE_URLI.'/public/files/proofofincome/'; die;
 				$file = Input::file('proof_of_income');
    				$filename = microtime() . '.' .$file->getClientOriginalName();
    				$file->move(SITE_URLI.'/public/files/proofofincome/',$filename);
    				$appyLoan->proof_of_income = $filename;
 			}

 			$otherDocument = isset($getReqData['other_document'])?$getReqData['other_document']:'';
 			if (input::hasFile('other_document')) {
 				$file = Input::file('other_document');
    				$filename = microtime() . '.' .$file->getClientOriginalName();
    				$file->move(SITE_URLI.'/public/files/otherdocument/',$filename);
    				$appyLoan->other_document = $filename;
 			}

 		if ($appyLoan->save()) {
        		if ($appyLoan->know_accredit == '10') {
            			$appyLoan->know_accredit = 'Internet';
        		} else if ($appyLoan->know_accredit == '11') {
            			$appyLoan->know_accredit = 'Walk-in';
		        } else if ($appyLoan->know_accredit == '12') {
		            $appyLoan->know_accredit = 'Referral';
      			} else if ($appyLoan->know_accredit == '13') {
            			$appyLoan->know_accredit = 'Repeat Customer';
        		}

      			if ($appyLoan->loan_type == '8') {
              			$appyLoan->loan_type = 'Unsecured';
       			} else if ($appyLoan->loan_type == '9') {
              			$appyLoan->loan_type = 'Secured';
      			}

      			if ($appyLoan->loan_purpose == '10') {
         			$appyLoan->loan_purpose = 'Business';
			} else if ($appyLoan->loan_purpose == '11') {
				$appyLoan->loan_purpose = 'Personal-Education';
			} else if ($appyLoan->loan_purpose == '12') {
				$appyLoan->loan_purpose = 'Personal-Medical';
			} else if ($appyLoan->loan_purpose == '13') {
				$appyLoan->loan_purpose = 'Personal-Others';
			} else if ($appyLoan->loan_purpose == '14') {
				$appyLoan->loan_purpose = 'Personal-Pay Day';
			}

			if ($appyLoan->marital_status == '1') {
				$appyLoan->marital_status = 'Single';
			} else if ($appyLoan->marital_status == '2') {
				$appyLoan->marital_status = 'Married';
			} else if ($appyLoan->marital_status == '3') {
				$appyLoan->marital_status = 'Divorced';
			} else if ($appyLoan->marital_status == '4') {
				$appyLoan->marital_status = 'Widowed';
			}

			$property_ownership_val = null;
			if ($appyLoan->property_ownership == 10) {
				$property_ownership_val = 'OWNHDB1R';
			} else if ($appyLoan->property_ownership == 11) {
				$property_ownership_val = 'OWNHDB2R';
			}  else if ($appyLoan->property_ownership == 12) {
				$property_ownership_val = 'OWNHDB3R';
			}  else if ($appyLoan->property_ownership == 13) {
				$property_ownership_val = 'OWNHDB4R';
			} else if ($appyLoan->property_ownership == 14) {
				$property_ownership_val = 'OWNHDB5R';
			}  else if ($appyLoan->property_ownership == 15) {
				$property_ownership_val = 'OWNACEC';
			} else if ($appyLoan->property_ownership == 16) {
				$property_ownership_val = 'OWNLNDED';
			} else if ($appyLoan->property_ownership == 17) {
				$property_ownership_val = 'HDBPRS';
			} else if ($appyLoan->property_ownership == 18) {
				$property_ownership_val = 'NOTOWNPP';
			}

			// get eresidentialType from table
			$residentialType = "";
			if ($appyLoan->residential_type) {
				$residentialTypeData = ResidentialType::where('id',$appyLoan->residential_type)->first();
				if ($residentialTypeData) {
					$residentialType = $residentialTypeData->name;
				}
			}

			// get education level from table
			$educationLevel = "";
			if ($appyLoan->education_level) {
				$educationLevelData = EducationLevel::where('id',$appyLoan->education_level)->first();
				if ($educationLevelData) {
					$educationLevel = $educationLevelData->name;
				}
			}

             		$params = array(
					'name' => $appyLoan->name,
					'reason_for_loan' => $appyLoan->reason_for_loan,
					'hand_phone' => $appyLoan->phone,
					'email' => $appyLoan->email,
					'loantype' => $appyLoan->loan_type,
					'purpose' => $appyLoan->loan_purpose,
					'marital_status' => $appyLoan->marital_status,
					'residential_type' => $residentialType , // $appyLoan->residential_type,
					'property_ownership' => $property_ownership_val,
					'company' => $appyLoan->company_name,
					'position_code' => $appyLoan->job_title,
					'specialisation_code' => $appyLoan->specialization,
					'start_date_employment' => $appyLoan->start_date_of_employment,
					'work_location_years_month' => $appyLoan->length_of_current_employment_years . ' years '.$appyLoan->length_of_current_employment_months,
					'stay_address_years_month' => 'NA',
					'salary_paid' => $appyLoan->salary_paid,
					'annual_income' => $appyLoan->annual_income,
					'month_salary' => $appyLoan->monthly_income,
					'pay_day' => $appyLoan->pay_date_of_salary,
					'work_phone' => $appyLoan->office_number ? $appyLoan->office_number : '',
					'bank' => $appyLoan->bank_name ? $appyLoan->bank_name : '',
					'bank_account_number' => $appyLoan->account_number ? $appyLoan->account_number : '',
					'dependants' => $appyLoan->number_of_dependents,
					'nok_name' =>  $appyLoan->next_of_kin_name ? $appyLoan->next_of_kin_name : '',
					'nok_contact' => $appyLoan->next_of_kin_contact_number ? $appyLoan->next_of_kin_contact_number : '',
					'nok_relationship' => $appyLoan->next_of_kin_relationship ? $appyLoan->next_of_kin_relationship : '',
					'unit' => $appyLoan->residential_address_one ? $appyLoan->residential_address_one : '',
					'postal' => $appyLoan->residential_address_two ? $appyLoan->residential_address_two : '' ,
					'know_accredit' => $appyLoan->know_accredit ? $appyLoan->know_accredit : '',
					'branch_id' => $appyLoan->office_branch,
					'amount' => $appyLoan->loan_amount_requested,
					'fi_loandate' => '0000-00-00',
					'fi_moneylender' => $appyLoan->name_of_license_moneylender ? $appyLoan->name_of_license_moneylender : '',
					'fi_loanamount' => $appyLoan->loan_amount,
					'fi_outloan' => $appyLoan->outstanding_loan,
					'form_type' => 'Others',
					'uid' => $appyLoan->uid ? $appyLoan->uid : (isset($getData['id_number']) ? $getData['id_number'] : ""),
					'sex' => $appyLoan->sex ? : (isset($getData['sex']) ? $getData['sex'] : ""),
					'nationality' => isset($getData['nationality']) ? $getData['nationality'] : '',
					'dob' => isset($getData['dob']) ? $getData['dob'] : '',
					'educationLevel' => $educationLevel ? : '',
					'passport_number' => isset($getData['passport_number']) ? $getData['passport_number'] : '',
					'passport_expiry_date' => $appyLoan->passport_expiry_date ? : (isset($getData['passport_expiry_date']) ? $getData['passport_expiry_date'] : ""),
					'country_of_birth' => $appyLoan->country_of_birth ? : (isset($getData['country_of_birth']) ? $getData['country_of_birth'] : ""),
					'hdb_type' => isset($getData['hdb_type']) ? $getData['hdb_type'] : '',
					'race' => isset($getData['race']) ? $getData['race'] : '',
					'housing_type' => isset($getData['housing_type']) ? $getData['housing_type'] : '',
					'address1' => isset($getData['residential_address_one']) ? $getData['residential_address_one'] : ''
 				);

			$api_url = \Config::get('values.api_url');
			$result = $this->httpPost($api_url . "api/loan_application/add", $params);
			//print_r($result);exit;
			$manage = json_decode($result, true);

			// Second Email Start.
          		$emailTemplate1 = EmailTemplates::find(8);
		        $messageBody1 = $emailTemplate1->message;
		        $messageBody1 = str_replace('[name]', $appyLoan->name, $messageBody1);
	          	// $messageBody = str_replace('[name]', $password, $messageBody);
	          	$data1 = ['message' => $messageBody1];

			/************* PHP Mailer *************************/
	          	$mail1 = new PHPMailer\PHPMailer(true);                              
	          	try {
		            $mail1->SMTPDebug = 0;                                 
		            $mail1->isSMTP();                                      
		            $mail1->Host = SMTP_HOST;  
		            $mail1->SMTPAuth = true;                               
		            $mail1->Username = SMTP_USERNAME;                 
		            $mail1->Password = SMTP_PASSWORD;                           
		            $mail1->SMTPSecure = SMTP_SECURE;                            
		            $mail1->Port = PORT;                                    
		            $mail1->setFrom($emailTemplate1->send_from,$emailTemplate1->send_from);
		            $mail1->addAddress($emailTemplate1->send_from);
		            $mail1->isHTML(true);
		            $mail1->Subject = $emailTemplate1->subject;
		            $mail1->Body    = $messageBody1;
		            $mail1->send();
          		} catch (Exception $e) {
	                	// echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	          	}

	          	// third email start.
	          	$emailTemplate2 = EmailTemplates::find(9);
	          	$messageBody2 = $emailTemplate2->message;
	          	$messageBody2 = str_replace('[name]', $appyLoan->name, $messageBody2);
	          	// $messageBody = str_replace('[name]', $password, $messageBody);
	          	$data2 = ['message' => $messageBody2];
	          	/************* PHP Mailer *************************/
	          	$mail2 = new PHPMailer\PHPMailer(true);                              
	          	try {
		            $mail2->SMTPDebug = 0;                                 
		            $mail2->isSMTP();                                      
		            $mail2->Host = SMTP_HOST;  
		            $mail2->SMTPAuth = true;                               
		            $mail2->Username = SMTP_USERNAME;                 
		            $mail2->Password = SMTP_PASSWORD;                           
		            $mail2->SMTPSecure = SMTP_SECURE;                            
		            $mail2->Port = PORT;                                    
		            $mail2->setFrom($emailTemplate2->send_from,$emailTemplate2->send_from);
		            $mail2->addAddress($appyLoan->email);
		            $mail2->isHTML(true);
		            $mail2->Subject = $emailTemplate2->subject;
		            $mail2->Body    = $messageBody2;
		            $mail2->send();
	          	} catch (Exception $e) {
	               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	          	}

             		$loanNo = rand(18973824, 989721389);
             		$loanNo = '#ind'.$loanNo.$appyLoan->id;
             		DB::table('loan_enquiry_individual')
                  			->where('id', $appyLoan->id)
                    			->update(array('loan_no'=>$loanNo));

 			$response['status'] = 1;
  			$response['msg']  = 'Loan enquiry submitted successfully';
  			$response['data'] = [];

  			return Response::json($response);

 			} else {
 				$response['status'] = 0;
  			  	$response['msg']  = 'Loan enquiry not submitted !';
  			  	$response['data'] = [];

  			  	return Response::json($response);
 			}
   		} else {
   			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];

		  	return Response::json($response);
		}
   	}

   	/**************  Apply Loan Company ***********************/

   	public function applyLoanCompany()
   	{
   		$result = array();
   		$response = array();
   		$getReqData = Input::all();
      $getData = json_decode($getReqData['Body'],true);
      $isToken = isset($getData['is_token'])?$getData['is_token']:0;
      if($isToken == 1)
      {

        $token = $getData['token'];

        $user = JWTAuth::toUser($token);
        $user_id = $user->id;
      }  		
   		if(!empty($getData))
   		{
         $appyLoan = new LoanEnquiryCompany();
         if($isToken == 0)
         {
            $post = new User;
            $post->name = $getData['name'];
            $post->email = $getData['email'];
            $post->role_id = 2;          
            $post->loan_amount = $getData['loan_amount_requested'];
            $post->office_number = $getData['office_number'];          
            $post->is_loginfirst = 1;
            $post->save();

            $email = $getData['email'];          
            $password = rand(18973824, 989721389);
             DB::table('users')
                  ->where('id', $post->id)
                    ->update(array('password' => bcrypt($password)));
            $emailTemplate = EmailTemplates::find(1);
            $messageBody = $emailTemplate->message;
            $messageBody = str_replace('[name]', $post->name, $messageBody);
            $messageBody = str_replace('[password]', $password, $messageBody);
            $data = ['message' => $messageBody];
        
            /*********** PHP Mailer *************************/
            $mail = new PHPMailer\PHPMailer(true);                              
            try {
             
              $mail->SMTPDebug = 0;                                 
              $mail->isSMTP();                                      
              $mail->Host = SMTP_HOST;  
              $mail->SMTPAuth = true;                               
              $mail->Username = SMTP_USERNAME;                 
              $mail->Password = SMTP_PASSWORD;                           
              $mail->SMTPSecure = SMTP_SECURE;                            
              $mail->Port = PORT;                                    
              $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
              $mail->addAddress($post->email);
              $mail->isHTML(true);
              $mail->Subject = $emailTemplate->subject;
              $mail->Body    = $messageBody;
              $mail->send();
            } catch (Exception $e) {
                 // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }  
            //Second Email Start.
          $emailTemplate1 = EmailTemplates::find(8);
          $messageBody1 = $emailTemplate1->message;
          $messageBody1 = str_replace('[name]', $post->name, $messageBody1);
          //$messageBody = str_replace('[name]', $password, $messageBody);
          $data1 = ['message' => $messageBody1];
          /************* PHP Mailer *************************/
          $mail1 = new PHPMailer\PHPMailer(true);                              
          try {
           
            $mail1->SMTPDebug = 0;                                 
            $mail1->isSMTP();                                      
            $mail1->Host = SMTP_HOST;  
            $mail1->SMTPAuth = true;                               
            $mail1->Username = SMTP_USERNAME;                 
            $mail1->Password = SMTP_PASSWORD;                           
            $mail1->SMTPSecure = SMTP_SECURE;                            
            $mail1->Port = PORT;                                    
            $mail1->setFrom($emailTemplate1->send_from,$emailTemplate1->send_from);
            $mail1->addAddress($emailTemplate1->send_from);
            $mail1->isHTML(true);
            $mail1->Subject = $emailTemplate1->subject;
            $mail1->Body    = $messageBody1;
            $mail1->send();
          } catch (Exception $e) {
               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
          }

          //third email start.
          $emailTemplate2 = EmailTemplates::find(9);
          $messageBody2 = $emailTemplate2->message;
          $messageBody2 = str_replace('[name]', $post->name, $messageBody2);
          //$messageBody = str_replace('[name]', $password, $messageBody);
          $data2 = ['message' => $messageBody2];
          /************* PHP Mailer *************************/
          $mail2 = new PHPMailer\PHPMailer(true);                              
          try {
           
            $mail2->SMTPDebug = 0;                                 
            $mail2->isSMTP();                                      
            $mail2->Host = SMTP_HOST;  
            $mail2->SMTPAuth = true;                               
            $mail2->Username = SMTP_USERNAME;                 
            $mail2->Password = SMTP_PASSWORD;                           
            $mail2->SMTPSecure = SMTP_SECURE;                            
            $mail2->Port = PORT;                                    
            $mail2->setFrom($emailTemplate2->send_from,$emailTemplate2->send_from);
            $mail2->addAddress($post->email);
            $mail2->isHTML(true);
            $mail2->Subject = $emailTemplate2->subject;
            $mail2->Body    = $messageBody2;
            $mail2->send();
          } catch (Exception $e) {
               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
          }         

         }
         if($isToken == 0)
         {
           $appyLoan->user_id = $post->id;          
         }
         else
         {
            $appyLoan->user_id = $user_id;
         }
   			
   			
   			$appyLoan->name = isset($getData['name'])?$getData['name']:'';
   			$appyLoan->office_number = isset($getData['office_number'])?$getData['office_number']:'';
   			$appyLoan->email = isset($getData['email'])?$getData['email']:'';
   			$appyLoan->know_accredit = isset($getData['know_accredit'])?$getData['know_accredit']:0;
   			$appyLoan->loan_type = isset($getData['loan_type'])?$getData['loan_type']:0;

   			$appyLoan->loan_amount_requested = isset($getData['loan_amount_requested'])?$getData['loan_amount_requested']:0.00;

        $appyLoan->tenure = isset($getData['tenure'])?$getData['tenure']:0;
        
   			$appyLoan->loan_purpose = isset($getData['loan_purpose'])?$getData['loan_purpose']:0;
   			$appyLoan->reason_for_loan = isset($getData['reason_for_loan'])?$getData['reason_for_loan']:'';
   			if(!empty($getData['financial_year_end_as_at']))
   			{
   				$appyLoan->financial_year_end_as_at = $getData['financial_year_end_as_at'];
   			}
    
   			$appyLoan->annual_gross_profit = isset($getData['annual_gross_profit'])?$getData['annual_gross_profit']:0.00;
   			$appyLoan->annual_net_profit = isset($getData['annual_net_profit'])?$getData['annual_net_profit']:0.00;
   			$appyLoan->total_asset = isset($getData['total_asset'])?$getData['total_asset']:0.0;
   			$appyLoan->total_current_asset = isset($getData['total_current_asset'])?$getData['total_current_asset']:0.00;
   			
   			$appyLoan->total_liabilities = isset($getData['total_liabilities'])?$getData['total_liabilities']:0.00;
   			$appyLoan->total_current_liabilities = isset($getData['total_current_liabilities'])?$getData['total_current_liabilities']:0.00;
   			$appyLoan->company_office_address_one = isset($getData['company_office_address_one'])?$getData['company_office_address_one']:'';
   			$appyLoan->company_office_address_two = isset($getData['company_office_address_two'])?$getData['company_office_address_two']:0;
   			$appyLoan->residential_type = isset($getData['residential_type'])?$getData['residential_type']:0;
   			
   			
   			$appyLoan->property_ownership = isset($getData['property_ownership'])?$getData['property_ownership']:0;

   			$appyLoan->annual_profit = isset($getData['annual_profit'])?$getData['annual_profit']:0.00;
   			$appyLoan->outstanding_loan = isset($getData['outstanding_loan'])?$getData['outstanding_loan']:'';
   			
   			$appyLoan->name_of_license_moneylender = isset($getData['name_of_license_moneylender'])?$getData['name_of_license_moneylender']:'';
        if(isset($getData['loan_amount']) && !empty($getData['loan_amount']))
        {
          $appyLoan->loan_amount = $getData['loan_amount'];
        }
        else{
          $appyLoan->loan_amount =0.00;
        }
   		//	$appyLoan->loan_amount = isset($getData['loan_amount'])?$getData['loan_amount']:0.00;
   			$appyLoan->payment_frequency = isset($getData['payment_frequency'])?$getData['payment_frequency']:'';
   			$appyLoan->bank_name = isset($getData['bank_name'])?$getData['bank_name']:'';
   			$appyLoan->account_number = isset($getData['account_number'])?$getData['account_number']:'';
          $appyLoan->approved_status = 1;
        $appyLoan->active_status = 1;
        $appyLoan->office_branch = isset($getData['office_branch'])?$getData['office_branch']:'';

        $proofOfIncome = isset($getReqData['proof_of_income'])?$getReqData['proof_of_income']:'';
        if(input::hasFile('proof_of_income'))
        {
           //echo SITE_URLI.'/public/files/proofofincome/'; die;
          $file = Input::file('proof_of_income');
          
            $filename = microtime() . '.' .$file->getClientOriginalName();
            $file->move(SITE_URLI.'/public/files/proofofincome/',$filename);
            $appyLoan->proof_of_income = $filename;
        }
        $otherDocument = isset($getReqData['other_document'])?$getReqData['other_document']:'';
        if(input::hasFile('other_document'))
        {
          $file = Input::file('other_document');          
            $filename = microtime() . '.' .$file->getClientOriginalName();
            $file->move(SITE_URLI.'/public/files/otherdocument/',$filename);
            $appyLoan->other_document = $filename;
        }

   			if($appyLoan->save())
   			{
   				//Second Email Start.
	          $emailTemplate1 = EmailTemplates::find(8);
	          $messageBody1 = $emailTemplate1->message;
	          $messageBody1 = str_replace('[name]', $appyLoan->name, $messageBody1);
	          //$messageBody = str_replace('[name]', $password, $messageBody);
	          $data1 = ['message' => $messageBody1];
	          /************* PHP Mailer *************************/
	          $mail1 = new PHPMailer\PHPMailer(true);                              
	          try {
	           
	            $mail1->SMTPDebug = 0;                                 
	            $mail1->isSMTP();                                      
	            $mail1->Host = SMTP_HOST;  
	            $mail1->SMTPAuth = true;                               
	            $mail1->Username = SMTP_USERNAME;                 
	            $mail1->Password = SMTP_PASSWORD;                           
	            $mail1->SMTPSecure = SMTP_SECURE;                            
	            $mail1->Port = PORT;                                    
	            $mail1->setFrom($emailTemplate1->send_from,$emailTemplate1->send_from);
	            $mail1->addAddress($emailTemplate1->send_from);
	            $mail1->isHTML(true);
	            $mail1->Subject = $emailTemplate1->subject;
	            $mail1->Body    = $messageBody1;
	            $mail1->send();
	          } catch (Exception $e) {
	               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	          }

	          //third email start.
	          $emailTemplate2 = EmailTemplates::find(9);
	          $messageBody2 = $emailTemplate2->message;
	          $messageBody2 = str_replace('[name]', $appyLoan->name, $messageBody2);
	          //$messageBody = str_replace('[name]', $password, $messageBody);
	          $data2 = ['message' => $messageBody2];
	          /************* PHP Mailer *************************/
	          $mail2 = new PHPMailer\PHPMailer(true);                              
	          try {
	           
	            $mail2->SMTPDebug = 0;                                 
	            $mail2->isSMTP();                                      
	            $mail2->Host = SMTP_HOST;  
	            $mail2->SMTPAuth = true;                               
	            $mail2->Username = SMTP_USERNAME;                 
	            $mail2->Password = SMTP_PASSWORD;                           
	            $mail2->SMTPSecure = SMTP_SECURE;                            
	            $mail2->Port = PORT;                                    
	            $mail2->setFrom($emailTemplate2->send_from,$emailTemplate2->send_from);
	            $mail2->addAddress($appyLoan->email);
	            $mail2->isHTML(true);
	            $mail2->Subject = $emailTemplate2->subject;
	            $mail2->Body    = $messageBody2;
	            $mail2->send();
	          } catch (Exception $e) {
	               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	          }
          $loanNo = rand(18973824, 989721389);
             $loanNo = '#comp'.$loanNo.$appyLoan->id;
             DB::table('loan_enquiry_company')
                  ->where('id', $appyLoan->id)
                    ->update(array('loan_no'=>$loanNo));

   				$response['status'] = 1;
			  	$response['msg']  = 'Loan enquiry submitted successfully';
			  	$response['data'] = [];
			  	return Response::json($response);
   			}
   			else
   			{
   				$response['status'] = 0;
			  	$response['msg']  = 'Loan enquiry not submitted !';
			  	$response['data'] = [];
			  	return Response::json($response);
   			}
   			

   		}
   		else
   		{
   			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];
		  	return Response::json($response);
   		}
   	}



   	/**************  Get Myinfo data ***********************/

   	public function getMyInfo()
   	{
   		$getReqData = file_get_contents('php://input');
   		$getData = $this->getJson($getReqData);
   		if(!empty($getData))
   		{
   		  
   		}
   		else
   		{
			$response['status'] = 0;
		  	$response['msg']  = 'Request data not found !';
		  	$response['data'] = [];
		  	return Response::json($response);
   		}
   	}


	
	

	 /************** Forgot Password ***********************/
	public function forgotpassword(){
		$response = array();
		$result = array();
		$getReqData = file_get_contents('php://input');
		// get decode json data
		$getData = $this->getJson($getReqData);

		$email = $getData['email'];

		$userData = User::where('email',$email)->first();
		if(!empty($userData)) {
			if($userData->is_website_user && !empty($userData->website_password))
			{
				$password = $userData->website_password;
			} else {
                              $password = rand(18973824, 989721389);
		        }
			DB::table('users')
							->where('id', $userData->id)
							->update(array('password' => bcrypt($password)));

			// send and email 
			$emailTemplate = EmailTemplates::find(1);
			$messageBody = $emailTemplate->message;
			$messageBody = str_replace('[name]', $userData->username, $messageBody);
			$messageBody = str_replace('[password]', $password, $messageBody);

		   // return view('emails.message')->with('data',$messageBody);   // check email before sending
			$data = ['message' => $messageBody];
			// Mail::send('emails.message', ['data' => $messageBody], function($m) use ($emailTemplate,$userData)
			// {
			// 	$m->to($userData->email,$userData->username)
			// 	 ->subject($emailTemplate->subject)
			// 	 ->from($emailTemplate->send_from);
			//    });
			/************* PHP Mailer *************************/
			$mail = new PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
			try {
				//Server settings
				$mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = SMTP_HOST;  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = SMTP_USERNAME;                 // SMTP username
				$mail->Password = SMTP_PASSWORD;                           // SMTP password
				$mail->SMTPSecure = SMTP_SECURE;                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = PORT;                                    // TCP port to connect to

				//Recipients
				$mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
				$mail->addAddress($userData->email);     // Add a recipient
				//Content
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $emailTemplate->subject;
				$mail->Body    = $messageBody;

				$mail->send();
			} catch (Exception $e) {
				   // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
			}
			 $response['status'] = 1;
		  	$response['msg']  = 'An email sent you with new password on your registered email. Please check.';
		  	$response['data'] = [];
		  	return $response; die();			
		}
		else
		{
			 $response['status'] = 0;
		  	$response['msg']  = 'We cannot find an account for contains this email. Please enter your correct email.';
		  	$response['data'] = [];
		  	return Response::json($response); 
		}

	}


	

	/************** Change Password 1***********************/
	public function changePassword(Request $request){
		$response = array();
		$result = array();
		$getReqData = file_get_contents('php://input');
		// get decode json data
		$getData = $this->getJson($getReqData);		
		$token = $getData['token'];
		$old_password = $getData['opassword'];
		$new_password = $getData['password'];

		$user = JWTAuth::toUser($token); // authorization JWT Token

		$user_id = $user->id;
		// check old password
		if (Hash::check($old_password, $user->password))
		{ 
			if (Hash::check($new_password, $user->password))
			{ 
				$response['status'] = 0;
		  		$response['msg']  = 'Old password and new password cannot same.';
		  		$response['data'] = [];
				return Response::json($response);
			}
			else
			{
				$password = Hash::make($new_password);
				// update password in DB
				User::where('id', $user_id)->update(array('password' => $password));

				$response['status'] = 1;
		  	    $response['msg']  = 'Password updated successfully.';
			  	$response['data'] = [];
			  	return Response::json($response);

			}
		 }
		 else
		 {
		 	$response['status'] = 0;
		  	$response['msg']  = 'Old password not correct.';
		  	$response['data'] = [];
			return Response::json($response);
		}

	}
	
	

	  /************** Edit Profile ***********************/
	public function editProfile(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];
		$name = $data['name'];
		$username = $data['username'];
		$email = $data['email'];
		$phone = $data['phone'];
		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;
		
		// check username unique
		$userNameUnique = DB::table('users')->where('username',$username)->where('id','!=',$user_id)->first();
		if(!empty($userNameUnique)){
			 return Response::json(array('status'=>false,'message'=>"Username not unique."));
		}
		// check email unique
		$userEmailUnique = DB::table('users')->where('email',$email)->where('id','!=',$user_id)->first();
		if(!empty($userEmailUnique)){
			 return Response::json(array('status'=>true,'message'=>"Email not unique."));
		}

		// update edit basic info
		$userObj = User::find($user_id);
		$userObj->name= trim($name);
		$userObj->email= trim($email);
		$userObj->username= trim($username);
		$userObj->phone= trim($phone);
		$userObj->save();
		return Response::json(array('status'=>true,'message'=>"Profile details updated successfully."));
	}


 


	 /************** Get Id Type ***********************/

	 public function getIdType()
	 {
	 	$result = array();
	 	$data = IdType::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);	 
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
          	$result[$i]['id'] = $value['id'];
            $result[$i]['name'] = $value['name'];
          	$i++;
          }          
		      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	 /************** Get Education Level ***********************/

	 public function getEducationLevel()
	 {
	 	$result = array();

	 	$data = EducationLevel::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
          
		  return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }

    /************** Get Reason For Loan ***********************/

   public function getReasonForLoan()
   {
    $result = array();

    $data = ReasonForLoan::where('status',1)->orderBy('name', 'ASC')->get();
    $data = json_decode(json_encode($data), true);
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
            $result[$i]['name'] = $value['name'];
            $i++;
          }
          
      return $result;
    }
    else
    {
      $result = '';
      return $result;
    }

   }

	 /************** Get Business Type ***********************/

	 public function getBusinessType()
	 {
	 	$result = array();
	 
	 	$data = BusinessType::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
       
       return $result;
	 	}
	 	else
	 	{
	 	    $return = '';
        return $result;
	 	}

	 }

	  /************** Get Know Accredit ***********************/

	 public function getKnowAccredit()
	 {
	 	$result = array();
	 
	 	$data = KnowAccredit::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
       return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }

	  /************** Get Loan Type ***********************/

	 public function getLoanType()
	 {
	 	$result = array();
	 
	 	$data = LoanType::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
         return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	  /************** Get Loan Type ***********************/

	 public function getLoanPurpose()
	 {
	 	$result = array();
	 
	 	$data = LoanPurpose::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
         
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	  /************** Get Residential ***********************/

	 public function getResidentialType()
	 {
	 	$result = array();
	 
	 	$data = ResidentialType::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
         
      return $result;
	 	}
	 	else
	 	{
	 	   $result = '';
      return $result;
	 	}

	 }

	  /************** Get Property Ownership ***********************/

	 public function getPropertyOwnership()
	 {
	 	$result = array();
	 	
	 	$data = PropertyOwnership::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
        
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	 /************** Get Employment Status ***********************/

	 public function getEmploymentStatus()
	 {
	 	$result = array();
	 
	 	$data = EmploymentStatus::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
          
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }

	 /************** Get job Title List ***********************/

	 public function getJobTitle()
	 {
	 	$result = array();
	 
	 	$data = JobTitle::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
        
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }


	  /************** Get Specilization ***********************/

	 public function getSpecilization()
	 {
	 	$result = array();
	 
	 	$data = Specialization::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
        
      return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }



	 /************** Get Office Branch ***********************/

	 public function getOfficeBranch()
	 {
	 	$result = array();
	 
	 	$data = OfficeBranch::where('status',1)->orderBy('name', 'ASC')->get();
	 	$data = json_decode(json_encode($data), true);	 
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
          	$result[$i]['name'] = $value['name'];
          	$i++;
          }
          return $result;
	 	}
	 	else
	 	{
	 		$result = '';
      return $result;
	 	}

	 }
   public function getbankname()
   {
    $result = array();
   
    $data = Bank::where('status',1)->orderBy('name', 'ASC')->get();
    $data = json_decode(json_encode($data), true);   
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['id'] = $value['id'];
            $result[$i]['name'] = $value['name'];
            $i++;
          }
          return $result;
    }
    else
    {
      $result = '';
      return $result;
    }

   }
	  /************** Get Admin Document ***********************/

	 public function getAdminDocument()
	 {

    $settingdata =  DB::table('setting')->where('id',1)->first();
	 	$result = array();
	 	$response = array();
    $text = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      Why do we use it?

    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
    ';
	 	/*$getReqData = file_get_contents('php://input');		
		$getData = $this->getJson($getReqData);		
		$token = $getData['token'];
		$user = JWTAuth::toUser($token);*/


	 	$data = AdminDocuments::orderByRaw('priority = 0, priority')->where('status',1)->get();
	 	$data = json_decode(json_encode($data), true);	 
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
          	$result[$i]['title'] = $value['title'];
          	$result[$i]['url'] = SITE_URL. '/public/files/admindocuments/'.$value['document'];
            $result[$i]['priority'] = $value['priority'];
            $result[$i]['text'] = $text;
          	$i++;
          }
          $response['status'] = 1;
		  $response['msg']  = 'Document List';
		  $response['data'] = $result;
      if(isset($settingdata) && !empty($settingdata))
      {
         $response['loan_types']['is_individual_loan_show'] = $settingdata->is_individual_loan_show;  
        $response['loan_types']['is_company_loan_show'] = $settingdata->is_company_loan_show; 
      }
  
		  return Response::json($response);
	 	}
	 	else
	 	{
	 		$response['status'] = 0;
		  	$response['msg']  = 'Record not found !';
		  	$response['data'] = [];
			return Response::json($response);
	 	}

	 }


	 /************** Get About Us ***********************/

	 public function getAboutContent()
	 {
	 	$result = array();
	 	$response = array();

	 /*	$getReqData = file_get_contents('php://input');		
		$getData = $this->getJson($getReqData);		
		$token = $getData['token'];
		$user = JWTAuth::toUser($token);*/


	 	$data = DB::table('about_content')->get();
	 	$data = json_decode(json_encode($data), true);	 	
	 	if(!empty($data))
	 	{
	 	  $i = 0;
          foreach ($data as $key => $value)
          {
          	$result[$i]['content'] = $value['content'];          	
          	$i++;
          }
          $response['status'] = 1;
		  $response['msg']  = 'About Content';
		  $response['data'] = $result;
		  return Response::json($response);
	 	}
	 	else
	 	{
	 		$response['status'] = 0;
		  	$response['msg']  = 'Record not found !';
		  	$response['data'] = [];
			return Response::json($response);
	 	}

	 }


   /************** Get FAQ ***********************/

   public function getFaq()
   {
    $result = array();
    $response = array();
    $faq = array();
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);

    $data = DB::table('Faq')->where('status',1)->get();
    $dataFaqCategory = FaqCategories::with('faqlist')->where('status',1)->get();
    
    $dataFaqCategory = json_decode(json_encode($dataFaqCategory),true);  
    //print_r($dataFaqCategory[0]['faqlist'][0]['name']); die;  
    if(!empty($dataFaqCategory))
    {
      $i = 0;
      foreach ($dataFaqCategory as $key => $value)
      {
        if(!empty($value['faqlist']))
        {
          $j = 0;
          foreach ($value['faqlist'] as $key11 => $value11)
          {
             $faq[$j]['question'] = $value11['question'];           
             $faq[$j]['answer'] = $value11['answer'];
             $j++;
          }
          $result[$i]['category']['name'] = $value['name'];
          $result[$i]['category']['value'] = $faq;
          
        }
        $i++;
      }
      $response['status'] = 1;
      $response['msg']  = 'Faq';
      $response['data'] = $result;
      return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }
   /* $data = json_decode(json_encode($data), true);    
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            $result[$i]['question'] = $value['question'];           
            $result[$i]['answer'] = $value['answer'];
            $i++;
          }
      $response['status'] = 1;
      $response['msg']  = 'Faq';
      $response['data'] = $result;  
      return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }*/

   }


   /************** Loan Calculator ***********************/

   public function loanCalculator()
   {
    $result = array();
    $response = array();

    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    
   $loanAmount = $getData['loan_amount'];
   $tenure = $getData['tenure'];
   $interestRate = $getData['interest'];
   $paymentFrequency = $getData['payment_frequency'];
   $dueDate= date('Y-m-d'); 
   $emi  = $this->pmt($interestRate,$paymentFrequency,$loanAmount,$tenure); 
   $totalPaidAmont = $emi*$tenure;
   $dueBalance = $loanAmount;

    if(!empty($emi) || $emi > 0)
    {
      $j=0;
      $k = 1;
      for ($i=1; $i <= $tenure; $i++)
      {
        if($paymentFrequency == 365)
        {
            $dueDate = date('Y-m-d',strtotime($dueDate.' +1 day'));
        }
        else if($paymentFrequency == 52)
        {                       
           $dueDate = date('Y-m-d',strtotime($dueDate.' +7 days'));
        }
        else if($paymentFrequency == 12)
        {
            $dueDate = date('Y-m-d',strtotime($dueDate.' +1 month'));
        }
        else if($paymentFrequency == 1)
        {
            $dueDate = date('Y-m-d',strtotime($dueDate.' +1 year'));
        }
           
        $rateInt = $interestRate/$paymentFrequency;
        $k = $k-1;
        $interestAmount = number_format($dueBalance*$rateInt/100,2,'.','');    
        $principalAmount = number_format($emi-$interestAmount,2,'.','');
       //echo $dueBalance; die;
        $dueBalance = number_format($dueBalance-$principalAmount,2,'.','');
        $result[$j]['installment_name'] = 'Installment '.$i;
        $result[$j]['due_date'] = $dueDate;
        $result[$j]['principal_amount'] = $principalAmount;
        $result[$j]['interest_amount'] = $interestAmount;
        $result[$j]['total_emi'] = number_format($emi,2,'.','');
        $result[$j]['due_balance'] = $dueBalance;
        $j++;
        $k++;
      } 
      $response['status'] = 1;
      $response['msg']  = 'Loan Calculator';
      $response['total_amount'] = number_format($totalPaidAmont,2,'.','');
      $response['data'] = $result;
      return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }

   }

   public function pmt($interest, $paymentFrequency, $loan,$tenure) 
    {
        //echo $tenure; die;
        $intr =$interest/($paymentFrequency*100); //get percentage
        $EMI= $loan * $intr / (1 - (pow(1/(1 + $intr),$tenure))); # This for 
        return $EMI;
    }







/************** Check Email  ***********************/
public function checkEmail()
{
    $result = array();
    $response = array();
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);

    if(!empty($getData))
    {
       $dataUser = DB::table('users')->where('email',$getData['email'])->first();
       $data = json_decode(json_encode($dataUser), true); 
       if($data['is_loginfirst'] != 1)
       {
         if(!empty($data))
         {
            $response['status'] = 0;
            $response['msg']  = 'This email is exist.Please login with your credentials';
            $response['data'] = [];
            return Response::json($response);
         }
         else
         {
          $api_url = \Config::get('values.api_url');
  		     $getUrl = $api_url.'api/checkloan/'.$getData['email'];
              $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $getUrl);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              $output = curl_exec($ch);
              $info = curl_getinfo($ch);
              curl_close($ch);
              if(!empty($output))
              {
              	if(isset(json_decode($output)->applied_loan_already) && json_decode($output)->applied_loan_already != 'No')
              	{
              		 $response['status'] = 0;
  			          $response['msg']  = 'This email is exist in ERP System.Please login with your credentials';
  			          $response['data'] = [];
  			          return Response::json($response);
              	}
              	else{
              		  $response['status'] = 1;
  			          $response['msg']  = 'Successfully';
  			          $response['data'] = [];
  			          return Response::json($response);
              	}
              }
           $response['status'] = 1;
            $response['msg']  = 'Successfully';
            $response['data'] = [];
            return Response::json($response);
         }
     }  
     else{
          $response['status'] = 1;
          $response['msg']  = 'Successfully';
          $response['data'] = [];
          return Response::json($response);
        }

    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }
}



/************** Check Phone No.  ***********************/
public function checkPhone()
{
    $result = array();
    $response = array();
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);
    if(!empty($getData))
    {
       $dataUser = DB::table('users')->where('phone',$getData['phone'])->first();
       $data = json_decode(json_encode($dataUser), true); 
        if($data['is_loginfirst'] != 1)
       {
       if(!empty($data))
       {
          $response['status'] = 0;
          $response['msg']  = 'This phone number is exist.Please try another phone number !';
          $response['data'] = [];
          return Response::json($response);
       }
       else
       {
         $response['status'] = 1;
          $response['msg']  = 'Successfully';
          $response['data'] = [];
          return Response::json($response);
       }
     }
      else
       {
         $response['status'] = 1;
          $response['msg']  = 'Successfully';
          $response['data'] = [];
          return Response::json($response);
       }

    }
    else
    {
      $response['status'] = 0;
        $response['msg']  = 'Record not found !';
        $response['data'] = [];
      return Response::json($response);
    }
}

/************** Check User Duplicate  ***********************/
public function checkUserDuplicate()
{
	$result = array();
    $response = array();
    $getReqData = file_get_contents('php://input');
    $getData = $this->getJson($getReqData);

    if (!empty($getData)) {
        $field = $getData['field'];
        $userId = $getData['userId'];

        if ($field == "phone") { // field phone validation
            if (!is_numeric($getData['phone'])) {
                $response['status'] = 0;
                $response['msg']  = 'This phone number is contains characters!';
                $response['data'] = [];

                return Response::json($response);
            }

            $dataUser = DB::table('users')
                        ->where('phone',$getData['phone'])
                        ->first();
            if ($userId) {
                $dataUser = DB::table('users')
                            ->where('phone',$getData['phone'])
                            ->whereNotIn('id', [ $userId ])
                            ->first();
            }
            $data = json_decode(json_encode($dataUser), true); 
            if ($data['is_loginfirst'] != 1) {
                if (!empty($data)) {
                    $response['status'] = 0;
                    $response['msg']  = 'This phone number is exist. Please try another phone number!';
                    $response['data'] = [];

                    return Response::json($response);
                }
            }

        } else { // field email validation
            if (!filter_var($getData['email'], FILTER_VALIDATE_EMAIL)) {
                $response['status'] = 0;
                $response['msg']  = 'Email Address is not valid';
                $response['data'] = [];

                return Response::json($response);
            }

            $dataUser = DB::table('users')
                          ->where('email',$getData['email'])
                          ->first();
            if ($userId) {
                $dataUser = DB::table('users')
                          ->where('email',$getData['email'])
                          ->whereNotIn('id', [ $userId ])
                          ->first();
            }
            $data = json_decode(json_encode($dataUser), true); 
            if ($data['is_loginfirst'] != 1) {
                if (!empty($data)) {
                    $response['status'] = 0;
                    $response['msg']  = 'This email is exist. Please login with your credentials';
                    $response['data'] = [];

                    return Response::json($response);
                /*} else {
                    $getUrl = env('API_URL').'api/checkloan/'.$getData['email'];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $getUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $output = curl_exec($ch);
                    $info = curl_getinfo($ch);
                    curl_close($ch);
                    if (!empty($output)) {
                        if (isset(json_decode($output)->applied_loan_already) && json_decode($output)->applied_loan_already != 'No') {
                            $response['status'] = 0;
                            $response['msg']  = 'This email is exist in ERP System. Please login with your credentials';
                            $response['data'] = [];

                            return Response::json($response);
                        } else {
                            $response['status'] = 1;
                            $response['msg']  = 'Successfully';
                            $response['data'] = [];

                            return Response::json($response);
                        }
                    }
                    $response['status'] = 1;
                    $response['msg']  = 'Successfully';
                    $response['data'] = [];
		    return Response::json($response);*/
                }
            }
        }

        $response['status'] = 1;
        $response['msg']  = 'Successfully';
        $response['data'] = [];

        return Response::json($response);
    }
}

/************** Get Front QR Code ***********************/

   public function getFrontQRCode()
   {
    $result = array();
    $response = array();
    $imagePath = SITE_URL. '/public/files/frontqrcode/paymentqrcode.png';
      $extension = pathinfo($imagePath, PATHINFO_EXTENSION);
      $img = file_get_contents($imagePath);
      $dataEncoded = base64_encode($img);
      $base64Str = 'data:image/' . $extension . ';base64,' . $dataEncoded;
      $result['url'] =   $base64Str;  
      $response['status'] = 1;
      $response['msg']  = 'QR Code';
      $response['data'] = $result;
       echo json_encode($response,JSON_UNESCAPED_SLASHES); die;
   }

 //priyanka work 4 feb
  function getdatafromwebsite($url,$token,$email,$user)
   {  
    // echo $token;
            $header = array(
          'Authorization: bearer '. $token);
            $getUrl = $url."/".$email;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_URL, $getUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
              if(isset(json_decode($output)->error) && json_decode($output)->error == 'TOKEN_EXPIRED')
              {	
              	   $params = array(
                   "email" => $email,
                   "password" => $user->website_password
	                );
	                $api_url = \Config::get('values.api_url'); 
	                $result12=$this->httpPost($api_url."api/login",$params);
	                $manage = json_decode($result12, true);
	                 if(isset($manage['success']) && $manage['success'] == 1)
		                { 
		                      DB::table('users')
		                      ->where('id', $user->id)
		                      ->update(array('website_token' =>$manage['token']));
		                  $this->getdatafromwebsite($url,$token,$email,$user);
		                  
		                }
              }
              else{
              	return $output;
              }
	    
             
   }
   /************** Payment Schedule ***********************/

   public function getPaymentScheduleListNew()
   {
	    $result = array();
	    $response = array();    
	    $getReqData = file_get_contents('php://input');   
	    $getData = $this->getJson($getReqData);   
	    $token = $getData['token'];
	    $user = JWTAuth::toUser($token);
	    $i = 0;
      // print_r($user);
	    $dataLoanInd = LoanEnquiryIndividual::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>0])->get();    
	    $dataLoanInd = json_decode(json_encode($dataLoanInd), true);
      // print_r($dataLoanInd);
	    $dataLoanComp = LoanEnquiryCompany::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>0])->get();   
	    $dataLoanComp = json_decode(json_encode($dataLoanComp), true);
      // print_r($dataLoanComp);
	    
	    if(!empty($dataLoanInd))
	    {
	          foreach ($dataLoanInd as $key => $value)
	          {
	            $result[$i]['id'] = $value['id'];
	            $result[$i]['loan_no'] = $value['loan_no'];            
	            $result[$i]['loan_type'] = 3;
	            $i++;
	          }
	    }

	    if(!empty($dataLoanComp))
	    {
	          $j = $i;
	          foreach ($dataLoanComp as $key => $value)
	          {
	            $result[$j]['id'] = $value['id'];
	            $result[$j]['loan_no'] = $value['loan_no'];
	            $result[$j]['loan_type'] = 4;            
	            $j++;
	          }
	    }
 //print_r($user->email);exit;
 $user->is_website_user = 1;
	    if($user->is_website_user ==1)
	    {
//print_r("called");exit;
            $api_url = \Config::get('values.api_url');
		     $websitedata =  $this->getdatafromwebsite($api_url.'api/loandetails',$user->website_token,$user->email,$user) ;
                     //die($websitedata);
          // print_r($websitedata);
		     if(!empty($websitedata))
		      {
                       //print_r($websitedata);exit;
		        foreach (json_decode($websitedata) as $key => $value) {
		          if(isset($value->loan_details))
		          {
		              if($value->loan_details->is_completed !='Closed')
		              {
                                $idd='';
                                $loan_typee='';
		              	if($value->loan_details->loan_enquiry_individual_id != '')
		              	{
		              		$idd=$value->loan_details->loan_enquiry_individual_id;
		              		$loan_typee=3;
		              	}
		              	else 
		                if($value->loan_details->loan_enquiry_company_id != ''){
		                	$idd=$value->loan_details->loan_enquiry_company_id;
		                	$loan_typee=4;
		              	}
		                array_push($result, array('id'=>$idd,'loan_no'=>$idd,'loan_type'=>$loan_typee,'laon_from'=>'ERP'));
		              }
		          }
		          
		        }
		      }
	    }
	    if(!empty($result))
	    {
	       $response['status'] = 1;
	        $response['msg']  = 'Payment Schedule List';
	        $response['data'] = $result;
	        return Response::json($response);
	    }
	    else
	    {
	      $response['status'] = 0;
	        $response['msg']  = 'N.A.';
	        $response['data'] = [];
	      return Response::json($response);
	    }

   }
    /************** Payment Schedule Details ***********************/

   public function getPaymentScheduleDetails()
   {
	    $result = array();
	    $response = array();    
	    $getReqData = file_get_contents('php://input');   
	    $getData = $this->getJson($getReqData);   
	    $token = $getData['token'];
	    $user = JWTAuth::toUser($token);
	    $loanId = $getData['loan_id'];
	    $loanType = $getData['loan_type'];
	    if($loanType == 3)
	    {
	      //echo $user->id; die;
	      $data = Payments::with('loanenquiryindividual')->where(['user_id'=>$user->id,'loan_enquiry_individual_id'=>$loanId,'is_completed'=>0])->get();    
	    }
	    else if($loanType == 4)
	    {
	      $data = Payments::with('loanenquiryindividual')->where(['user_id'=>$user->id,'loan_enquiry_company_id'=>$loanId,'is_completed'=>0])->get();
	    }
	    $data = json_decode(json_encode($data), true);
	     if(empty($data))
	   {
	   	 $newdata='';
           $api_url = \Config::get('values.api_url');
	   	 $websitedata = $this->getdatafromwebsite($api_url.'api/loandetails',$user->website_token,$user->email,$user); 

	   	 
	   	  if(!empty($websitedata))
	      {
	        foreach (json_decode($websitedata) as $key => $value) 
	        {
	          if(isset($value->loan_details))
	          {
	             if($value->loan_details->is_completed!='Closed' &&($value->loan_details->loan_enquiry_individual_id == $loanId || $value->loan_details->loan_enquiry_company_id == $loanId ))
	              {
              		// $x = $value->loan_details->loan_amount+$value->loan_details->total_ps_interest;
              		// $per_EMI_amount=$x/$value->loan_details->total_emi;
                  // $alreadypaid=$x-$value->loan_details->due_balance;
              		$due_balance=$value->loan_details->due_balance;
					//          $y = $per_EMI_amount;
					// $resultmod = fmod($alreadypaid,$y);
					// //echo $result.'<br/>';
					// $result2 = $alreadypaid/$y;
					//echo floor($result2);die;
					$i = 1;
					foreach ($value->all_payment_schedule as $key6 => $value6) {
						// if($i >floor($result2))
						// 	{
								// if($i == floor($result2)+1)
								// { echo 'if enter'.$i;;
								// 	foreach ($value6 as $key7 => $value7)
								// 		{
										
								// 			if($key7=='date')
						  //             		{
						  //             			$value->all_payment_schedule[$key6]->due_date = $value->all_payment_schedule[$key6]->$key7;
								// 					unset($value->all_payment_schedule[$key6]->$key7);
						  //             		}
						  //             		if($key7 == 'amount')
						  //             		{
						  //             			$value->all_payment_schedule[$key6]->total_emi = round($value->all_payment_schedule[$key6]->$key7-$resultmod,2);
								// 					unset($value->all_payment_schedule[$key6]->$key7);
						              		
						  //             		}
						  //             		if($key7 == 'principal')
						  //             		{
						  //             			$value->all_payment_schedule[$key6]->principal_amount = round($value->all_payment_schedule[$key6]->$key7-$resultmod,2);
								// 					unset($value->all_payment_schedule[$key6]->$key7);
						              		
						  //             		}
						  //             		if($key7 == 'interest')
						  //             		{
						  //             			$value->all_payment_schedule[$key6]->interest_amount = $value->all_payment_schedule[$key6]->$key7;
								// 					unset($value->all_payment_schedule[$key6]->$key7);
						              		
						  //             		}
						              		
						  //             		if($key7 == 'receipt_number')
						  //             		{
								// 				$value->all_payment_schedule[$key6]->receipt_no = $value->all_payment_schedule[$key6]->$key7;
								// 					unset($value->all_payment_schedule[$key6]->$key7);
						  //             		}
							 //              	$value->all_payment_schedule[$key6]->installment_name ='Installment '.$i;
							 //              	$value->all_payment_schedule[$key6]->due_balance =round($x-($per_EMI_amount*$i),2);
							 //              	$value->all_payment_schedule[$key6]->status =1;

								// 		}

								// }
								// else{ echo 'else enter'.$i;
									foreach ($value6 as $key7 => $value7)
										{
											if($key7=='date')
						              		{
						              			$value->all_payment_schedule[$key6]->due_date = $value->all_payment_schedule[$key6]->$key7;
													unset($value->all_payment_schedule[$key6]->$key7);
						              		}
						              		if($key7 == 'due_amount')
						              		{
						              			$value->all_payment_schedule[$key6]->total_emi = $value->all_payment_schedule[$key6]->$key7;
													unset($value->all_payment_schedule[$key6]->$key7);
						              		
						              		}
						              		if($key7 == 'principal_left')
						              		{
						              			$value->all_payment_schedule[$key6]->principal_amount = $value->all_payment_schedule[$key6]->$key7;
													unset($value->all_payment_schedule[$key6]->$key7);
						              		
						              		}
						              		if($key7 == 'interest_left')
						              		{
						              			$value->all_payment_schedule[$key6]->interest_amount = $value->all_payment_schedule[$key6]->$key7;
													unset($value->all_payment_schedule[$key6]->$key7);
						              		
						              		}
						              		
						              		if($key7 == 'receipt_number')
						              		{
												$value->all_payment_schedule[$key6]->receipt_no = $value->all_payment_schedule[$key6]->$key7;
													unset($value->all_payment_schedule[$key6]->$key7);
						              		}
                              if($key7 == 'installment_no')
                              {

							              	$value->all_payment_schedule[$key6]->installment_name ='Installment '.$value->all_payment_schedule[$key6]->$key7;
                              unset($value->all_payment_schedule[$key6]->$key7);
                              }
                              //$value->all_payment_schedule[$key6]->due_balance =round($x-($per_EMI_amount*$i),2);
							              	//$value->all_payment_schedule[$key6]->due_balance =;
							              	if($key7 == 'status')
                              {

                              $value->all_payment_schedule[$key6]->status1 =$value->all_payment_schedule[$key6]->$key7;
                              unset($value->all_payment_schedule[$key6]->$key7);
                              }
                              //$value->all_payment_schedule[$key6]->status =1;
										}
								//}
							
							$data[]=$value->all_payment_schedule[$key6];	
						//}
							$i++;
							# code...
						}
	              		//$data=$value->all_payment_schedule;
	              		
	              		 $data = json_decode(json_encode($data), True);
	              		 //print_r( $data);die;
	              		 $data[0]['loanenquiryindividual']['account_number']='';
	              }
	          }
	      	}

	   	  }
		}
	    //$totalDueAmount = 0;
    
	    if(!empty($data))
	    {
	      $i = 0;
	          foreach ($data as $key => $value)
	          {
	            	//print_r($value);die;
	           // $totalDueAmount = $totalDueAmount+$value['total_emi'];           
	            $result[$i]['installment_name'] = $value['installment_name'];
	            $result[$i]['due_on'] = date('d-M-Y',strtotime($value['due_date']));
	            $result[$i]['principal_amount'] = $value['principal_amount'];
	            $result[$i]['interest_amount'] = $value['interest_amount'];
              //$result[$i]['balance_due'] = $value['due_balance'];
	            $result[$i]['balance_due'] = $value['total_emi'];
	           // $result[$i]['late_fee'] = $value['late_fee'];
	            //$result[$i]['status'] = $value['status1'];
             //  if($value['status'] == 1)
	            // {
	            //   $result[$i]['status'] = 'pending';  
	            // }
	            // else if($value['status'] == 2)
	            // {
	            //   $result[$i]['status'] = 'Over Due';
	            // }
	            
	            $i++;
	          }
	          $response['status'] = 1;
	          $response['msg']  = 'Payment Schedule Details';
	          $response['account_number']  = $data[0]['loanenquiryindividual']['account_number'];
           // $response['total_due']  = number_format($totalDueAmount,2,'.','');
            //$response['total_due']  = number_format($due_balance,2,'.','');
	          $response['total_due']  = $due_balance;
	          $response['data'] = $result;
	          return Response::json($response);
	    }
	    else
	    {
	      $response['status'] = 0;
	      $response['msg']  = 'Payment schedule details not found !';
	      $response['account_number']  = '';
	      $response['total_due']  = '';
	      $response['data'] = $result;
	      return Response::json($response);
	    }

   }


   /************** Re-Payment Schedule ***********************/

   public function getRePaymentScheduleList()
   {
	    $result = array();
	    $response = array();    
	    $getReqData = file_get_contents('php://input');   
	    $getData = $this->getJson($getReqData);   
	    $token = $getData['token'];
	    $user = JWTAuth::toUser($token);
	    
	    $dataLoanInd = LoanEnquiryIndividual::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>1])->get();    
	    $dataLoanInd = json_decode(json_encode($dataLoanInd), true);
	   
	    $dataLoanComp = LoanEnquiryCompany::where(['user_id'=>$user->id,'active_status'=>2,'is_completed'=>1])->get();   
	    $dataLoanComp = json_decode(json_encode($dataLoanComp), true);

	    // print_r($user); die;
	    if(!empty($dataLoanInd))
	    {
	      $i = 0;
	          foreach ($dataLoanInd as $key => $value)
	          {
	            $result[$i]['id'] = $value['id'];
	            $result[$i]['loan_no'] = $value['loan_no'];            
	            $result[$i]['loan_type'] = 3;
	             $result[$i]['laon_from'] = 'LOCAL';   
	            $i++;
	          }
	    }

	    if(!empty($dataLoanComp))
	    {
	      $j = $i;
	          foreach ($dataLoanComp as $key => $value)
	          {
	            $result[$j]['id'] = $value['id'];
	            $result[$j]['loan_no'] = $value['loanenquiryindividual']['loan_no'];
	            $result[$j]['loan_type'] = 4;  
	            $result[$j]['laon_from'] = 'LOCAL';          
	            $j++;
	          }
	    }
	    if($user->is_website_user ==1)
	    {
                     $api_url = \Config::get('values.api_url');
		     $websitedata = $this->getdatafromwebsite($api_url.'api/loandetails',$user->website_token,$user->email,$user);
		     if(!empty($websitedata))
		      {
            $result['Inactive']=array();
            $result['Active']=array();
		        foreach (json_decode($websitedata) as $key => $value) {
		          if(isset($value->loan_details))
		          {
		              if($value->loan_details->is_completed=='Closed')
		              {
		              	if($value->loan_details->loan_enquiry_individual_id != '')
		              	{
		              		$idd=$value->loan_details->loan_enquiry_individual_id;
		              		$loan_typee=3;
		              	}
		              	else 
		                if($value->loan_details->loan_enquiry_company_id != ''){
		                	$idd=$value->loan_details->loan_enquiry_company_id;
		                	$loan_typee=4;
		              	}
		                array_push($result['Inactive'], array('id'=>$idd,'loan_no'=>$idd,'loan_type'=>$loan_typee,'laon_from'=>'ERP'));
		              }
                  //open
                  if($value->loan_details->is_completed=='Active')
                  {
                    if($value->loan_details->loan_enquiry_individual_id != '')
                    {
                      $idd1=$value->loan_details->loan_enquiry_individual_id;
                      $loan_typee1=3;
                    }
                    else 
                    if($value->loan_details->loan_enquiry_company_id != ''){
                      $idd1=$value->loan_details->loan_enquiry_company_id;
                      $loan_typee1=4;
                    }
                    array_push($result['Active'], array('id'=>$idd1,'loan_no'=>$idd1,'loan_type'=>$loan_typee1,'laon_from'=>'ERP'));
                  }
		          }
		          
		        }
		      }
	    }
	    if(!empty($result))
	    {
	       $response['status'] = 1;
	        $response['msg']  = 'Re-Payment Schedule List';
	        $response['data'] = $result;
	        return Response::json($response);
	    }
	    else
	    {
	      
	      $response['status'] = 0;
	        $response['msg']  = 'N.A.';
	        $response['data'] = [];
	      return Response::json($response);
	    }

   }
  
    /************** Re-Payment Schedule Details ***********************/

   public function getRePaymentScheduleDetails()
   {
   	$flag=false;
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    $loanId = $getData['loan_id'];
    $loanType = $getData['loan_type'];
    if($loanType == 3)
    {
      //echo $user->id; die;
      $data = Payments::where(['user_id'=>$user->id,'loan_enquiry_individual_id'=>$loanId,'is_completed'=>1])->get();    
    }
    else if($loanType == 4)
    {
      $data = Payments::where(['user_id'=>$user->id,'loan_enquiry_company_id'=>$loanId,'is_completed'=>1])->get();
    }
    	
    $data = json_decode(json_encode($data), true);

   
	   if(empty($data))
	   {
	   	 $newdata='';
$api_url = \Config::get('values.api_url');
	   	 $websitedata = $this->getdatafromwebsite($api_url.'api/loandetails',$user->website_token,$user->email,$user);
	   	  if(!empty($websitedata))
	      {
	      	$flag=true;
	        foreach (json_decode($websitedata) as $key => $value) 
	        {
	          if(isset($value->loan_details))
	          {
	              //$value->loan_details->is_completed=='Closed' &&
                if(($value->loan_details->loan_enquiry_individual_id == $loanId || $value->loan_details->loan_enquiry_company_id == $loanId ))
	              {
	              	foreach ($value->all_repayments as $key6 => $value6) {
		              	foreach ($value6 as $key7 => $value7) {
		              		if($key7=='date')
		              		{
		              			
		              			$value->all_repayments[$key6]->date_of_payment = $value->all_repayments[$key6]->$key7;
									unset($value->all_repayments[$key6]->$key7);
		              		}
		              		if($key7 == 'amount')
		              		{
		              			$value->all_repayments[$key6]->total_emi = $value->all_repayments[$key6]->$key7;
									unset($value->all_repayments[$key6]->$key7);
		              		
		              		}
		              		if($key7 == 'receipt_number')
		              		{
								$value->all_repayments[$key6]->receipt_no = $value->all_repayments[$key6]->$key7;
									unset($value->all_repayments[$key6]->$key7);
		              		}
		              		if($key7 == 'installment_name')
		              		{
								$value->all_repayments[$key6]->loan_from = 'ERP';
									unset($value->all_repayments[$key6]->$key7);
		              		}
						
		              	}


	              	}
	              		$data=$value->all_repayments;
	              		 $data = json_decode(json_encode($data), True);
	              }
	          }
	      	}
	   	  }
		}

    $totalDueAmount = 0;
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {            
            $result[$i]['payment_id'] = $value['id'];         
            $result[$i]['date_of_payment'] = date('d-M-Y',strtotime($value['date_of_payment']));
            $result[$i]['amount'] = $value['total_emi'];
            $result[$i]['date_of_receipt'] = date('d-M-Y',strtotime($value['date_of_payment']));
            $result[$i]['receipt_no'] = $value['receipt_no']; 
            $result[$i]['laon_from'] = isset($value['loan_from'])?$value['loan_from']:'LOCAL';   
            $i++;
          }
          if($flag == true)
          {
          	$response['loan_from'] = 'ERP';
          }
          else{
			$response['loan_from'] = 'LOCAL';
          }
          $response['status'] = 1;
          $response['msg']  = 'Re Payment Schedule Details';          
          $response['data'] = $result;
          return Response::json($response);
    }
    else
    {
      $response['status'] = 0;
      $response['msg']  = 'Re Payment schedule details not found !';      
      $response['data'] = $result;
      return Response::json($response);
    }

   }


    /************** Download PDF ***********************/

   public function downloadPDF()
   {
    //phpinfo();die;
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);  
    // print_r($getData); 
    $token = $getData['token'];    
    $user = JWTAuth::toUser($token);
    $paymentId = $getData['payment_id'];
    $loanfrom = $getData['loanfrom'];
    $mpdf = new \Mpdf\Mpdf([
              'mode' => 'utf-8', 
              'format' => [220, 236], 
              'orientation' => 'L'
                ]);
    if(strtolower($loanfrom) == 'local')
    {
      $officeBranchId = 0;
      $data = Payments::where(['id'=>$paymentId,'is_completed'=>1])->first();
      if($data->loan_type == 3)
      {      
        $loan_no = isset($data->loanenquiryindividual->loan_no)?$data->loanenquiryindividual->loan_no:'';
        $officeBranchId = isset($data->loanenquiryindividual->office_branch)?$data->loanenquiryindividual->office_branch:'';
        ;
       
      }
      else if($data->loan_type == 4)
      {
        $loan_no = isset($data->loanenquirycompany->loan_no)?$data->loanenquirycompany->loan_no:'';
         $officeBranchId = $data->loanenquirycompany->office_branch;
        ;
      }

      $dataOfficeBranch = OfficeBranch::where('id',$officeBranchId)->first();
  
      $data = json_decode(json_encode($data), true);
      if(!empty($data))
      {
          //echo SITE_URLI; die;
      
           if(empty($data->url))
           {
              $url = SITE_URLI.'/public/files/receiptSingle/singreceipt'.$paymentId.'.pdf';
           

              // $html = file_get_contents(SITE_URL. '/public/reciept.html');
	      $html = file_get_contents("https://mobile.sivren.org/accredit-admin/public/reciept.html");
	      $html = file_get_contents(\Config::get('values.app_url') . "public/reciept.html");
              // print_r($html);
              $html = str_replace('[receipt_no]',$data['receipt_no'],
                          $html);
              $html = str_replace('[date]',date('d-M-Y'),
                          $html);
              $html = str_replace('[loan_no]',$loan_no,
                          $html);
              $html = str_replace('[installment_name]',$data['installment_name'],
                          $html);
              $html = str_replace('[principal_amount]','$'.$data['principal_amount'],
                          $html);
              $html = str_replace('[interest_amount]','$'.$data['interest_amount'],
                          $html);
              $html = str_replace('[interest_amount]','$'.$data['interest_amount'],
                          $html);
              $html = str_replace('[outstanding_amount]','$'.$data['due_balance'],
                          $html);
              $html = str_replace('[total_emi]','$'.$data['total_emi'],
                          $html);
              $html = str_replace('[late_fee]','$'.$data['late_fee'],
                          $html);
              if(!empty($dataOfficeBranch))
              {
                $html= str_replace('[office_branch]',$dataOfficeBranch['name'],$html);
              }
              
              //print_r($html); die;
              $mpdf->WriteHTML($html);
              $mpdf->Output($url,'F');
              $pdfUrl = SITE_URL.'/public/files/receiptSingle/singreceipt'.$paymentId.'.pdf';

              DB::table('payments')
                ->where('id', $paymentId)
                ->update(array('url' =>$pdfUrl));
           }
           else
           {
             $pdfUrl = $data->url;
           }

            $imagePath = $pdfUrl;
            $extension = pathinfo($imagePath, PATHINFO_EXTENSION);
            $img = file_get_contents($imagePath);
            $dataEncoded = base64_encode($img);
            $base64Str = 'data:application/pdf'.';base64,' . $dataEncoded;

          $result['date_of_payment'] = date('d-M-Y',strtotime($data['due_date']));
          $result['amount'] = $data['total_emi'];
          $result['date_of_receipt'] = date('d-M-Y',strtotime($data['date_of_payment']));
          $result['receipt_no'] = $data['receipt_no'];   
          $result['url'] = $base64Str;
            $response['status'] = 1;
            $response['msg']  = 'Download PDF';          
            $response['data'] = $result;
            $response['pdf_server_url'] = $pdfUrl;

            return Response::json($response);
      }
      else
      {
        $response['status'] = 0;
        $response['msg']  = 'Record not found !';      
        $response['data'] = $result;
        return Response::json($response);
      }
    }
    else if(strtolower($loanfrom) == 'erp')
    {

          $filename=$getData['loan_id'].'_'.$getData['istallment_count'];
         
$api_url = \Config::get('values.api_url');
 $getUrl = $api_url .'api/getReceiptPDF/'.$getData['loan_id'].'/'.$getData['istallment_count'];
          // echo $getUrl;

            $output= $this->common_erp_download($getUrl,$user);
            // echo 'output'.$output;
            // die;
            if(!empty($output))
            {
              $url = SITE_URLI.'/public/files/erpfiles/single/'.$filename.'.pdf';
              $mpdf->WriteHTML($output);
              $mpdf->Output($url,'F');
              $pdfUrl = SITE_URL.'/public/files/erpfiles/single/'.$filename.'.pdf';
            //  print_r($pdfUrl);die;
             $response['status'] = 1;
            $response['msg']  = 'Download PDF';          
            $response['data'] = '';
            $response['pdf_server_url'] = $pdfUrl;
            return Response::json($response);

            }
    }
   }

   
   /************** Download Bulk PDF ***********************/

   public function downloadBulkPDF()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);  
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    $loanfrom = $getData['loanfrom'];
   // $dateFrom = $getData['date_from'];
   // $dateTo = $getData['date_to'];
    $loanId = $getData['loan_id'];
    $loanType = $getData['loan_type'];
    $html11 = '';
    $html = '';
    $totalPricipalAmount = 0;
    $totalInterestAmount = 0;
    $totalAmountRequested = 0;   
    $totalPayableAmount  = 0;
    $pricipalAmount = 0; 
    $interestAmount = 0;
    $lateFee = 0;
    $payableAmount = 0;
    $isData = 0;
    $mpdf = new \Mpdf\Mpdf([
              'tempDir' => '/tmp',
              'mode' => 'utf-8',
              'format' => [220, 236], 
              'orientation' => 'L',
              'debug'=>true
                ]);
    if(strtolower($loanfrom) != 'erp')
    {
    
      if($loanType == 3)
      {
        $data = Payments::where(['loan_enquiry_individual_id'=>$loanId])->get(); 
        if(!empty($data))
        {
          $totalcount_data=sizeof($data);
          if($totalcount_data > 1)
          {
            $dateFrom=$data[0]->due_date;
            $dateTo=$data[$totalcount_data-1]->due_date;
          }
          else if($totalcount_data == 1){
             $dateFrom=$data[0]->due_date;
            $dateTo=$data[0]->due_date;
          }
        }  
      }
      else if($loanType == 4)
      {
       // $data = Payments::whereBetween('date_of_payment',[$dateFrom,$dateTo])->where(['loan_enquiry_company_id'=>$loanId])->get();
         $data = Payments::where(['loan_enquiry_company_id'=>$loanId])->get();
      }  

        if(!$data->isEmpty())
        {
           
          if($loanType == 3)
          {      
            $loan_no = isset($data[0]->loanenquiryindividual->loan_no)?$data[0]->loanenquiryindividual->loan_no:'';  
            $totalAmountRequested = isset($data[0]->loanenquiryindividual->loan_amount_requested)?$data[0]->loanenquiryindividual->loan_amount_requested:'';    
          }
          else if($loanType == 4)
          {
            $loan_no = isset($data[0]->loanenquirycompany->loan_no)?$data[0]->loanenquirycompany->loan_no:'';
            $totalAmountRequested = isset($data[0]->loanenquirycompany->loan_amount_requested)?$data[0]->loanenquirycompany->loan_amount_requested:'';
          }   
           $customerName = isset($data[0]->userinfo->name)?$data[0]->userinfo->name:'';
           $customerAddress = isset($data[0]->userinfo->residential_address_one)?$data[0]->userinfo->residential_address_one:'';
           $customerPhone = isset($data[0]->userinfo->phone)?$data[0]->userinfo->phone:'';
           $customerEmail = isset($data[0]->userinfo->email)?$data[0]->userinfo->email:''; 
        }    
        $data = json_decode(json_encode($data), true); 

        if(!empty($data))
        { 
           $officeBranchId = $data[0]['loanenquiryindividual']['office_branch'];        
           $dataOfficeBranch = OfficeBranch::where('id',$officeBranchId)->first();
           $branchName = isset($dataOfficeBranch['name'])?$dataOfficeBranch['name']:'';
           $branchAddress = isset($dataOfficeBranch['address'])?$dataOfficeBranch['address']:'';
           $branchPhone = isset($dataOfficeBranch['phone'])?$dataOfficeBranch['phone']:'';

           $html = file_get_contents(SITE_URL. '/public/bulkReciept.html');
           $html = str_replace(array('date_from','date_to','branch_name','branch_address','branch_phone','customer_name','customer_address','customer_phone','customer_email'),array($dateFrom,$dateTo,$branchName,$branchAddress,$branchPhone,$customerName,$customerAddress,$customerPhone,$customerEmail),$html);
        
            $digits = 4;
             $randNo = rand(pow(10, $digits-1), pow(10, $digits)-1);       
             $url = SITE_URLI.'/public/files/receiptBulk/bulkreceipt'.$randNo.'.pdf';
                
             $i = 0;
            foreach ($data as $key => $value)
            {
              $dateFrom = strtotime($dateFrom);
              $dateFrom = date('Y-m-d',$dateFrom);
              $dateTo = strtotime($dateTo);
              $dateTo = date('Y-m-d',$dateTo);
          
              if($value['is_completed'] == 1){
                $isData = 1;
              $html11 .= '<tr>
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">'. date('d-M-Y',strtotime($value["date_of_payment"])) .'</td>
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["principal_amount"] .'</td>
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["interest_amount"] .'</td>
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["late_fee"] .'</td>
                          
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["total_emi"] .'</td>
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["principal_amount"] .'</td>
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["interest_amount"] .'</td>
                          <td style="font-size: 14px; font-family: Arial, Helvetica, sans-serif; color: #000; padding-bottom: 5px; border: 1px solid rgba(0, 0, 0, 0.4);  padding: 8px;">$'. $value["late_fee"] .'</td>
                          
                      </tr>';
              $result[$i]['date_of_payment'] = date('d-M-Y',strtotime($value['due_date']));
              $result[$i]['amount'] = $value['total_emi'];
              $result[$i]['date_of_receipt'] = date('d-M-Y',strtotime($value['date_of_payment']));
              $result[$i]['receipt_no'] = $value['receipt_no'];          
              $pricipalAmount = $pricipalAmount+$value['principal_amount'];
              if($value['interest_amount'] > 0)
              {
                $interestAmount = number_format($interestAmount+$value['interest_amount'],2,'.','');
              }
              
              $payableAmount = $payableAmount+$value['total_emi'];
              
              $lateFee = number_format($lateFee+$value['late_fee'],2,'.','');
              $i++;
            }
            $totalPricipalAmount = number_format($totalPricipalAmount+$value['principal_amount'],2,'.','');
            $totalInterestAmount = number_format($totalInterestAmount+$value['interest_amount'],2,'.','');
            $totalPayableAmount = $totalPayableAmount+$value['total_emi'];
            }
            if($isData == 1)
            {
              $totaloutstanigAmount = number_format($totalPayableAmount -$payableAmount,2,'.',''); 
             $totalOutStandingPriciplaAmount = number_format($totalPricipalAmount -$pricipalAmount,2,'.','');
             //echo $totalOutStandingPriciplaAmount; die; 
             $html = str_replace(array('loan_no','loan_date','loan_amount_requested','principal_amount','total_pricipal_amount','total_interest_amount','interest_amount','total_payable_amount','payable_amount','late_fee','total_outstanding_amount','total_outstanding_pricipal_amount'),array($loan_no,date('d-M-Y',strtotime($data[0]['created_at'])),'$'.$totalAmountRequested,'$'.$pricipalAmount,'$'.$totalPricipalAmount,'$'.$totalInterestAmount,'$'.$interestAmount,'$'.$totalPayableAmount,'$'.$payableAmount,'$'.$lateFee,'$'.$totaloutstanigAmount,$totalOutStandingPriciplaAmount),$html);
                $html = str_replace('[replace_tr]',$html11, $html);
              //print_r($html); die;
              $mpdf->WriteHTML($html);        
              $mpdf->Output($url,'F');
              $pdfUrl = SITE_URL.'/public/files/receiptBulk/bulkreceipt'.$randNo.'.pdf';
              

              $imagePath = $pdfUrl;
              $extension = pathinfo($imagePath, PATHINFO_EXTENSION);
              $img = file_get_contents($imagePath);
              $dataEncoded = base64_encode($img);
              $base64Str = 'data:application/pdf'.';base64,' . $dataEncoded;

              $response['status'] = 1;
              $response['msg']  = 'Download Bulk PDF';          
              $response['data'] = $result;
              $response['url'] = $base64Str;
              $response['pdf_server_url'] = $pdfUrl;
              return Response::json($response);
            }  
            else
            {
              $response['status'] = 0;
              $response['msg']  = 'Download Bulk PDF';          
              $response['data'] = $result;
              $response['url'] = '';
              
              return Response::json($response);
            }
             

            
        }
        else
        {
          $response['status'] = 0;
          $response['msg']  = 'Record not found !';      
          $response['data'] = [];
          return Response::json($response);
        }
    }
    else if(strtolower($loanfrom) == 'erp')
    {

          $filename=$getData['loan_id'];

$api_url = \Config::get('values.api_url');

          $getUrl = $api_url .'api/getSOAPDF/'.$getData['loan_id'];
            $output= $this->common_erp_download($getUrl,$user);
            //print_r( $output);die;
            if(!empty($output))
            {
             // $output= str_replace("Borrower's", "Borrower&#39;s", $output);
               $output= str_replace("page-break-after: always;", " ", $output);
            //  $output= str_replace("'", "\"", $output);
             // $output= str_replace("</span>", " ", $output);
             
              $output= trim($output);
             
             // print_r($output);die;
             // $output= str_replace("'","&#39;",$output);
              $url = SITE_URLI.'/public/files/erpfiles/bulk/'.$filename.'.pdf';
              $mpdf->WriteHTML(trim($output));        
              $mpdf->Output($url,'F');
              $pdfUrl = SITE_URL.'/public/files/erpfiles/bulk/'.$filename.'.pdf';
            
            $response['status'] = 1;
            $response['msg']  = 'Download PDF';          
            $response['data'] = '';
            $response['pdf_server_url'] = $pdfUrl;
            return Response::json($response);

            }
    }
   }



   /************** Get Notifications ***********************/

   public function getNotifications()
   {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);
    //print_r($user); die;
    $data = Notifications::where('user_id',$user->id)->orderBy('id', 'DESC')->get();
    $data = json_decode(json_encode($data), true);
   // print_r($data); die;
    $curDate = date('Y-m-d H:i:s');
    if(!empty($data))
    {
      $i = 0;
          foreach ($data as $key => $value)
          {
            $seconds = time()-strtotime($value['created_at']);

            //echo $seconds; die;
            //$curDate).getTime()) / 1000;
            if($seconds < 60)
            {
              $time = round($seconds).' Sec Ago';
            }
            else if($seconds >= 60 && $seconds < 3600)
            {
              $time = round($seconds/60);
              $time = $time.' Mins Ago';
            }
            else if($seconds >= 3600 && $seconds < 86400)
            {
              $time  = round($seconds/3600);
              $time = $time.' Hours Ago';
            }
            else if($seconds >= 86400 && $seconds < 172800)
            {
              $time = 'Today';
            }
            else if($seconds >= 172800 && $seconds < 259200)
            {
              $time = 'Yesterday';
            }
            else
            {
              $time = date('d-M-Y',strtotime($value['created_at']));
            }

            $result[$i]['id'] = $value['id'];
            $result[$i]['is_read'] = $value['is_read'];
            $result[$i]['loan_no'] = $value['loan_no'];
            $result[$i]['message'] = $value['message'];
            $result[$i]['time'] = $time;
            $i++;
          }
       
        $response['status'] = 1;
        $response['msg']  = 'Notification List';          
        $response['data'] = $result;
        return Response::json($response);
    }
    else
    {
        $response['status'] = 0;
        $response['msg']  = 'Notification not found !';          
        $response['data'] = $result;
        return Response::json($response);
    }

   }



/************** updateNotification ***********************/

  public function updateNotification()
  {
    $result = array();
    $response = array();    
    $getReqData = file_get_contents('php://input');   
    $getData = $this->getJson($getReqData);   
    $token = $getData['token'];
    $user = JWTAuth::toUser($token);   
    $isRead = $getData['is_read']; //1 for unread, 0 for read
    $id = $getData['id'];   
    if($id > 0)
    {
      Notifications::where('id',$id)->update(array('is_read'=>$isRead)); 
       $response['status'] = 1;
       $response['msg']  = 'Record updated successfully';          
       $response['data'] = [];
       return Response::json($response);
      
    }
    else
    {
       $response['status'] = 0;
       $response['msg']  = 'Record not found !';      
       $response['data'] = [];
       return Response::json($response);
    }
    
    
  }

  public function updateProfile()
  {
    $result = array();
    $response = array();    
    $getReqData = Input::all();
    $getData = json_decode($getReqData['Body'],true);  
    $token = $getData['token']; 
    $user = JWTAuth::toUser($token);
    $userId = $user->id;
    $name = isset($getData['name'])?$getData['name']:'';
    $phone = isset($getData['phone'])?$getData['phone']:'';    
    $email = isset($getData['email'])?$getData['email']:'';    
    if($userId > 0)
    {
       if(input::hasFile('image'))
          {
             //echo SITE_URLI.'/public/files/proofofincome/'; die;
              $file = Input::file('image');            
              $filename = microtime() . '.' .$file->getClientOriginalName();
              $file->move(SITE_URLI.'/public/files/profileImage/',$filename);
              
          }
          else
          {
            $filename = '';
          }
          if(!empty($filename))
          {
		  DB::table('users')->where('id',$userId)->update(array('name'=>$name,'phone'=>$phone,'image'=>$filename,'email'=>$email));
		  $baseUrl = str_replace("/index.php", "", url('/'));
            $result['image'] = $baseUrl . '/public/files/profileImage/' . $filename;
          }
          else
          {
            
            try{
            DB::table('users')->where('id',$userId)->update(array('name'=>$name,'phone'=>$phone,'email'=>$email));
            } catch(\Illuminate\Database\QueryException $e){
              $response['status'] = 0;
              $response['msg']  = 'Duplicate Entry Exist!';      
              $response['data'] = [];
              return Response::json($response);

            }
            if(!empty($user->image))
	    {
		    $baseUrl = str_replace("/index.php", "", url('/'));
              $result['image'] = $baseUrl . '/public/files/profileImage/' . $user->image;
            }
            else
            {
              $result['image'] = '';
            }
            
          }
          

          $result['name'] = $name;
          $result['phone'] = $phone;
          $result['email'] = $email;
          

           $response['status'] = 1;
           $response['msg']  = 'Profile updated successfully'; 
           $response['data'] = $result;
           return Response::json($response);

    }
    else
    {
       $response['status'] = 0;
       $response['msg']  = 'Profile not found !';      
       $response['data'] = [];
       return Response::json($response);
    }

  }

 

	
	/************** Update FCM  ***********************/
	public function updateFcmToken(Request $request){
		$jData = file_get_contents('php://input');
		// get decode json data
		$data = $this->getJson($jData);
		$token = $data['token'];
		$fcm_token = $data['fcm_token'];

		$user = JWTAuth::toUser($token); // authorization JWT Token
		$user_id = $user->id;

		$updateUser = array(
			'fcm_token' => $fcm_token,
		);
		DB::table('users')->where('id', $user_id)->update($updateUser);

		return response()->json(array('status'=>true,'message'=>'FCM Token updated successfully'));
	}
	
	/**-----------------Firebase Notification ------------**/
	
	function SendNotification( $deviceData, $notification,$type){
		/* print_r($deviceData);
		print_r($notification);
		 die; */
		/* Check device data not empty */ 
		if( empty( $deviceData ) ){
			return true;
		}else{
			$androidDevice = array();
			$iosDevice = array();
			
			 $androidDevice = $deviceData;
			if( count( $androidDevice ) > 0 ){
				  $SERVER_KEY = 'd3apoI51_ig:APA91bERt35X-4PbVWx3GMEWBcH7KND_xWdFNtIQXupmVjmmwYXLqyPYXim1Xonaf9PT3toOqLi8A3EglJdp5dBC94ZLsV8YYHblkdbgx69Tc48STBWwLuda8qvYSy739uT3iNx9APPJ'; 
				$jsonString = $this->sendPushNotificationToGCMSever( $androidDevice, $notification, $SERVER_KEY,$type);
				return true;
				// $jsonObject = json_decode($jsonString);


								
			} 
			
		}
	}
		
	/**-----------------Firebase Notification Send ------------**/	
	function sendPushNotificationToGCMSever($token, $notification, $SERVER_KEY,$type){
		$path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
		// send notification when send message  
		$fields = array();
		if($type=='saveMessage') {
			 $fields = array(
				'registration_ids' => array_values($token),
				'data'=>array(
					'siteId'=>$notification['siteId'],
					'issueId'=>$notification['issueId'],
					'message'=>$notification['msg'],
					'title'=>$notification['title'],
					'name'=>$notification['username'],
					'dateTime'=>date('d M | h:i A'),
					'vibrate'=>1,
					'sound'=>1,
          'force-start'=> 1,
					'icon'=>'notification_icon'
				),
				'priority'=>'high',
				'notification'=>array(
				'title'=>$notification['title'],
				'body'=>$notification['msg'],
         "click_action"=>"FCM_PLUGIN_ACTIVITY",
          'force-start'=> 1
				)
			); 
		}
		
		 $jsonData = json_encode($fields);
		  /* echo 'dffd ';
		 print_r($jsonData); die; */
		$headers = array(
			'Authorization:key=' . $SERVER_KEY,
			'Content-Type:application/json'
		);      
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		$result = curl_exec($ch);
		// echo "<pre>"; print_r($result);die;
		curl_close($ch);
		return $result;
	}
  
	function getDataMYINFO($result,$uid)
  {
    $params = array();
$api_url = \Config::get('values.api_url');
    $url=$api_url .'api/getmyinfodata1/'.$uid;
    //print_r($url);exit;   
    $result12=$this->httpGet($url);
    $jsontoarray=json_decode($result12,true);
 
    $arrayorresult = array();    
    if (!empty($jsontoarray['success'])) {
      $arrayorresult=$jsontoarray['success'];
    }

    $idType = "FIN";
    if (array_key_exists("residentialstatus", $arrayorresult)) {
	    if (array_key_exists("desc", $arrayorresult['residentialstatus'])) {
		    if (!empty($arrayorresult['residentialstatus']['desc'])) {
			if (strtoupper($arrayorresult['residentialstatus']['desc']) == "PR") {
			    $idType = "PR";
		    	} else {
			    $idType = "SC";
		    	}
		    }
	    }
    }
	// echo "<pre>".print_r($arrayorresult, 1)."</pre>";exit;

    if(!empty($result))
    {
      foreach ($result as $key => $value)
      {


if ($value['slug'] == 'job_title')
	{
	  if ($idType == "FIN") {
              $result[$key]['lock_myinfo'] = false;
          }

          if (array_key_exists("occupation",$arrayorresult))
          {
		  if (array_key_exists("desc", $arrayorresult["occupation"])) {
			  $result[$key]['modal'] = $arrayorresult['occupation']['desc'] ? $arrayorresult['occupation']['desc'] : '' ;
		  } else if (array_key_exists("value", $arrayorresult["occupation"])) {
			  $result[$key]['modal'] = $arrayorresult['occupation']['value'] ? $arrayorresult['occupation']['value'] : '' ;
		  }
		  // $result[$key]['modal'] = $arrayorresult['occupation']['value'] ? $arrayorresult['occupation']['value'] : '' ;
		if ($idType == "FIN") {
			$result[$key]['lock_myinfo'] = false;
		}

          } 




       }












     if ($value['slug'] == 'residential_type') {

       if (array_key_exists('housingtype',$arrayorresult)) {

         if ($arrayorresult['housingtype']['desc'] == "DETACHED HOUSE" || $arrayorresult['housingtype']['desc'] == "SEMI-DETACHED HOUSE" || 
     $arrayorresult['housingtype']['desc'] == "TERRACE HOUSE") {

              $result[$key]['modal'] = 11;
              // $result[$key]['lock_myinfo'] = true;
       } else if ($arrayorresult['housingtype']['desc'] == "CONDOMINIUM" || $arrayorresult['housingtype']['desc'] == "EXECUTIVE CONDOMINIUM" || 
     $arrayorresult['housingtype']['desc'] == "APARTMENT") {
              $result[$key]['modal'] = 12;
              // $result[$key]['lock_myinfo'] = true;
      }


       }


     }

      if ($value['slug'] == 'education_level') {
         if (array_key_exists('edulevel',$arrayorresult)) {
            if ($arrayorresult['edulevel']['code'] == "0") {

                $result[$key]['modal'] = 11;
                $result[$key]['lock_myinfo'] = true;
            
            }

           if ($arrayorresult['edulevel']['code'] == "1") {

                $result[$key]['modal'] = 12;
                $result[$key]['lock_myinfo'] = true;
            
            }


            if ($arrayorresult['edulevel']['code'] == "2" || $arrayorresult['edulevel']['code'] == "3" || $arrayorresult['edulevel']['code'] == "4") {

                $result[$key]['modal'] = 13;
                $result[$key]['lock_myinfo'] = true;

            }


           if ($arrayorresult['edulevel']['code'] == "5" || $arrayorresult['edulevel']['code'] == "6" || $arrayorresult['edulevel']['code'] == "8" || $arrayorresult['edulevel']['code'] == "N") {

                $result[$key]['modal'] = 16;
                $result[$key]['lock_myinfo'] = true;

            }

            if ($arrayorresult['edulevel']['code'] == "9") {

                $result[$key]['modal'] = 15;
                $result[$key]['lock_myinfo'] = true;

            }

           if ($arrayorresult['edulevel']['code'] == "7") {

                $result[$key]['modal'] = 14;
                $result[$key]['lock_myinfo'] = true;

            }









         }
  
     }



if ($value['slug'] == 'property_ownership') {
	$result[$key]['lock_myinfo'] = false;
           if (array_key_exists("hdbtype",$arrayorresult))
             {
                 
              if ($arrayorresult['hdbtype']['desc'] == '1-ROOM FLAT (HDB)'){

                $result[$key]['modal'] = 10;
             }
   
           if ($arrayorresult['hdbtype']['desc'] == '2-ROOM FLAT (HDB)'){

                $result[$key]['modal'] = 11;
             }
            if ($arrayorresult['hdbtype']['desc'] == '3-ROOM FLAT (HDB)'){

                $result[$key]['modal'] = 12;
             }
             if ($arrayorresult['hdbtype']['desc'] == '4-ROOM FLAT (HDB)'){

                $result[$key]['modal'] = 13;
             }
             if ($arrayorresult['hdbtype']['desc'] == '5-ROOM FLAT (HDB)'){

                $result[$key]['modal'] = 14;
             }
 


           }
       }


        if($value['slug'] == 'salution')
        {
          if (array_key_exists("sex",$arrayorresult))
          {
             foreach ($result[$key]['value'] as $key1 => $value1) 
             {
              if(strtolower($arrayorresult['sex']['desc']) == 'male')
              {
                if($value1['name'] == 'Mr.'){
                    $result[$key]['modal']= $value1['id'];
                }
              }
              if(strtolower($arrayorresult['sex']['desc']) == 'female')
              {
                if($value1['name'] == 'Mrs.'){
                    $result[$key]['modal']= $value1['id'];
                   $result[$key]['lock_myinfo'] = true;
                }
              }
             }
           
          }
        }

	if ($value['slug'] == 'race') {
             $result[$key]['lock_myinfo'] = true;
             if (array_key_exists("race",$arrayorresult))
             {
                   $result[$key]['modal'] = $arrayorresult['race']['desc'];
             }

        }
        
        if ($value['slug'] == 'sex')
	{
            $result[$key]['lock_myinfo'] = true;
            if (array_key_exists("sex",$arrayorresult)) {
           	$result[$key]['modal'] = ucfirst(strtolower($arrayorresult['sex']['desc']));
 		//$arrayorresult['sex']['desc'];
	    }
        }

        if($value['slug'] == 'education_level')
        {
          if (array_key_exists("edulevel",$arrayorresult))
          {

              foreach ($result[$key]['value'] as $key1 => $value1) { 

                      
               $countsame=similar_text(strtolower($value1['name']),strtolower($arrayorresult['edulevel']['desc']));
                 if($countsame >11)
                 {
                   $result[$key]['modal']= $value1['id'];
                   
                 }
            }
       
          }  
        }
        
       if ($value['slug'] == 'company_name')
       {
	   if ($idType == "FIN") {
          	$result[$key]['lock_myinfo'] = true;
           }
           if (array_key_exists("employment",$arrayorresult))
           {
		   $result[$key]['modal'] = $arrayorresult['employment']['value'];
           } 
       }

      if ($value['slug'] == 'country_of_birth')
      {
	  $result[$key]['lock_myinfo'] = true;
          if (array_key_exists("birthcountry",$arrayorresult))
          {
              $result[$key]['modal'] = $arrayorresult['birthcountry']['desc'];
          } 
       }

 	// marital_status
        /*if ($value['slug'] == 'marital_status') {
		if ($idType == "SC" || $idType == "PR") {
			$result[$key]['lock_myinfo'] = true;
		}
          	if (array_key_exists("marital_status",$arrayorresult)) {
		        $result[$key]['modal'] = $arrayorresult['marital_status']['value'];
          	}
 	}*/

       if ($value['slug'] == 'passport_number') {
           if ($idType == "SC") {
	     	$result[$key]['lock_myinfo'] = true;
	   }
           if (array_key_exists("passportnumber",$arrayorresult)) {
              $result[$key]['modal'] = $arrayorresult['passportnumber']['value'];
          } 
       }

       if ($value['slug'] == 'passport_expiry_date') {
	  if ($idType == "SC") {
	     $result[$key]['lock_myinfo'] = true;
	  }
          if (array_key_exists("passportexpirydate",$arrayorresult))
          {
              $result[$key]['modal'] = $arrayorresult['passportexpirydate']['value'];
          } 
       }

       	// pass_expiry_date
        if ($value['slug'] == 'pass_expiry_date') {
		if ($idType == "FIN") {
		        $result[$key]['lock_myinfo'] = true;
		}
         	if (array_key_exists("passexpirydate",$arrayorresult)) {
	        	$result[$key]['modal'] = $arrayorresult['passexpirydate']['value'];
          	}
        }

       if ($value['slug'] == 'housing_type')
       {
		if ($idType != "FIN") {
              		$result[$key]['lock_myinfo'] = true;
       		}     
	       if (array_key_exists("housingtype",$arrayorresult))
          	{
              $result[$key]['modal'] = $arrayorresult['housingtype']['desc'];
          } 
       }

       if ($value['slug'] == 'home_contact_number')
       {
          if (array_key_exists("homeno",$arrayorresult))
          {

              $result[$key]['modal'] = $arrayorresult['homeno']['nbr']['value'];
              $result[$key]['lock_myinfo'] = true;

          } 




       }



       if ($value['slug'] == 'residential_status')
       {
          if (array_key_exists("residentialstatus",$arrayorresult))
          {
		$result[$key]['modal'] = $arrayorresult['residentialstatus']['desc'];
		if ($idType != "FIN") {
                    $result[$key]['lock_myinfo'] = true;
		}
          } 
       }


        if($value['slug'] == 'name')
	{
             $result[$key]['lock_myinfo'] = true;
          if (array_key_exists("name",$arrayorresult))
          {
             $result[$key]['modal']= $arrayorresult['name']['value'];
           }
        }

	if($value['slug'] == 'dob')
	{
	  $result[$key]['lock_myinfo'] = true;
          if (array_key_exists("dob",$arrayorresult))
          {
             $result[$key]['modal']= $arrayorresult['dob']['value'];
          }
        }

	if($value['slug'] == 'nationality')
        {
             	$result[$key]['lock_myinfo'] = true;      
		if (array_key_exists("nationality",$arrayorresult)) {
             		$result[$key]['modal'] = $arrayorresult['nationality']['desc'];
            		
			foreach ($result[$key]['value'] as $key1 => $value1) {          
             			$countsame=similar_text(strtolower($value1['name']),strtolower($arrayorresult['nationality']['desc']));
               			if($countsame > 6)
               			{
                 			//$result[$key]['modal']= $value1['id'];
               			}
            		}
          	}
        }
	
	if($value['slug'] == 'id_type')
        {
          if (array_key_exists("passportnumber",$arrayorresult))
          {
            if(!empty($arrayorresult['passportnumber']['value']))
            {
              foreach ($value['value'] as $key1 => $value1) {
                if($value1['name'] == 'Passport No')
                {
                   $result[$key]['modal']= $value1['id'];
                    $result[$key]['lock_myinfo'] = true;
                }
              }
             
            }
            
          }
        }
        if($value['slug'] == 'id_number')
	{
      	   $result[$key]['lock_myinfo'] = true;
          if (array_key_exists("uinfin",$arrayorresult))
          {
            if(!empty($arrayorresult['uinfin']['value']))
            {
              $result[$key]['modal']= $arrayorresult['uinfin']['value'];
            }
            
          }
        }
        if($value['slug'] == 'phone')
        {
          if (array_key_exists("mobileno",$arrayorresult))
          {
            if(!empty($arrayorresult['mobileno']['nbr']['value']))
            {
              $result[$key]['modal']= $arrayorresult['mobileno']['nbr']['value'];
            }
            
          }
        }
        if($value['slug'] == 'email')
        {
          if (array_key_exists("email",$arrayorresult))
          {
            if(!empty($arrayorresult['email']['value']))
            {
              $result[$key]['modal']= $arrayorresult['email']['value'];
            }
            
          }
        }


 if($value['slug'] == 'mailing_address')
        {
          if (array_key_exists("mailadd",$arrayorresult))
          {
             $addressstring='';
            $result[$key]['lock_myinfo'] = true;



           if($arrayorresult['mailadd']['type'] == 'Unformatted')
            {
              if(!empty($arrayorresult['mailadd']['line1']['value']))
                {
                  $addressstring .=$arrayorresult['mailadd']['line1']['value'].',';
                }
                if(!empty($arrayorresult['mailadd']['line2']['value']))
                {
                  $addressstring .=$arrayorresult['mailadd']['line2']['value'];
                }
            }
            else{
               if(!empty($arrayorresult['mailadd']['block']['value']))
                {
                  $addressstring .=$arrayorresult['mailadd']['block']['value'].',';
                }
                if(!empty($arrayorresult['mailadd']['street']['value']))
                {
                  $addressstring .=$arrayorresult['mailadd']['street']['value'].',';
                }
                if(!empty($arrayorresult['mailadd']['country']['desc']))
                {
                  $addressstring .=$arrayorresult['mailadd']['country']['desc'];
                }
            }


              $result[$key]['modal']= $addressstring;
          }
        }













        if($value['slug'] == 'residential_address_one')
	{
		if ($idType != "FIN") {
            		$result[$key]['lock_myinfo'] = true;
		}
          if (array_key_exists("regadd",$arrayorresult))
          {
             $addressstring='';
            if($arrayorresult['regadd']['type'] == 'Unformatted')
            {
              if(!empty($arrayorresult['regadd']['line1']['value']))
                {
                  $addressstring .=$arrayorresult['regadd']['line1']['value'].',';
                }
                if(!empty($arrayorresult['regadd']['line2']['value']))
                {
                  $addressstring .=$arrayorresult['regadd']['line2']['value'];
                }
            }
            else{
               if(!empty($arrayorresult['regadd']['block']['value']))
                {
                  $addressstring .=$arrayorresult['regadd']['block']['value'].',';
                }
                if(!empty($arrayorresult['regadd']['street']['value']))
                {
                  $addressstring .=$arrayorresult['regadd']['street']['value'].',';
                }
                if(!empty($arrayorresult['regadd']['country']['desc']))
                {
                  $addressstring .=$arrayorresult['regadd']['country']['desc'];
                }
            }
             
             
              $result[$key]['modal']= $addressstring;
          }
        }
        if($value['slug'] == 'residential_address_two')
        {
 	  if ($idType != "FIN") {
         	$result[$key]['lock_myinfo'] = true;
          }     
	  if (array_key_exists("regadd",$arrayorresult))
          {
            if(!empty($arrayorresult['regadd']['postal']['value']))
            {
              $result[$key]['modal']= $arrayorresult['regadd']['postal']['value'];
            }
            
          }
        }
        
        
      }
       
    }
    return  $result;
  }
}
