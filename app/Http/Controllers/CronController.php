<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\EmailTemplates;
use App\Models\LoanEnquiryIndividual;
use App\Models\LoanEnquiryCompany;
use App\Models\Notifications;
use Illuminate\Http\Request;
use Response;
use Illuminate\Notifications\Notifiable;
use Redirect;
use Validator;
use Hash;
use Auth;
use Mail;
use Input;
use DB;
use Cookie;
use JWTAuth;
use JWTAuthException;

use PHPMailer\PHPMailer;
use PHPMailer\Exception;


class CronController extends Controller
{
	public function __construct(){
	}


  public function reminder()
  {
    $todayDate = date('Y-m-d');
    $dataPayments = DB::table('payments')->where('is_completed',0)->get();  

    if(!$dataPayments->isEmpty())
    {
       $emailTemplate = EmailTemplates::find(7);
       $messageBody = $emailTemplate->message;
      foreach ($dataPayments as $key => $value)
      {
          if($value->due_date > $todayDate){
            $dueDate = $value->due_date;
            $diff = strtotime($dueDate) - strtotime($todayDate);
            $days = abs(round($diff / 86400));
           
            if($days == 2)
            {
            	 
              $dataUser = DB::table('users')->where('id',$value->user_id)->first();

              if(!empty($dataUser))
              {
                $messageBody = str_replace('[name]', $dataUser->name, $messageBody);
                $messageBody = str_replace('[installment_name]', $value->installment_name, $messageBody);
                $messageBody = str_replace('[date]', date('d-M-Y',strtotime($value->due_date)), $messageBody);
               // print_r($messageBody);die;
                $mail = new PHPMailer\PHPMailer(true);                             
                try {
                   
                    $mail->SMTPDebug = 0;                                 // Enable 
                    $mail->isSMTP();                                      // Set 
                    $mail->Host = SMTP_HOST; 
                    $mail->SMTPAuth = true;                               // Enable 
                    $mail->Username = SMTP_USERNAME;                 // SMTP username
                    $mail->Password = SMTP_PASSWORD;                           // SMTP 
                    $mail->SMTPSecure = SMTP_SECURE;                            // 
                    $mail->Port = PORT;                                    // TCP port 
                     $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
                    $mail->addAddress($dataUser->email);             
                    $mail->isHTML(true);                                  // Set email 
                    $mail->Subject = $emailTemplate->subject;
                    $mail->Body    = $messageBody;
                    $mail->send();
                } catch (Exception $e) {
                       // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                }
              
               $msg = str_replace('[installment_name]',$value->installment_name, installmentDue);
               $msg = str_replace('[date]', date('d-M-Y',strtotime($value->due_date)), $msg);

               $token = $dataUser->firebase_token;
               $data = array(
                            'title'=>'Accredit',
                            'installment_name'=>$value->installment_name,
                            'msg'=>$msg
                    );
               $this->sendPushNotification($token,$data);
                $post22 = new Notifications;               
                $post22->user_id = $value->user_id;
                $post22->loan_no = '';
                $post22->message = $msg;
                $post22->status = 1;
                $post22->save(); 
              }
            }  
        if($days == 3)
                    {
                       
                      $dataUser = DB::table('users')->where('id',$value->user_id)->first();

                      if(!empty($dataUser))
                      {
                        $messageBody = str_replace('[name]', $dataUser->name, $messageBody);
                        $messageBody = str_replace('[installment_name]', $value->installment_name, $messageBody);
                        $messageBody = str_replace('[date]', date('d-M-Y',strtotime($value->due_date)), $messageBody);
                       // print_r($messageBody);die;
                        $mail = new PHPMailer\PHPMailer(true);                             
                        try {
                           
                            $mail->SMTPDebug = 0;                                 // Enable 
                            $mail->isSMTP();                                      // Set 
                            $mail->Host = SMTP_HOST; 
                            $mail->SMTPAuth = true;                               // Enable 
                            $mail->Username = SMTP_USERNAME;                 // SMTP username
                            $mail->Password = SMTP_PASSWORD;                           // SMTP 
                            $mail->SMTPSecure = SMTP_SECURE;                            // 
                            $mail->Port = PORT;                                    // TCP port 
                            $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
                            $mail->addAddress($dataUser->email);             
                            $mail->isHTML(true);                                  // Set email 
                            $mail->Subject = $emailTemplate->subject;
                            $mail->Body    = $messageBody;
                            $mail->send();
                        } catch (Exception $e) {
                               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                        }
                      
                       $msg = str_replace('[installment_name]',$value->installment_name, installmentDue);
                       $msg = str_replace('[date]', date('d-M-Y',strtotime($value->due_date)), $msg);

                       $token = $dataUser->firebase_token;
                       $data = array(
                                    'title'=>'Accredit',
                                    'installment_name'=>$value->installment_name,
                                    'msg'=>$msg
                            );
                       $this->sendPushNotification($token,$data);
                        $post22 = new Notifications;               
                        $post22->user_id = $value->user_id;
                        $post22->loan_no = '';
                        $post22->message = $msg;
                        $post22->status = 1;
                        $post22->save(); 
                      }
                    } 
                    if($dueDate == $todayDate)
                    {
                      $dataUser = DB::table('users')->where('id',$value->user_id)->first();
                      if(!empty($dataUser))
                      {
                        $messageBody = str_replace('[name]', $dataUser->name, $messageBody);
                        $messageBody = str_replace('[installment_name]', $value->installment_name, $messageBody);
                        $messageBody = str_replace('[date]', date('d-M-Y',strtotime($value->due_date)), $messageBody);

                        $mail = new PHPMailer\PHPMailer(true);                             
                        try {
                           
                            $mail->SMTPDebug = 0;                                 // Enable 
                            $mail->isSMTP();                                      // Set 
                            $mail->Host = SMTP_HOST; 
                            $mail->SMTPAuth = true;                               // Enable 
                            $mail->Username = SMTP_USERNAME;                 // SMTP username
                            $mail->Password = SMTP_PASSWORD;                           // SMTP 
                            $mail->SMTPSecure = SMTP_SECURE;                            // 
                            $mail->Port = PORT;                                    // TCP port 
                            $mail->setFrom($emailTemplate->send_from,$emailTemplate->send_from);
                            $mail->addAddress($dataUser->email);             
                            $mail->isHTML(true);                                  // Set email 
                            $mail->Subject = $emailTemplate->subject;
                            $mail->Body    = $messageBody;
                            $mail->send();
                        } catch (Exception $e) {
                               // echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
                        }

                       $msg = str_replace('[installment_name]',$value->installment_name, installmentDue);
                       $msg = str_replace('[date]', date('d-M-Y',strtotime($value->due_date)), $msg);

                       $token = $dataUser->firebase_token;
                       $data = array(
                                    'title'=>'Accredit',
                                    'installment_name'=>$value->installment_name,
                                    'msg'=>$msg
                            );
                       $this->sendPushNotification($token,$data);
                        $post22 = new Notifications;               
                        $post22->user_id = $value->user_id;
                        $post22->loan_no = '';
                        $post22->message = $msg;
                        $post22->status = 1;
                        $post22->save(); 
                      }
                    }

                    }
              }
            }
  }

  function sendPushNotification($token,$data)
  {

      $apiKey = 'AIzaSyDqkuG-TRzNwkZ-dPkt5iFtwsW2eJsHe8U';

      $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
      // send notification when send message  
      
      $fields = array(

                  'registration_ids' =>array($token),
                  'data'=>array(                  
                  'installment_name'=> $data['installment_name'],
                  'message'=>$data['msg'],
                  'title'=>$data['title'],
                  'dateTime'=>date('d-M-Y'),
                  'vibrate'=>1,
                  'sound'=>1,
                  'icon'=>'notification_icon'
                  ),
                  'priority'=>'high',
                  'notification'=>array(
                  'title'=>$data['title'],
                  'body'=>$data['msg'],
                  'icon'=>'notification_icon'
                  ),
                  'android'=>array(
                  'icon'=>HTTP_PATH.'public/images/logo.png'
                  ),

      );
 
    $jsonData = json_encode($fields);
   
    $headers = array(
    'Authorization:key=' . $apiKey,
    'Content-Type:application/json'
    );     

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
 }

	
}
