<?php

namespace App\Http\Middleware;
use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use DB;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $getReqData = file_get_contents('php://input');
            $getData = $this->getJson($getReqData);
            if(isset($getData['token'])){
                $user = JWTAuth::toUser($getData['token']);
            }
			
            
        }
        
        catch (TokenBlacklistedException $e) {

            return response()->json(['token_blacklisted'], 500);

        }catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], 500);

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], 500);

        } catch (JWTException $e) {

            return response()->json(['token_absent'], 500);

        }

           return $next($request);
        }

    public function getJson($jData){
        $data = json_decode($jData,true);       
        if(isset($data) && !empty($data)){
            $_REQUEST = $data;
        } else {
            $data = $_POST;
        }
        return $data;
    }
}
