<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class FaqCategories extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    public function faqlist()
    {
    	return $this->hasMany('App\Models\Faq','faq_category_id')->where('status', 1);
     }

    protected $table = 'faq_categories';

    
}
