<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class Admin extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'admins';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_type()
    {
        return $this->belongsTo('App\Models\UserType', 'user_type_id');
    }
     public function site()
    {
        return $this->belongsTo('App\Models\Site', 'site_id');
    }

    // get all table name
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }
}
