<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class Payments extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'payments';

    public function loanenquiryindividual()
    {
    	return $this->belongsTo('App\Models\LoanEnquiryIndividual','loan_enquiry_individual_id');
    }

    public function loanenquirycompany()
    {
    	return $this->belongsTo('App\Models\LoanEnquiryCompany','loan_enquiry_company_id');
    }
    public function userinfo()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
   
}
