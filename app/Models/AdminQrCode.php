<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class AdminQrCode extends Authenticatable
{
    use Notifiable;
    use SortableTrait;
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'admin_qr_code';

    
}
