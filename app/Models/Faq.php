<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class Faq extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'Faq';

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'created_by');
    }

    public function faqcategory()
    {
        return $this->belongsTo('App\Models\FaqCategories', 'faq_category_id');
    }

    
}
