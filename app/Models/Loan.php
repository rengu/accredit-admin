<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class Loan extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'loan_application';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    
}
