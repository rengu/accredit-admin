<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class LoanEnquiryIndividual extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'loan_enquiry_individual';

    public function loantype()
    {
    	return $this->belongsTo('App\Models\LoanType','loan_type');
    }

    public function knowaccrdit()
    {
    	return $this->belongsTo('App\Models\KnowAccredit','know_accredit');
    }

    public function loanpurpose()
    {
    	return $this->belongsTo('App\Models\LoanPurpose','loan_purpose');
    }

    public function reasonforloan()
    {
    	return $this->belongsTo('App\Models\ReasonForLoan','reason_for_loan');
    }
    public function residentialtype()
    {
    	return $this->belongsTo('App\Models\ResidentialType','residential_type');
    }
    public function propertyownership()
    {
    	return $this->belongsTo('App\Models\PropertyOwnership','property_ownership');
    }
    public function educationlevel()
    {
    	return $this->belongsTo('App\Models\EducationLevel','education_level');
    }

    public function employmentstatus()
    {
    	return $this->belongsTo('App\Models\EmploymentStatus','employment_status');
    }

    public function jobtitle()
    {
    	return $this->belongsTo('App\Models\JobTitle','job_title');
    }
    public function specialization()
    {
    	return $this->belongsTo('App\Models\Specialization','specialization');
    }

    public function officebranch()
    {
        return $this->belongsTo('App\Models\OfficeBranch','office_branch');
    }
   

}
