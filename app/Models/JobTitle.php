<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;
class JobTitle extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'job_title';

    
}
