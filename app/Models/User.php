<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Commands\SortableTrait;

class User extends Authenticatable
{
    use Notifiable;
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function idtype()
    {
        return $this->belongsTo('App\Models\IdType', 'id_type');
    }
     public function educationlevel()
    {
        return $this->belongsTo('App\Models\EducationLevel', 'education_level');
    }

    public function businesstype()
    {
        return $this->belongsTo('App\Models\BusinessType', 'business_type');
    }

   
}
