<?php
use App\Models\SubadminAuths;
function getProfileImage($userData=null)
{
    if (file_exists(UPLOAD_FULL_USER_IMAGE_PATH . '/' . $userData->profile_image) && $userData->profile_image != "") { 
        echo HTML::image(DISPLAY_FULL_USER_IMAGE_PATH.$userData->profile_image, 'Profile Image'); 
    } else {
        echo HTML::image('public/images/default-profile-picture.png','profile image');
    } 
    
}

// get time ago 
function timeago($date) {
	$etime = time() - strtotime($date);
	if ($etime < 1)
	{
		return '0 seconds';
	}
	$a = array( 365 * 24 * 60 * 60  =>  'year',
		30 * 24 * 60 * 60  =>  'month',
		24 * 60 * 60  =>  'day',
		60 * 60  =>  'hour',
		60  =>  'minute',
		1  =>  'second'
		);
	$a_plural = array( 'year'   => 'years',
		'month'  => 'months',
		'day'    => 'days',
		'hour'   => 'hours',
		'minute' => 'minutes',
		'second' => 'seconds'
		);
	foreach ($a as $secs => $str)
	{
		$d = $etime / $secs;
		if ($d >= 1)
		{
			$r = round($d);
			return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
		}
	}
}


/*************** get days form current date ***************/
function getDaysFromDate($date){
    $days = (strtotime(date('Y-m-d H:i:s')) - strtotime($date)) / (60 * 60 * 24);
    $daysRound = round($days);
    if($daysRound==0 || $daysRound==1){
    	$txt = 'day';
    } else {
    	$txt = 'days';
    }
    return $daysRound." ".$txt;
}

    function showMainModule($admin_type = null,$module = null)
    {

       $data = SubadminAuths::where('admin_type_id',$admin_type)->where('module',$module)->first();

      $data = json_decode(json_encode($data), true);
      if(!empty($data))
      {
        return 1;
      }   
      else
      {

      }
    }

    function isShow($admin_type = null,$controller = null)
    {
      if($controller == 'Adminusers')
      {
         $data = SubadminAuths::where('admin_type_id',$admin_type)->where('action','!=','role')->get();
      }
      else{
         $data = SubadminAuths::where('admin_type_id',$admin_type)->get();
      }
        $valueData = 0;    
       
        $data = json_decode(json_encode($data), true);
        //print_r($data); die;
        if(!empty($data))
        {
            foreach ($data as $key => $value)
            {
               if($controller == $value['controller'])
                {
                    $valueData = 1;
                    break;
                }
            }
        }
        else
        {
            $valueData =  0;
        }

        if($valueData == 1)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

    function isShow1($admin_type=null,$controller=null,$action=null)
    {
        $valueData = 0;    
        $data = SubadminAuths::where('admin_type_id',$admin_type)->get();
        $data = json_decode(json_encode($data), true);
        //print_r($data); die;
        if(!empty($data))
        {
            foreach ($data as $key => $value)
            {
               if($controller == $value['controller'] && $action == $value['action'])
                {
                    $valueData = 1;
                    break;
                }
            }
        }
        else
        {
            $valueData =  0;
        }

        if($valueData == 1)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }


  /*

     public function isShow1($userid=null,$controller=null,$action=null)
    { 
         $tblUser = TableRegistry::get('Users');
         $tblAuth = TableRegistry::get('SubadminAuths');
         $data = $tblAuth->find('all')->where(['user_id'=>$userid])->toArray();
         $valueData = 0;         
         if(!empty($data))
         {
            foreach ($data as $value) 
            {
                if($controller == $value->controller && $action == $value->action)
                {
                    $valueData = 1;
                    break;
                }
            }
         }
         else
         {
            $valueData = 0;
         }

         if($valueData == 1)
         {
            return 1; die;
         }
         else
         {
            return 0; die;
         }
         
    }

    public function showMainModule($userId = null,$module = null)
    {
        $tblUser = TableRegistry::get('SubadminAuths');
        $data = $tblUser->find('all')->where(['user_id'=>$userId,'module'=>$module])->first();     
        if(!empty($data))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
*/

